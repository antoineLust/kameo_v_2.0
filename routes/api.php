<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use MailchimpTransactional\ApiClient;

use App\Http\Controllers\Api\BillsController;
use App\Http\Controllers\Api\UsersController;
use App\Http\Controllers\Api\BikeTestsController;
use App\Http\Controllers\Api\BoxOrdersController;
use App\Http\Controllers\Api\CalendarsController;
use App\Http\Controllers\Api\BikeOrdersController;
use App\Http\Controllers\Api\BikesStockController;
use App\Http\Controllers\Api\EntretiensController;
use App\Http\Controllers\Api\GroupedOrdersController;
use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\BikesCatalogController;
use App\Http\Controllers\Api\BoxesCatalogController;
use App\Http\Controllers\Api\AccessoryOrdersController;
use App\Http\Controllers\Api\CompaniesContactsController;
use App\Http\Controllers\Api\AccessoriesCatalogController;
use App\Http\Controllers\Api\AccessoriesCategoriesController;
use App\Http\Controllers\Api\TempAccessoriesCategoriesController;
use App\Http\Controllers\Api\PDFController;
use App\Http\Controllers\Api\AccessoriesStockController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\CompaniesController;
use App\Http\Controllers\Api\LogsController;
use App\Http\Controllers\Api\BikesAssemblyController;
use App\Http\Controllers\Api\CalendarDaysManagementController;
use App\Http\Controllers\Api\ConditionsController;
use App\Http\Controllers\Api\BuildingsController;
use App\Http\Controllers\Api\EntretiensDetailsController;
use App\Http\Controllers\Api\ExternalBikesController;
use App\Http\Controllers\Api\ServicesController;
use App\Http\Controllers\Api\Cash4BikeController;
use App\Http\Controllers\Api\OffersController;
use App\Http\Controllers\Api\OffersDetailsController;
use App\Http\Controllers\Api\ReservationsController;
use App\Http\Controllers\Api\ChatsController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// All routes

// Not protected routes
Route::post('login', [AuthController::class, 'login']);

// Cash4Bike
Route::post('cash4Bike/calculate', [Cash4BikeController::class, 'calculateCash4Bike']);

// Users
Route::get('users', [UsersController::class, 'index']);
Route::get('users/get-user-by-id/{id}', [UsersController::class, 'getUserById']);

// Chat
Route::get('chat', [ChatsController::class, 'index']);
Route::get('chat/show/{id}', [ChatsController::class, 'show']);
Route::get('chat/sessions', [ChatsController::class, 'getSessions']);
Route::get('chat/get-session-by-client-id/{id}', [ChatsController::class, 'getSessionByClientId']);
Route::post('chat/create', [ChatsController::class, 'create']);
Route::post('chat/assign', [ChatsController::class, 'assignEmployee']);
Route::post('chat/resetClientNotif', [ChatsController::class, 'resetClientNotif']);
Route::post('chat/incrementClientNotif', [ChatsController::class, 'incrementClientNotif']);
Route::post('chat/resetEmployeeNotif', [ChatsController::class, 'resetEmployeeNotif']);
Route::post('chat/incrementEmployeeNotif', [ChatsController::class, 'incrementEmployeeNotif']);
// Routes for public website
Route::get('bikes-catalog/get-all-bikes-for-shop/{utilisation}', [BikesCatalogController::class, 'getAllBikesForShop']);
Route::get('bikes-catalog/show/{bike}', [BikesCatalogController::class, 'show']);


// Protected routes
Route::middleware([
    'auth:api',
    'logsQueries',
])->group(function () {

    // Auth routes
    Route::post('/logout', [AuthController::class, 'logout']);

    // Assistance
    Route::post('assistance', [UsersController::class, 'assistance']);

    // Users
    Route::get('users/show/{user}', [UsersController::class, 'show']);
    Route::get('users/auth', [UsersController::class, 'auth']);
    Route::get('users/get-users-by-company/{id}', [UsersController::class, 'getUsersByCompany']);
    Route::get('users/get-employees-by-work-address/{id}', [UsersController::class, 'getEmployeesByWorkAddress']);
    Route::get('users/get-employee/{role}', [UsersController::class, 'getEmployee']);
    Route::get('users/get-firstname-and-lastname-by-id/{id}', [UsersController::class, 'getFirstnameAndLastnameById']);
    Route::get('users/getLocation', [UsersController::class, 'getLocation']);
    Route::get('users/get-user-by-company-id/{id}', [UsersController::class, 'getUserByCompany']);
    Route::post('users/create', [UsersController::class, 'create']);
    Route::post('users/update/{user}', [UsersController::class, 'update']);
    Route::post('users/destroy/{user}', [UsersController::class, 'destroy'])->middleware('userPermission:admin');
    Route::post('users/updatePassword/{user}', [UsersController::class, 'updatePassword']);

    // Companies
    Route::get('companies', [CompaniesController::class, 'index']);
    Route::get('companies/show/{company}', [CompaniesController::class, 'show'])->middleware('userPermission:admin');
    Route::get('companies/get-company-by-id/{id}', [CompaniesController::class, 'getCompanyById'])->middleware('userPermission:admin');
    Route::get('companies/get-user-auth-company/', [CompaniesController::class, 'getUserAuthCompany']);
    Route::get('companies/get-companies-by-audience/{audience}', [CompaniesController::class, 'getCompaniesByAudience'])->middleware('userPermission:admin');
    Route::post('companies/create', [CompaniesController::class, 'create'])->middleware('userPermission:admin');
    Route::post('companies/update/{company}', [CompaniesController::class, 'update'])->middleware('userPermission:admin');
    Route::post('companies/destroy/{company}', [CompaniesController::class, 'destroy'])->middleware('userPermission:admin');
    Route::post('companies-upload-logo/{id}', [CompaniesController::class, 'uploadLogo'])->middleware('userPermission:admin');
    Route::post('companies/delete-picture', [CompaniesController::class, 'deletePicture'])->middleware('userPermission:admin');
    Route::post('companies/create-client-for-entretien', [CompaniesController::class, 'createClientForEntretien'])->middleware('userPermission:admin');

    // Companies contacts
    Route::get('contacts', [CompaniesContactsController::class, 'index'])->middleware('userPermission:admin');
    Route::get('contacts/get-contacts-by-id/{id}', [CompaniesContactsController::class, 'getContactsByCompanyId'])->middleware('userPermission:admin');
    Route::get('contacts/show/{contact}', [CompaniesContactsController::class, 'show'])->middleware('userPermission:admin');
    Route::get('contacts/get-contacts-by-company-id/{id}', [CompaniesContactsController::class, 'getContactsByCompanyId'])->middleware('userPermission:admin');
    Route::post('contacts/create', [CompaniesContactsController::class, 'create'])->middleware('userPermission:admin');
    Route::post('contacts/update/{contact}', [CompaniesContactsController::class, 'update'])->middleware('userPermission:admin');
    Route::post('contacts/destroy/{contact}', [CompaniesContactsController::class, 'destroy'])->middleware('userPermission:admin');

    // Bikes catalog
    Route::get('bikes-catalog', [BikesCatalogController::class, 'index'])->middleware('userPermission:admin|order|search|fleetManager');
    Route::get('bikes-catalog/get-sizes/{id}', [BikesCatalogController::class, 'getSizesModel'])->middleware('userPermission:admin|order|search|fleetManager');
    Route::get('bikes-catalog/get-by-id/{id}', [BikesCatalogController::class, 'getBikeCatalogById'])->middleware('userPermission:admin|order|search|fleetManager');
    Route::get('bikes-catalog/get-models-from-brand/{brand}', [BikesCatalogController::class, 'getModelsFromBrand'])->middleware('userPermission:admin|order|search|fleetManager');
    Route::get('bikes-catalog/get-models-and-brands', [BikesCatalogController::class, 'getModelsAndBrands'])->middleware('userPermission:admin|order|search|fleetManager');
    Route::post('bikes-catalog/create', [BikesCatalogController::class, 'create'])->middleware('userPermission:admin');
    Route::post('bikes-catalog/update/{id}', [BikesCatalogController::class, 'update'])->middleware('userPermission:admin');
    Route::post('bikes-catalog/destroy/{bike}', [BikesCatalogController::class, 'destroy'])->middleware('userPermission:admin');
    Route::post('bikes-catalog/upload/{id}', [BikesCatalogController::class, 'upload'])->middleware('userPermission:admin');
    Route::post('bikes-catalog/delete-picture/{id}', [BikesCatalogController::class, 'deletePicture'])->middleware('userPermission:admin');
    Route::post('bikes-catalog/get-pictures/{id}', [BikesCatalogController::class, 'getPictures'])->middleware('userPermission:admin');

    // Boxes catalog
    Route::get('boxes-catalog', [BoxesCatalogController::class, 'index'])->middleware('userPermission:admin');
    Route::get('boxes-catalog/get-box-catalog-by-id/{id}', [BoxesCatalogController::class, 'getBoxCatalogById'])->middleware('userPermission:admin');

    // Accessories catalog
    Route::get('accessories-catalog', [AccessoriesCatalogController::class, 'index'])->middleware('userPermission:admin|order|search|fleetManager');
    Route::get('accessories-catalog/show/{accessory}', [AccessoriesCatalogController::class, 'show'])->middleware('userPermission:admin');
    Route::get('accessories-catalog/get-brands', [AccessoriesCatalogController::class, 'getBrands']);
    Route::get('accessories-catalog/get-by-id/{id}', [AccessoriesCatalogController::class, 'getById']);
    Route::get('accessories-catalog/get-by-id-cafetaria/{id}', [AccessoriesCatalogController::class, 'getByIdCafetaria']);
    Route::get('accessories-catalog/get-from-category-cafetaria/{category}', [AccessoriesCatalogController::class, 'getFromCategoryCafetaria']);
    Route::get('accessories-catalog/get-models-and-brands', [AccessoriesCatalogController::class, 'getModelsAndBrands']);
    Route::get('accessories-catalog/get-models-from-brand/{brand}', [AccessoriesCatalogController::class, 'getModelsFromBrand']);
    // Route::get('accessories-catalog/get-accessory-by-stock/{id}', [AccessoriesCatalogController::class, 'getAccessoryByStock']);
    Route::get('accessories-catalog/get-models-from-category/{category}', [AccessoriesCatalogController::class, 'getModelsFromCategory']);
    Route::get('accessories-catalog/get-accessory-sizes/{id}', [AccessoriesCatalogController::class, 'getAccessorySizes']);
    Route::get('accessories-catalog/get-all-accessories-for-shop/{category}', [AccessoriesCatalogController::class, 'getAllAccessoriesForShop']);
    Route::get('accessories-catalog/get-models-from-category-for-select/{category}', [AccessoriesCatalogController::class, 'getAllAccessoriesForSelect']);
    Route::post('accessories-catalog/create', [AccessoriesCatalogController::class, 'create'])->middleware('userPermission:admin');
    Route::post('accessories-catalog/update/{accessory}', [AccessoriesCatalogController::class, 'update'])->middleware('userPermission:admin');
    Route::post('accessories-catalog/destroy/{accessory}', [AccessoriesCatalogController::class, 'destroy'])->middleware('userPermission:admin');
    Route::post('accessories-catalog/upload/{id}', [AccessoriesCatalogController::class, 'upload'])->middleware('userPermission:admin');
    Route::post('accessories-catalog/delete-picture/{id}', [AccessoriesCatalogController::class, 'deletePicture'])->middleware('userPermission:admin');
    Route::post('accessories-catalog/get-pictures/{id}', [AccessoriesCatalogController::class, 'getPictures'])->middleware('userPermission:admin');

    // Orders
    Route::get('grouped-orders', [GroupedOrdersController::class, 'index']);
    Route::get('grouped-orders/show/{order}', [GroupedOrdersController::class, 'show'])->middleware('userPermission:admin|fleetManager|order');
    Route::post('grouped-orders/verify-order', [GroupedOrdersController::class, 'verifyOrder'])->middleware('userPermission:admin|fleetManager|order');
    Route::post('grouped-orders/create', [GroupedOrdersController::class, 'create'])->middleware('userPermission:admin');
    Route::post('grouped-orders/update/{order}', [GroupedOrdersController::class, 'update'])->middleware('userPermission:admin');
    Route::post('grouped-orders/destroy/{order}', [GroupedOrdersController::class, 'destroy'])->middleware('userPermission:admin');
    Route::post('grouped-orders/create-from-cafetaria', [GroupedOrdersController::class, 'createFromCafetaria'])->middleware('userPermission:order|admin');
    Route::post('grouped-orders/confirmed/{order}', [GroupedOrdersController::class, 'confirmOrder'])->middleware('userPermission:fleetManager');

    // Bills
    Route::get('bills', [BillsController::class, 'index'])->middleware('userPermission:bill');
    Route::get('bills/show/{bill}', [BillsController::class, 'show'])->middleware('userPermission:bill');
    Route::post('bills/create', [BillsController::class, 'create'])->middleware('userPermission:bill');
    Route::post('bills/update/{bill}', [BillsController::class, 'update'])->middleware('userPermission:bill');
    Route::post('bills/destroy/{bill}', [BillsController::class, 'destroy'])->middleware('userPermission:bill');

    // Bike orders
    Route::get('bike-orders', [BikeOrdersController::class, 'index'])->middleware('userPermission:admin|order');
    Route::get('bike-orders/show/{bike}', [BikeOrdersController::class, 'show'])->middleware('userPermission:admin');
    Route::get('bike-orders/get-from-grouped-orders/{id}', [BikeOrdersController::class, 'getBikesFromGroupedOrder'])->middleware('userPermission:admin');
    Route::post('bike-orders/create', [BikeOrdersController::class, 'create'])->middleware('userPermission:admin');
    Route::post('bike-orders/update/{bike}', [BikeOrdersController::class, 'update'])->middleware('userPermission:admin');
    Route::post('bike-orders/destroy/{bike}', [BikeOrdersController::class, 'destroy'])->middleware('userPermission:admin');

    // Box orders
    Route::get('box-orders', [BoxOrdersController::class, 'index'])->middleware('userPermission:admin|order');
    Route::get('box-orders/show/{order}', [BoxOrdersController::class, 'show'])->middleware('userPermission:admin');
    Route::get('box-orders/get-boxes-from-grouped-order/{id}', [BoxOrdersController::class, 'getBoxesFromGroupedOrder'])->middleware('userPermission:admin');
    Route::post('box-orders/create', [BoxOrdersController::class, 'create'])->middleware('userPermission:admin');
    Route::post('box-orders/update/{order}', [BoxOrdersController::class, 'update'])->middleware('userPermission:admin');
    Route::post('box-orders/destroy/{accessory}', [BoxOrdersController::class, 'destroy'])->middleware('userPermission:admin');

    // Accessory orders
    Route::get('accessory-orders', [AccessoryOrdersController::class, 'index'])->middleware('userPermission:admin|order');
    Route::get('accessory-orders/get-from-grouped-order/{id}', [AccessoryOrdersController::class, 'getAccessoriesFromGroupedOrder'])->middleware('userPermission:admin');
    Route::get('accessory-orders/show/{accessory}', [AccessoryOrdersController::class, 'show'])->middleware('userPermission:admin');
    Route::post('accessory-orders/create', [AccessoryOrdersController::class, 'create'])->middleware('userPermission:admin');
    Route::post('accessory-orders/update/{accessory}', [AccessoryOrdersController::class, 'update'])->middleware('userPermission:admin');
    Route::post('accessory-orders/destroy/{accessory}', [AccessoryOrdersController::class, 'destroy'])->middleware('userPermission:admin');

    // Bikes stock
    Route::get('bikes-stock', [BikesStockController::class, 'index']);
    Route::get('bikes-stock/show/{bike}', [BikesStockController::class, 'show'])->middleware('userPermission:admin|accounting');
    Route::get('bikes-stock/get-bikes-by-user-auth/{id}', [BikesStockController::class, 'getBikesByUserAuth'])->middleware('userPermission:admin|accounting|fleetManager');
    Route::get('bikes-stock/get-bike-by-id/{id}', [BikesStockController::class, 'getBikeStockById'])->middleware('userPermission:admin|accounting');
    Route::get('bikes-stock/get-bikes-by-company-id/{id}', [BikesStockController::class, 'getBikesByCompanyId'])->middleware('userPermission:admin|accounting');
    Route::get('bikes-stock/get-bikes-not-assembled', [BikesStockController::class, 'getBikesNotAssembled'])->middleware('userPermission:admin|accounting');
    Route::get('bikes-stock/get-all-bikes-select', [BikesStockController::class, 'getBikesWithSelect'])->middleware('userPermission:admin|accounting');
    Route::get('bikes-stock/get-shared-bikes-by-company-id/{id}', [BikesStockController::class, 'getSharedBikesByCompanyId'])->middleware('userPermission:admin|accounting|fleetManager');
    Route::post('bikes-stock/get-bikes-by-catalog-id-and-size', [BikesStockController::class, 'getBikesByCatalogIdAndSize'])->middleware('userPermission:admin|accounting');
    Route::post('bikes-stock/create', [BikesStockController::class, 'create'])->middleware('userPermission:admin|accounting');
    Route::post('bikes-stock/update/{id}', [BikesStockController::class, 'update'])->middleware('userPermission:admin|accounting');
    Route::post('bikes-stock/receptionCommande/{id}', [BikesStockController::class, 'receptionCommande'])->middleware('userPermission:admin|accounting');
    Route::post('bikes-stock/destroy/{bike}', [BikesStockController::class, 'destroy'])->middleware('userPermission:admin|accounting');
    Route::post('bikes-stock/get-availables-bike-for-sharing-plan', [BikesStockController::class, 'getAvailablesBikeForSharingPlan']);

    // Accessories stock
    Route::get('accessories-stock', [AccessoriesStockController::class, 'index'])->middleware('userPermission:admin');
    Route::get('accessories-stock/show/{bike}', [AccessoriesStockController::class, 'show'])->middleware('userPermission:admin');
    Route::get('accessories-stock/get-accessories-by-catalog-and-size', [AccessoriesStockController::class, 'getAccessoriesByCatalogIdAndSize'])->middleware('userPermission:admin');
    Route::get('accessories-stock/get-accessory-by-id/{id}', [AccessoriesStockController::class, 'getAccessoryStockById'])->middleware('userPermission:admin');
    Route::post('accessories-stock/create', [AccessoriesStockController::class, 'create'])->middleware('userPermission:admin');
    Route::post('accessories-stock/update/{id}', [AccessoriesStockController::class, 'update'])->middleware('userPermission:admin');
    Route::post('accessories-stock/receptionCommande/{id}', [AccessoriesStockController::class, 'receptionCommande'])->middleware('userPermission:admin');
    Route::post('accessories-stock/destroy/{bike}', [AccessoriesStockController::class, 'destroy'])->middleware('userPermission:admin');

    // Accessories categories
    Route::get('accessories-categories', [AccessoriesCategoriesController::class, 'index'])->middleware('userPermission:admin');
    Route::get('temp-accessories-categories', [TempAccessoriesCategoriesController::class, 'index'])->middleware('userPermission:admin|order|search|fleetManager');

    // Purchase Order
    Route::post('purchase-order/generate', [PDFController::class, 'purchaseOrder'])->middleware('userPermission:admin');

    // Generate Invoice
    Route::post('invoice/generate/{order}', [PDFController::class, 'invoice'])->middleware('userPermission:admin|accounting');

    //Generate creditNote
    Route::post('creditNote/generate/{order}', [PDFController::class, 'creditNote'])->middleware('userPermission:admin|accounting');

    // News
    Route::get('news', [NewsController::class, 'index']);
    Route::get('last-news', [NewsController::class, 'lastNews']);
    Route::get('news/show/{news}', [NewsController::class, 'show']);
    Route::post('news/create', [NewsController::class, 'create'])->middleware('userPermission:admin');
    Route::post('news/update/{news}', [NewsController::class, 'update'])->middleware('userPermission:admin');
    Route::post('news/destroy/{news}', [NewsController::class, 'destroy'])->middleware('userPermission:admin');
    Route::post('news/delete-picture', [NewsController::class, 'deletePicture'])->middleware('userPermission:admin');

    // Conditions
    Route::get('conditions', [ConditionsController::class, 'index'])->middleware('userPermission:admin|fleetManager|search|order');
    Route::get('conditions/orderable', [ConditionsController::class, 'getAllOrderable'])->middleware('userPermission:admin|fleetManager|search|order');
    Route::get('conditions/show/{id}', [ConditionsController::class, 'show'])->middleware('userPermission:admin|fleetManager');
    Route::get('conditions/show-by-company-id/{id}', [ConditionsController::class, 'showByCompanyId'])->middleware('userPermission:admin|fleetManager');
    Route::get('conditions/get-shared-bikes-conditions-by-company-id/{id}', [ConditionsController::class, 'getSharedBikesConditionsByCompanyId'])->middleware('userPermission:admin|fleetManager');
    Route::get('conditions/get-conditions-by-company-id/{id}', [ConditionsController::class, 'getConditionsByCompanyId'])->middleware('userPermission:admin|fleetManager');
    Route::post('conditions/create', [ConditionsController::class, 'create'])->middleware('userPermission:admin');
    Route::post('conditions/update/{condition}', [ConditionsController::class, 'update'])->middleware('userPermission:admin');
    Route::post('conditions/destroy/{condition}', [ConditionsController::class, 'destroy'])->middleware('userPermission:admin');
    Route::post('conditions/remove-accessory-orderable/{id}', [ConditionsController::class, 'removeAccessoryOrderable'])->middleware('userPermission:admin');
    Route::post('conditions/create-accessory-orderable', [ConditionsController::class, 'createAccessoryOrderable'])->middleware('userPermission:admin');
    Route::post('conditions/delete-all-accessory-orderables/{id}', [ConditionsController::class, 'deleteAllAccessoryOrderables'])->middleware('userPermission:admin');
    Route::post('conditions/add-all-accessory-orderables/{id}', [ConditionsController::class, 'addAllAccessoryOrderables'])->middleware('userPermission:admin');
    Route::post('conditions/add-new-shared-bike/{id}', [ConditionsController::class, 'addNewSharedBike'])->middleware('userPermission:admin|fleetManager');
    Route::post('conditions/delete-shared-bike/{id}', [ConditionsController::class, 'deleteSharedBike'])->middleware('userPermission:admin|fleetManager');
    Route::get('conditions/get-shared-bikes-conditions', [ConditionsController::class, 'getSharedBikesConditions'])->middleware('userPermission:admin|fleetManager|search');

    // Buildings
    Route::get('buildings', [BuildingsController::class, 'index']);
    Route::get('buildings/get-buildings-by-company-id/{id}', [BuildingsController::class, 'getBuildingByCompanyId']);

    // Entretiens
    Route::get('entretiens', [EntretiensController::class, 'index'])->middleware('userPermission:admin|fleetManager|order|search');
    Route::get('entretiens/show/{entretien}', [EntretiensController::class, 'show'])->middleware('userPermission:admin');
    Route::get('entretiens/get-entretiens-by-bike-id/{id}', [EntretiensController::class, 'getEntretiensByBikeId'])->middleware('userPermission:admin');
    Route::post('entretiens/create', [EntretiensController::class, 'create'])->middleware('userPermission:admin');
    Route::post('entretiens/update/{entretien}', [EntretiensController::class, 'update'])->middleware('userPermission:admin');
    Route::post('entretiens/updateStatusCalendar/{entretien}', [EntretiensController::class, 'updateStatusCalendar'])->middleware('userPermission:admin');
    Route::post('entretiens/updateCommentCalendar/{entretien}', [EntretiensController::class, 'updateCommentCalendar'])->middleware('userPermission:admin');
    Route::post('entretiens/destroy/{entretien}', [EntretiensController::class, 'destroy'])->middleware('userPermission:admin');
    Route::post('entretiens/generate-invoice', [EntretiensController::class, 'generateInvoice'])->middleware('userPermission:admin');

    // Entretiens details
    Route::get('entretiens-details', [EntretiensDetailsController::class, 'index'])->middleware('userPermission:admin|fleetManager|order|search');
    Route::get('entretiens-details/show/{entretien}', [EntretiensDetailsController::class, 'show'])->middleware('userPermission:admin');
    // Route::get('entretiens-details/get-entretien-details/{id}', [EntretiensDetailsController::class, 'getEntretienDetailsApi'])->middleware('userPermission:admin');
    Route::post('entretiens-details/create', [EntretiensDetailsController::class, 'create'])->middleware('userPermission:admin');
    Route::post('entretiens-details/update/{entretien}', [EntretiensDetailsController::class, 'update'])->middleware('userPermission:admin');
    Route::post('entretiens-details/updateReverse/{entretien}', [EntretiensDetailsController::class, 'updateReverse'])->middleware('userPermission:admin');
    Route::post('entretiens-details/destroy/{entretien}', [EntretiensDetailsController::class, 'destroy'])->middleware('userPermission:admin');

    // External bikes
    Route::get('external-bikes', [ExternalBikesController::class, 'index']);
    Route::get('external-bikes/show/{bike}', [ExternalBikesController::class, 'show']);
    Route::get('external-bikes/get-external-bikes-by-company-id/{id}', [ExternalBikesController::class, 'getExternalBikesByCompanyId']);
    Route::get('external-bikes/get-external-bike-by-id/{id}', [ExternalBikesController::class, 'getExternalBikeById']);
    Route::post('external-bikes/create', [ExternalBikesController::class, 'create'])->middleware('userPermission:admin');
    Route::post('external-bikes/update/{bike}', [ExternalBikesController::class, 'update'])->middleware('userPermission:admin');
    Route::post('external-bikes/destroy/{bike}', [ExternalBikesController::class, 'destroy'])->middleware('userPermission:admin');

    // Services
    Route::get('services', [ServicesController::class, 'index'])->middleware('userPermission:admin');
    Route::get('services/get-categories', [ServicesController::class, 'getCategories'])->middleware('userPermission:admin');
    Route::get('services/get-services-by-category/{category}', [ServicesController::class, 'getServicesByCategory'])->middleware('userPermission:admin');
    Route::get('services/get-service-by-id/{id}', [ServicesController::class, 'getServiceById'])->middleware('userPermission:admin');
    Route::get('services/show/{service}', [ServicesController::class, 'show'])->middleware('userPermission:admin');
    Route::post('services/create', [ServicesController::class, 'create'])->middleware('userPermission:admin');
    Route::post('services/update/{service}', [ServicesController::class, 'update'])->middleware('userPermission:admin');
    Route::post('services/destroy/{service}', [ServicesController::class, 'destroy'])->middleware('userPermission:admin');

    // Bikes assembly
    Route::get('bikes-assembly', [BikesAssemblyController::class, 'index'])->middleware('userPermission:admin');
    Route::get('bikes-assembly/show/{bike}', [BikesAssemblyController::class, 'show'])->middleware('userPermission:admin');
    Route::post('bikes-assembly/create', [BikesAssemblyController::class, 'create'])->middleware('userPermission:admin');
    Route::post('bikes-assembly/update/{bike}', [BikesAssemblyController::class, 'update'])->middleware('userPermission:admin');
    Route::post('bikes-assembly/destroy/{bike}', [BikesAssemblyController::class, 'destroy'])->middleware('userPermission:admin');

    // Calendar
    Route::get('calendar', [CalendarsController::class, 'index'])->middleware('userPermission:admin');
    Route::post('calendar/create', [CalendarsController::class, 'create'])->middleware('userPermission:admin');
    Route::post('calendar/update', [CalendarsController::class, 'update'])->middleware('userPermission:admin');
    Route::post('calendar/updateAssembly', [CalendarsController::class, 'updateAssembly'])->middleware('userPermission:admin');
    Route::post('calendar/updateOrder', [CalendarsController::class, 'updateOrder'])->middleware('userPermission:admin');
    Route::post('calendar/updateTest', [CalendarsController::class, 'updateTest'])->middleware('userPermission:admin');
    Route::post('calendar/updateComment', [CalendarsController::class, 'updateComment'])->middleware('userPermission:admin');
    Route::post('calendar/updateCommentAndStatusFromEntretien', [CalendarsController::class, 'updateCommentAndStatusFromEntretien'])->middleware('userPermission:admin');
    Route::post('calendar/updateCommentEntretien', [CalendarsController::class, 'updateCommentEntretien'])->middleware('userPermission:admin');
    Route::post('calendar/updateEntretien', [CalendarsController::class, 'updateEntretien'])->middleware('userPermission:admin');
    Route::post('calendar/destroy', [CalendarsController::class, 'destroy'])->middleware('userPermission:admin');

    // Bike test
    Route::get('bike-tests', [BikeTestsController::class, 'index'])->middleware('userPermission:admin');
    Route::get('bikes-tests/show/{bike}', [BikeTestsController::class, 'show'])->middleware('userPermission:admin');
    Route::post('bike-tests/create', [BikeTestsController::class, 'create'])->middleware('userPermission:admin');
    Route::post('bike-tests/update/{bikeTest}', [BikeTestsController::class, 'update'])->middleware('userPermission:admin');

    // Offers
    Route::get('offers', [OffersController::class, 'index'])->middleware('userPermission:admin');
    Route::get('offers/show/{offer}', [OffersController::class, 'show'])->middleware('userPermission:admin');
    Route::post('offers/create', [OffersController::class, 'create'])->middleware('userPermission:admin');
    Route::post('offers/update/{offer}', [OffersController::class, 'update'])->middleware('userPermission:admin');
    Route::post('offers/destroy/{offer}', [OffersController::class, 'destroy'])->middleware('userPermission:admin');
    Route::post('offers/generate/{offer}', [PDFController::class, 'generateOffer'])->middleware('userPermission:admin');

    // Offers details
    Route::get('offers/details', [OffersDetailsController::class, 'index'])->middleware('userPermission:admin');
    Route::post('offer-bike-details/create', [OffersDetailsController::class, 'createBikeDetails'])->middleware('userPermission:admin');
    Route::post('offers-details/destroy/{offer}', [OffersDetailsController::class, 'destroy'])->middleware('userPermission:admin');
    Route::post('offer-accessory-details/create', [OffersDetailsController::class, 'createAccessoryDetails'])->middleware('userPermission:admin');
    Route::post('offer-box-details/create', [OffersDetailsController::class, 'createBoxDetails'])->middleware('userPermission:admin');

    // Reservations
    Route::get('reservations/load-by-user-auth/{id}', [ReservationsController::class, 'indexByAuth']);
    Route::get('reservations', [ReservationsController::class, 'index']);
    Route::get('reservations/show/{reservation}', [ReservationsController::class, 'show'])->middleware('userPermission:search|fleetManager');
    Route::get('reservations/get-reservation-by-id/{id}', [ReservationsController::class, 'getReservationById'])->middleware('userPermission:search|fleetManager');
    Route::get('reservations/get-reservation-code-by-reservation/{id}', [ReservationsController::class, 'getReservationCodeByReservationId'])->middleware('userPermission:search|fleetManager');
    Route::post('reservations/create', [ReservationsController::class, 'create'])->middleware('userPermission:search|fleetManager');
    Route::post('reservations/update/{reservation}', [ReservationsController::class, 'update'])->middleware('userPermission:search|fleetManager');
    Route::post('reservations/delete/{reservation}', [ReservationsController::class, 'delete'])->middleware('userPermission:search|fleetManager');
    Route::get('reservation-codes', [ReservationsController::class, 'getAllCodes'])->middleware('userPermission:search|fleetManager');
    Route::get('reservation-details', [ReservationsController::class, 'getAllDetails'])->middleware('userPermission:search|fleetManager');

    // Logs
    Route::get('logs/queries', [LogsController::class, 'getQueries'])->middleware('userPermission:admin');

    // Datatable
    Route::get('datatable/companies-list', [CompaniesController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/accessories-catalog-list', [AccessoriesCatalogController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/accessories-stock-list', [AccessoriesStockController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/bikes-assembly-list', [BikesAssemblyController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/bikes-catalog-list', [BikesCatalogController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/bikes-stock-list', [BikesStockController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/bills-list', [BillsController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/entretiens-list', [EntretiensController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/conditions-list', [ConditionsController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/grouped-orders-list', [GroupedOrdersController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/news-list', [NewsController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/offers-list', [OffersController::class, 'getDataForDataTable'])->middleware('userPermission:admin');
    Route::get('datatable/users-list', [UsersController::class, 'getDataForDataTable'])->middleware('userPermission:admin');

    // Calendar day management
    Route::get('calendar/days', [CalendarDaysManagementController::class, 'getDays']);
    route::post('calendar/check-day', [CalendarDaysManagementController::class, 'checkDay']);
});



// Location request
Route::post('location-request/create', function (Request $request) {

    // Send email
    if (config('app.env') == 'local') {
        function sendMail($message)
        {
            try {
                $mailchimp = new ApiClient();
                $mailchimp->setApiKey(config('services.mailchimp.key'));

                $mailchimp->messages->send(["message" => $message]);
            } catch (Error $e) {
                echo 'Error: ', $e->getMessage(), "\n";
            }
        }

        $message = [
            "from_name"     => "KAMEO Bikes",
            "from_email"    => "info@kameobikes.com",
            "subject"       => "Une nouvelle demande de location a été faite !",
            "html"          => view('emails.locationRequest', [
                'data'      => $request->all(),
                'company'   => 'Kameo Bikes',
            ])->render(),
            "to" => [
                [
                    "type"  => "to",
                    "email" => "benjamin@kameobikes.com",
                    "name"  => "Benjamin Van Rentegrhem"
                ]
            ]
        ];
        sendMail($message);
    } else if (config('app.env') == 'production') {
        function sendMail($message)
        {
            try {
                $mailchimp = new ApiClient();
                $mailchimp->setApiKey(config('services.mailchimp.key'));

                $mailchimp->messages->send(["message" => $message]);
            } catch (Error $e) {
                echo 'Error: ', $e->getMessage(), "\n";
            }
        }

        $message = [
            "from_name"     => "KAMEO Bikes",
            "from_email"    => "info@kameobikes.com",
            "subject"       => "Une nouvelle demande de location a été faite !",
            "html"          => view('emails.locationRequest', [
                'data'      => $request->all(),
                'company'   => 'Kameo Bikes',
            ])->render(),
            "to" => [
                [
                    "type"  => "to",
                    "email" => 'antoine@kameobikes.com',
                    "name"  => 'Antoine Lust'
                ],
                [
                    "type"  => "to",
                    "email" => 'benjamin@kameobikes.com',
                    "name"  => 'Benjamin Van Rentegrhem'
                ]
            ]
        ];
        sendMail($message);
    }
});
