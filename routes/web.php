<?php

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BikesCatalogController;
use App\Http\Controllers\AccessoriesCatalogController;
use App\Http\Controllers\Cash4BikeController;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|

Route::get('/shop', function($locale = null){
    if(isset($locale) && in_array($locale, config('app.available_locales'))){
        App::setLocale($locale);
    }
    return view('shop.pages.index');
})
->name('shop');


Route::get('/', function($locale = null){
    if(isset($locale) && in_array($locale, config('app.available_locales'))){
        App::setLocale($locale);
    }
    return view('shop.index');
});
*/

// Route::get('/phpinfo', function () {
//     return phpinfo();
// });



Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath'],
    ],
    function () {
        Route::get('/{any?}', function () {
            return view('index');
        })
            ->where('any', '^((?!storage).)*$')
            ->name('index');
        /*
    Route::get('/louer-un-velo', function () {
        return view('shop.pages.bike-short-location');
    })->name('bike-short-location');

    Route::get('/magasin-de-vélos-liège', function () {
        return view('shop.pages.index');
    })->name('shop');

    Route::get('/pro', function () {
        return view('pro.pages.index');
    })->name('pro');

    Route::get('/repair', function () {
        return view('repair.pages.index');
    })->name('repair');


    Route::get('/', function () {
        return view('index');
    })->name("index");

    Route::get('/magasin-de-vélos-liège/blog', function () {
        return view('shop.pages.blog');
    })
    ->name('blog-shop');

    Route::get('/magasin-de-vélos-liège/blog/choisir-son-velo', function () {
        return view('shop.pages.choisir-son-velo');
    })
    ->name('choisir-son-velo');

    Route::get('/magasin-de-vélos-liège/blog/nouveau-magasin', function () {
        return view('shop.pages.nouveau-magasin');
    })
    ->name('nouveau-magasin');


    Route::get('/magasin-de-vélos-liège/catalogue/{utilisation?}', [BikesCatalogController::class, 'getAllBikesForShop'])
    ->name('catalog');

    Route::get('/magasin-de-vélos-liège/catalogue_accessoire/{category?}', [AccessoriesCatalogController::class, 'getAllAccessoriesForShop'])
    ->name('catalog-accessories');


    Route::get('/magasin-de-vélos-liège/catégories_vélos', function () {
        return view('shop.pages.catalog-overview');
    })
    ->name('catalog-overview');

    Route::get('/magasin-de-vélos-liège/catégories_accessoires', function () {
        return view('shop.pages.catalog-accessories-overview');
    })
    ->name('catalog-accessories-overview');


    Route::get('/magasin-de-vélos-liège/détail_vélo/{bike}', [BikesCatalogController::class, 'getBikeForShop'])
    ->name('catalog-bike');



    Route::get('/magasin-de-vélos-liège/détail_accessoire/{accessory}', [AccessoriesCatalogController::class, 'getAccessoryForShop'])
    ->name('accessory-details');


    Route::get('/pro/employer', function () {
        return view('pro.pages.employer');
    })
    ->name('employer');

    Route::get('/pro/velos-partages', function () {
        return view('pro.pages.velos-partages');
    })
    ->name('velos-partages');


    Route::get('/pro/employee', function () {
        return view('pro.pages.employee');
    })
    ->name('employee');


    Route::get('/pro/independant', function () {
        return view('pro.pages.independant');
    })
    ->name('independant');

    Route::get('/pro/contact', function () {
        return view('pro.pages.contact');
    })
    ->name('contact_pro');

    Route::get('/politique-confidentialite', function () {
        return view('pro.pages.politique-confidentialite');
    })
    ->name('politique-confidentialite');

    Route::get('/conditions-generales', function () {
        return view('pro.pages.conditions-generales');
    })
    ->name('conditions-generales');


    */
    }
);


// Dashboard routes
