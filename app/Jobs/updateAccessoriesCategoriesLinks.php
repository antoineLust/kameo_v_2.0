<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;


class updateAccessoriesCategoriesLinks implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      DB::table('temp_accessories_categories')->truncate();
      $accessoriesCategories = DB::table('accessories_categories')->get();
      foreach ($accessoriesCategories as $accessoriesCategory) {
        $continue       = true;
        $category       = $accessoriesCategory->name;
        $categoryID     = $accessoriesCategory->id;
        $parentCategory = $accessoriesCategory->parent_category;
        $tempArray      = array();
        $tempArray[0]   = $category;
        $tempArrayID[0] = $categoryID;

        $main       = null;
        $sub_1      = null;
        $sub_2      = null;
        $sub_3      = null;
        $sub_4      = null;
        $main_ID    = null;
        $sub_1_ID   = null;
        $sub_2_ID   = null;
        $sub_3_ID   = null;
        $sub_4_ID   = null;

        while($continue){
          if($parentCategory == NULL){
            $continue = false;
          }else{
            $accessory_parent   = DB::table('accessories_categories')->find($parentCategory);
            $parentCategory     =$accessory_parent->parent_category;
            array_push($tempArrayID, $accessory_parent->id);
            array_push($tempArray, $accessory_parent->name);
          }
        }
        $full = "";
        while(count($tempArray)>0){
          $sub_4_ID   = $sub_3_ID;
          $sub_3_ID   = $sub_2_ID;
          $sub_2_ID   = $sub_1_ID;
          $sub_1_ID   = $main_ID;
          $sub_4      = $sub_3;
          $sub_3      = $sub_2;
          $sub_2      = $sub_1;
          $sub_1      = $main;
          $main       = array_shift($tempArray);
          $main_ID    = array_shift($tempArrayID);

        }
        DB::table('temp_accessories_categories')->insert([
          'id'        => $categoryID,
          'main'      => $main,
          'main_id'   => $main_ID,
          'sub_1'     => $sub_1,
          'sub_1_id'  => $sub_1_ID,
          'sub_2'     => $sub_2,
          'sub_2_id'  => $sub_2_ID,
          'sub_3'     => $sub_3,
          'sub_3_id'  => $sub_3_ID,
          'sub_4'     => $sub_4,
          'sub_4_id'  => $sub_4_ID
        ]);
      }

    }
}
