<?php

    namespace App\Helpers;

    use App\Models\Bill;
    use SepaQr\Data;
    use Endroid\QrCode\Builder\Builder;
    use Endroid\QrCode\Color\Color;
    use Endroid\QrCode\Writer\PngWriter;
    use Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
    use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
    use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelHigh;
    use Illuminate\Support\Facades\App;

    function generateSepaQrCode($data){

        // Define data for qrCode payement
        $payementData = Data::create()
            ->setName($data['name'])
            ->setIban($data['iban'])
            ->setBic($data['bic'])
            ->setAmount($data['amount'])
            ->setRemittanceReference($data['reference']);

        // Create QrCode
        Builder::create()
            ->writer(new PngWriter())
            ->data($payementData)
            ->errorCorrectionLevel(new ErrorCorrectionLevelHigh())
            ->roundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->labelText('Scannez pour payer')
            ->labelAlignment(new LabelAlignmentCenter())
            ->labelTextColor(new Color(60, 178, 149))
            ->foregroundColor(new Color(60, 178, 149))
            ->logoPath(public_path('images/logo.jpg'))
            ->logoResizeToWidth(100)
            ->logoResizeToHeight(100)
            ->build()
            ->saveToFile(public_path('storage/qrcode/invoiceQrCode.png'));

    }

    function createReferenceNumber($id){

        $invoiceId          = $id;
        $base_modulo        = substr('00' . $invoiceId, -6);
        $base_modulo        = date('Y') . $base_modulo;
        $base_modulo_check  = $base_modulo % 97;
        $base_modulo_check  = substr('0' . $base_modulo_check, -2);
        $reference          = substr($base_modulo . $base_modulo_check, -12);
        $reference          = substr($reference, 0, 3).'/'.substr($reference, 3, 4).'/'.substr($reference, 7, 5);

        return $reference;

    }

    function leasingPrice($priceHTVA){
        return (($priceHTVA*(1-0.27)*(1.7) + (3*84+4*100)*1.3)/36);
    }