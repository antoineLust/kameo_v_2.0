<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Boxes_catalog extends Model
{
    use HasFactory;

    public $table = "boxes_catalog";

    public function boxes(){
        return $this->hasMany(Boxe::class);
    }

    public function orders(){
        return $this->hasMany(Boxe_orders::class);
    }
}
