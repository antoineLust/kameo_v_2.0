<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_work_address extends Model
{
    use HasFactory;

    public $table = 'users_works_addresses';
}
