<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company_address extends Model
{
    use HasFactory;

    public $table = 'companies_addresses';

    public function companies(){
        return $this->hasMany(Company::class);
    }
}
