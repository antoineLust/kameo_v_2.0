<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat_sessions extends Model
{
    use HasFactory;

    public function entretiens(){
        return $this->hasMany(Chat::class);
    }
}