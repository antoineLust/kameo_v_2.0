<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Boxe extends Model
{
    use HasFactory;

    public function catalog(){
        return $this->belongsTo(Boxes_catalog::class);
    }

    public function building(){
        return $this->belongsTo(Building::class);
    }

    public function reservationDetails(){
        return $this->hasMany(Reservation_detail::class);
    }
}
