<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    use HasFactory;

    public function customerBuildingAccess(){
        return $this->belongsTo(Customers_bikes_access::class);
    }

    public function commany(){
        return $this->belongsTo(Company::class);
    }

    public function bikeBuildingAccess(){
        return $this->belongsTo(Bikes_buildings_access::class);
    }

    public function boxes(){
        return $this->hasMany(Boxe::class);
    }

    public function calendar(){
        return $this->belongsTo(Calendar::class);
    } 
}
