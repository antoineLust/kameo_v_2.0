<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entretien_detail extends Model
{
    use HasFactory;

    public function entretien(){
        return $this->belongsTo(Entretien::class);
    }
}
