<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Accessory_category extends Model
{
    use HasFactory;

    public $table = 'accessory_categories';

    public function accessories(){
        return $this->hasMany(Accessories_catalog::class);
    }
}
