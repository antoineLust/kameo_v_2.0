<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function company(){
        return $this->belongsTo(Company::class, 'companies_id', 'id');
    }
}
