<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use HasFactory;

    public function company(){
        return $this->belongsTo(Company::class, 'companies_id', 'id');
    }

    public function details(){
        return $this->hasMany(Offer_detail::class);
    }
}
