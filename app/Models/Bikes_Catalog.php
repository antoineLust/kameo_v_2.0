<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bikes_Catalog extends Model
{
    use HasFactory;

    public $table = "bikes_catalog";

    public function bikes(){
        return $this->hasMany(Bike::class);
    }

    public function orders(){
        return $this->hasMany(Bike_order::class);
    }
}
