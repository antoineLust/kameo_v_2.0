<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bikes_buildings_access extends Model
{
    use HasFactory;

    public $table = "bikes_buildings_access";

    public function bike(){
        return $this->belongsTo(Bike::class);
    }

    public function buildings(){
        return $this->hasMany(Building::class);
    }
}
