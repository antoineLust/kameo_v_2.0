<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Grouped_order extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class, 'users_id', 'id');
    }

    public function company(){
        return $this->belongsTo(Company::class, 'companies_id', 'id');
    }

    public function bikes(){
        return $this->hasMany(Bike_order::class, 'grouped_orders_id', 'id');
    }

    public function accessories(){
        return $this->hasMany(Accessory_order::class, 'grouped_orders_id', 'id');
    }

    public function boxes(){
        return $this->hasMany(Boxe_order::class, 'grouped_orders_id', 'id');
    }
}
