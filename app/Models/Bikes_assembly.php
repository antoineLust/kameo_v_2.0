<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bikes_assembly extends Model
{
    use HasFactory;

    protected $table = 'bikes_assembly';

    public function bike(){
        return $this->belongsTo(Bike::class, 'bikes_id', 'id');
    }
}
