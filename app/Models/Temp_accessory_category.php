<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Temp_accessory_category extends Model
{
    use HasFactory;

    public $table = 'temp_accessories_categories';

    public function accessories(){
        return $this->hasMany(Accessories_catalog::class);
    }

}
