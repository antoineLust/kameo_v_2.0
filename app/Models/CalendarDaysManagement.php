<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CalendarDaysManagement extends Model
{
    use HasFactory;

    public $table = 'calendar_days_management';
}
