<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bikes_orderable extends Model
{
    use HasFactory;

    public $table = "bikes_orderable";
}
