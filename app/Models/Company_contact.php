<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company_contact extends Model
{
    use HasFactory;

    public $table = 'companies_contacts';

    public function company(){
        return $this->belongsTo(Company::class, 'companies_id', 'id');
    }
}
