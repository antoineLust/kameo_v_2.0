<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bike_order extends Model
{
    use HasFactory;

    public function bike(){
        return $this->belongsTo(Bike::class);
    }

    public function catalog(){
        return $this->belongsTo(Bikes_Catalog::class, 'bikes_catalog_id', 'id');
    }

    public function grouped(){
        return $this->belongsTo(Grouped_order::class);
    }

    public function calendar(){
        return $this->belongsTo(Calendar::class);
    } 
}
