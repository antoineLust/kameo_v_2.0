<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Accessory extends Model
{
    use HasFactory;

    public $table = "accessories";

    public function catalog(){
        return $this->belongsTo(Accessories_catalog::class, 'accessories_catalog_id', 'id');
    }
    public function company(){
        return $this->belongsTo(Company::class, 'companies_id', 'id');
    }
}
