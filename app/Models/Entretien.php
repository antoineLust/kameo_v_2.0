<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entretien extends Model
{
    use HasFactory;

    public function bike(){
        return $this->belongsTo(Bike::class);
    }

    public function entretienDetails(){
        return $this->hasMany(Entretien_detail::class);
    }

    public function billDetails(){
        return $this->hasMany(Bill_detail::class);
    }

    public function calendar(){
        return $this->belongsTo(Calendar::class);
    } 
}