<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class External_bike extends Model
{
    use HasFactory;

    public function company()
    {
        return $this->belongsTo(Company::class, 'companies_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
