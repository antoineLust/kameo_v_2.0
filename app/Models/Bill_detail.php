<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill_detail extends Model
{
    use HasFactory;


    public function bill(){
        return $this->belongsTo(Bill::class);
    }

    public function entretien(){
        return $this->belongsTo(Entretien::class);
    }
}
