<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Accessories_catalog extends Model
{
    use HasFactory;

    public $table = "accessories_catalog";

    public function category(){
        return $this->belongsTo(Accessory_category::class, 'accessory_categories_id', 'id');
    }

    public function accessories(){
        return $this->hasMany(Accessory::class);
    }
}
