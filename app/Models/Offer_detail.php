<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offer_detail extends Model
{
    use HasFactory;
    
    public function company(){
        return $this->belongsTo(Company::class, 'companies_id', 'id');
    }
    public function offer(){
        return $this->belongsTo(Offer::class);
    }
    public function bikes(){
        return $this->hasMany(Bike::class);
    }
    public function accessories(){
        return $this->hasMany(Accessory::class);
    }
    public function boxes(){
        return $this->hasMany(Boxe::class, 'details_id', 'id');
    }
}
