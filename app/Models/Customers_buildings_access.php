<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customers_buildings_access extends Model
{
    use HasFactory;

    public $table = "customers_buildings_access";

    public function user(){
        return $this->belongsTo(User::class);
    }
}
