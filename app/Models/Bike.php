<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bike extends Model
{
    use HasFactory;

    public function catalog(){
        return $this->belongsTo(Bikes_Catalog::class, 'bikes_catalog_id', 'id');
    }
    
    public function company(){
        return $this->belongsTo(Company::class, 'companies_id', 'id');
    }

    public function entretiens(){
        return $this->hasMany(Entretien::class);
    }

    public function bikes(){
        return $this->hasMany(Accessory::class);
    }

    public function orders(){
        return $this->hasMany(Bike_order::class);
    }

    public function buildingAccess(){
        return $this->hasMany(Bikes_buildings_access::class);
    }

    public function bikeAccess(){
        return $this->hasMany(Customers_bikes_access::class);
    }

    public function reservations(){
        return $this->hasMany(Reservation::class);
    }

    public function bikeTests(){
        return $this->hasMany(Bike_test::class);
    }
}