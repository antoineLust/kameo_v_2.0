<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    use HasFactory;

    public function entretien(){
        return $this->hasMany(Entretien::class);
    }

    public function order(){
        return $this->hasMany(Bike_order::class);
    }

    public function building(){
        return $this->hasMany(Building::class);
    }

    // manque les tests de vélos non réalisés
}
