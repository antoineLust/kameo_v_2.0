<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function address(){
        return $this->belongsTo(User_address::class, 'users_addresses_id', 'id');
    }

    public function company(){
        return $this->belongsTo(Company::class, 'companies_id', 'id');
    }

    public function accessories(){
        return $this->hasMany(Accessory::class);
    }

    public function bills(){
        return $this->hasMany(Bill::class);
    }

    public function bikesAccess(){
        return $this->hasMany(Customers_bikes_access::class);
    }

    public function buildingsAccess(){
        return $this->hasMany(Customers_buildings_access::class);
    }

    public function externalBikes(){
        return $this->hasMany(External_bike::class);
    }

    public function reservations(){
        return $this->hasMany(Reservation::class);
    }

    public function bikeTests(){
        return $this->hasMany(Bike_test::class);
    }
}
