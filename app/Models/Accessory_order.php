<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Accessory_order extends Model
{
    use HasFactory;

    public function grouped(){
        return $this->belongsTo(Grouped_order::class);
    }

    public function catalog(){
        return $this->belongsTo(Accessories_catalog::class, 'accessories_catalog_id', 'id');
    }

    public function accessory(){
        return $this->belongsTo(Accessory::class);
    }
}
