<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Boxe_order extends Model
{
    use HasFactory;

    public function catalog(){
        return $this->belongsTo(Boxes_catalog::class, 'boxes_catalog_id', 'id');
    }

    public function grouped(){
        return $this->belongsTo(Grouped_order::class);
    }
}
