<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    use HasFactory;

    public $table = "companies";

    public function address(){
        return $this->belongsTo(Company_address::class, 'companies_addresses_id', 'id');
    }

    public function contact(){
        return $this->hasMany(Company_contact::class, 'companies_id', 'id');
    }

    public function bikes(){
        return $this->hasMany(Bike::class);
    }

    public function employes(){
        return $this->hasMany(User::class);
    }

    public function bills(){
        return $this->hasMany(Bill::class);
    }

    public function buildings(){
        return $this->hasMany(Building::class);
    }

    public function externalBikes(){
        return $this->hasMany(External_bike::class);
    }

    public function offers(){
        return $this->hasMany(Offer::class);
    }
}