<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation_detail extends Model
{
    use HasFactory;

    public function boxe(){
        return $this->belongsTo(Boxe::class);
    }
}
