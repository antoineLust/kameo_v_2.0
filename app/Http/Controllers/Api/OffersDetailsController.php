<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accessory;
use App\Models\Offer;
use App\Models\Offer_detail;
use Illuminate\Http\Request;

class OffersDetailsController extends Controller
{
    // Get all entretiens details
    public function index()
    {
        return response()->json(Offer_detail::all());
    }

    // Get one offer detail
    public function show($id)
    {
        return response()->json(Offer_detail::find($id));
    }
    // Delete one offer detail
    public function destroy($id)
    {
        Offer_detail::where('id', $id)->delete();
        return response()->json($id);
    }

    // Create one offer detail
    public function createBikeDetails(Request $request){
        if(array_key_exists('bikes_catalog_id', $request->all())){
            $request->validate([
                'offerId'                           => 'required|string',
                'amount'                            => 'required',
                'bikes_catalog_id'                  => 'required|integer',
                'size'                              => 'required|string',
                'contract_type'                     => 'required|string',
            ]);

            $id = Offer_detail::insertGetId([
                'offers_id'                         => $request->offerId,
                'contract_amount'                   => floatval($request->amount),
                'item_id'                           => $request->bikes_catalog_id,
                'size'                              => $request->size,
                'contract_type'                     => $request->contract_type,
                'item_type'                         => 'bike',
            ]);

            return response()->json(Offer_detail::find($id));
        }
    }

    public function createAccessoryDetails(Request $request){
        if(array_key_exists('accessories_catalog_id', $request->all())){
            $request->validate([
                'offerId'                           => 'required|string',
                'amount'                            => 'required',
                'accessories_catalog_id'            => 'required|integer',
                'size'                              => 'required|string',
                'contract_type'                     => 'required|string',
            ]);

            $id = Offer_detail::insertGetId([
                'offers_id'                         => $request->offerId,
                'contract_amount'                   => floatval($request->amount),
                'item_id'                           => $request->accessories_catalog_id,
                'size'                              => $request->size,
                'contract_type'                     => $request->contract_type,
                'item_type'                         => 'accessory',
            ]);

            return response()->json(Offer_detail::find($id));
        }
    }
    public function createBoxDetails(Request $request){
        if(array_key_exists('boxes_catalog_id', $request->all())){
            $request->validate([
                'offerId'                           => 'required|string',
                'installation_price'                => 'required|numeric',
                'location_price'                    => 'required|numeric',
                'boxes_catalog_id'                  => 'required|integer',
            ]);
            $id = Offer_detail::insertGetId([
                'offers_id'                         => $request->offerId,
                'contract_amount'                   => floatval($request->location_price),
                'installation_amount'               => floatval($request->installation_price),
                'item_id'                           => $request->boxes_catalog_id,
                'contract_type'                     => 'leasing',
                'item_type'                         => 'boxe',
            ]);

            return response()->json(Offer_detail::find($id));
        }
    }

}
