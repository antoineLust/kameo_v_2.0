<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accessory;
use Illuminate\Http\Request;
use App\Models\Accessories_catalog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Models\Temp_accessory_category;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class AccessoriesCatalogController extends Controller
{
    // Get all accessories catalog
    public function index()
    {
        $accessories = Accessories_catalog::with('category')->get();
        foreach ($accessories as $accessory) {
            $accessory->leasingPrice = round($accessory->price_htva * (1.5) / 36, 2);
            $accessory->sizes = explode(',', $accessory->sizes);
        }
        return response()->json($accessories);
    }

    // Get for datatable
    public function datatable(Request $request)
    {

        $accessories = Accessories_catalog::all()->toArray();

        $accessories = array_slice($accessories, ($request->page_number - 1) * $request->interval, $request->interval);

        return response()->json([
            'accessories'   => $accessories,
            'count'         => count($accessories),
        ]);
    }

    // public function getAccessoryByStock($id){

    //     $stock = Accessory::find($id);
    //     $accessory = Accessories_catalog::find($stock->accessories_catalog_id);

    //     if(isset($accessory)){
    //         $accessory->sizes = explode(',', $accessory->sizes);
    //     }

    //     return response()->json($accessory);
    // }

    public function getByIdCafetaria($id)
    {
        $accessory = Accessories_catalog::select('id', 'brand', 'model', 'sizes', 'accessory_categories_id')->find($id);
        if (isset($accessory)) {
            $accessory->sizes = explode(',', $accessory->sizes);
            return response()->json($accessory);
        }

        return response()->json([]);
    }

    public function getById($id)
    {
        $accessory = Accessories_catalog::find($id);
        if (isset($accessory)) {
            $accessory->sizes = explode(',', $accessory->sizes);
        }
        return response()->json($accessory);
    }

    public function getModelsAndBrands()
    {

        $items = [];

        foreach (Accessories_catalog::all() as $accessory) {
            $new_item = (object) [];
            $new_item->id = $accessory->id;
            $new_item->brand = $accessory->brand;
            $new_item->model = $accessory->model;
            array_push($items, $new_item);
        }

        return response()->json($items);
    }

    public function getBrands()
    {

        $brands = [];

        foreach (Accessories_catalog::all() as $accessory) {
            array_push($brands, $accessory->brand);
        }

        $unique = array_unique($brands);
        sort($unique);

        return response()->json($unique);
    }

    public function getFromCategoryCafetaria($category)
    {
        return response()->json(Accessories_catalog::select('id', 'brand', 'model', 'price_htva', 'accessory_categories_id')->where('accessory_categories_id', $category)->get());
    }

    public function getModelsFromCategory($category)
    {
        return response()->json(Accessories_catalog::select('id', 'model')->where('accessory_categories_id', $category)->get());
    }

    public function getModelsFromBrand($brand)
    {
        return response()->json(Accessories_catalog::select('id', 'model')->where('brand', $brand)->get());
    }

    public function getAccessorySizes($id)
    {

        $accessory = Accessories_catalog::find($id);

        if (isset($accessory)) {
            return response()->json(explode(',', $accessory->sizes));
        } else {
            return response()->json([]);
        }
    }

    // Get one accessory catalog
    public function show($id)
    {

        $path = storage_path('app/public/accessories_pictures/' . $id);

        $accessory = Accessories_catalog::with('category')->find($id);

        if (is_dir($path)) {
            $files                  = scandir($path);
            $files                  = array_diff($files, array('.', '..'));
            $accessory->pictures    = $files;
            $accessory->sizes       = explode(',', $accessory->sizes);
        }

        return response()->json($accessory);
    }

    // Create bikes catalog
    public function create(Request $request)
    {
        $request->form = json_decode($request->form, true);
        // Validations criteria
        Validator::make($request->form, [
            'model'                     => 'required|string',
            'brand'                     => 'required|string',
            'provider'                  => 'required|string',
            'accessory_categories_id'   => 'required|integer',
            'sizes'                     => 'required',
            'display'                   => 'required|string',
            'status'                    => 'required|string',
            'minimal_stock'             => 'required|integer',
            'optimal_stock'             => 'required|integer',
            'buying_price'              => 'required|numeric',
            'price_htva'                => 'required|numeric',
            'add_contract_type'         => 'required|string',
            'add_s'                     => 'required|integer',
            'add_m'                     => 'required|integer',
            'add_l'                     => 'required|integer',
            'add_xl'                    => 'required|integer',
            'add_unique'                   => 'required|integer',
        ])->validate();

        $id = Accessories_catalog::insertGetId([
            'model'                     => $request->form['model'],
            'brand'                     => $request->form['brand'],
            'provider'                  => $request->form['provider'],
            'accessory_categories_id'   => $request->form['accessory_categories_id'],
            'description'               => array_key_exists('description', $request->form) ? $request->form['description'] : null,
            'sizes'                     => implode(',', $request->form['sizes']),
            'display'                   => $request->form['display'],
            'status'                    => $request->form['status'],
            'minimal_stock'             => ($request->form['minimal_stock'] === NULL) ? 0 : $request->form['minimal_stock'],
            'optimal_stock'             => ($request->form['optimal_stock'] === NULL) ? 0 : $request->form['optimal_stock'],
            'buying_price'              => $request->form['buying_price'],
            'price_htva'                => $request->form['price_htva']
        ]);
        $accessory = Accessories_catalog::with('category')->find($id);
        $accessory->sizes = explode(',', $accessory->sizes);

        if ($request->file()) {
            $uploadedPictures = $this->upload($request->file(), $id);
            $accessory->pictures = $uploadedPictures;
        }

        if($request->form['add_xs'] !== 0){
            for($i = 0; $i < $request->form['add_xs']; $i++){
                Accessory::insert([
                    'accessories_catalog_id'    => $id,
                    'size'                      => 'XS',
                    'contract_type'             => $request->form['add_contract_type'],
                    'status'                    => 'actif',
                    'companies_id'              => 12,
                ]);
            }
        }
        if($request->form['add_s'] !== 0){
            for($i = 0; $i < $request->form['add_s']; $i++){
                Accessory::insert([
                    'accessories_catalog_id'    => $id,
                    'size'                      => 'S',
                    'contract_type'             => $request->form['add_contract_type'],
                    'status'                    => 'actif',
                    'companies_id'              => 12,
                ]);
            }
        }
        if($request->form['add_m'] !== 0){
            for($i = 0; $i < $request->form['add_m']; $i++){
                Accessory::insert([
                    'accessories_catalog_id'    => $id,
                    'size'                      => 'M',
                    'contract_type'             => $request->form['add_contract_type'],
                    'status'                    => 'actif',
                    'companies_id'              => 12,
                ]);
            }
        }
        if($request->form['add_l'] !== 0){
            for($i = 0; $i < $request->form['add_l']; $i++){
                Accessory::insert([
                    'accessories_catalog_id'    => $id,
                    'size'                      => 'L',
                    'contract_type'             => $request->form['add_contract_type'],
                    'status'                    => 'actif',
                    'companies_id'              => 12,
                ]);
            }
        }
        if($request->form['add_xl'] !== 0){
            for($i = 0; $i < $request->form['add_xl']; $i++){
                Accessory::insert([
                    'accessories_catalog_id'    => $id,
                    'size'                      => 'XL',
                    'contract_type'             => $request->form['add_contract_type'],
                    'status'                    => 'actif',
                    'companies_id'              => 12,
                ]);
            }
        }
        if($request->form['add_unique'] !== 0){
            for($i = 0; $i < $request->form['add_unique']; $i++){
                Accessory::insert([
                    'accessories_catalog_id'    => $id,
                    'size'                      => 'UNIQUE',
                    'contract_type'             => $request->form['add_contract_type'],
                    'status'                    => 'actif',
                    'companies_id'              => 12,
                ]);
            }
        }

        return response()->json($accessory);
    }

    // Update accessories catalog
    public function update(Request $request)
    {
        // Decode json
        $request->form = json_decode($request->form, true);
        // Validations criteria
        Validator::make($request->form, [
            'model'                     => 'required|string',
            'brand'                     => 'required|string',
            'provider'                  => 'required|string',
            'accessory_categories_id'   => 'required|integer',
            'sizes'                     => 'required',
            'display'                   => 'required|string',
            'status'                    => 'required|string',
            'minimal_stock'             => 'required|integer',
            'optimal_stock'             => 'required|integer',
            'buying_price'              => 'required|numeric',
            'price_htva'                => 'required|numeric',
        ])->validate();
        Accessories_catalog::where('id', $request->form['id'])
            ->update([
                'model'                     => $request->form['model'],
                'brand'                     => $request->form['brand'],
                'provider'                  => $request->form['provider'],
                'accessory_categories_id'   => $request->form['accessory_categories_id'],
                'description'               => array_key_exists('description', $request->form) ? $request->form['description'] : null,
                'sizes'                     => is_array($request->form['sizes']) ? implode(',', $request->form['sizes']) : '',
                'display'                   => $request->form['display'],
                'status'                    => $request->form['status'],
                'minimal_stock'             => ($request->form['minimal_stock'] === NULL) ? 0 : $request->form['minimal_stock'],
                'optimal_stock'             => ($request->form['optimal_stock'] === NULL) ? 0 : $request->form['optimal_stock'],
                'buying_price'              => $request->form['buying_price'],
                'price_htva'                => $request->form['price_htva']
            ]);

        $accessory = Accessories_catalog::find($request->form['id']);
        $accessory->sizes = explode(',', $accessory->sizes);

        $path = storage_path('app/public/accessories_pictures/' . $request->form['id']);

        // Return response
        if ($request->file()) {
            if (array_key_exists('pictures', $request->form)) {
                $newArray = $request->form['pictures'];
                foreach ($this->upload($request->file(), $request->form['id']) as $picture) {
                    array_push($newArray, $picture);
                }
            } else {
                $newArray = [];
                foreach ($this->upload($request->file(), $request->form['id']) as $picture) {
                    array_push($newArray, $picture);
                }
            }
            $accessory->pictures = $newArray;
        } else if (is_dir($path)) {
            $files                  = scandir($path);
            $files                  = array_diff($files, array('.', '..'));
            $accessory->pictures    = $files;
        }
        return response()->json($accessory);
    }

    // Destroy accessory catalog
    public function destroy(Request $request)
    {
        $request->validate([
            'accessoryId' => 'required|integer'
        ]);
        Accessories_catalog::where('id', $request->accessoryId)
            ->update([
                'status'    => 'inactif'
            ]);
        return response()->json(Accessories_catalog::find($request->accessoryId));
    }

    // Upload images
    public function upload($files, $id)
    {
        File::ensureDirectoryExists(public_path('/storage/accessories_pictures/' . $id));
        $uploadedPictures = [];
        foreach ($files as $key => $file) {
            $fileName       = $file->getClientOriginalName();
            $formdataKey    = str_replace('.', '_', $fileName);
            $path           = $file->storeAs('accessories_pictures/' . $id, $fileName, 'public');
            array_push($uploadedPictures, $fileName);
        }
        return $uploadedPictures;
    }

    // Delete image
    public function deletePicture(Request $request, $id)
    {
        $array  = $request->accessory['pictures'];
        $key    = array_search($request->pictureName, $array);
        unset($array[$key]);
        $path   = public_path('/storage/accessories_pictures/' . $id . '/' . $request->pictureName);
        if (File::exists($path)) {
            File::delete($path);
        }
        return response()->json($array);
    }

    // Get all bikes catalogs
    public function getAllAccessoriesForShop($category)
    {
        if ($category) {
            $accessories = array();
            foreach (Temp_accessory_category::where('main', '=', $category)->orWhere('sub_1', '=', $category)->orWhere('sub_2', '=', $category)->get() as $subCategory) {
                foreach (Accessories_catalog::where('accessory_categories_id', '=', $subCategory->id)->get() as $accessoryCatalog) {
                    $accessoryCatalog->stock = $reserve = Accessory::where('contract_type', '=', 'stock')->where('accessories_catalog_id', '=', $accessoryCatalog->id)->get();
                    array_push($accessories, $accessoryCatalog);
                }
            }
            // foreach(Temp_accessory_category::where('main', '=', $category)->orWhere('sub_1', '=', $category)->orWhere('sub_2', '=', $category)->get() as $subCategory){
            //     foreach(Accessories_catalog::where('accessory_categories_id', '=', $subCategory->id)->get() as $accessoryCatalog){
            //         $accessoryCatalog->stock=$reserve = Accessory::where('contract_type', '=', 'stock')->where ('accessories_catalog_id', '=', $accessoryCatalog->id)->get()->count();
            //         array_push($accessories, $accessoryCatalog);
            //     }
            // }
        } else {
            $accessories = Accessories_catalog::all();
        }
        return response()->json($accessories);
    }

    public function getAccessoryForShop($id)
    {
        $accessory = Accessories_catalog::find($id);
        $accessory->sizes = explode(',', $accessory->sizes);
        return view('shop.pages.accessory-details', compact('accessory'));
    }

    // Get datatable info
    public function getDataForDataTable(Request $request)
    {
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        $sortDirection = json_decode($request->sortDirection, true);
        if ($search) {
            $q = explode(' ', $search);
            $res = Accessories_catalog::select('id', 'brand', 'model', 'sizes', 'price_htva');
            for($i = 0; $i < count($q); $i++){
                $res = $res->where(function($query) use ($q, $i) {
                    $query->where('brand', 'like', '%' . $q[$i] . '%')
                    ->orWhere('model', 'like', '%' . $q[$i] . '%')
                    ->orWhere('sizes', 'like', '%' . $q[$i] . '%')
                    ->orWhere('price_htva', 'like', '%' . $q[$i] . '%')
                    ->orWhere('id', 'like', '%' . $q[$i] . '%');
                });
            }
            $accessories = $res->orderBy($sortDirection['column'], $sortDirection['direction'])
                ->limit($limit)
                ->offset(($page - 1) * $limit)
                ->get();
            
        } else {
            $accessories = Accessories_catalog::select('id', 'brand', 'model', 'sizes', 'price_htva')->orderBy($sortDirection['column'], $sortDirection['direction'])->limit($limit)->offset(($page - 1) * $limit)->get();
        }
        foreach ($accessories as $accessory) {
            $accessory->price_htva = number_format($accessory->price_htva * 1.21, 2, ',', ' ') . '€';
        }
        return response()->json($accessories);
    }

    public function getAllAccessoriesForSelect($idCategory)
    {
        if ($idCategory) {
            $category = Temp_accessory_category::where('id', '=', $idCategory)->first()->toArray();
            if (isset($category['sub_4_id'])) {
                $accessories = Accessories_catalog::join('temp_accessories_categories', 'temp_accessories_categories.id', '=', 'accessories_catalog.accessory_categories_id')
                    ->where('temp_accessories_categories.sub_4_id', '=', $category['sub_4_id'])->select('accessories_catalog.id', 'model')->get();
            } elseif (isset($category['sub_3_id'])) {
                $accessories = Accessories_catalog::join('temp_accessories_categories', 'temp_accessories_categories.id', '=', 'accessories_catalog.accessory_categories_id')
                    ->where('temp_accessories_categories.sub_3_id', '=', $category['sub_3_id'])->select('accessories_catalog.id', 'model')->get();
            } elseif (isset($category['sub_2_id'])) {
                $accessories = Accessories_catalog::join('temp_accessories_categories', 'temp_accessories_categories.id', '=', 'accessories_catalog.accessory_categories_id')
                    ->where('temp_accessories_categories.sub_2_id', '=', $category['sub_2_id'])->select('accessories_catalog.id', 'model')->get();
            } elseif (isset($category['sub_1_id'])) {
                $accessories = Accessories_catalog::join('temp_accessories_categories', 'temp_accessories_categories.id', '=', 'accessories_catalog.accessory_categories_id')
                    ->where('temp_accessories_categories.sub_1_id', '=', $category['sub_1_id'])->select('accessories_catalog.id', 'model')->get();
            } elseif (isset($category['main_id'])) {
                $accessories = Accessories_catalog::join('temp_accessories_categories', 'temp_accessories_categories.id', '=', 'accessories_catalog.accessory_categories_id')
                    ->where('temp_accessories_categories.main_id', '=', $category['main_id'])->select('accessories_catalog.id', 'model')->get();
            }
        } else {
            $accessories = Accessories_catalog::all();
        }
        return response()->json($accessories);
    }
}
