<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use App\Models\Offer_detail;
use Illuminate\Http\Request;

class OffersController extends Controller
{
    // Get all offers
    public function index(){
        return response()->json(Offer::with('company')->get()); 
    }

    // Get one offer
    public function show($id){
        return response()->json([
            'offer'         => Offer::with('company')->find($id),
            'offerDetails'  => Offer_detail::where('offers_id', $id)->get(),
        ]);
    }

    // Create a new offer
    public function create(Request $request){
        // Validate request
        $request->validate([
            'companies_id'                      => 'required|integer',
            'users_id'                          => 'nullable|integer',
            'employee_id'                       => 'required|integer',
        ]);
        // Create offer
        $id = offer::insertGetId([
            'companies_id'                      => $request->companies_id,
            'users_id'                          => $request->users_id,
            'probability'                       => 0,
            'employee_id'                       => $request->employee_id,
        ]);
        // Return offer
        return response()->json(Offer::with('company')->find($id));
    }
    
    // Update an offer
    public function update(Request $request, $id){
        // Validate request
        $request->validate([
            'description'                       => 'nullable|string',
            'title'                             => 'nullable|string',
            'probability'                       => 'nullable|integer',
            'step'                              => 'nullable|string',
            'global_discount'                   => 'nullable|string',
            'validity_date'                     => 'nullable|date',
        ]);
        // Update offer
        Offer::where('id', $id)->update([
            'description'                       => $request->description,
            'title'                             => $request->title,
            'probability'                       => $request->probability,
            'step'                              => $request->step,
            'global_discount'                   => $request->global_discount,
            'validity_date'                     => $request->validity_date,
        ]);
        // Return offer
        return response()->json(Offer::with('company')->find($id));
    }

    // Destroy an offer
    public function destroy($id){

        $fileName = Offer::find($id)->file_name;

        unlink(public_path('storage/' . $fileName));

        Offer::where('id', $id)->delete();

        return response()->json($id);
    }

    // Get datatable info
    public function getDataForDataTable(Request $request){
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        if($search){
            $offers = Offer::join('companies', 'offers.companies_id', '=', 'companies.id')
                ->join('offer_details', 'offers.id', '=', 'offer_details.offers_id')
                ->select('offers.id', 'offers.step', 'companies.name as client')
                ->where('offers.id', 'like', '%' . $search . '%')
                ->orWhere('offers.step', 'like', '%' . $search . '%')
                ->orWhere('companies.name', 'like', '%' . $search . '%')
                ->orderBy('offers.id', 'desc')
                ->limit($limit)
                ->offset(($page - 1) * $limit)
                ->get();
        }
        else{
            $offers = Offer::join('companies', 'offers.companies_id', '=', 'companies.id')
            ->join('offer_details', 'offers.id', '=', 'offer_details.offers_id')
            ->select('offers.id', 'offers.step', 'companies.name as client')->orderBy('offers.id', 'desc')->limit($limit)->offset(($page - 1) * $limit)->get();
        }
        foreach($offers as $offer){
            $offer['bikes'] = Offer_detail::where('offers_id', $offer->id)->where('item_type', 'bike')->count();
            $offer['accessories'] = Offer_detail::where('offers_id', $offer->id)->where('item_type', 'accessory')->count();
            $offer['boxes'] = Offer_detail::where('offers_id', $offer->id)->where('item_type', 'box')->count();
        }
        return response()->json($offers);
    }
}

