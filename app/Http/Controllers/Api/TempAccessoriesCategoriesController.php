<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Temp_accessory_category as ModelsTemp_accessory_category;
use Illuminate\Support\Facades\Artisan;



class TempAccessoriesCategoriesController extends Controller
{
    public function index()
    {
        $categories = ModelsTemp_accessory_category::all();
        $categories = ModelsTemp_accessory_category::join('accessories_catalog', 'accessories_catalog.accessory_categories_id', '=', 'temp_accessories_categories.main_id')
            ->select('temp_accessories_categories.id', 'temp_accessories_categories.main', 'temp_accessories_categories.sub_1', 'temp_accessories_categories.sub_2', 'temp_accessories_categories.sub_3', 'temp_accessories_categories.sub_4', 'accessories_catalog.model')
            ->orderBy('temp_accessories_categories.main', 'asc')
            ->orderBy('temp_accessories_categories.sub_1', 'asc')
            ->orderBy('temp_accessories_categories.sub_2', 'asc')
            ->orderBy('temp_accessories_categories.sub_3', 'asc')
            ->orderBy('temp_accessories_categories.sub_4', 'asc')
            ->orderBy('accessories_catalog.model', 'asc')->get();

        foreach ($categories as $category) {
            $temp = $category->main;
            if ($category->sub_1 != NULL) {
                $temp = $temp . " > " . $category->sub_1;
            }
            if ($category->sub_2 != NULL) {
                $temp = $temp . " > " . $category->sub_2;
            }
            if ($category->sub_3 != NULL) {
                $temp = $temp . " > " . $category->sub_3;
            }
            if ($category->sub_4 != NULL) {
                $temp = $temp . " > " . $category->sub_4;
            }
            $category->description = $temp;
        }
        return $categories;
    }


    public function refresh()
    {
        $exitCode = Artisan::call('updateAccessoriesCategoriesLinks');
    }
}
