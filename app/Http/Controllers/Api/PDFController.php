<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Bike;
use App\Models\Bike_order;
use App\Models\Company;
use App\Models\Accessory;
use App\Models\Accessory_order;
use App\Models\Boxe;
use App\Models\Boxe_order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Models\Accessories_catalog;
use App\Models\Bill;
use App\Models\Bill_detail;
use App\Models\Grouped_order;
use App\Models\User;
use App\Models\Offer;
use App\Models\Offer_detail;
use BaconQrCode\Renderer\Color\Rgb;
use Endroid\QrCode\Bacon\MatrixFactory;
use Endroid\QrCode\Builder\Builder;
use Endroid\QrCode\Color\Color;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelMedium;
use Endroid\QrCode\Label\Alignment\LabelAlignmentCenter;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;
use SepaQr\Data;

use function App\Helpers\createReferenceNumber;
use function App\Helpers\generateSepaQrCode;

class PDFController extends Controller
{

    public function purchaseOrder(Request $request)
    {
        // Define data
        $purchaseOrderPath = 'purchaseOrder.pdf';

        // Retrieve data from database
        $items = [];
        foreach ($request->all() as $item) {
            if (isset($item['bike_id'])) {
                $other_keys = Bike::select('frame_number', 'locker_reference', 'key_reference')->where('id', $item['bike_id'])->first();
                if ($other_keys) {
                    $item = array_merge($item, $other_keys->toArray());
                }
            }
            $items[] = $item;
        }

        // Initialize the DOMPDF class
        $pdf = App::make('dompdf.wrapper');
        // Retrieve the HTML generated in the view using Blade
        $pdf->loadView('pdf.purchaseOrder.model-1', compact('items'));
        // Upload to server
        $pdf->save(public_path("storage/purchaseOrder/" . $purchaseOrderPath));

        // Return pdf file name
        return response()->json([
            'success'   => true,
            'fileName'  => "/storage/purchaseOrder/" . $purchaseOrderPath
        ]);
    }

    public function invoice(Request $request, $order)
    {
        // Define data
        $totalHTVA      = 0;
        $company        = Company::with('address', 'contact')->find(Grouped_order::select('companies_id')->where('id', intval($order))->first()->companies_id);
        $outID          = Bill::where('date', '>=', date('Y-01-01'))->max('out_id') + 1;
        $invoiceId      = Bill::max('id') + 1;
        $reference      = createReferenceNumber($invoiceId);
        $invoicePath    = $invoiceId . '_' . $outID . '_' . date('Y-m-d') . '.pdf';
        // dd($request->all());
        // Get others details for invoice
        foreach ($request->all() as $item) {
            // dd($item);
            if ($item['name'] === 'bike') {
                // Get other keys for add into order
                $other_keys = Bike::select('frame_number', 'locker_reference', 'key_reference', 'size', 'contract_type', 'users_id')->where('id', $item['bike_id'])->first();
                // Get accessories order
                $item['accessories'] = Accessory::select('*')->where('bikes_id', $item['bike_id'])->get();
                foreach ($item['accessories'] as $accessory) {
                    $accessory->catalog = Accessories_catalog::find($accessory->accessories_catalog_id);
                }
                if ($other_keys['contract_type'] === 'selling') {
                    $other_keys['selling_price']    = Bike::select('selling_price')->where('id', $item['bike_id'])->first()['selling_price'];
                    $item = array_merge($item, $other_keys->toArray());
                } else {
                    $other_keys['leasing_price']    = Bike::select('leasing_price')->where('id', $item['bike_id'])->first()['leasing_price'];
                    $other_keys['contract_start']   = Bike::select('contract_start')->where('id', $item['bike_id'])->first()['contract_start'];
                    $other_keys['contract_end']     = Bike::select('contract_end')->where('id', $item['bike_id'])->first()['contract_end'];
                    $item = array_merge($item, $other_keys->toArray());
                }
                // Get user if bike is personnal
                if ($item['users_id'] !== null) {
                    $item = array_merge($item, User::select('firstname', 'lastname')->where('id', $item['users_id'])->first()->toArray());
                }
            } else if ($item['name'] === 'accessory') {
                $other_keys = Accessory::select('size', 'contract_type')->where('id', $item['accessory_id'])->first();
                if ($other_keys['contract_type'] === 'selling') {
                    $other_keys['selling_price']    = accessory::select('selling_price')->where('id', $item['accessory_id'])->first()['selling_price'];
                    $item = array_merge($item, $other_keys->toArray());
                } else {
                    $other_keys['leasing_price']    = accessory::select('leasing_price')->where('id', $item['accessory_id'])->first()['leasing_price'];
                    $other_keys['contract_start']   = accessory::select('contract_start')->where('id', $item['accessory_id'])->first()['contract_start'];
                    $other_keys['contract_end']     = accessory::select('contract_end')->where('id', $item['accessory_id'])->first()['contract_end'];
                    $item = array_merge($item, $other_keys->toArray());
                }
            } else if ($item['name'] === 'boxe') {
                $other_keys = Boxe::select('contract_start', 'contract_end', 'reference')->where('id', $item['boxe_id'])->first();
                $item = array_merge($item, $other_keys->toArray());
            }

            $items[] = $item;
            // dd($items);
        }


        // Calculate price
        foreach ($items as $item) {
            $totalHTVA += intval($item['selling_price']);
        }
        $totalTVAC = $totalHTVA * 1.21;
        // Create Qr code
        // generateSepaQrCode(
        //     [
        //         'name'          => 'KAMEO bikes',
        //         'iban'          => 'BE90 5230 8139 8132',
        //         'bic'           => 'TRIOBEBB',
        //         'amount'        => $totalTVAC,
        //         'reference'     => $reference,
        //     ]
        // );
        // Initialize the DOMPDF class
        // dd('test');
        $pdf = App::make('dompdf.wrapper');
        // Retrieve the HTML generated in the view using Blade
        $pdf->loadView('pdf.invoice.model-1', compact('items', 'company', 'reference', 'outID'));
        // Upload to server
        $pdf->save(public_path("storage/invoices/" . $invoicePath));
        // Insert bill into database
        Bill::insertGetId([
            'id'                    => $invoiceId,
            'out_id'                => $outID,
            'communication'         => $reference,
            'companies_id'          => $company->id,
            'beneficiary_company'   => $company->name,
            'invoice_sent'          => 0,
            'invoice_paid'          => 0,
            'accounting'            => 0,
            'file_name'             => 'invoices/' . $invoicePath,
            'date'                  => date('Y-m-d'),
            'amount_htva'           => $totalHTVA,
            'amount_tvac'           => $totalTVAC,
            'created_at'           => date('Y-m-d H:i:s'),
            'updated_at'           => date('Y-m-d H:i:s'),
        ]);

        // Insert bill details into database
        foreach ($items as $item) {
            Bill_detail::insert([
                'type'                => $item['name'],
                'item_id'             => $item['id'],
                'bills_id'            => $invoiceId,
                'amount_htva'         => $item['selling_price'],
                'amount_tvac'         => $item['selling_price'] * 1.21,
            ]);
        }

        // Return response
        return response()->json([
            'success'   => true,
            'fileName'  => "/storage/invoices/" . $invoicePath,
        ]);
    }

    public function  creditNote(Request $request, $order)
    {
        // Define data
        $company        = Company::with('address', 'contact')->find(Grouped_order::select('companies_id')->where('id', intval($order))->first()->companies_id);
        $outID          = Bill::where('date', '>=', date('Y-01-01'))->max('out_id') + 1;
        $invoiceId      = Bill::max('id') + 1;
        $invoicePath    = 'CN_' . $invoiceId . '_' . $outID . '_' . date('Y-m-d') . '.pdf';

        // Get others details for invoice
        foreach ($request->all() as $item) {
            if ($item['name'] === 'bike') {
                // Get other keys for add into order
                $other_keys = Bike::select('frame_number', 'locker_reference', 'key_reference', 'size', 'contract_type', 'users_id')->where('id', $item['bike_id'])->first();
                // Get accessories order
                $item['accessories'] = Accessory::select('*')->where('bikes_id', $item['bike_id'])->get();
                foreach ($item['accessories'] as $accessory) {
                    $accessory->catalog = Accessories_catalog::find($accessory->accessories_catalog_id);
                }
                if ($other_keys['contract_type'] === 'selling') {
                    $other_keys['selling_price']    = Bike::select('selling_price')->where('id', $item['bike_id'])->first()['selling_price'];
                    $item = array_merge($item, $other_keys->toArray());
                } else {
                    $other_keys['leasing_price']    = Bike::select('leasing_price')->where('id', $item['bike_id'])->first()['leasing_price'];
                    $other_keys['contract_start']   = Bike::select('contract_start')->where('id', $item['bike_id'])->first()['contract_start'];
                    $other_keys['contract_end']     = Bike::select('contract_end')->where('id', $item['bike_id'])->first()['contract_end'];
                    $item = array_merge($item, $other_keys->toArray());
                }
                // Get user if bike is personnal
                if ($item['users_id'] !== null) {
                    $item = array_merge($item, User::select('firstname', 'lastname')->where('id', $item['users_id'])->first()->toArray());
                }
            } else if ($item['name'] === 'accessory') {
                $other_keys = Accessory::select('size', 'contract_type')->where('id', $item['accessory_id'])->first();
                if ($other_keys['contract_type'] === 'selling') {
                    $other_keys['selling_price']    = accessory::select('selling_price')->where('id', $item['accessory_id'])->first()['selling_price'];
                    $item = array_merge($item, $other_keys->toArray());
                } else {
                    $other_keys['leasing_price']    = accessory::select('leasing_price')->where('id', $item['accessory_id'])->first()['leasing_price'];
                    $other_keys['contract_start']   = accessory::select('contract_start')->where('id', $item['accessory_id'])->first()['contract_start'];
                    $other_keys['contract_end']     = accessory::select('contract_end')->where('id', $item['accessory_id'])->first()['contract_end'];
                    $item = array_merge($item, $other_keys->toArray());
                }
            } else if ($item['name'] === 'boxe') {
                $other_keys = Boxe::select('contract_start', 'contract_end', 'reference')->where('id', $item['boxe_id'])->first();
                $item = array_merge($item, $other_keys->toArray());
            }

            $items[] = $item;
        }

        $accessories = [];
        $accessories_order = [];
        $bikes_order = [];
        $bikes = [];

        // Update accessory & bike stock
        foreach ($items as $item) {
            if ($item['name'] === 'accessory' && $item['contract_type'] === 'selling') {
                Accessory::where('id', $item['accessory_id'])->update([
                    'contract_type'                 => 'stock',
                    'companies_id'                  => null,
                    'users_id'                      => null,
                    'selling_price'                 => null,
                    'selling_date'                  => null,
                    'leasing_price'                 => null,
                    'contract_start'                => null,
                    'delivery_date'                 => null,
                    'estimated_delivery_date'       => null,
                ]);
                array_push($accessories, Accessory::find($item['accessory_id']));
                Accessory_order::where('accessories_id', $item['accessory_id'])->update([
                    'accessories_id'                => null,
                ]);
                array_push($accessories_order, ...Accessory_order::where('accessories_id', $item['accessory_id'])->get()->toArray());
            } else if ($item['name'] === 'bike' && $item['contract_type'] === 'selling') {
                Bike::where('id', $item['bike_id'])->update([
                    'contract_type'                 => 'stock',
                    'companies_id'                  => null,
                    'users_id'                      => null,
                    'selling_price'                 => null,
                    'selling_date'                  => null,
                    'leasing_price'                 => null,
                    'contract_start'                => null,
                    'client_name'                   => null,
                    'delivery_date'                 => null,
                    'estimated_delivery_date'       => null,
                    'stolen_date'                   => null,
                    'billing_type'                  => null,
                ]);
                array_push($bikes, Bike::where('id', $item['bike_id'])->get()->toArray());
                Bike_order::where('bikes_id', $item['bike_id'])->update([
                    'bikes_id'                      => null,
                ]);
                array_push($bikes_order, Bike_order::where('bikes_id', $item['bike_id'])->get()->toArray());
            }
        }
        // Generate PDF
        // Initialize the DOMPDF class
        $pdf = App::make('dompdf.wrapper');
        // Retrieve the HTML generated in the view using Blade
        $pdf->loadView('pdf.creditNote.model-1', compact('items', 'company', 'outID'));
        // Upload to server
        $pdf->save(public_path("storage/creditNotes/" . $invoicePath));

        // Calculate price
        $totalHTVA = 0;
        foreach ($items as $item) {
            $totalHTVA -= intval($item['selling_price']);
        }
        $totalTVAC = $totalHTVA * 1.21;

        // Insert bill into database
        Bill::insertGetId([
            'id'                    => $invoiceId,
            'out_id'                => $outID,
            'companies_id'          => $company->id,
            'beneficiary_company'   => $company->name,
            'invoice_sent'          => 0,
            'invoice_paid'          => 0,
            'accounting'            => 0,
            'file_name'             => 'invoices/' . $invoicePath,
            'date'                  => date('Y-m-d'),
            'amount_htva'           => $totalHTVA,
            'amount_tvac'           => $totalTVAC,
            'created_at'           => date('Y-m-d H:i:s'),
            'updated_at'           => date('Y-m-d H:i:s'),
        ]);

        // Return response
        return response()->json([
            'success'           => true,
            'fileName'          => "/storage/creditNotes/" . $invoicePath,
            'accessories'       => $accessories,
            'accessories_order' => $accessories_order,
            'bikes_order'       => $bikes_order,
            'bikes'             => $bikes
        ]);
    }
    public function generateOffer(Request $request, $offer)
    {
        // Get all infos for the offer
        $company        = Company::with('address', 'contact')->find(Offer::select('companies_id')->where('id', intval($offer))->first()->companies_id);
        $allItems       = $request->all();
        $offer          = Offer::with('company')->find(intval($offer));
        $user           = User::with('company', 'address')->find($offer->users_id);
        $employee       = User::with('company', 'address')->find($offer->employee_id);
        $bikes          = [];
        $accessories    = [];
        $boxes          = [];
        $totalHTVA = 0;
        foreach ($allItems as $item) {
            $totalHTVA += intval($item['contract_amount']);
            if ($item['item_type'] === 'box') {
                $totalHTVA += intval($item['installation_price']);
            }
            if ($item['item_type'] === 'bike') {
                $bikes[] = $item;
            } else if ($item['item_type'] === 'accessory') {
                $accessories[] = $item;
            } else if ($item['item_type'] === 'boxe') {
                $boxes[] = $item;
            }
        }
        $totalTVAC = $totalHTVA * 1.21;

        // Generate PDF

        // Initialize the DOMPDF class
        $pdf = App::make('dompdf.wrapper');
        // Retrieve the HTML generated in the view using Blade
        $pdf->loadView('pdf.offer.model-1', compact('allItems', 'bikes', 'boxes', 'accessories', 'company', 'totalHTVA', 'totalTVAC', 'offer', 'user', 'employee'));
        // Set file name
        $fileName =  $company['id'] . '_' . $offer['id'] . '_' . date('Y-m-d') . '.pdf';
        // Upload to server
        $pdf->save(public_path("storage/offers/" . $fileName));

        Offer::where('id', $offer['id'])->update([
            'file_name' => 'offers/' . $fileName
        ]);

        // Return response
        return response()->json([
            'success'   => true,
            'url'       => "/storage/offers/" . $fileName
        ]);
    }
}
