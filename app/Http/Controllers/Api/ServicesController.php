<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Service_entretien;

class ServicesController extends Controller
{
    public function index()
    {
        return response()->json(Service_entretien::all());
    }

    public function getCategories()
    {
        $services = Service_entretien::all()->toArray();

        $categories = array_map(function ($item) {
            return $item['category'];
        }, $services);

        return response()->json(array_unique($categories));
    }

    public function getServicesByCategory($category)
    {
        return response()->json(Service_entretien::where('category', $category)->get());
    }

    public function getServiceById($id)
    {
        return response()->json(Service_entretien::find($id));
    }
}
