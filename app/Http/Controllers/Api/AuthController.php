<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData,[
            'name' => 'required|max:55',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422);
        }

        $requestData['password'] = Hash::make($requestData['password']);

        $user = User::create($requestData);

        return response([ 'status' => true, 'message' => 'User successfully register.' ], 200);
    }

    public function login(Request $request)
    {
        $requestData = $request->all();
        $validator = Validator::make($requestData,[
            'email'     => 'email|required',
            'password'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 422);
        }

        if(! auth()->attempt($requestData)){
            return response()->json(['error' => 'UnAuthorised Access'], 401);
        }

        if(DB::table('oauth_access_tokens')->where('user_id', auth()->user()->id)->exists()){
            DB::table('oauth_access_tokens')->where('user_id', auth()->user()->id)->delete();
        }
        
        $access_token = $request->user()->createToken('authToken')->accessToken; 

        // Add one connection to the user and update the last connection date
        $user = User::find(auth()->user()->id);
        $user->nb_connection = $user->nb_connection + 1;
        $user->last_connection = date('Y-m-d H:i:s');
        $user->save();  

        return response(['user' => auth()->user(), 'access_token' => $access_token], 200);
    }

    public function me(Request $request)
    {
        $user = $request->user();

        return response()->json(['user' => $user], 200);
    }

    public function logout (Request $request)
    {
        DB::table('oauth_access_tokens')
            ->where('user_id', $request->user()->id)
            ->update(['revoked' => 1]);
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }
}
