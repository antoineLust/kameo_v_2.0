<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Bill;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BillsController extends Controller
{
    // Get all bills
    public function index()
    {
        return response()->json(Bill::with('company', 'user')->orderByDesc('bills.id')->get());
    }

    // Get bill by id
    public function show($id)
    {
        return response()->json(Bill::with('company', 'user')->find($id));
    }

    // Create bill
    public function create(Request $request)
    {

        // Define request parameter
        $request->form = json_decode($request->form, true);

        // File validation
        $request->validate([
            'file'                            => 'required|mimes:pdf|max:2048',
        ]);

        // Form validation
        Validator::make($request->form, [
            'companies_id'               => 'required',
            'beneficiary_company'        => 'required|string',
            'date'                       => 'required|date_format:Y-m-d',
            'amount_htva'                => 'required|numeric',
            'amount_tvac'                => 'required|numeric',
            'invoice_sent_date'          => 'required_if:invoice_sent, 1|date_format:Y-m-d',
            'invoice_paid_date'          => 'required_if:invoice_paid, 1|date_format:Y-m-d',
            'invoice_limit_paid_date'    => 'required|date_format:Y-m-d',
            'communication'              => 'string'
        ])->validate();

        // Insert function in model
        $id = Bill::insertGetId([
            'beneficiary_company'                      => $request->form['beneficiary_company'],
            'date'                                     => $request->form['date'],
            'amount_htva'                              => $request->form['amount_htva'],
            'amount_tvac'                              => $request->form['amount_tvac'],
            'billing_group'                            => 1,
            'invoice_sent'                             => (array_key_exists('invoice_sent', $request->form)) ? $request->form['invoice_sent'] : 0,
            'invoice_paid'                             => (array_key_exists('invoice_paid', $request->form)) ? $request->form['invoice_paid'] : 0,
            'invoice_sent_date'                        => (array_key_exists('invoice_sent', $request->form) && $request->form['invoice_sent'] == 1) ? $request->form['invoice_sent_date'] : null,
            'invoice_paid_date'                        => (array_key_exists('invoice_paid', $request->form) && $request->form['invoice_paid'] == 1) ? $request->form['invoice_paid_date'] : null,
            'invoice_limit_paid_date'                  => $request->form['invoice_limit_paid_date'],
            'communication'                            => $request->form['communication'],
            'file_name'                                => 'no-file',
            'companies_id'                             => $request->form['companies_id'],
        ]);

        // Upload file if file is send
        if ($request->file) {
            $path = $request->file('file')->storeAs('invoices', $id . '_' . date('d-m-Y') . '_' . $request->form['beneficiary_company'] . '.pdf', 'public');
            Bill::where('id', $id)->update([
                'file_name' => $path
            ]);
        }

        // Return response
        return response()->json(Bill::with('company', 'user')->find($id));
    }

    // Update bill
    public function update(Request $request)
    {

        // Define request parameter
        $request->form = json_decode($request->form, true);

        // dd($request->form);

        // File validation
        if ($request->file) {
            // File validation
            $request->validate([
                'file' => 'required|mimes:pdf|max:2048',
            ]);

            // Upload file
            $path = $request->file('file')->storeAs('invoices', $request->form['id'] . '_' . time() . '_' . $request->form['beneficiary_company'] . '.pdf', 'public');
            Bill::where('id', $request->form['id'])->update([
                'file_name' => $path
            ]);
        }

        // Form validation
        Validator::make($request->form, [
            'date'                       => 'required|date_format:Y-m-d',
            'amount_htva'                => 'required|numeric',
            'amount_tvac'                => 'required|numeric',
            'invoice_sent_date'          => 'required_if:invoice_sent, 1',
            'invoice_paid_date'          => 'required_if:invoice_paid, 1',
            'invoice_limit_paid_date'    => 'required|date_format:Y-m-d',
            'communication'              => 'string'
        ])->validate();

        // Insert function in model
        Bill::where('id', $request->form['id'])
            ->update([
                'date'                                     => $request->form['date'],
                'amount_htva'                              => $request->form['amount_htva'],
                'amount_tvac'                              => $request->form['amount_tvac'],
                'billing_group'                            => 1,
                'invoice_sent'                             => $request->form['invoice_sent'],
                'invoice_paid'                             => $request->form['invoice_paid'],
                'invoice_sent_date'                        => ($request->form['invoice_sent'] == 1) ? $request->form['invoice_sent_date'] : null,
                'invoice_paid_date'                        => ($request->form['invoice_paid'] == 1) ? $request->form['invoice_paid_date'] : null,
                'invoice_limit_paid_date'                  => $request->form['invoice_limit_paid_date'],
                'communication'                            => $request->form['communication'],
                'file_name'                                => ($request->file) ? $path : $request->form['file_name'],
                'companies_id'                             => $request->form['companies_id'],
            ]);

        // Return response
        return response()->json(Bill::with('company')->orderByDesc('bills.id')->find($request->form['id']));
    }


    // Destroy bill
    public function destroy(Request $request)
    {
        // Retrieve bill
        $bill = Bill::find($request->billId);
        // Verify if bill exist
        if (Storage::disk('public')->exists('invoices/' . $bill->file_name)) {
            // Delete file
            if ($bill->file_name != 'example.pdf') {
                Storage::disk('public')->delete($bill->file_name);
            }
        }

        // Delete bill
        Bill::where('id', $bill->id)
            ->delete();
        // Return response
        return response()->json(Bill::with('company', 'user')->orderByDesc('bills.id')->get());
    }

    // Get datatable info
    public function getDataForDataTable(Request $request)
    {
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        $sortDirection = json_decode($request->sortDirection, true);
        if ($search) {
            $bills = Bill::join('companies', 'companies.id', '=', 'bills.companies_id')
                ->select('companies.name as client', Bill::raw('(CASE WHEN bills.amount_tvac > 0 THEN "IN" ELSE "OUT" END) AS type'), 'bills.id', 'bills.date', 'bills.amount_tvac', 'bills.invoice_limit_paid_date', 'bills.communication', 'bills.invoice_sent', 'bills.invoice_paid', 'bills.invoice_limit_paid_date', 'bills.accounting', 'bills.file_name')
                ->where('companies.name', 'like', '%' . $search . '%')
                ->orWhere('bills.id', 'like', '%' . $search . '%')
                ->orWhere('bills.date', 'like', '%' . $search . '%')
                ->orWhere('bills.amount_tvac', 'like', '%' . $search . '%')
                ->orWhere('bills.invoice_limit_paid_date', 'like', '%' . $search . '%')
                ->orWhere('bills.communication', 'like', '%' . $search . '%')
                ->orderBy($sortDirection['column'], $sortDirection['direction'])
                ->limit($limit)
                ->offset(($page - 1) * $limit)
                ->get();
        } else {
            $bills = Bill::join('companies', 'companies.id', '=', 'bills.companies_id')
                ->select('companies.name as client', Bill::raw('(CASE WHEN bills.amount_tvac > 0 THEN "IN" ELSE "OUT" END) AS type'),  'bills.id', 'bills.date', 'bills.amount_tvac', 'bills.invoice_limit_paid_date', 'bills.invoice_sent', 'bills.invoice_paid', 'bills.invoice_limit_paid_date', 'bills.accounting', 'bills.file_name')
                ->orderBy($sortDirection['column'], $sortDirection['direction'])
                ->limit($limit)
                ->offset(($page - 1) * $limit)
                ->get();
        }
        return response()->json($bills);
    }
}
