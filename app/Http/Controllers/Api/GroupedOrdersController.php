<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Grouped_order;
use App\Http\Controllers\Controller;
use App\Models\Accessories_catalog;
use App\Models\Accessory_order;
use App\Models\Bike_order;
use App\Models\Bikes_Catalog;
use App\Models\Boxe_order;
use App\Models\Company;
use App\Models\Calendar;
use App\Models\Condition;
use App\Models\User;
use Error;
use Illuminate\Support\Facades\Auth;
use MailchimpTransactional\ApiClient;
use Illuminate\Support\Facades\DB;

class GroupedOrdersController extends Controller
{

    // Get all grouped orders
    public function index()
    {
        return response()->json(Grouped_order::with('bikes', 'company', 'accessories', 'boxes', 'user')->orderByDesc('grouped_orders.id')->get());
    }

    // Grouped order by id
    public function show($id)
    {
        return response()->json(Grouped_order::with('bikes', 'company', 'accessories', 'boxes', 'user')->find($id));
    }

    public function verifyOrder(Request $request)
    {

        $user_order = Grouped_order::with('bikes', 'company', 'accessories', 'boxes', 'user')->where('users_id', Auth::user()->id)->first();
        $company_orders = Grouped_order::with('bikes', 'company', 'accessories', 'boxes', 'user')->where('companies_id', Auth::user()->companies_id)->get();
        $hasOrder = isset(array($user_order)[0]);
        $allItemsDone = null;
        $allItemsStatus = null;
        $status = [];
        $totalLeasing = 0;
        $totalSelling = 0;

        if ($hasOrder) {

            $statusMap = function ($item) {
                return $item['status'];
            };

            $amountSellingMap = function ($item) {
                if($item['type'] == "selling"){
                    return intval($item['amount']);
                }
                return 0;
            };

            $amountLeasingAccessoriesMap = function ($item) {
                if($item['type'] == "leasing"){
                    return intval($item['amount']);
                }
                return 0;
            };

            $amountLeasingBikesMap = function ($item)  {             
                if($item['type'] == "leasing" || $item['type'] == "annual_leasing" || $item['type'] == "full_leasing"){
                    return intval($item['amount']);
                }
                return 0;
            };

            $bikes = $user_order->bikes->toArray();
            $accessories = $user_order->accessories->toArray();
            $boxes = $user_order->boxes->toArray();

            $totalLeasing += array_sum(array_map($amountLeasingBikesMap, $bikes));
            $totalLeasing += array_sum(array_map($amountLeasingAccessoriesMap, $accessories));

            $totalSelling += array_sum(array_map($amountSellingMap, $bikes));
            $totalSelling += array_sum(array_map($amountSellingMap, $accessories));

            $status = [...array_map($statusMap, $bikes), ...array_map($statusMap, $accessories), ...array_map($statusMap, $boxes)];

            if (in_array("new", $status) || in_array("confirmed", $status)) {
                $allItemsDone = false;
            } else if (in_array("done", $status)) {
                $allItemsDone = true;
            }
        } else {
            $allItemsDone = true;
        }

        if (in_array("new", $status)) {
            $allItemsStatus =  "new";
        } else if (in_array("confirmed", $status)) {
            $allItemsStatus =  "confirmed";
        } else if (in_array("done", $status)) {
            $allItemsStatus =  "done";
        }

        return response()->json([
            'userOrder'         => $user_order,
            'companyOrders'     => $company_orders,
            'hasOrder'          => $hasOrder,
            'allItemsDone'      => $allItemsDone,
            'allItemsStatus'    => $allItemsStatus,
            'leasingPrice'      => number_format($totalLeasing, 2, '.', ""),
            'sellingPrice'      => number_format($totalSelling, 2, '.', "")
        ]);
    }

    // Create grouped order
    public function create(Request $request)
    {
        // Validations criteria
        $request->validate([
            'companies_id'              => 'required|integer',
            'users_id'                  => 'integer',
        ]);

        $id = Grouped_order::insertGetId([
            'companies_id'                      => $request->companies_id,
            'users_id'                          => $request->users_id,
            "updated_at"                        => date('Y-m-d H:i:s'),
            "created_at"                        => date('Y-m-d H:i:s')
        ]);
        // Return response
        return response()->json(Grouped_order::with('bikes', 'company', 'accessories', 'boxes', 'user')->find($id));
    }

    // Destroy grouped order
    public function destroy(Request $request)
    {
        Grouped_order::where('id', $request->orderId)->delete();

        Calendar::where('item_id', $request->orderId)->where('item_type', 'Order to prepare')->delete();

        return response()->json(Grouped_order::find($request->orderId));
    }

    // Create order from cafetaria
    public function createFromCafetaria(Request $request)
    {
        // Validations criteria
        $request->validate([
            'user'                  => 'required|integer',
            'catalog_id'            => 'required|integer',
            'bikeSize'              => 'required|string',
            'bikeContractType'      => 'required|string',
            'comment'               => 'nullable|string',
            'accessories'           => 'nullable|array',
        ]);

        // Define data
        $user               = User::find($request->user);
        $bikePrice          = Bikes_Catalog::find($request->catalog_id)->price_htva;
        $sellingDiscount    = Condition::where('companies_id', strval($user['companies_id']))->first()->cafetaria_discount_selling;
        $leasingDiscount    = Condition::where('companies_id', strval($user['companies_id']))->first()->cafetaria_discount_leasing;

        // Define bike price
        if ($request->bikeContractType == 'leasing') {
            $bikePrice = round((intval($bikePrice) * (1 - 0.27) * (1 + 0.7) + (3 * 84 + 4 * 100) * (1 + 0.3)) / 36, 2);
            if ($leasingDiscount !== null && $leasingDiscount !== '0') {
                $bikePrice = round($bikePrice - (($bikePrice / 100) * intval($leasingDiscount)), 2);
            }
        } else if ($request->bikeContractType == 'annual_leasing') {
            $bikePrice = round(((intval($bikePrice) * (1 - 0.27) * (1 + 0.7) + (3 * 84 + 4 * 100) * (1 + 0.3)) / 36) * 12, 2);
            if ($leasingDiscount !== null && $leasingDiscount !== '0') {
                $bikePrice = round($bikePrice - (($bikePrice / 100) * intval($leasingDiscount)), 2);
            }
        } else if ($request->bikeContractType === 'pre_financed_leasing') {
            $bikePrice = round((intval($bikePrice) + (3 * 84 + (700 - 400) / (5000 - 2000) * (intval($bikePrice)) + 33.3333) * (1 + 0.3)), 2);
            if ($leasingDiscount !== null && $leasingDiscount !== '0') {
                $bikePrice = round($bikePrice - (($bikePrice / 100) * intval($leasingDiscount)), 2);
            }
        }

        // Define accessories price
        if (count($request->accessories) > 0) {
            $accessories = $request->accessories;
            foreach ($accessories as $key => $accessory) {

                if ($accessory['type'] === 'free') {
                    $accessories[$key]['price'] = 0;
                } else if ($accessory['type'] === 'selling') {
                    $accessories[$key]['price'] = Accessories_catalog::find($accessory['id'])->price_htva;
                    if ($sellingDiscount !== null && $sellingDiscount !== '0') {
                        $accessories[$key]['price'] = round($accessories[$key]['price'] - (($accessories[$key]['price'] / 100) * intval($sellingDiscount)), 2);
                    }
                } else if ($accessory['type'] === 'leasing') {
                    $accessories[$key]['price'] = round((intval(Accessories_catalog::find($accessory['id'])->price_htva) * (1 - 0.27) * (1 + 0.7) + (3 * 84 + 4 * 100) * (1 + 0.3)) / 36, 2);
                    if ($leasingDiscount !== null && $leasingDiscount !== '0') {
                        $accessories[$key]['price'] = round($accessories[$key]['price'] - (($accessories[$key]['price'] / 100) * intval($leasingDiscount)), 2);
                    }
                }
            }
        }

        // Create grouped order
        $groupedOrderId = Grouped_order::insertGetId([
            'companies_id'                      => $user['companies_id'],
            'users_id'                          => $request->user,
            "updated_at"                        => date('Y-m-d H:i:s'),
            "created_at"                        => date('Y-m-d H:i:s')
        ]);

        // Create bike order
        $bikeOrderId = Bike_order::insertGetId([
            'size'                              => $request->bikeSize,
            'type'                              => $request->bikeContractType,
            'comment'                           => $request->comment,
            'grouped_orders_id'                 => $groupedOrderId,
            'bikes_catalog_id'                  => $request->catalog_id,
            'status'                            => 'new',
            'amount'                            => $bikePrice,
        ]);

        $accessories_order = [];

        // Create accessories order
        if (count($request->accessories) > 0) {
            foreach ($accessories as $accessory) {
                $order_id = Accessory_order::insertGetId([
                    'accessories_catalog_id'       => $accessory['id'],
                    'amount'                       => $accessory['price'],
                    'grouped_orders_id'            => $groupedOrderId,
                    'status'                       => 'new',
                    'type'                         => ($accessory['type'] === 'free') ? 'selling' : $accessory['type'],
                    'size'                         => $accessory['size'],
                ]);
                array_push($accessories_order, Accessory_order::find($order_id));
            }
        }

        $fleetManager = User::where('companies_id', $user['companies_id'])->where('permission', 'fleetManager')->get();

        // Send email
        if (config('app.env') == 'local') {
            function sendMail($message)
            {
                try {
                    $mailchimp = new ApiClient();
                    $mailchimp->setApiKey(config('services.mailchimp.key'));

                    $mailchimp->messages->send(["message" => $message]);
                } catch (Error $e) {
                    echo 'Error: ', $e->getMessage(), "\n";
                }
            }

            $message = [
                "from_name"     => "KAMEO Bikes",
                "from_email"    => "info@kameobikes.com",
                "subject"       => "Une nouvelle commande a été passée par " . $user['firstname'] . " " . $user['lastname'] . " !",
                "html"          => view('emails.cafetariaOrderNotification', [
                    'user'      => $user,
                    'company'   => Company::find($user['companies_id'])->name
                ])->render(),
                "to" => [
                    [
                        "type"  => "to",
                        "email" => "benjamin@kameobikes.com",
                        "name"  => "Benjamin Van Rentegrhem"
                    ]
                ]
            ];
            sendMail($message);
        } else if (config('app.env') == 'production') {
            function sendMail($message)
            {
                try {
                    $mailchimp = new ApiClient();
                    $mailchimp->setApiKey(config('services.mailchimp.key'));

                    $mailchimp->messages->send(["message" => $message]);
                } catch (Error $e) {
                    echo 'Error: ', $e->getMessage(), "\n";
                }
            }

            foreach ($fleetManager as $key => $manager) {
                if ($key === 0) {
                    $message = [
                        "from_name"      => "KAMEO Bikes",
                        "from_email"    => "info@kameobikes.com",
                        "subject"       => "Une nouvelle commande a été passée par " . $user['firstname'] . " " . $user['lastname'] . " !",
                        "html"          => view('emails.cafetariaOrderNotification', [
                            'user'      => $user,
                            'company'   => Company::find($user['companies_id'])->name
                        ])->render(),
                        "to" => [
                            [
                                "type"  => "to",
                                "email" => $manager->email,
                                "name"  => $manager->firstname . " " . $manager->lastname
                            ],
                            [
                                "type"  => "cc",
                                "email" => "antoine@kameobikes.com",
                                "name"  => "Antoine Lust"
                            ],
                            [
                                "type"  => "cc",
                                "email" => "julien@kameobikes.com",
                                "name"  => "Julien Jamard"
                            ],
                            [
                                "type"  => "cc",
                                "email" => "simon@kameobikes.com",
                                "name"  => "Simon Spinneux"
                            ],
                        ]
                    ];
                    sendMail($message);
                } else {
                    $message = [
                        "from_name"      => "KAMEO Bikes",
                        "from_email"    => "info@kameobikes.com",
                        "subject"       => "Une nouvelle commande a été passée par " . $user['firstname'] . " " . $user['lastname'] . " !",
                        "html"          => view('emails.cafetariaOrderNotification', [
                            'user'      => $user,
                            'company'   => Company::find($user['companies_id'])->name
                        ])->render(),
                        "to" => [
                            [
                                "type"  => "to",
                                "email" => $manager->email,
                                "name"  => $manager->firstname . " " . $manager->lastname
                            ],
                        ]
                    ];
                    sendMail($message);
                }
            }
        }

        // Return response
        return response()->json([
            'accessories_order' => $accessories_order,
            'bike_order'        => Bike_order::find($bikeOrderId)
        ]);
    }

    // Confirm order
    public function confirmOrder(Request $request)
    {
        Bike_order::where('grouped_orders_id', $request->orderId)->update([
            'status' => 'confirmed'
        ]);
        Accessory_order::where('grouped_orders_id', $request->orderId)->update([
            'status' => 'confirmed'
        ]);
        // Return response
        return response()->json([
            'accessories_order' => Accessory_order::where('grouped_orders_id', $request->orderId)->get()->toArray(),
            'bike_order'        => Bike_order::where('grouped_orders_id', $request->orderId)->get()->toArray()
        ]);
    }

    // Get datatable info
    public function getDataForDataTable(Request $request)
    {
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        $sortDirection = json_decode($request->sortDirection, true);
        if ($search) {
            $q = explode(' ', $search);
            for ($i = 0; $i < count($q); $i++) {
                $orders = DB::select('SELECT @ID := grouped_orders.id as id , companies.name as client, CONCAT(users.firstname, " ", users.lastname) as user,
                    (SELECT COUNT(*) FROM bike_orders WHERE grouped_orders_id = @ID AND status = "done") as bikes_done,
                    (SELECT COUNT(*) FROM bike_orders WHERE grouped_orders_id = @ID AND status <> "done") as bikes_not_done,
                    (SELECT COUNT(*) FROM accessory_orders WHERE grouped_orders_id = @ID AND status = "done") as accessories_done,
                    (SELECT COUNT(*) FROM accessory_orders WHERE grouped_orders_id = @ID AND status <> "done") as accessories_not_done,
                    (SELECT COUNT(*) FROM boxe_orders WHERE grouped_orders_id = @ID AND status = "done") as boxes_done,
                    (SELECT COUNT(*) FROM boxe_orders WHERE grouped_orders_id = @ID AND status <> "done") as boxes_not_done
                    FROM `grouped_orders`
                    INNER JOIN companies ON companies.id = grouped_orders.companies_id
                    INNER JOIN users ON users.id = grouped_orders.users_id
                    WHERE companies.name LIKE "%' . $q[$i] . '%"
                    OR users.firstname LIKE "%' . $q[$i] . '%"
                    OR users.lastname LIKE "%' . $q[$i] . '%"
                    OR grouped_orders.id LIKE "%' . $q[$i] . '%"
                    GROUP BY grouped_orders.id ORDER BY ' . $sortDirection['column'] . ' ' . $sortDirection['direction'] . ' LIMIT ' . $limit . ' OFFSET ' . ($page - 1) * $limit . '');
            }
        } else {
            $orders = DB::select('SELECT @ID := grouped_orders.id as id , companies.name as client, CONCAT(users.firstname, " ", users.lastname) as user,
                (SELECT COUNT(*) FROM bike_orders WHERE grouped_orders_id = @ID AND status = "done") as bikes_done,
                (SELECT COUNT(*) FROM bike_orders WHERE grouped_orders_id = @ID AND status <> "done") as bikes_not_done,
                (SELECT COUNT(*) FROM accessory_orders WHERE grouped_orders_id = @ID AND status = "done") as accessories_done,
                (SELECT COUNT(*) FROM accessory_orders WHERE grouped_orders_id = @ID AND status <> "done") as accessories_not_done,
                (SELECT COUNT(*) FROM boxe_orders WHERE grouped_orders_id = @ID AND status = "done") as boxes_done,
                (SELECT COUNT(*) FROM boxe_orders WHERE grouped_orders_id = @ID AND status <> "done") as boxes_not_done
                FROM `grouped_orders`
                INNER JOIN companies ON companies.id = grouped_orders.companies_id
                INNER JOIN users ON users.id = grouped_orders.users_id
                GROUP BY grouped_orders.id ORDER BY ' . $sortDirection['column'] . ' ' . $sortDirection['direction'] . ' LIMIT ' . $limit . ' OFFSET ' . ($page - 1) * $limit . '');
        }
        return response()->json($orders);
    }
}
