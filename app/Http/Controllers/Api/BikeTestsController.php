<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Bike_test;
use App\Models\Calendar;
use Illuminate\Http\Request;

class BikeTestsController extends Controller
{
    public function index(){
        return response()->json(Bike_test::with('bike')->orderByDesc('id')->get());
    }

    // Get one bike test
    public function show($id){
        return response()->json(Bike_test::find($id));
    }


    public function create(Request $request){
        // Validations criteria
        $validated = $request->validate([
            'date'                      => 'required|date|date_format:Y-m-d',
            'bikes_id'                  => 'required|integer',
            'status'                    => 'required|string'
        ]);
        
        $id = Bike_test::insertGetId([
            'date'                      => $request->date,
            'bikes_id'                  => $request->bikes_id,
            'status'                    => $request->status,
        ]);

        return response()->json(Bike_test::with('bike')->where('id', $id)->get()[0]);
    }

    public function update(Request $request, $id){
        // Validations criteria
        $validated = $request->validate([
            'date'                      => 'required|date|date_format:Y-m-d',
            'start_time'                => 'required|string',
            'end_time'                  => 'required|string',
            'bikes_id'                  => 'required|integer',
            'status'                    => 'required|string',
            'comment'                   => 'required_if:status,done'
        ]);
        Bike_test::where('id', $id)->update([
            'date'                      => $request->date,
            'start_time'                => $request->start_time,
            'end_time'                  => $request->end_time,
            'bikes_id'                  => $request->bikes_id,
            'status'                    => $request->status,
            'comment'                   => $request->comment
        ]);

        Calendar::where('item_id', '=', $id)
            ->where('item_type', '=', 'Test')
            ->update([
                'status'    => $request->status == 'done' ? $request->status : 'todo',
                'comment'   => $request->comment
            ]);

        return response()->json(Bike_test::with('bike')->where('id', $id)->get()[0]);      
    }



}
