<?php

namespace App\Http\Controllers\Api;

use App\Models\Boxe_order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BoxOrdersController extends Controller
{

    // Get all boxes orders
    public function index(){
        return response()->json(Boxe_order::with('catalog')->get());
    }

    // Get one box order by id
    public function show($id){
        return response()->json(Boxe_order::with('catalog')->find($id)); 
    }

    public function getBoxesFromGroupedOrder($id){
        return response()->json(Boxe_order::with('catalog')->where('grouped_orders_id', $id)->get());
    }

    // Create a boxe order
    public function create(Request $request){
        // Validations criteria
        $request->validate([
            'status'                    => 'required|string',
            'installation_price'        => 'required|numeric',
            'location_price'            => 'required|numeric',
            'estimated_delivery_date'   => 'required|date|date_format:Y-m-d',
            'boxes_catalog_id'          => 'required|integer',
            'grouped_orders_id'         => 'required|integer'
        ]);
        // Create new boxe order
        $id = Boxe_order::insertGetId([
            'status'                    => $request->status,
            'installation_price'        => $request->installation_price,
            'amount'                    => $request->location_price,
            'estimated_delivery_date'   => $request->estimated_delivery_date,
            'boxes_catalog_id'          => $request->boxes_catalog_id,
            'grouped_orders_id'         => $request->grouped_orders_id
        ]);
        // Return response
        return response()->json(Boxe_order::with('catalog')->find($id));
    }



    // Update box Order
    public function update(Request $request){
        // Validations criteria
        $request->validate([
            'status'                    => 'required|string',
            'installation_price'        => 'required|numeric',
            'amount'                    => 'required|numeric',
            'estimated_delivery_date'   => 'required|date|date_format:Y-m-d',
            'boxes_catalog_id'          => 'required|integer',
            'grouped_orders_id'         => 'required|integer'
        ]);
        // Update box order
        Boxe_order::where('id', $request->id)
            ->update([
                'status'                    => $request->status,
                'installation_price'        => $request->installation_price,
                'amount'                    => $request->amount,
                'estimated_delivery_date'   => $request->estimated_delivery_date,
                'boxes_catalog_id'          => $request->boxes_catalog_id,
                'grouped_orders_id'         => $request->grouped_orders_id
            ]);
        // Return response
        return response()->json(Boxe_order::with('catalog')->find($request->id));
    }

    // Delete box order
    public function destroy(Request $request){
        Boxe_order::destroy($request->boxOrderId);
    }

}
