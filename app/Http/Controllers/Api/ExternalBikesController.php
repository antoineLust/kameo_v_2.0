<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\External_bike;
use Illuminate\Http\Request;

class ExternalBikesController extends Controller
{
    // Get all external bikes
    public function index()
    {
        return response()->json(External_bike::with('company')->get());
    }

    public function getExternalBikesByCompanyId($id)
    {
        return response()->json(External_bike::with('company')->where('companies_id', $id)->get());
    }

    public function getExternalBikeById($id)
    {
        return response()->json(External_bike::with('company')->find($id));
    }

    // Create a new external bike
    public function create(Request $request)
    {
        // Validate the request
        $this->validate($request, [
            'brand'         => 'required|string|max:55',
            'model'         => 'nullable|string|max:55',
            'color'         => 'required|string|max:55',
            'frame_number'  => 'nullable|string|max:55',
            'companies_id'  => 'required|integer',
        ]);

        // Create a new external bike
        $id = External_bike::insertGetId([
            'brand'             => $request->brand,
            'model'             => ($request->model !== null) ? $request->model : 'Inconnu',
            'color'             => $request->color,
            'frame_reference'   => $request->frame_number,
            'companies_id'      => $request->companies_id
        ]);

        // Return the created external bike
        return response()->json(External_bike::find($id));
    }
}
