<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use function App\Helpers\leasingPrice;

class Cash4BikeController extends Controller
{
    public function calculateCash4Bike(Request $request){
        $response=array();
        $type=$request->type;
        $revenu=$request->gross_salary_value;            
        $bike_value=$request->bike_amount_value/1.21;
        $distance_home_work=$request->distance_home_work;
        $home_work_prime=$request->home_work_prime;
        $frequency=$request->frequency;
        $type="employe";

        if($type=='ouvrier'){
            $impactOnGrossSalary=(leasingPrice($bike_value)*1.21*12/17.58);
        }else if($type=="employe"){
            $impactOnGrossSalary=(leasingPrice($bike_value)*1.21*12/18.08);
        }
        $socialCotisation=$impactOnGrossSalary*0.1307;
        $basisForTaxes=$impactOnGrossSalary-$socialCotisation;
        if($revenu<636.49){
            $taxRate=0;
        }
        else if($revenu>=636.49 && $revenu < 951.87){
            $taxeRate=(($revenu-636.49)*0.25)/$revenu;
        }else if($revenu >= 951.87 && $revenu < 1680.32){
            $taxRate=(($revenu-951.87)*0.4)/$revenu;
        }else if($revenu >= 1680.32 && $revenu < 2908.05){
            $taxRate=(($revenu-1949.17)*0.45 + (1680.32-951.87)*0.4)/$revenu;
        }else if($revenu >= 2908.05 ){
            $taxRate=(($revenu-2908.05)*0.5 + (2908.05-1680.32)*0.44 + (1680.32-951.87)*0.4+ (951.87-636.49)*0.4)/$revenu;
        }
  
        $taxes=$basisForTaxes*$taxRate;
  
        $impactOnNetSalary=($basisForTaxes-$taxes);
        $impactBikeAllowance=($home_work_prime*$frequency*2*$distance_home_work*4);
  
        $response=array();
        $response['leasingPrice']=round(leasingPrice($bike_value*1.21), 2);
        $response['impactOnGrossSalary']=round($impactOnGrossSalary, 2);
        $response['impactOnNetSalary']=round($impactOnNetSalary, 2);
        $response['impactBikeAllowance']=round($impactBikeAllowance, 2);
        return response()->json($response);
    }
}
