<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accessory_category;

class AccessoriesCategoriesController extends Controller
{
    // Get all accessories categories
    public function index(){
        return response()->json(Accessory_category::all()); 
    }
}
