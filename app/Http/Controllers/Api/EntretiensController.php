<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accessory;
use App\Models\Bike;
use App\Models\Bill;
use App\Models\Calendar;
use App\Models\Company;
use App\Models\Company_contact;
use App\Models\Entretien;
use App\Models\Entretien_detail;
use App\Models\External_bike;
use Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use MailchimpTransactional\ApiClient;

use function App\Helpers\createReferenceNumber;

class EntretiensController extends Controller
{
    // Get all entretiens
    public function index()
    {
        return response()->json([
            'entretiens'            => Entretien::all(),
            'entretiens_details'    => Entretien_detail::all()
        ]);
    }

    // Get one entretien
    public function show($id)
    {
        return response()->json([
            'entretien'         => Entretien::find($id),
            'entretien_details' => Entretien_detail::where('entretiens_id', $id)->get()
        ]);
    }

    public function getEntretiensByBikeId($id)
    {
        return response()->json(Entretien::where('bikes_id', $id)->get());
    }

    // Create a new entretien
    public function create(Request $request)
    {
        // Validate the request
        Validator::make($request->all(), [
            'company_id'                => 'required|integer',
            'bike_id'                   => 'required|integer',
            'external_bike'             => 'required|integer|max:1',
            'planned_date'              => 'required',
            'date'                      => 'required',
            'status'                    => 'required|string',
            'description'               => 'nullable|string',
            'confidential_description'  => 'nullable|string',
        ])->validate();
        // Create the entretien
        $id = Entretien::insertGetId([
            'bikes_id'                  => $request->bike_id,
            'external_bike'             => $request->external_bike,
            'planned_date'              => ($request->planned_date === 'skip') ? null : $request->planned_date,
            'date'                      => ($request->date === 'skip') ? null : $request->date,
            'status'                    => $request->status,
            'description'               => $request->description,
            'number_entretien'          => (Entretien::where('bikes_id', $request->bikes_id)->count()) + 1,
            'assembled'                 => 1,
            'created_at'                => now(),
            'updated_at'                => now(),
        ]);

        Bike::where('id', $request->bikes_id)->update([
            'comment' => $request->confidential_description,
        ]);

        // Return the created entretien
        return response()->json(Entretien::find($id));
    }

    // Update an entretien
    public function update(Request $request, $id)
    {
        // Validate the request
        Validator::make($request->all(), [
            'status'                    => 'required|string',
            'address'                   => 'nullable|string',
            'external_bike'             => 'required|integer|max:1',
            'planned_date'              => 'nullable',
            'date'                      => 'nullable',
            'planned_time'              => 'nullable|string',
            'client_warned'             => 'required|integer|max:1',
            'avoid_billing'             => 'required|integer|max:1',
            'description'               => 'nullable|string',
            'confidential_description'  => 'nullable|string',
        ])->validate();

        // Update the entretien
        Entretien::where('id', $id)->update([
            'status'                    => $request->status,
            'address'                   => $request->address,
            'planned_date'              => $request->planned_date,
            'date'                      => $request->date,
            'planned_time'              => $request->planned_time,
            'client_warned'             => $request->client_warned,
            'avoid_billing'             => $request->avoid_billing,
            'description'               => $request->description,
            'updated_at'                => now(),
        ]);

        Bike::where('id', $request->bikes_id)->update([
            'comment' => $request->confidential_description,
        ]);

        $CalendarEntretien = Calendar::where([['item_id', '=', $request->id], ['item_type', '=', 'Maintenance']]);

        if ($CalendarEntretien->first() != NULL) {
            $CalendarId = $CalendarEntretien->first()->id;

            if ($request->status !== 'waiting_pieces' && $request->status !== 'done') {
                $request->status = 'todo';
            }

            // Update the entretien
            $CalendarEntretien->update([
                'status'                => $request->status,
                'comment'               => $request->description,
                'confidential_comment'  => $request->confidential_description,
                'updated_at'            => now(),
            ]);
        }

        // Return the updated entretien
        return response()->json(Entretien::find($id));
    }

    // Update an entretien's status from the calendar modal
    public function updateStatusCalendar(Request $request, $id)
    {
        // Validate the request
        Validator::make($request->all(), [
            'status'    => 'required|string',
        ])->validate();

        // Update the entretien
        Entretien::where('id', $id)->update([
            'status'        => $request->status,
            'updated_at'    => now(),
        ]);

        // Return the updated entretien
        return response()->json(Entretien::find($id));
    }

    // Update an entretien's comments from the calendar modal
    public function updateCommentCalendar(Request $request, $id)
    {

        // Update the entretien
        Entretien::where('id', $id)->update([
            'description'               => $request->comment,
            'updated_at'                => now(),
        ]);

        $bikeId = Entretien::where('id', $id)->first()->bikes_id;

        Bike::where('id', $bikeId)->update([
            'comment' => $request->confidential_comment,
        ]);

        // Return the updated entretien
        return response()->json(Entretien::find($id));
    }

    // Get datatable info
    public function getDataForDataTable(Request $request)
    {
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        $sortDirection = json_decode($request->sortDirection, true);
        if ($search) {
            $q = explode(' ', $search);
            $res = Entretien::join('bikes', 'entretiens.bikes_id', '=', 'bikes.id')
                ->join('companies', 'bikes.companies_id', '=', 'companies.id')
                ->join('bikes_catalog', 'bikes.bikes_catalog_id', '=', 'bikes_catalog.id')
                ->select('entretiens.id', 'entretiens.status', 'entretiens.date', 'entretiens.planned_date', 'companies.name as client', 'bikes.id', 'bikes_catalog.brand', 'bikes_catalog.model', 'bikes.users_id');
            for ($i = 0; $i < count($q); $i++) {
                $res = $res->where(function ($query) use ($q, $i) {
                    $query->where('companies.name', 'like', '%' . $q[$i] . '%')
                        ->orWhere('bikes_catalog.brand', 'like', '%' . $q[$i] . '%')
                        ->orWhere('bikes_catalog.model', 'like', '%' . $q[$i] . '%')
                        ->orWhere('entretiens.status', 'like', '%' . $q[$i] . '%')
                        ->orWhere('entretiens.date', 'like', '%' . $q[$i] . '%')
                        ->orWhere('entretiens.planned_date', 'like', '%' . $q[$i] . '%')
                        ->orWhere('bikes.id', 'like', '%' . $q[$i] . '%')
                        ->orWhere('entretiens.id', 'like', '%' . $q[$i] . '%');
                });
            }
            switch ($sortDirection['column']) {
                case "id":
                    $sortDirection['column'] = "entretiens.id";
                    break;
                case "status":
                    $sortDirection['column'] = "entretiens.status";
                    break;
                case "date":
                    $sortDirection['column'] = "entretiens.date";
                    break;
                case "planned_date":
                    $sortDirection['column'] = "entretiens.planned_date";
                    break;
                case "client":
                    $sortDirection['column'] = "companies.name";
                    break;
                case "bike_id":
                    $sortDirection['column'] = "bikes.id";
                    break;
                case "type":
                    $sortDirection['column'] = "bikes_catalog.brand";
                    break;
                default:
                    $sortDirection['column'] = "entretiens.id";
                    break;
            }
            $entretiens = $res->orderBy($sortDirection['column'], $sortDirection['direction'])
                ->limit($limit)
                ->offset(($page - 1) * $limit)
                ->get();
        } else {
            $entretiens = Entretien::join('bikes', 'entretiens.bikes_id', '=', 'bikes.id')
                ->join('bikes_catalog', 'bikes.bikes_catalog_id', '=', 'bikes_catalog.id')
                ->join('external_bikes', 'entretiens.bikes_id', '=', 'external_bikes.id')
                ->join('companies', 'bikes.companies_id', '=', 'companies.id')
                ->select('entretiens.id', 'entretiens.status', 'entretiens.date', 'entretiens.planned_date', 'companies.name as client', 'entretiens.bikes_id', 'bikes_catalog.brand', 'bikes_catalog.model', 'bikes.users_id', 'entretiens.external_bike')
                ->orderBy($sortDirection['column'], $sortDirection['direction'])
                ->limit($limit)
                ->offset(($page - 1) * $limit)
                ->get();
        }
        foreach ($entretiens as $entretien) {
            if ($entretien->external_bike == 1) {
                $entretien['client'] = Company::where('id', External_bike::where('id', $entretien->bikes_id)->first()->companies_id)->first()->name;
                $entretien['bike'] = $entretien->bikes_id . ' - ' . $entretien->brand;
                unset($entretien->brand);
                unset($entretien->model);
                unset($entretien->bikes_id);
                $entretien['type'] = 'Vélo externe';
                unset($entretien->users_id);
            } else if ($entretien->external_bike == 0) {
                $entretien['bike'] = $entretien->bikes_id . ' - ' . $entretien->brand . ' ' . $entretien->model;
                unset($entretien->brand);
                unset($entretien->model);
                unset($entretien->bikes_id);
                $entretien['type'] = ($entretien->users_id == NULL) ? 'Partagé' : 'Personnel';
                unset($entretien->users_id);
            }
        }
        return response()->json($entretiens);
    }

    public function destroy(Request $request)
    {
        dd($request->all());
        Entretien::find($id)->delete();
        Entretien_detail::where('entretiens_id', $id)->delete();
    }

    public function generateInvoice(Request $request){
        if(count($request->entretien_accessories) > 0){
            for($i = 0; $i < count($request->entretien_accessories); $i++){
                if(Entretien_detail::where('id', $request->entretien_accessories[$i]['id'])->first()->accessory_stock_id === NULL){
                    dd('pasok');
                    $catalogId = $request->entretien_accessories[$i]['catalog']['id'];
                    $detailsId = $request->entretien_accessories[$i]['id'];
                    $entretienId = $request->entretien_id;
                    if(Accessory::where('id', $catalogId)->where('contract_type', 'stock')->where('size', $request->entretien_accessories[$i]['accessory_size'])->exists()){
                        $accessory = Accessory::where('id', $catalogId)->where('contrct_type', 'stock')->first();
                        $accessory->contract_type = 'selling';
                        $accessory->selling_price = $request->entretien_accessories[$i]['catalog']['price_htva'];
                        $accessory->delivery_date = date('Y-m-d');
                        $accessory->estimated_delivery_date = date('Y-m-d');
                        $accessory->selling_date = date('Y-m-d');
                        $accessory->companies_id = $request->company['id'];
                        $accessory->bikes_id = $request->bike_id;
                        $accessory->users_id = (Bike::where('id', $request->bike_id)->first()->users_id == NULL) ? NULL : Bike::where('id', $request->bike_id)->first()->users_id;
                        $accessory->updated_at = now();
                        $accessory->save();
                        $stockId = $accessory->id;
                        Entretien_detail::where('id', $detailsId)->update(['accessory_stock_id' => $stockId]);
                    }
                    else{
                        $stockId = Accessory::insertGetId([
                            'accessories_catalog_id' => $catalogId,
                            'contract_type' => 'selling',
                            'size' => $request->entretien_accessories[$i]['accessory_size'],
                            'selling_price' => $request->entretien_accessories[$i]['catalog']['price_htva'],
                            'delivery_date' => date('Y-m-d'),
                            'estimated_delivery_date' => date('Y-m-d'),
                            'companies_id' => $request->company['id'],
                            'bikes_id' => $request->bike_id,
                            'users_id' => (Bike::where('id', $request->bike_id)->first()->users_id == NULL) ? NULL : Bike::where('id', $request->bike_id)->first()->users_id,
                            'created_at' => now(),
                            'updated_at' => now()
                        ]);
                        Entretien_detail::where('id', $detailsId)->update(['accessory_stock_id' => $stockId]);
                    }
                }
            }
        }
        $totalHTVA = 0.00;
        $items = [];
        for($i = 0; $i < count($request->entretien_accessories); $i++){
            $totalHTVA += number_format($request->entretien_accessories[$i]['amount'], 2, '.', '');
            $items[] = [
                'description' => $request->entretien_accessories[$i]['catalog']['brand'] . ' ' . $request->entretien_accessories[$i]['catalog']['model'],
                'price' => $request->entretien_accessories[$i]['amount'],
                'type' => 'accessory'
            ];
        }
        for($i = 0; $i < count($request->entretien_services); $i++){
            $totalHTVA += number_format($request->entretien_services[$i]['amount'], 2, '.', '');
            $items[] = [
                'description' => $request->entretien_services[$i]['detail']['description'] . ($request->entretien_services[$i]['details'] !== NULL ? ' - ' . $request->entretien_services[$i]['details'] : ''),
                'price' => $request->entretien_services[$i]['amount'],
                'type' => 'service'
            ];
        }
        for($i = 0; $i < count($request->entretien_other); $i++){
            $totalHTVA += number_format($request->entretien_other[$i]['amount'], 2, '.', '');
            $items[] = [
                'description' => $request->entretien_other[$i]['description'],
                'price' => $request->entretien_other[$i]['amount'],
                'type' => 'other'
            ];
        }
        $company        = $request->company;
        $totalTVAC      = $totalHTVA * 0.21;
        $outID          = Bill::where('date', '>=', date('Y-01-01'))->max('out_id') + 1;
        $invoiceId      = Bill::max('id') + 1;
        $reference      = createReferenceNumber($invoiceId);
        $invoicePath    = $invoiceId . '_' . $outID . '_' . date('Y-m-d') . '.pdf';
        Bill::insertGetId([
            'id' => $invoiceId,
            'beneficiary_company' => $request->company['name'],
            'out_id' => $outID,
            'amount_htva' => $totalHTVA,
            'amount_tvac' => $totalTVAC,
            'date' => date('Y-m-d'),
            'billing_group' => $request->company['billing_group'],
            'communication' => $reference,
            'file_name' => $invoicePath,
            'invoice_paid' => $request->invoice_infos['payment_now'] == true ? 1 : 0,
            'invoice_paid_date' => $request->invoice_infos['payment_now'] == true ? $request->invoice_infos['payment_date'] : NULL,
            'payement' => $request->invoice_infos['payment_type'],
            'companies_id' => $request->company['id'],
            'type' => 'Entretien',
            'invoice_sent' => 1,
            'invoice_sent_date' => date('Y-m-d'),
            'accounting' => 0,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        // Initialize the DOMPDF class
        $pdf = App::make('dompdf.wrapper');
        // Retrieve the HTML generated in the view using Blade
        $pdf->loadView('pdf.invoice.model-2', compact('items', 'company', 'totalHTVA', 'totalTVAC', 'reference', 'outID', 'invoiceId'));
        // Upload to server
        $pdf->save(public_path("storage/invoices/" . $invoicePath));

        $contactsBilling = Company_contact::where('companies_id', $request->company['id'])->where('type', 'billing')->get();
        $contacts = Company_contact::where('companies_id', $request->company['id'])->where('type', 'contact')->get();

        if(count($contactsBilling) > 0){
            foreach($contactsBilling as $contactBilling){
                if($contactBilling->email !== NULL){
                    function sendMail($message){
                        try {
                            $mailchimp = new ApiClient();
                            $mailchimp->setApiKey(config('services.mailchimp.key'));
                    
                            $mailchimp->messages->send(["message" => $message]);
                        } catch (Error $e) {
                            echo 'Error: ', $e->getMessage(), "\n";
                        }
                    }
                    
                    $message = [
                        "from_name"     => "KAMEO Bikes",
                        "from_email"    => "info@kameobikes.com",
                        "subject"       => "Une nouvelle facture est disponible",
                        "html"          => view('emails.newInvoice', [
                                'contact'   => $contactBilling,
                                'company'   => Company::find($request->company['id'])->name
                            ])->render(),
                        "to" => [
                            [
                                "type"  => "to",
                                "email" => (config('app.env') === 'production') ? $contactBilling['email'] : "benjamin@kameobikes.com",
                                "name"  => ($contactBilling['firstname'] !== NULL && $contactBilling['lastname'] !== NULL) ? $contactBilling['firstname'] . ' ' . $contactBilling['lastname'] : ''
                            ]
                            ],
                            "attachments" => [
                                [
                                    "type" => "application/pdf",
                                    "name" => $invoicePath,
                                    "content" => base64_encode(file_get_contents(public_path("storage/invoices/" . $invoicePath)))
                                ]
                            ]
                    ];
                    sendMail($message);
                }
            }
        }
        else if(count($contacts) > 0){
            foreach($contacts as $contact){
                if($contact->email !== NULL){
                    function sendMail($message){
                        try {
                            $mailchimp = new ApiClient();
                            $mailchimp->setApiKey(config('services.mailchimp.key'));
                    
                            $mailchimp->messages->send(["message" => $message]);
                        } catch (Error $e) {
                            echo 'Error: ', $e->getMessage(), "\n";
                        }
                    }
                    
                    $message = [
                        "from_name"     => "KAMEO Bikes",
                        "from_email"    => "info@kameobikes.com",
                        "subject"       => "Une nouvelle facture est disponible",
                        "html"          => view('emails.newInvoice', [
                                'contact'   => $contact,
                                'company'   => Company::find($request->company['id'])->name
                            ])->render(),
                        "to" => [
                            [
                                "type"  => "to",
                                "email" => (config('app.env') === 'production') ? $contact['email'] : "benjamin@kameobikes.com",
                                "name"  => ($contact['firstname'] !== NULL && $contact['lastname'] !== NULL) ? $contact['firstname'] . ' ' . $contact['lastname'] : ''
                            ]
                            ],
                            "attachments" => [
                                [
                                    "type" => "application/pdf",
                                    "name" => $invoicePath,
                                    "content" => base64_encode(file_get_contents(public_path("storage/invoices/" . $invoicePath)))
                                ]
                            ]
                    ];
                    sendMail($message);
                }
            }
        }

        // Return pdf file name
        return response()->json([
            'success'   => true,
            'fileName'  => "/storage/invoices/" . $invoicePath
        ]);
    }
}
