<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accessory;
use App\Models\Accessory_order;
use App\Models\Grouped_order;
use Illuminate\Http\Request;

class AccessoryOrdersController extends Controller
{

    // Get all accessoriues orders
    public function index(){
        return response()->json(Accessory_order::with('catalog')->get());
    }

    // Get one accessory order by id
    public function show($id){
        return response()->json(Accessory_order::with('catalog')->find($id));
    }

    // Get one accessory order by id
    public function getAccessoriesFromGroupedOrder($id){
        return response()->json(Accessory_order::with('catalog')->where('grouped_orders_id', $id)->get());
    }

    // Update accessory order
    public function update(Request $request){
        // Validations criteria
        $request->validate([
            'amount'                    => 'required|numeric',
            'size'                      => 'required|string',
            'type'                      => 'required|string',
            'estimated_delivery_date'   => 'required|date|date_format:Y-m-d',
            'accessories_catalog_id'    => 'required|integer',
            'grouped_orders_id'         => 'required|integer'
        ]);

        // Update accessory stock
        if($request->accessories_id != null){
            if(Accessory_order::select('*')->where('id', $request->id)->first()->accessories_id !== null){
                Accessory::where('id', Accessory_order::select('*')->where('id', $request->id)->first()->accessories_id)->update([
                    'contract_type'                 => 'stock',
                    'companies_id'                  => null,
                    'users_id'                      => null,
                    'selling_price'                 => null,
                    'selling_date'                  => null,
                    'leasing_price'                 => null,
                    'contract_start'                => null,
                    'delivery_date'                 => null,
                    'estimated_delivery_date'       => null,
                ]);
            }
            Accessory::where('id', $request->accessories_id)
                ->update([
                    'contract_type'     => $request->type,
                    'companies_id'      => Grouped_order::find($request->grouped_orders_id)->companies_id,
                    'selling_price'     => ($request->type === 'selling') ? $request->amount : null,
                    'leasing_price'     => ($request->type === 'leasing') ? $request->amount : null,
                ]);
        }
        else if($request->accessories_id == null){
            Accessory::where('id', Accessory_order::find($request->id)->accessories_id)
                ->update([
                    'contract_type'                 => 'stock',
                    'companies_id'                  => null,
                    'users_id'                      => null,
                    'selling_price'                 => null,
                    'selling_date'                  => null,
                    'leasing_price'                 => null,
                    'contract_start'                => null,
                    'delivery_date'                 => null,
                    'estimated_delivery_date'       => null,
                ]);
        }

        // Update accessory order
        Accessory_order::where('id', $request->id)
            ->update([
                'amount'                    => $request->amount,
                'size'                      => $request->size,
                'type'                      => $request->type,
                'estimated_delivery_date'   => $request->estimated_delivery_date,
                'delivery_date'             => $request->delivery_date,
                'accessories_catalog_id'    => $request->accessories_catalog_id,
                'grouped_orders_id'         => $request->grouped_orders_id,
                'accessories_id'            => $request->accessories_id,
                'status'                    => $request->status,
            ]);
        // Return response
        return response()->json(Accessory_order::with('catalog')->find($request->id));
    }


    // Creation of the box order
    public function create(Request $request){
        // Validations criteria
        $request->validate([
            'amount'                    => 'required|numeric',
            'size'                      => 'required|string',
            'contract_type'             => 'required|string',
            'estimated_delivery_date'   => 'required|date|date_format:Y-m-d',
            'accessories_catalog_id'    => 'required|integer',
            'grouped_orders_id'         => 'required|integer'
        ]);
        // Create new accessory order
        $id = Accessory_order::insertGetId([
            'amount'                    => $request->amount,
            'size'                      => $request->size,
            'type'                      => $request->contract_type,
            'estimated_delivery_date'   => $request->estimated_delivery_date,
            'accessories_catalog_id'    => $request->accessories_catalog_id,
            'grouped_orders_id'         => $request->grouped_orders_id
        ]);
        // Return response
        return response()->json(Accessory_order::with('catalog')->find($id));
    }

    // Delete accessory order
    public function destroy(Request $request){
        Accessory_order::where('id', $request->accessoryOrderId)->delete();
    }
    
}
