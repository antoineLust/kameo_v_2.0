<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accessories_orderable;
use App\Models\Bikes_orderable;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Company_address;
use App\Models\Company_contact;
use App\Models\Condition;
use App\Models\User;
use App\Models\Shared_bikes_condition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use DateTime;

class CompaniesController extends Controller
{
    // Get all companies
    public function index()
    {
        return response()->json(Company::with('address', 'contact')->get());
    }

    public function getCompanyById($id){
        return response()->json(Company::with('address', 'contact')->find($id));
    }
    
    public function getCompaniesByAudience($audience){
        return response()->json(Company::with('address', 'contact')->where('audience', $audience)->get());
    }

    // Get one company
    public function show($id)
    {
        return response()->json([
            'company'       => Company::with('address', 'contact')->find($id),
            'conditions'    => Condition::where('companies_id', $id)->first()
        ]);
    }

    public function getUserAuthCompany()
    {
        // dd(Auth::user()->companies_id);
        return response()->json(Company::find(Auth::user()->companies_id));
    }

    // Upload company logo
    public function fileUpload($file, $id)
    {
        $fileExtension  = $file['file']->getClientOriginalExtension();
        $date           = new DateTime();
        $timestamp      = $date->getTimestamp();
        $path           = $file['file']->storeAs('companies_logo', $id . '-' . $timestamp . '.' . $fileExtension, 'public');
        Company::where('id', $id)
            ->update([
                'logo'  => $path,
            ]);
        return $path;
    }

    // Delete company logo
    public function deletePicture(Request $request)
    {
        Storage::delete('public/' . $request->company['logo']);
        Company::where('id', $request->company['id'])
            ->update([
                'logo' => 'companies_logo/default.png',
            ]);
    }

    // Create user
    public function create(Request $request)
    {
        // Decode json
        $request->form = json_decode($request->form, true);

        $rules = [
            'audience'      => 'required',
            'type'          => 'required',
            'address'       => 'nullable|string|max:255',
            'name'          => 'required_Unless:audience,B2C|string|max:255',
            'vat_number'    => 'required_Unless:audience,B2C',
            'firstname'     => 'required_if:audience,B2C|string|max:255',
            'lastname'      => 'required_if:audience,B2C|string|max:255',
            'email'         => 'required_if:audience,B2C|email',
            'phone'         => 'required_if:audience,B2C',
            'function'      => 'nullable',
        ];

        // apparement on ne peut pas faire de required_if avec plusieurs champs en même temps donc ça devra faire l'affaire
        if ($request->form['audience'] === "B2B" && $request->form['type'] === "client" && !array_key_exists('conditions', $request->form)) {
            $rules['conditions'] = 'required_if:audience,B2B';
        }

        // Validations rules
        Validator::make($request->form, $rules)->validate();


        // Adapt name field to audience
        if ($request->form['audience'] === "B2B" || $request->form['audience'] === "INDEPENDANT") {
            $name = $request->form['name'];
        } else if ($request->form['audience'] === "B2C") {
            $name = $request->form['firstname'] . ' ' . $request->form['lastname'];
        }
        // Create company in database
        $id = Company::insertGetId([
            'name'                      => $name,
            'vat_number'                => array_key_exists('vat_number', $request->form) ? $request->form['vat_number'] : null,
            'type'                      => $request->form['type'],
            'aquisition'                => array_key_exists('aquisition', $request->form) ? $request->form['aquisition'] : 'Kameo bikes',
            'audience'                  => $request->form['audience'],
            'logo'                      => $request->form['audience'] != 'B2C' ? 'companies_logo/default.png' : null,
            'companies_addresses_id'    => DB::table('companies_addresses')->insertGetId([
                'street'    => ($request->form['address'] !== "") ? explode(', ', $request->form['address'])[1] : null,
                'number'    => ($request->form['address'] !== "") ? explode(', ', $request->form['address'])[0] : null,
                'city'      => ($request->form['address'] !== "") ? explode(', ', $request->form['address'])[3] : null,
                'zip'       => ($request->form['address'] !== "") ? explode(', ', $request->form['address'])[2] : null,
                'country'   => ($request->form['address'] !== "") ? explode(', ', $request->form['address'])[4] : null,
                'condensed' => ($request->form['address'] !== "") ? $request->form['address'] : null
            ]),
        ]);
        // Create company contact in database
        if ($request->form['audience'] === "B2C") {
            Company_contact::insert([
                'firstname'     => $request->form['firstname'],
                'lastname'      => $request->form['lastname'],
                'email'         => $request->form['email'],
                'phone'         => $request->form['phone'],
                'function'      => (array_key_exists('function', $request->form) ? $request->form['function'] : null),
                'type'          => 'contact',
                'companies_id'  => $id
            ]);
        }
        // Upload logo if one is given
        if ($request->file() && $request->form['audience'] != "B2C") {
            Company::where('id', $id)
                ->update([
                    'logo'  => $this->fileUpload($request->file(), $id),
                ]);
        }
        // Create conditions
        if ($request->form['audience'] === "B2B" && $request->form['type'] === "client" && array_key_exists('conditions', $request->form)) {
            if (Condition::where('companies_id', $id)->exists()) {
                Condition::where('companies_id', $id)->update([
                    'access' => implode(', ', $request->form['conditions']),
                ]);
            } else {
                Condition::insert([
                    'companies_id'  => $id,
                    'access'        => implode(', ', $request->form['conditions']),
                ]);
                if (str_contains(implode($request->form['conditions']), 'cafetaria')) {
                    Bikes_orderable::insert([
                        'company_id'  => $id,
                        'catalog_id'  => '*',
                    ]);
                    Accessories_orderable::insert([
                        'company_id'  => $id,
                        'catalog_id'  => '*',
                    ]);
                }
                if (str_contains(implode($request->form['conditions']), 'partage')) {
                    Shared_bikes_condition::insert([
                        'company_id'  => $id,
                    ]);
                }
            }
        }

        // Return created company
        return response()->json([
            'company'       => Company::with('address', 'contact')->find($id),
            'condition'     => Condition::where('companies_id', $id)->first()
        ]);
    }

    // Update company
    public function update(Request $request)
    {
        // Decode json
        $request->form = json_decode($request->form, true);
        // Validations rules
        Validator::make($request->form, [
            'audience'              => 'required',
            'type'                  => 'required',
            'name'                  => 'required',
            'vat_number'            => 'required_Unless:audience,B2C',
            'address.condensed'     => 'required|string|max:255',
            'billing_group'         => 'required|max:1',
        ])->validate();

        $company = Company::with('address', 'contact')->find($request->form['id']);
        if ($request->file()) {
            $oldMedia = Company::select('logo')->where('id', $request->form['id'])->first()->logo;
            if ($oldMedia != 'companies_logo/default.png') {
                Storage::delete('public/' . $oldMedia);
            }
            $company->logo = $this->fileUpload($request->file(), $request->form['id']);
        }

        // Update company
        Company::where('id', $request->form['id'])
            ->update([
                'name'              => $request->form['name'],
                'vat_number'        => $request->form['vat_number'],
                'billing_group'     => $request->form['billing_group'],
                'type'              => $request->form['type'],
                'audience'          => $request->form['audience'],
                'aquisition'        => $request->form['aquisition'],
                'status'            => $request->form['status'],
                'logo'              => $company->logo != 'companies_logo/default.png' ? $company->logo : 'companies_logo/default.png',
            ]);
        // Update company address
        Company_address::where('id', $request->form['address']['id'])
            ->update([
                'street'    => explode(', ', $request->form['address']['condensed'])[1],
                'number'    => explode(', ', $request->form['address']['condensed'])[0],
                'city'      => explode(', ', $request->form['address']['condensed'])[3],
                'zip'       => explode(', ', $request->form['address']['condensed'])[2],
                'country'   => explode(', ', $request->form['address']['condensed'])[4],
                'condensed' => $request->form['address']['condensed']
            ]);

        // Update conditions if exist
        if ($request->form['audience'] === "B2B" && $request->form['type'] === "client" && array_key_exists('conditions', $request->form) && !!$request->form['conditions']) {
            if (Condition::where('companies_id', $request->form['id'])->exists()) {
                // Update conditions
                Condition::where('companies_id', $request->form['id'])->update([
                    'access' => implode(', ', $request->form['conditions']),
                ]);

                // Create bikes orderable if cafetaria is in conditions
                if (str_contains(implode($request->form['conditions']), 'cafetaria')) {
                    if (!Bikes_orderable::where('company_id', $request->form['id'])->exists()) {
                        Bikes_orderable::insert([
                            'company_id'  => $request->form['id'],
                            'catalog_id'  => '*',
                        ]);
                    }
                    if (!Accessories_orderable::where('company_id', $request->form['id'])->exists()) {
                        Accessories_orderable::insert([
                            'company_id'  => $request->form['id'],
                            'catalog_id'  => '*',
                        ]);
                    }
                }
                // Reset cafetaria column if cafetaria is not in conditions and delete orderable items
                else {
                    // Update cafetaria column
                    Condition::where('companies_id', $request->form['id'])->update([
                        'cafetaria_billing_type'            => null,
                        'cafetaria_tva'                     => null,
                        'cafetaria_discount_selling'        => null,
                        'cafetaria_discount_leasing'        => null,
                    ]);
                    // Delete orderable items
                    Bikes_orderable::where('company_id', $request->form['id'])->delete();
                    Accessories_orderable::where('company_id', $request->form['id'])->delete();
                }
                // Create shared bikes condition if partage is in conditions
                if (str_contains(implode($request->form['conditions']), 'partage')) {
                    if (!Shared_bikes_condition::where('company_id', $request->form['id'])->exists()) {
                        Shared_bikes_condition::insert([
                            'company_id'  => $request->form['id'],
                        ]);
                    }
                }
                // Reset shared bikes condition if partage is not in conditions
                else {
                    Shared_bikes_condition::where('company_id', $request->form['id'])->delete();
                }
            } else {
                // Create conditions
                Condition::insert([
                    'companies_id'  => $request->form['id'],
                    'access'        => implode(', ', $request->form['conditions']),
                ]);
                // Create bikes and accessories orderable if conditions contains cafetaria
                if (str_contains(implode($request->form['conditions']), 'cafetaria')) {
                    Bikes_orderable::insert([
                        'company_id'  => $request->form['id'],
                        'catalog_id'  => '*',
                    ]);
                    Accessories_orderable::insert([
                        'company_id'  => $request->form['id'],
                        'catalog_id'  => '*',
                    ]);
                }
            }
        }
        // Delete conditions if not exist
        else if (Condition::where('companies_id', $request->form['id'])->exists()) {
            Condition::where('companies_id', $request->form['id'])->delete();
            Bikes_orderable::where('company_id', $request->form['id'])->delete();
            Accessories_orderable::where('company_id', $request->form['id'])->delete();
        }

        if ($request->form['audience'] === "B2B" && $request->form['type'] === "client") {

            if (!!$request->form['conditions']) {
                $conditions = implode(',', $request->form['conditions']);
            } else {
                $conditions = '';
            }

            $equivalent = [
                'cafetaria'     => 'order',
                'partage'       => 'search',
            ];

            foreach (User::where('companies_id', $request->form['id'])->get() as $user) {

                $permissions = $user->permission;

                foreach (['cafetaria', 'partage'] as $access) {

                    $condition = $equivalent[$access];

                    if (!(str_contains($conditions, $access) && str_contains($permissions, $condition))) {
                        if (str_contains($conditions, $access) && !str_contains($permissions, $condition)) {
                            $permissions .= ',' . $condition;
                        } else if (str_contains($permissions, $condition)) {
                            if (str_contains($permissions, ',' . $condition)) {
                                $permissions = str_replace(',' . $condition, '', $permissions);
                            } else if (str_contains($permissions, $condition)) {
                                $permissions = str_replace($condition, '', $permissions);
                            }
                        }
                    }
                }

                User::where('id', $user->id)->update([
                    'permission' => trim($permissions, ','),
                ]);
            }
        }

        // Return response
        return response()->json([
            'company'       => Company::with('address', 'contact')->find($request->form['id']),
            'conditions'    => Condition::where('companies_id', $request->form['id'])->first()
        ]);
    }

    // Destroy company
    public function destroy(Request $request)
    {
        // Delete company
        Company::where('id', $request->companyId)
            ->update([
                'status'    => 'inactif'
            ]);
        // Return response
        return response()->json(Company::with('address', 'contact')->find($request->companyId));
    }

    // Create company for entretien
    public function createClientForEntretien(Request $request)
    {
        // Validation
        Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname'  => 'required|string|max:255',
            'email'     => 'nullable|email|max:255',
            'phone'     => 'required|string|max:255',
            'address'   => 'nullable|string|max:255',
        ])->validate();

        // Create company in database
        $id = Company::insertGetId([
            'name'                      => $request->firstname . ' ' . $request->lastname,
            'type'                      => 'client',
            'audience'                  => 'B2C',
            'logo'                      => 'companies_logo/default.png',
            'companies_addresses_id'    => DB::table('companies_addresses')->insertGetId([
                'street'    => (isset($request->address) ? explode(', ', $request->address)[1] : null),
                'number'    => (isset($request->address) ? explode(', ', $request->address)[0] : null),
                'city'      => (isset($request->address) ? explode(', ', $request->address)[3] : null),
                'zip'       => (isset($request->address) ? explode(', ', $request->address)[2] : null),
                'country'   => (isset($request->address) ? explode(', ', $request->address)[4] : null),
                'condensed' => (isset($request->address) ? $request->address : null),
            ]),
        ]);

        // Create company contact in database
        Company_contact::insert([
            'firstname'     => $request->firstname,
            'lastname'      => $request->lastname,
            'email'         => $request->email,
            'phone'         => $request->phone,
            'type'          => 'contact',
            'companies_id'  => $id
        ]);

        // Return response
        return response()->json(Company::with('address', 'contact')->find($id));
    }

    // Get datatable info
    public function getDataForDataTable(Request $request)
    {
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        $sortDirection = json_decode($request->sortDirection, true);
        if ($search) {
            $q = explode(' ', $search);
            $res = Company::select('id', 'name', 'vat_number', 'audience');
            for($i = 0; $i < count($q); $i++) {
                $res->where(function($query) use ($q, $i) {
                    $query->where('name', 'like', '%' . $q[$i] . '%')
                        ->orWhere('vat_number', 'like', '%' . $q[$i] . '%')
                        ->orWhere('audience', 'like', '%' . $q[$i] . '%')
                        ->orWhere('id', 'like', '%' . $q[$i] . '%');
                });
            }
            return $res->orderBy($sortDirection['column'], $sortDirection['direction'])
                ->limit($limit)
                ->offset(($page - 1) * $limit)
                ->get();
        } else {
            $companies = Company::select('id', 'name', 'vat_number', 'audience')->orderBy($sortDirection['column'], $sortDirection['direction'])->limit($limit)->offset(($page - 1) * $limit)->get();
        }
        return response()->json($companies);
    }
}
