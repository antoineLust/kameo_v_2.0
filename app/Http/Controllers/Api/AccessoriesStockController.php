<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accessory;
use Illuminate\Http\Request;
use Termwind\Components\Dd;

class AccessoriesStockController extends Controller
{
    // Get all accessories stock
    public function index()
    {
        return response()->json(Accessory::with('catalog', 'company')->get());
    }
    // Get accessory stock by id
    public function show($id)
    {
        return response()->json(Accessory::with('catalog', 'company')->find($id));
    }

    public function getAccessoriesByCatalogIdAndSize(Request $request)
    {
        return response()->json(Accessory::with('catalog', 'company')->where('accessories_catalog_id', $request->catalogId)->where('size', $request->size)->where('contract_type', 'stock')->get());
    }

    public function getAccessoryStockById($id)
    {
        return response()->json(Accessory::with('catalog', 'company')->find($id));
    }

    // Create new accessory stock
    public function create(Request $request)
    {
        $request->validate([
            'brand'                     => 'required|string',
            'accessories_catalog_id'    => 'required|integer',
            'size'                      => 'required|string',
            'contract_type'             => 'required|string',
            'estimated_delivery_date'   => 'required_if:contract_type,order|date|after_or_equal:buying_date',
            'delivery_date'             => 'required_if:contract_type,stock|date|after_or_equal:buying_date',
        ]);

        for ($i = 1; $i <= $request->nb_accessories; $i++) {
            Accessory::insert([
                'accessories_catalog_id'    => $request->accessories_catalog_id,
                'size'                      => $request->size,
                'contract_type'             => $request->contract_type,
                'estimated_delivery_date'   => $request->estimated_delivery_date,
                'delivery_date'             => $request->delivery_date,
                'status'                    => 'actif',
                'companies_id'              => 12,
            ]);
        }
        // Return created accessory stock
        return response()->json([
            'message' => 'Accessory stock created',
        ]);
    }

    // Update accessory stock
    public function update(Request $request)
    {
        $request->validate([
            'catalog.brand'             => 'required|string',
            'accessories_catalog_id'    => 'required|integer',
            'size'                      => 'required|string',
            'contract_type'             => 'required|string',
            'estimated_delivery_date'   => 'required_if:contract_type,order|date|nullable',
            'delivery_date'             => 'required_unless:contract_type,order|date|nullable',
            'companies_id'              => 'required_unless:contract_type,order,stock|integer',
            'bikes_id'                  => 'integer|nullable',
            'users_id'                  => 'integer|nullable',
            'selling_date'              => 'required_if:contract_type,selling|date|nullable',
            'selling_price'             => 'required_if:contract_type,selling|numeric|nullable',
            'contract_start'            => 'required_if:contract_type,leasing|date|nullable',
            'contract_end'              => 'required_if:contract_type,leasing|date|nullable',
            'leasing_price'             => 'required_if:contract_type,leasing|numeric|nullable',
            'stolen_date'               => 'required_if:contract_type,stolen|date|nullable',
            'reimbursement_insurance'   => 'required_if:contract_type,stolen|numeric|nullable',
        ]);

        Accessory::where('id', $request->id)
            ->update([
                'accessories_catalog_id'    => $request->accessories_catalog_id,
                'size'                      => $request->size,
                'contract_type'             => $request->contract_type,
                'estimated_delivery_date'   => $request->estimated_delivery_date,
                'delivery_date'             => $request->delivery_date,
                'status'                    => 'actif',
                'companies_id'              => $request->companies_id,
                'bikes_id'                  => $request->bikes_id,
                'users_id'                  => $request->users_id,
                'selling_date'              => $request->selling_date,
                'selling_price'             => $request->selling_price,
                'contract_start'            => $request->contract_start,
                'contract_end'              => $request->contract_end,
                'leasing_price'             => $request->leasing_price,
                'stolen_date'               => $request->stolen_date,
                'reimbursement_insurance'   => $request->reimbursement_insurance,
            ]);
        // Return updated accessory stock
        return response()->json(Accessory::with('company', 'catalog')->find($request->id));
    }

    public function destroy($id)
    {
        Accessory::where('id', $id)->update([
            'status' => 'inactif',
        ]);
        return response()->json(Accessory::with('company', 'catalog')->find($id));
    }

    public function receptionCommande(Request $request)
    {
        // Set new company name by companies id
        // Update bike stock
        Accessory::where('id', $request['id'])
            ->update([
                'contract_type'                             => $request['contract_type'],
            ]);
        // Return updated bike
        return response()->json(
            Accessory::with('company', 'catalog')->find($request->id)
        );
    }

    // Get datatable info
    public function getDataForDataTable(Request $request)
    {
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        $sortDirection = json_decode($request->sortDirection, true);
        $contractType = $request->contractTypeFilter;
        if ($search) {
            $q = explode(' ', $search);
            $res = Accessory::join('companies', 'companies.id', '=', 'accessories.companies_id')
                ->join('accessories_catalog', 'accessories_catalog.id', '=', 'accessories.accessories_catalog_id')
                ->select('companies.name', 'accessories_catalog.brand', 'accessories_catalog.model', 'contract_type', 'leasing_price', 'selling_price', 'accessories.id', 'accessories_catalog.price_htva');
            for ($i = 0; $i < count($q); $i++) {
                $res->where(function ($query) use ($q, $i) {
                    $query->where('companies.name', 'like', '%' . $q[$i] . '%')
                        ->orWhere('accessories_catalog.brand', 'like', '%' . $q[$i] . '%')
                        ->orWhere('accessories_catalog.model', 'like', '%' . $q[$i] . '%')
                        ->orWhere('contract_type', 'like', '%' . $q[$i] . '%')
                        ->orWhere('contract_type' == 'leasing' ? 'leasing_price' : 'selling_price', 'like', '%' . $q[$i] . '%')
                        ->orWhere('accessories.id', 'like', '%' . $q[$i] . '%');
                });
            }
            if ($contractType == 'all') {
                $accessories = $res
                    ->orderBy($sortDirection['column'], $sortDirection['direction'])
                    ->limit($limit)
                    ->offset(($page - 1) * $limit)
                    ->get();
            } else {
                $accessories = $res
                    ->where('contract_type', $contractType)
                    ->orderBy($sortDirection['column'], $sortDirection['direction'])
                    ->limit($limit)
                    ->offset(($page - 1) * $limit)
                    ->get();
            }
        } else {
            if ($contractType == 'all') {
                $accessories = Accessory::join('companies', 'companies.id', '=', 'accessories.companies_id')
                    ->join('accessories_catalog', 'accessories_catalog.id', '=', 'accessories.accessories_catalog_id')
                    ->select('companies.name', 'accessories_catalog.brand', 'accessories_catalog.model', 'contract_type', 'selling_price', 'leasing_price', 'accessories.id', 'accessories_catalog.price_htva')
                    ->orderBy($sortDirection['column'], $sortDirection['direction'])
                    ->limit($limit)
                    ->offset(($page - 1) * $limit)
                    ->get();
            } else {
                $accessories = Accessory::join('companies', 'companies.id', '=', 'accessories.companies_id')
                    ->join('accessories_catalog', 'accessories_catalog.id', '=', 'accessories.accessories_catalog_id')
                    ->select('companies.name', 'accessories_catalog.brand', 'accessories_catalog.model', 'contract_type', 'selling_price', 'leasing_price', 'accessories.id', 'accessories_catalog.price_htva')
                    ->where('contract_type', $contractType)
                    ->orderBy($sortDirection['column'], $sortDirection['direction'])
                    ->limit($limit)
                    ->offset(($page - 1) * $limit)
                    ->get();
            }
        }
        foreach ($accessories as $accessory) {
            $accessory->leasing_price = number_format($accessory->leasing_price * 1.21, 2, ',', ' ');
            $accessory->selling_price = number_format($accessory->selling_price * 1.21, 2, ',', ' ');
            $accessory->price_htva = number_format($accessory->price_htva * 1.21, 2, ',', ' ');
            if ($accessory->contract_type == 'monthly_leasing') {
                $accessory['price'] = $accessory->leasing_price . '€/mois';
            } else if ($accessory->contract_type == 'leasing') {
                $accessory['price'] = ($accessory->leasing_price . '€/mois');
            } else if ($accessory->contract_type == 'annual_leasing') {
                $accessory['price'] = $accessory->leasing_price . '€/an';
            } else if ($accessory->contract_type == 'preFinanced_leasing') {
                $accessory['price'] = $accessory->leasing_price . '€/mois';
            } else if ($accessory->contract_type == 'selling') {
                $accessory['price'] = $accessory->selling_price . '€';
            } else {
                $accessory['price'] = $accessory->price_htva . '€';
            }
        }
        return response()->json($accessories);
    }
}
