<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Bike;
use App\Models\Bikes_assembly;
use App\Models\Bikes_Catalog;
use App\Models\Calendar;
use Illuminate\Http\Request;

class BikesAssemblyController extends Controller
{
    // Get all bikes assembly
    public function index(){
        return response()->json(Bikes_assembly::with('bike')->get());
    }

    // Show a bike assembly
    public function show($id){
        return response()->json(Bikes_assembly::with('bike')->find($id));
    }

    // Create bikes assembly
    public function create(Request $request){
        // Validate request
        $this->validate($request, [
            'bikes_id'  => 'required',
            'date'      => 'required',
        ]);

        // Create bikes assembly
        $id = Bikes_assembly::insertGetId([
            'bikes_id'  => $request->bikes_id,
            'date'      => $request->date,
        ]);

        // Return response
        return response()->json(Bikes_assembly::with('bike')->find($id));
    }

    // Update bikes assembly
    public function update(Request $request, $id){
        // Validate request
        $this->validate($request, [
            'status'    => 'required',
            'date'      => 'required',
        ]);

        // Update bikes assembly
        Bikes_assembly::where('id', $id)->update([
            'status'    => $request->status,
            'date'      => $request->date,
        ]);

        if($request->status === 'done'){
            Bike::where('id', $request->bikes_id)->update([
                'assembled'    => 1,
            ]);
        }
        else if($request->status === 'todo'){
            Bike::where('id', $request->bikes_id)->update([
                'assembled'    => 0,
            ]);
        }

        // Return response
        return response()->json(Bikes_assembly::with('bike')->find($id));
    }

    // Delete bikes assembly
    public function destroy($id){
        // Delete bikes assembly
        Bikes_assembly::where('id', $id)->delete();

        Calendar::where('item_id', $id)->where('item_type', 'Bike Assembly')->delete();

        // Return response
        return response()->json(['message' => 'Bikes assembly deleted']);
    }

    // Get datatable info
    public function getDataForDataTable(Request $request){
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        if($search){
            $bikes = Bikes_assembly::join('bikes', 'bikes_assembly.bikes_id', '=', 'bikes.id')
                ->select('bikes.id', 'bikes.frame_reference', 'bikes_assembly.date', 'bikes_assembly.status', 'bikes_assembly.id')
                ->where('bikes.frame_reference', 'like', '%'.$search.'%')
                ->orWhere('bikes_assembly.date', 'like', '%'.$search.'%')
                ->orWhere('bikes_assembly.status', 'like', '%'.$search.'%')
                ->orWhere('bikes.id', 'like', '%'.$search.'%')
                ->orWhere('bikes_assembly.id', 'like', '%'.$search.'%')
                ->orderBy('bikes_assembly.id', 'desc')
                ->limit($limit)
                ->offset(($page - 1) * $limit)
                ->get();
        }
        else{
            $bikes = Bikes_assembly::join('bikes', 'bikes_assembly.bikes_id', '=', 'bikes.id')
            ->select('bikes.id', 'bikes.frame_reference', 'bikes_assembly.date', 'bikes_assembly.status', 'bikes_assembly.id')->orderBy('bikes_assembly.id', 'desc')->limit($limit)->offset(($page - 1) * $limit)->get();
        }
        foreach($bikes as $bike){
            $bike->date = date('d/m/Y', strtotime($bike->date));
            $bike['bike'] = Bikes_Catalog::where('id', $bike->id)->first()->brand . ' ' . Bikes_Catalog::where('id', $bike->id)->first()->model . ' - ' . $bike->frame_reference;
        }
        return response()->json($bikes);
    }
}
