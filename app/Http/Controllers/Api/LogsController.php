<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LogsController extends Controller
{

    public function getQueries()
    {
        // Get log in log/queries.log
        $file           = storage_path('logs/queries.log');
        $log            = file_get_contents($file);
        $queries        = explode("\n", $log);
        $decodeQueries  = [];
        // Delete last element (empty)
        array_pop($queries);
        // Cut the all before local.INFO:
        $queries    = array_map(function ($query) {
            return substr($query, strpos($query, 'INFO:') + 6);
        }, $queries);
        // Cut all after the last }
        $queries    = array_map(function ($query) {
            return substr($query, 0, strrpos($query, '}') + 1);
        }, $queries);
        // Decode all queries
        foreach ($queries as $query) {
            $decodeQueries[] = json_decode($query, true);
        };
        $decodeQueries = array_reverse($decodeQueries);
        // Return all queries
        return response()->json($decodeQueries);
    }
}
