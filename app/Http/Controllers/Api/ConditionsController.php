<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accessories_orderable;
use App\Models\Bikes_orderable;
use App\Models\Condition;
use App\Models\Shared_bikes_condition;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Validator;

class ConditionsController extends Controller
{
    // Get all conditions
    public function index()
    {
        return response()->json(Condition::all());
    }
    
    public function getSharedBikesConditionsByCompanyId($id){
        return response()->json(Shared_bikes_condition::where('company_id', $id)->get());
    }

    public function userAuthCondition()
    {
        $condition = Condition::where('companies_id', Auth::user()->companies_id)->first();
        if (str_contains($condition->access, 'cafetaria')) {
            $bikeOrderable          = Bikes_orderable::where('company_id', $condition->companies_id)->first();
            $accessoryOrderable     = Accessories_orderable::where('company_id', $condition->companies_id)->get();
        }
        if (str_contains($condition->access, 'partage')) {
            $sharedBikes = Shared_bikes_condition::where('company_id', $condition->companies_id)->get();
        }
        // catalog_id est retourné en une array de NaN, totallement incompréhensible
        if (isset($bikeOrderable->catalog_id)) {
            $bikeOrderable->catalogs_id = $bikeOrderable->catalog_id;
        }
        return response()->json([
            'condition'                 => $condition,
            'bikeOrderable'             => (isset($bikeOrderable)) ? $bikeOrderable : null,
            'accessoryOrderable'        => (isset($accessoryOrderable)) ? $accessoryOrderable : null,
            'sharedBikes'               => (isset($sharedBikes)) ? $sharedBikes : null
        ]);
    }


    public function getConditionsByCompanyId($id){
        return response()->json(Condition::where('companies_id', $id)->first());
    }
    
    // Get all orderable conditions
    public function getAllOrderable()
    {
        $bikes          = Bikes_orderable::all();
        $accessories    = Accessories_orderable::all();
        return response()->json([
            'bikes'         => $bikes,
            'accessories'   => $accessories
        ]);
    }
    
    public function showByCompanyId($id){
        $condition = Condition::where('companies_id', $id)->first();
        if(str_contains($condition->access, 'cafetaria')){
            $bikeOrderable          = Bikes_orderable::where('company_id', $condition->companies_id)->first();
            $accessoryOrderable     = Accessories_orderable::where('company_id', $condition->companies_id)->get();
        }
        if(str_contains($condition->access, 'partage')){
            $sharedBikes = Shared_bikes_condition::where('company_id', $condition->companies_id)->get();
        }
        // catalog_id est retourné en une array de NaN, totallement incompréhensible
        if(isset($bikeOrderable->catalog_id)) {
            $bikeOrderable->catalogs_id = $bikeOrderable->catalog_id;
        }
        return response()->json([
            'condition'                 => $condition,
            'bikeOrderable'             => (isset($bikeOrderable)) ? $bikeOrderable : null,
            'accessoryOrderable'        => (isset($accessoryOrderable)) ? $accessoryOrderable : null,
            'sharedBikes'               => (isset($sharedBikes)) ? $sharedBikes : null
        ]);
    }

    public function show($id){
        $condition      = Condition::find($id);
        if (str_contains($condition->access, 'cafetaria')) {
            $bikeOrderable          = Bikes_orderable::where('company_id', $condition->companies_id)->first();
            $accessoryOrderable     = Accessories_orderable::where('company_id', $condition->companies_id)->get();
        }
        if (str_contains($condition->access, 'partage')) {
            $sharedBikes = Shared_bikes_condition::where('company_id', $condition->companies_id)->get();
        }
        // catalog_id est retourné en une array de NaN, totallement incompréhensible
        if (isset($bikeOrderable->catalog_id)) {
            $bikeOrderable->catalogs_id = $bikeOrderable->catalog_id;
        }
        return response()->json([
            'condition'                 => $condition,
            'bikeOrderable'             => (isset($bikeOrderable)) ? $bikeOrderable : null,
            'accessoryOrderable'        => (isset($accessoryOrderable)) ? $accessoryOrderable : null,
            'sharedBikes'               => (isset($sharedBikes)) ? $sharedBikes : null
        ]);
    }

    // Update condition
    public function update(Request $request)
    {
        // dd($request);
        if ($request->plan == 'cafetaria') {
            // Validate request
            Validator::make($request->data, [
                'cafetaria_billing_type'        => 'nullable',
                'cafetaria_tva'                 => 'string|nullable',
                'cafetaria_discount_selling'    => 'nullable|numeric|max:255',
                'cafetaria_discount_leasing'    => 'nullable|numeric|max:255'
            ])->validate();

            // Update condition
            $condition                              = Condition::find($request->data['id']);
            $condition->cafetaria_billing_type      = (is_array($request->data['cafetaria_billing_type'])) ? implode(',', $request->data['cafetaria_billing_type']) : $request->data['cafetaria_billing_type'];
            $condition->cafetaria_tva               = $request->data['cafetaria_tva'];
            $condition->cafetaria_discount_selling  = $request->data['cafetaria_discount_selling'];
            $condition->cafetaria_discount_leasing  = $request->data['cafetaria_discount_leasing'];
            $condition->save();

            if ($request->bikeOrderable) {

                $newBikeOrderableCatalogId = (is_array($request->bikeOrderable['catalogs_id'])) ? implode(',', $request->bikeOrderable['catalogs_id']) : $request->bikeOrderable['catalogs_id'];

                // Update bike orderable or create id not exist
                $bikeOrderable              = Bikes_orderable::where('company_id', intval($request->data['companies_id']))->first();
                if ($bikeOrderable != null) {
                    $bikeOrderable->catalog_id = $newBikeOrderableCatalogId;
                    $bikeOrderable->save();
                } else {
                    Bikes_orderable::insert([
                        'company_id'   => intval($request->data['companies_id']),
                        'catalog_id'   => '[]'
                    ]);
                    Bikes_orderable::where('company_id', intval($request->data['companies_id']))->update([
                        'catalog_id'   => $newBikeOrderableCatalogId
                    ]);
                }
            }

            $bikeOrderable->catalogs_id = $bikeOrderable->catalog_id;

            // Return updated condition
            return response()->json([
                'condition'         => $condition,
                'bikeOrderable'     => (isset($bikeOrderable)) ? $bikeOrderable : null
            ]);
        }
    }

    // Remove accessory orderable from condition
    public function removeAccessoryOrderable($id)
    {
        Accessories_orderable::where('id', intval($id))->delete();
        return response()->json(['success' => true]);
    }

    // Create accessory orderable in condition
    public function createAccessoryOrderable(Request $request)
    {
        // Validate request
        $request->validate([
            'company_id'    => 'required|string',
            'accessory_id'  => 'required|integer',
            'isFree'        => 'required|string|max:1',
            'type'          => 'required|string',
        ]);

        // Create accessory orderable
        Accessories_orderable::insert([
            'company_id'    => $request->company_id,
            'catalog_id'    => $request->accessory_id,
            'isFree'        => $request->isFree,
            'type'          => $request->type,
        ]);

        // Return created accessory orderable
        return response()->json(Accessories_orderable::where('company_id', $request->company_id)->get());
    }

    // Delete all accessories orderable from condition
    public function deleteAllAccessoryOrderables($id)
    {
        Accessories_orderable::where('company_id', intval($id))->delete();
        return response()->json(['success' => true]);
    }

    // Add all accessories orderable from condition
    public function addAllAccessoryOrderables($id)
    {
        if (Accessories_orderable::where('company_id', intval($id))->first() == null) {
            Accessories_orderable::insert([
                'company_id'    => intval($id),
                'catalog_id'    => '*',
                'isFree'        => 'N',
                'type'          => 'available',
            ]);
            return response()->json(Accessories_orderable::where('company_id', intval($id))->get());
        }
    }

    // Add new shared bike
    public function addNewSharedBike(Request $request, $id)
    {
        // Validate request
        $request->validate([
            'weekend_access'                    => 'required|string|max:1',
            'max_time_before_reservation'       => 'required|integer',
            'max_time_reservation'              => 'required|integer',
            'users_id'                          => 'required|array|min:1',
            'bikes_id'                          => 'required|array|min:1',
            'buildings_id'                      => 'required|array|min:1',
            'days'                              => 'required|array|min:1',
            'start_taking_time'                 => 'required|string',
            'end_taking_time'                   => 'required|string',
            'start_delivery_time'               => 'required|string',
            'end_delivery_time'                 => 'required|string',
        ]);
        // Check if days contains duplicate
        $days = implode(',', $request->days);
        if (str_contains($days, ',')) {
            $days = explode(',', $days);
            $days = array_unique($days);
            $days = implode(',', $days);
        }
        // Verify if id exist in users_id
        foreach ($request->users_id as $user_id) {
            if (Shared_bikes_condition::where('users_id', intval($user_id))->first() != null) {
                // Return error if users_id contains duplicate
                // return response()->json([
                //     'message' => 'User ' . $user_id . '  exist in shared bikes',
                //     'error' => [
                //         'users_id'      => 'User ' . $user_id . ' already exist in shared bikes',
                //     ]
                // ], 422);

                // Delete shared
                $deletedSharedBike = Shared_bikes_condition::where('users_id', intval($user_id))->first();
                Shared_bikes_condition::where('users_id', strval($user_id))->where('company_id', strval($id))->delete();
            }
        }
        // Create shared bike
        $id = Shared_bikes_condition::insertGetId([
            'company_id'                        => intval($id),
            'week_end'                          => $request->weekend_access,
            'max_days_before_reservation'       => $request->max_time_before_reservation,
            'max_resevation_duration'           => $request->max_time_reservation,
            'users_id'                          => (is_array($request->users_id)) ? implode(',', $request->users_id) : $request->users_id,
            'bikes_id'                          => (is_array($request->bikes_id)) ? implode(',', $request->bikes_id) : $request->bikes_id,
            'buildings_id'                      => (is_array($request->buildings_id)) ? implode(',', $request->buildings_id) : $request->buildings_id,
            'days'                              => $days,
            'start_taking_time'                 => $request->start_taking_time,
            'end_taking_time'                   => $request->end_taking_time,
            'start_delivery_time'               => $request->start_delivery_time,
            'end_delivery_time'                 => $request->end_delivery_time,
        ]);
        // Return created shared bike
        return response()->json([
            'shared_bike'           => Shared_bikes_condition::find($id),
            'delete_shared_bike'    => (isset($deletedSharedBike)) ? $deletedSharedBike : null
        ]);
    }

    // Delete shared bike
    public function deleteSharedBike($id)
    {
        Shared_bikes_condition::where('id', intval($id))->delete();
        return response()->json(['success' => true]);
    }

    // Get all shared bikes
    public function getSharedBikesConditions()
    {
        return response()->json(Shared_bikes_condition::all());
    }

    // Get datatable info
    public function getDataForDataTable(Request $request)
    {
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        if ($search) {
            $conditions = Condition::join('companies', 'conditions.companies_id', '=', 'companies.id')
                ->select('conditions.id', 'conditions.access', 'companies.name')
                ->orderBy('conditions.id', 'desc')
                ->limit($limit)
                ->offset(($page - 1) * $limit)
                ->get();
        } else {
            $conditions = Condition::join('companies', 'conditions.companies_id', '=', 'companies.id')
                ->select('conditions.id', 'conditions.access', 'companies.name as client')->orderBy('conditions.id', 'desc')->limit($limit)->offset(($page - 1) * $limit)->get();
        }
        foreach ($conditions as $condition) {
            $condition['cafetaria'] = (str_contains($condition['access'], 'cafetaria')) ? 1 : 0;
            $condition['partage'] = (str_contains($condition['access'], 'partage')) ? 1 : 0;
            $condition['assistance'] = (str_contains($condition['access'], 'assistance')) ? 1 : 0;
            $condition['calendar'] = (str_contains($condition['access'], 'calendar')) ? 1 : 0;
            unset($condition['access']);
        }
        return response()->json($conditions);
    }
}
