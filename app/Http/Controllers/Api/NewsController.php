<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\Company;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use DateTime;

class NewsController extends Controller
{
    // Get all news
    public function index()
    {
        $allNews = News::all();
        return response()->json($allNews);
    }

    public function lastNews()
    {
        $lastNews = News::orderBy('id', 'DESC')->skip(0)->take(2)->get();
        return response()->json($lastNews);
    }

    // Create news
    public function create(Request $request)
    {
        // Decode json
        $request->form = json_decode($request->form, true);

        // Validations rules
        Validator::make($request->form, [
            'title'             => 'required|string',
            'content'           => 'required|string',
            'expiration_date'    => 'required|date',
        ])->validate();

        // Transform an array of object into an array of id
        $target_companies_id = [];
        if (array_key_exists('target_companies', $request->form)) {
            foreach ($request->form['target_companies'] as $company) {
                array_push($target_companies_id, $company['id']);
            }
        }

        // Create bikes catalog and return id
        $id = News::insertGetId([
            'title'                 => $request->form['title'],
            'content'               => $request->form['content'],
            'author'                => Auth::user()->id,
            'expiration_date'       => $request->form['expiration_date'],
            'target_group'          => array_key_exists('target_group', $request->form) ? implode(',', $request->form['target_group']) : 'all',
            'target_companies'      => !empty($target_companies_id) ? implode(',', $target_companies_id) : null,
            'created_at'            => new DateTime(),
            'updated_at'            => new DateTime(),
        ]);

        if ($request->file()) {
            $fileName = $this->fileUpload($request->file(), $id);
            News::where('id', $id)
                ->update([
                    'media' => $fileName,
                    'media_type' => explode('/', $request->file('file')->getMimeType())[0],
                ]);
        }
        $news = News::find($id);
        // Update of target group and target companies into arrays
        $news->target_group = explode(',', $news->target_group);
        $news->target_companies = explode(',', $news->target_companies);
        return response()->json($news);
    }


    public function show($id)
    {
        // if ($id != "undefined") {

        $news = News::find($id);
        $array = [];
        $news->target_companies = explode(',', $news->target_companies);
        if ($news->target_companies[0]) {
            foreach ($news->target_companies as $company) {
                $companyAudience = Company::select('audience')->where('id', $news->target_companies)->first();
                array_push($array, ['id' => $company, 'audience' => $companyAudience->audience]);
            }
        }
        $news->target_group = explode(',', $news->target_group);
        $news->target_companies = $array;
        return response()->json($news);
        // } else {
        //     return null;
        // }
    }

    // Update news
    public function update(Request $request, $id)
    {
        $request->form = json_decode($request->form, true);
        $news = News::find($request->form['id']);

        // Validations rules
        Validator::make($request->form, [
            'title'             => 'required|string',
            'content'           => 'required|string',
            'expiration_date'   => 'required|date',
        ])->validate();

        $target_companies_id = [];
        if (array_key_exists('target_companies', $request->form)) {
            foreach ($request->form['target_companies'] as $company) {
                array_push($target_companies_id, $company['id']);
            }
        }

        if ($request->file()) {
            $news->media_type = explode('/', $request->file('file')->getMimeType())[0];
            $oldMedia = News::select('media')->where('id', $request->form['id'])->first()->media;
            if ($oldMedia != 'default.jpg') {
                Storage::delete('public/news_media/' . $oldMedia);
            }
            $news->media = $this->fileUpload($request->file(), $request->form['id']);
        }

        News::where('id', $request->form['id'])
            ->update([
                'title'                 => $request->form['title'],
                'author'                => Auth::user()->id,
                'content'               => $request->form['content'],
                'media'                 => $news->media,
                'media_type'            => $news->media_type,
                'expiration_date'       => $request->form['expiration_date'],
                'target_group'          => array_key_exists('target_group', $request->form) ? implode(',', $request->form['target_group']) : 'all',
                'target_companies'      => !empty($target_companies_id) ? implode(',', $target_companies_id) : null,
                'status'                => $request->form['status'],
            ]);


        $news = News::find($request->form['id']);
        $news->target_group = $request->form['target_group'];
        $news->target_companies = $request->form['target_companies'];
        return response()->json($news);
    }

    // Upload file
    public function fileUpload($file, $id)
    {
        $fileExtension = $file['file']->getClientOriginalExtension();
        $date = new DateTime();
        $timestamp = $date->getTimestamp();
        $fileName = $id . '-' . $timestamp . '.' . $fileExtension;
        $file['file']->storeAs('news_media', $fileName, 'public');
        return $fileName;
    }

    public function deletePicture(Request $request)
    {
        Storage::delete('public/news_media/' . $request->news['media']);
        News::where('id', $request->news['id'])
            ->update([
                'media' => 'default.jpg',
                'media_type' => 'image',
            ]);
    }

    // Destroy news
    public function destroy(Request $request)
    {
        // Delete news
        News::where('id', $request->newsId)
            ->update([
                'status'    => 'inactif'
            ]);
        // Return response
        return response()->json(News::find($request->newsId));
    }

    // Get datatable info
    public function getDataForDataTable(Request $request)
    {
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        if ($search) {
            $news = News::select('id', 'title', 'expiration_date', 'created_at')
                ->where('title', 'like', '%' . $search . '%')
                ->orWhere('expiration_date', 'like', '%' . $search . '%')
                ->orWhere('created_at', 'like', '%' . $search . '%')
                ->orWhere('id', 'like', '%' . $search . '%')
                ->orderBy('id', 'desc')
                ->limit($limit)
                ->offset(($page - 1) * $limit)
                ->get();
        } else {
            $news = News::select('id', 'title', 'expiration_date', 'created_at')->orderBy('id', 'desc')->limit($limit)->offset(($page - 1) * $limit)->get();
        }
        foreach ($news as $n) {
            // $time = strtotime($n['created_at']);
            $date = new DateTime($n['created_at']);
            // dd($date->format('Y-m-d'));
            $n['created_date'] = $date->format('Y-m-d');
            // dd($n);
        }
        // dd($news);
        return response()->json($news);
    }
}
