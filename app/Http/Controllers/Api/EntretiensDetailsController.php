<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Accessory;
use App\Models\Entretien;
use App\Models\Entretien_detail;
use Illuminate\Http\Request;

class EntretiensDetailsController extends Controller
{
    // Get all entretiens details
    public function index()
    {
        $entretiensDetails = Entretien_detail::all();
        return response()->json($entretiensDetails);
    }

    // public function getEntretienDetailsApi($id){
    //     return response()->json(Entretien_detail::where('entretiens_id', $id)->get());
    // }

    // Get one entretien detail
    public function show($id)
    {
        return response()->json(Entretien_detail::where('entretiens_id', $id)->get());
    }

    // Delete one entretien detail
    public function destroy($id)
    {
        $entretien = Entretien::find(intval($id))->delete();
        $entretiensDetails = Entretien_detail::where('entretiens_id', intval($id));
        foreach ($entretiensDetails as $entretienD) {
            $entretienD->delete();
        }
        // $entretienDetail->delete();
        return response()->json($entretien);
    }

    // Create one entretien detail
    public function create(Request $request)
    {
        if ($request->type === 'service') {
            // dd($request->details);
            // Validate the request
            $this->validate($request, [
                'entretien_id'  => 'required',
                'category'      => 'required|string',
                'service'       => 'required',
                'price'         => 'required',
                'duration'      => 'required|string',
                'details'       => 'nullable|string',
            ]);
            // Create the entretien detail
            $id = Entretien_detail::insertGetId([
                'item_type'     => 'service',
                'item_id'       => $request->service['id'],
                'duration'      => intval($request->duration),
                'amount'        => floatval($request->price),
                'description'   => $request->service['description'],
                'details'       => $request->details,
                'entretiens_id' => intval($request->entretien_id),
            ]);

            // Return the entretien detail
            return response()->json(Entretien_detail::find($id));
        } else if ($request->type === 'accessory') {
            // Validate the request
            $this->validate($request, [
                'entretien_id'              => 'required',
                'category'                  => 'required|integer',
                'accessories_catalog_id'    => 'required',
                'size'                      => 'required|string',
                'amount'                    => 'required',
            ]);

            // Create the entretien detail
            $id = Entretien_detail::insertGetId([
                'item_type'         => 'accessory',
                'item_id'           => $request->accessories_catalog_id,
                'amount'            => floatval($request->amount),
                'accessory_size'    => $request->size,
                'entretiens_id'     => intval($request->entretien_id),
            ]);

            // Return the entretien detail
            return response()->json(Entretien_detail::find($id));
        } else if ($request->type === 'other') {
            // Validate the request
            $this->validate($request, [
                'entretien_id'   => 'required',
                'description'    => 'required|string',
                'price'          => 'required|integer',
                'duration'       => 'nullable',
            ]);

            // Create the entretien detail
            $id = Entretien_detail::insertGetId([
                'item_type'         => 'other_accessory',
                'amount'            => floatval($request->price),
                'description'       => $request->description,
                'duration'          => intval($request->duration),
                'entretiens_id'     => intval($request->entretien_id),
            ]);

            // Return the entretien detail
            return response()->json(Entretien_detail::find($id));
        }
    }

    // Update one entretien detail
    public function update(Request $request, $id)
    {
        if ($request->status == 'done') {
            $return_array = [];
            $accessories    = Entretien_detail::select('*')->where('entretiens_id', intval($id))->where('item_type', 'accessory')->get();
            $entretien      = Entretien::find($id);
            $bike           = Accessory::find($entretien['bikes_id']);
            if (count($accessories) > 0) {
                foreach ($accessories as $accessory) {
                    if ($accessory['item_id']) {
                        // Verification si l'accessoire qui a la bonne taille et le meme catalog_id existe deja dans le stock
                        $number_accessories = Accessory::where('accessories_catalog_id', $accessory['item_id'])->where('contract_type', 'stock')->where('size', $accessory['accessory_size'])->where('companies_id', null)->where('bikes_id', null)->count();
                        if ($number_accessories > 0) {
                            $stock_id   = Accessory::select('id')->where('accessories_catalog_id', $accessory['item_id'])->where('size', $accessory['accessory_size'])->first()->id;
                            Accessory::where('id', $stock_id)->update([
                                'contract_type' => 'selling',
                                'selling_price' => $accessory['amount'],
                                'selling_date'  => $entretien['date'],
                                'bikes_id'      => $entretien['bikes_id'],
                                'companies_id'  => $bike['companies_id'],
                                'users_id'      => ($bike['users_id'] !== null) ? $bike['users_id'] : null,
                            ]);
                            array_push($return_array, Accessory::find($stock_id));
                        }
                    }
                }
            }
            return response()->json($return_array);
        }
        return response()->json([]);
    }

    // Update one entretien detail when one maintenance goes from done back to todo
    public function updateReverse(Request $request, $id)
    {
        if ($request->status == 'todo') {
            $return_array = [];
            $accessories    = Entretien_detail::select('*')->where('entretiens_id', intval($id))->where('item_type', 'accessory')->get();
            $entretien      = Entretien::find($id);
            $bike           = Accessory::find($entretien['bikes_id']);
            if (count($accessories) > 0) {
                foreach ($accessories as $accessory) {
                    if ($accessory['item_id']) {
                        // Verification si l'accessoire qui a la bonne taille et le meme catalog_id existe deja dans le stock
                        $number_accessories = Accessory::where('accessories_catalog_id', $accessory['item_id'])->where('contract_type', 'selling')->where('size', $accessory['accessory_size'])->count();
                        if ($number_accessories > 0) {
                            $stock_id   = Accessory::select('id')->where('accessories_catalog_id', $accessory['item_id'])->where('size', $accessory['accessory_size'])->first()->id;
                            Accessory::where('id', $stock_id)->update([
                                'contract_type' => 'stock',
                                'selling_price' => null,
                                'selling_date'  => null,
                                'bikes_id'      => null,
                                'companies_id'  => null,
                                'users_id'      => null,
                            ]);
                            array_push($return_array, Accessory::find($stock_id));
                        }
                    }
                }
            }
            return response()->json($return_array);
        }
        return response()->json([]);
    }
}
