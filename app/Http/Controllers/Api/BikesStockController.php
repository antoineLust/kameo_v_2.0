<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Bike;
use App\Models\Company;
use App\Models\Entretien;
use App\Models\Reservation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class BikesStockController extends Controller
{
    // Get all bikes stock
    public function index()
    {
        return response()->json(Bike::with('catalog', 'company')->get());
    }

    public function getSharedBikesByCompanyId($id)
    {
        return response()->json(Bike::with('catalog')->where('companies_id', $id)->get());
    }

    public function getBikesByCompanyId($id)
    {
        return response()->json(Bike::with('catalog', 'company')->where('companies_id', $id)->get());
    }

    public function getBikesByCatalogIdAndSize(Request $request)
    {
        return response()->json(Bike::with('catalog', 'company')
            ->where('bikes_catalog_id', $request->catalogId)
            ->where('size', $request->size)
            ->where('contract_type', 'stock')
            ->whereIn('companies_id', [null, 12])
            ->where('users_id', null)
            ->get());
    }

    public function getBikesWithSelect()
    {
        return response()->json(Bike::with('catalog')->get());
    }

    public function getBikesNotAssembled()
    {
        $not_assembled_bikes = Bike::with('catalog', 'company')           
            ->leftJoin('bikes_assembly', 'bikes.id', '=', 'bikes_assembly.bikes_id') 
            ->whereNull('bikes_assembly.bikes_id')          
            ->where('assembled', 0)      
            ->get(['bikes.id', 'bikes.frame_reference', 'bikes.companies_id', 'bikes.bikes_catalog_id']);
        return response()->json($not_assembled_bikes);
    }

    public function getBikesByUserAuth($id)
    {
        return response()->json(Bike::with('catalog', 'company')->where('users_id', $id)->get());
    }

    public function getBikeStockById($id)
    {
        return response()->json(Bike::with('catalog', 'company')->find($id));
    }

    // Get bike stock by id
    public function show($id)
    {
        $bike = Bike::with('catalog', 'company')->find($id);
        if ($bike['users_id'] != null) {
            $bike['utilisation_type'] = 'staff';
        } else {
            $bike['utilisation_type'] = 'shared';
        }
        return response()->json($bike);
    }

    // Create new bike stock
    public function create(Request $request)
    {

        // Validate request
        $request->validate([
            'client_name'                               => 'nullable|string',
            'frame_number'                              => 'required|string|min:1',
            'size'                                      => 'required|string',
            'color'                                     => 'string|nullable',
            'brand'                                     => 'required|string',
            'contract_type'                             => 'nullable|string',
            'contract_start'                            => 'date|nullable',
            'contract_end'                              => 'date|nullable',
            'estimated_delivery_date'                   => 'date|nullable',
            'delivery_date'                             => 'date|nullable',
            'selling_date'                              => 'date|nullable',
            'frame_reference'                           => 'string|nullable',
            'key_reference'                             => 'string|nullable',
            'locker_reference'                          => 'string|nullable',
            'plate_number'                              => 'string|nullable',
            'billing_type'                              => 'string|nullable',
            'leasing_price'                             => 'numeric|nullable',
            'selling_price'                             => 'numeric|nullable',
            'usable'                                    => 'required|string|max:1',
            'insurance'                                 => 'nullable|string|max:1',
            'insurance_individual'                      => 'nullable|string|max:1',
            'insurance_civil_responsibility'            => 'nullable|string|max:1',
            'insurance_civil_responsibility_contract'   => 'string|nullable ',
            'bike_price'                                => 'numeric|nullable',
            'bike_buying_date'                          => 'date|nullable',
            'billing_group'                             => 'integer|max:1|nullable',
            'gps_id'                                    => 'string|nullable',
            'localisation'                              => 'string|nullable',
            'address'                                   => 'string|nullable',
            'comment_billing'                           => 'string|nullable',
            'stolen_date'                               => 'date|nullable',
            'reimbursement_insurance'                   => 'numeric|nullable',
            'status'                                    => 'required|string',
            'bikes_catalog_id'                          => 'required|integer|exists:bikes_catalog,id',
            'companies_id'                              => 'nullable|integer|exists:companies,id',
        ]);

        // Create new bike stock
        $id = Bike::insertGetId([
            'client_name'                               => 'KAMEO bikes',
            'frame_number'                              => $request['frame_number'],
            'size'                                      => $request['size'],
            'usable'                                    => $request['usable'],
            'contract_type'                             => 'order',
            'companies_id'                              => null,
            'bikes_catalog_id'                          => $request['bikes_catalog_id'],
            'status'                                    => $request['status'],
            'color'                                     => ($request['color'] != null) ? $request['color'] : null,
            'contract_start'                            => null,
            'contract_end'                              => null,
            'estimated_delivery_date'                   => ($request['estimated_delivery_date'] != null) ? $request['estimated_delivery_date'] : null,
            'delivery_date'                             => null,
            'selling_date'                              => null,
            'frame_reference'                           => null,
            'key_reference'                             => null,
            'locker_reference'                          => null,
            'plate_number'                              => null,
            'billing_type'                              => null,
            'leasing_price'                             => null,
            'selling_price'                             => null,
            'bike_price'                                => ($request['bike_price'] != null) ? $request['bike_price'] : null,
            'bike_buying_date'                          => ($request['bike_buying_date'] != null) ? $request['bike_buying_date'] : null,
            'billing_group'                             => null,
            'gps_id'                                    => null,
            'localisation'                              => null,
            'address'                                   => null,
            'comment_billing'                           => ($request['comment_billing'] != null) ? $request['comment_billing'] : null,
            'insurance'                                 => 'N',
            'insurance_individual'                      => 'N',
            'insurance_civil_responsibility'            => 'N',
            'insurance_civil_responsibility_contract'   => null,
            'stolen_date'                               => null,
            'reimbursement_insurance'                   => null,
        ]);

        // Return created bike stock
        return response()->json(Bike::with('company', 'catalog')->find($id));
    }

    // Update bike stock
    public function update(Request $request)
    {

        // Decode json
        $request->form = json_decode($request->form, true);

        // Set new company name by companies id
        if ($request->form['companies_id'] != null) {
            $newCompanyName                 = Company::find($request->form['companies_id'])->name;
            $request->form['client_name']   = $newCompanyName;
        } else {
            if ($request->form['contract_type'] === 'order' || $request->form['contract_type'] === 'stock') {
                $request->form['client_name'] = 'KAMEO bikes';
            } else {
                $request->form['client_name'] = null;
            }
        }
        // Validations
        Validator::make($request->form, [
            'client_name'                               => 'required|string',
            'frame_number'                              => 'required|string|min:1',
            'size'                                      => 'required|string',
            'color'                                     => 'string|nullable',
            'contract_type'                             => 'required|string',
            'contract_start'                            => 'date|nullable|required_if:contract_type,leasing',
            'contract_end'                              => 'date|nullable|required_if:contract_type,leasing',
            'estimated_delivery_date'                   => 'date|nullable',
            'delivery_date'                             => 'date|nullable',
            'selling_date'                              => 'date|nullable|required_if:contract_type,selling',
            'frame_reference'                           => 'string|required_unless:contract_type,order|nullable',
            'key_reference'                             => 'string|nullable',
            'locker_reference'                          => 'string|nullable',
            'plate_number'                              => 'string|nullable',
            'billing_type'                              => 'string|nullable|required_if:contract_type,leasing',
            'leasing_price'                             => 'numeric|nullable|required_if:contract_type,leasing',
            'selling_price'                             => 'numeric|nullable|required_if:contract_type,selling',
            'usable'                                    => 'required|string|max:1',
            'insurance'                                 => 'required|string|max:1',
            'insurance_individual'                      => 'required|string|max:1',
            'insurance_civil_responsibility'            => 'required|string|max:1',
            'insurance_civil_responsibility_contract'   => 'string|nullable ',
            'bike_price'                                => 'numeric|nullable',
            'bike_buying_date'                          => 'date|nullable',
            'billing_group'                             => 'integer|nullable',
            'gps_id'                                    => 'string|nullable',
            'localisation'                              => 'string|nullable',
            'address'                                   => 'string|nullable',
            'comment_billing'                           => 'string|nullable',
            'stolen_date'                               => 'date|nullable|required_if:contract_type,stolen',
            'reimbursement_insurance'                   => 'numeric|nullable|required_if:contract_type,stolen',
            'display_group'                             => 'required|string',
            'status'                                    => 'required|string',
            'bikes_catalog_id'                          => 'integer|nullable|exists:bikes_catalog,id',
            'companies_id'                              => 'nullable|integer|exists:companies,id',
            'users_id'                                  => 'nullable|integer|required_if:utilisation_type,staff',
            'utilisation_type'                          => 'nullable|string',
        ])->validate();

        // If leasing is end delete all futur entretiens
        if ($request->form['contract_type'] === 'selling' && Bike::find($request->form['id'])->status === 'leasing' && Entretien::where('bikes_id', $request->form['id'])->where('planned_date', '>', date('Y-m-d'))->count() === 0) {
            // Delete all futur entretiens
            Entretien::where('bikes_id', $request->form['id'])->where('planned_date', '>', date('Y-m-d'))->delete();
        } else if ($request->form['contract_type'] === 'selling') {
            // Delete all futur entretiens
            Entretien::where('bikes_id', $request->form['id'])->where('planned_date', '>', date('Y-m-d'))->delete();
            // Add new entretien after 3 months
            Entretien::insert([
                'bikes_id'                               => $request->form['id'],
                'planned_date'                           => date('Y-m-d', strtotime('+3 months')),
                'external_bike'                          => 0,
                'number_entretien'                       => 1,
                'assembled'                              => 1,
                'description'                            => 'Premier entretien - Gratuit',
                'status'                                 => 'automaticaly_planned',
            ]);
        } else if ($request->form['contract_type'] === 'leasing') {
            // Delete all futur entretiens
            Entretien::where('bikes_id', $request->form['id'])->where('planned_date', '>', date('Y-m-d'))->delete();
            // Add 4 new entretiens, first one after 3 months and the others each year depending on number of years between start and end
            $startDate      = date_create($request->form['contract_start']);
            $endDate        = date_create($request->form['contract_end']);
            $diff           = date_diff($startDate, $endDate);
            $numberOfYears  = $diff->y + 1;
            for ($i = 1; $i <= $numberOfYears; $i++) {
                if ($i === 1) {
                    $date = date_create($request->form['contract_start']);
                    date_add($date, date_interval_create_from_date_string('3 months'));
                } else {
                    $date = date_create($request->form['contract_start']);
                    date_add($date, date_interval_create_from_date_string($i - 1 . ' years'));
                }
                Entretien::insert([
                    'bikes_id'                               => $request->form['id'],
                    'planned_date'                           => date_format($date, 'Y-m-d'),
                    'external_bike'                          => 0,
                    'number_entretien'                       => $i,
                    'assembled'                              => 1,
                    'description'                            => 'Entretien automatique n°' . $i,
                    'status'                                 => 'automaticaly_planned',
                ]);
            }
        } else if ($request->form['contract_type'] !== 'leasing' || $request->form['contract_type'] !== 'selling') {
            // Delete all futur entretiens
            Entretien::where('bikes_id', $request->form['id'])->where('planned_date', '>', date('Y-m-d'))->delete();
        }

        // Update bike stock
        Bike::where('id', $request->id)
            ->update([
                'client_name'                               => $request->form['client_name'],
                'frame_number'                              => $request->form['frame_number'],
                'size'                                      => $request->form['size'],
                'color'                                     => $request->form['color'],
                'contract_type'                             => $request->form['contract_type'],
                'contract_start'                            => $request->form['contract_start'],
                'contract_end'                              => $request->form['contract_end'],
                'estimated_delivery_date'                   => $request->form['estimated_delivery_date'],
                'delivery_date'                             => $request->form['delivery_date'],
                'selling_date'                              => $request->form['selling_date'],
                'frame_reference'                           => $request->form['frame_reference'],
                'key_reference'                             => $request->form['key_reference'],
                'locker_reference'                          => $request->form['locker_reference'],
                'plate_number'                              => $request->form['plate_number'],
                'billing_type'                              => $request->form['billing_type'],
                'leasing_price'                             => $request->form['leasing_price'],
                'selling_price'                             => $request->form['selling_price'],
                'usable'                                    => $request->form['usable'],
                'insurance'                                 => $request->form['insurance'],
                'insurance_individual'                      => $request->form['insurance_individual'],
                'insurance_civil_responsibility'            => $request->form['insurance_civil_responsibility'],
                'insurance_civil_responsibility_contract'   => $request->form['insurance_civil_responsibility_contract'],
                'bike_price'                                => $request->form['bike_price'],
                'bike_buying_date'                          => $request->form['bike_buying_date'],
                'billing_group'                             => $request->form['billing_group'],
                'gps_id'                                    => $request->form['gps_id'],
                'localisation'                              => ($request->form['contract_type'] === 'stock') ? 'Quai de Rome' : $request->form['localisation'],
                'address'                                   => $request->form['address'],
                'comment_billing'                           => $request->form['comment_billing'],
                'stolen_date'                               => $request->form['stolen_date'],
                'reimbursement_insurance'                   => $request->form['reimbursement_insurance'],
                'display_group'                             => $request->form['display_group'],
                'status'                                    => $request->form['status'],
                'users_id'                                  => ($request->form['utilisation_type'] == 'staff') ? $request->form['users_id'] : null,
                'bikes_catalog_id'                          => $request->form['bikes_catalog_id'],
                'companies_id'                              => $request->form['companies_id'],
            ]);

        $bike = Bike::with('catalog', 'company')->find($request->form['id']);
        $bike['utilisation_type'] = $request->form['utilisation_type'];

        // Return updated bike
        return response()->json($bike);
    }

    // Update bike stock
    public function receptionCommande(Request $request)
    {

        // Set new company name by companies id
        // Update bike stock
        Bike::where('id', $request['id'])
            ->update([
                'contract_type'                             => $request['contract_type'],
            ]);

        $bike = Bike::with('catalog', 'company')->find($request['id']);
        // Return updated bike
        return response()->json($bike);
    }

    // Delete bike
    public function destroy($id)
    {
        Bike::where('id', $id)->update([
            'status' => 'inactif',
        ]);
        return response()->json(Bike::with('catalog', 'company')->find($id));
    }

    // Get available bikes for sharing plan
    public function getAvailablesBikeForSharingPlan(Request $request)
    {
        $formData       = $request->all();
        $user           = User::with('company')->find(Auth::user()->id);
        $reservations   = Reservation::all();
        $bikesAvailable = [];

        // Get bikes available
        foreach ($formData['bikes'] as $bike) {

            // Initialize variable
            $bikeAvailable = true;
            // Create time by start_date, start_time, end_date, end_time
            $requestedStartDate = date_create($formData['start_date'] . ' ' . $formData['start_time'] . ':00');
            $requestedEndDate   = date_create($formData['end_date'] . ' ' . $formData['end_time'] . ':00');
            // Check if bike is already reserved
            foreach ($reservations as $reservation) {
                // Create date by start and end
                $reservationStartDate = date_create($reservation->start);
                $reservationEndDate   = date_create($reservation->end);
                // L'utilisateur ne peut pas réserver un vélo qui à une réservation en cours, qui n'est pas disponible dans la tranche horaire demandée et qui na pas un lieu d'arrivée identique au lieu de départ (Sécurité en cas ou l'utilisateur a un soucis et n'enmene pas le vélo à la bonne adresse)
                if($reservation->bikes_id == $bike && $reservation->status == 'OK' && !($reservationEndDate <= $requestedStartDate || $reservationStartDate >= $requestedEndDate)){
                    $bikeAvailable = false;
                }
                // Get the last reservation for this bike
                $lastReservation = Reservation::where('bikes_id', $bike)->where('status', 'OK')->orderBy('end', 'desc')->get();
                if (count(($lastReservation)) > 0) {
                    if (intval($lastReservation[0]['building_end']) !== $formData['start_location']) {
                        $bikeAvailable = false;
                    }
                }
            }
            if ($bikeAvailable) {
                $bikesAvailable[] = $bike;
            }
        }

        return response()->json($bikesAvailable);
    }

    // Get datatable info
    public function getDataForDataTable(Request $request)
    {
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        $sortDirection = json_decode($request->sortDirection, true);
        $contractType = $request->contractTypeFilter;
        if ($search) {
            $q = explode(' ', $search);
            $res = Bike::join('bikes_catalog', 'bikes.bikes_catalog_id', '=', 'bikes_catalog.id')
                ->join('companies', 'bikes.companies_id', '=', 'companies.id')
                ->select('companies.name as client', 'bikes_catalog.brand', 'bikes_catalog.model', 'contract_type', 'selling_price', 'leasing_price', 'bikes.id', 'bikes.frame_number', 'bikes.users_id as utilisation', 'bikes_catalog.price_htva', 'bikes.key_reference', 'bikes.locker_reference');
            for ($i = 0; $i < count($q); $i++) {
                $res = $res->where(function ($query) use ($q, $i) {
                    $query->where('companies.name', 'like', '%' . $q[$i] . '%')
                        ->orWhere('bikes_catalog.brand', 'like', '%' . $q[$i] . '%')
                        ->orWhere('bikes_catalog.model', 'like', '%' . $q[$i] . '%')
                        ->orWhere('bikes.frame_number', 'like', '%' . $q[$i] . '%')
                        ->orWhere('bikes.users_id', 'like', '%' . $q[$i] . '%')
                        ->orWhere('bikes.id', 'like', '%' . $q[$i] . '%')
                        ->orWhere('bikes.key_reference', 'like', '%' . $q[$i] . '%')
                        ->orWhere('bikes.locker_reference', 'like', '%' . $q[$i] . '%')
                        ->orWhere('bikes.contract_type', 'like', '%' . $q[$i] . '%');
                });
            }
            if ($contractType == 'all') {
                $bikes = $res
                    ->orderBy($sortDirection['column'], $sortDirection['direction'])
                    ->limit($limit)
                    ->offset(($page - 1) * $limit)
                    ->get();
            } else {
                $bikes = $res
                    ->where('bikes.contract_type', $contractType)
                    ->orderBy($sortDirection['column'], $sortDirection['direction'])
                    ->limit($limit)
                    ->offset(($page - 1) * $limit)
                    ->get();
            }
        } else {
            if ($contractType == "all") {
                $bikes = Bike::join('bikes_catalog', 'bikes.bikes_catalog_id', '=', 'bikes_catalog.id')
                    ->join('companies', 'bikes.companies_id', '=', 'companies.id')
                    ->select('companies.name as client', 'bikes_catalog.brand', 'bikes_catalog.model', 'contract_type', 'selling_price', 'leasing_price', 'bikes.id', 'bikes.frame_number', 'bikes.users_id as utilisation', 'bikes_catalog.price_htva')
                    ->orderBy($sortDirection['column'], $sortDirection['direction'])
                    ->limit($limit)
                    ->offset(($page - 1) * $limit)
                    ->get();
            } else {
                $bikes = Bike::join('bikes_catalog', 'bikes.bikes_catalog_id', '=', 'bikes_catalog.id')
                    ->join('companies', 'bikes.companies_id', '=', 'companies.id')
                    ->select('companies.name as client', 'bikes_catalog.brand', 'bikes_catalog.model', 'contract_type', 'selling_price', 'leasing_price', 'bikes.id', 'bikes.frame_number', 'bikes.users_id as utilisation', 'bikes_catalog.price_htva')
                    ->where('contract_type', $contractType)
                    ->orderBy($sortDirection['column'], $sortDirection['direction'])
                    ->limit($limit)
                    ->offset(($page - 1) * $limit)
                    ->get();
            }
        }
        foreach ($bikes as $bike) {
            $bike->leasing_price = number_format($bike->leasing_price * 1.21, 2, ',', ' ');
            $bike->selling_price = number_format($bike->selling_price * 1.21, 2, ',', ' ');
            $bike->price_htva = number_format($bike->price_htva * 1.21, 2, ',', ' ');
            if ($bike->contract_type == 'monthly_leasing') {
                $bike['price'] = $bike->leasing_price . '€/mois';
            } else if ($bike->contract_type == 'leasing') {
                $bike['price'] = $bike->leasing_price . '€/mois';
            } else if ($bike->contract_type == 'annual_leasing') {
                $bike['price'] = $bike->leasing_price . '€/an';
            } else if ($bike->contract_type == 'preFinanced_leasing') {
                $bike['price'] = $bike->leasing_price . '€/mois';
            } else if ($bike->contract_type == 'selling') {
                $bike['price'] = $bike->selling_price;
            } else {
                $bike['price'] = $bike->price_htva . '€';
            }
            if ($bike->utilisation) {
                $bike->utilisation = 'Personnel';
            } else {
                $bike->utilisation = 'Partagé';
            }
        }
        return response()->json($bikes);
    }
}
