<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Company;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\User_address;
use Error;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use MailchimpTransactional\ApiClient;
use Stevebauman\Location\Facades\Location;

class UsersController extends Controller
{
    // Get user position
    public function getLocation(Request $request){
        $clientIP = $request->ip();
        // Changer l'ip manuelle ci dessous par $clientIP
        return response()->json(Location::get('178.51.185.234'));
    }
    
    public function getUserById($id){
        return response()->json(User::with('company', 'address')->find($id));
    }
    
    public function getEmployee($role){
        return response()->json(User::with('company', 'address')->where('employee_role', $role)->where('employee', 1)->get());
    }

    public function getFirstnameAndLastnameById($id){
        $exploded = explode(",", $id);
        if(count($exploded) > 1){
            $userArray = [];
            foreach($exploded as $user_id){
                $user = User::find(intval($user_id));
                array_push($userArray, $user->firstname . ' ' . $user->lastname);  
            }
            return response()->json(join(", ", $userArray));
        }else if(count($exploded) === 1){
            $user = User::find(intval($id));
            return response()->json($user->firstname . ' ' . $user->lastname);
        }       
    }

    public function getUsersByCompany($id){
        return response()->json(User::with('company', 'address')->where('companies_id', $id)->get());
    }

    public function getEmployeesByWorkAddress($id){
        return response()->json(User::with('company', 'address')->where('users_works_addresses_id', $id)->get());
    }

    // Get all users
    public function index(){
        return response()->json(User::with('company', 'address')->get());
    }

    public function getUserByCompany($id){
        return response()->json(User::select('id', 'firstname', 'lastname')->where('companies_id', $id)->get());
    }

    // Get user by id
    public function show($id){
        return response()->json(User::with('company', 'address')->where('id', intval($id))->first());

    }

    // Get anthenticate user
    public function auth(){
        if(Auth::check()){
            return response()->json(User::with('company', 'address')->where('id', Auth::user()->id)->first());
        }else{
            return response()->json(NULL);
        }
        return response()->json();
    }

    // Update user
    public function update(Request $request){
        // Decode json
        $request->form = json_decode($request->form, true);

        // Validations criteria
        Validator::make($request->form, [
            'firstname'         => 'required|string|max:255',
            'lastname'          => 'required|string|max:255',
            'email'             => 'required|unique:users,email,' .$request->form['id'],
            'phone'             => 'max:15',
            'size'              => 'nullable|max:3',
            'permission'        => 'required',
            'status'            => 'required',
            'address.condensed' => 'nullable|string|max:255',
        ])->validate();

        // Upload image if exists
        if($request->avatar){

            // Delete old image
            if($request->form['avatar'] != 'users/profil-pictures/default.png'){
                Storage::disk('public')->delete($request->form['avatar']);
            }

            // Image validation
            $request->validate([
                'avatar' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            // Upload image
            $path = $request->file('avatar')->storeAs('users/profil-pictures/' . time() . '-' . uniqid() . '.png', '', 'public');
        }

        // Update user
        User::where('id', $request->form['id'])
            ->update([
                'firstname'     => $request->form['firstname'],
                'lastname'      => $request->form['lastname'],
                'email'         => $request->form['email'],
                'size'          => $request->form['size'],
                'phone'         => $request->form['phone'],
                'permission'    => $request->form['permission'],
                'avatar'        => ($request->avatar) ? $path : $request->form['avatar'],
                'status'        => $request->form['status'],
            ]);
        User_address::where('id', $request->form['address']['id'])
            ->update([
                'street'    => $request->form['address']['condensed'] !== null ? explode(', ', $request->form['address']['condensed'])[1] : null,
                'number'    => $request->form['address']['condensed'] !== null ? explode(', ', $request->form['address']['condensed'])[0] : null,
                'city'      => $request->form['address']['condensed'] !== null ? explode(', ', $request->form['address']['condensed'])[3] : null,
                'zip'       => $request->form['address']['condensed'] !== null ? explode(', ', $request->form['address']['condensed'])[2] : null,
                'country'   => $request->form['address']['condensed'] !== null ? explode(', ', $request->form['address']['condensed'])[4] : null,
                'condensed' => $request->form['address']['condensed'] !== null ? $request->form['address']['condensed'] : null
            ]);

        // Return response
        return response()->json(User::with('company', 'address')->where('id', $request->form['id'])->first());
    }

    // Destroy user
    public function destroy(Request $request){

        // Delete user
        User::where('id', $request->userId)
            ->update([
                'status'    => 'inactif'
            ]);

        // Return response
        return response()->json(User::find($request->userId));
    }

    // Create user
    public function create(Request $request){
        // Validations criteria
        $request->validate([
            'firstname'     => 'required|string|max:255',
            'lastname'      => 'required|string|max:255',
            'email'         => 'required|email|unique:users,email',
            'phone'         => 'required',
            'size'          => 'min:3|max:3|nullable',
            'company'       => 'required',
            'permission'    => 'nullable',
            'address'       => 'nullable|string|max:255',
        ]);

        // Generate a random password
        function generatePassword()
        {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return substr(str_shuffle($chars), 0, 8);
        }
        $passwordNotHashed  = generatePassword();
        $passwordHashed     = Hash::make($passwordNotHashed);

        $adresse = ($request->address !== null) ? [...explode(', ', $request->address), $request->address] : array_fill(0, 6, null);
        // Create user
        $id = User::insertGetId([
                'firstname'                 => $request->firstname,
                'lastname'                  => $request->lastname,
                'password'                  => $passwordHashed,
                'email'                     => $request->email,
                'size'                      => $request->size,
                'phone'                     => $request->phone,
                'permission'                => $request->permission,
                'companies_id'              => $request->company,
                'users_addresses_id'        => DB::table('users_addresses')->insertGetId([
                                                    'street'    => $adresse[1],
                                                    'number'    => $adresse[0],
                                                    'city'      => $adresse[3],
                                                    'zip'       => $adresse[2],
                                                    'country'   => $adresse[4],
                                                    'condensed' => $adresse[5]
                                                ]),
            ]);

            // Send email
            if(config('app.env') === 'local'){
                function sendMail($message){
                    try {
                        $mailchimp = new ApiClient();
                        $mailchimp->setApiKey(config('services.mailchimp.key'));
                
                        $mailchimp->messages->send(["message" => $message]);
                    } catch (Error $e) {
                        echo 'Error: ', $e->getMessage(), "\n";
                    }
                }
                
                $message = [
                    "from_name"     => "KAMEO Bikes",
                    "from_email"    => "info@kameobikes.com",
                    "subject"       => "Welcome to KAMEO Bikes",
                    "html"          => view('emails.welcome', [
                            'password'  => $passwordNotHashed,
                            'firstname' => $request->firstname,
                            'email'     => $request->email,
                            'company'   => Company::find($request->company)->name
                        ])->render(),
                    "to" => [
                        [
                            "type"  => "to",
                            "email" => "benjamin@kameobikes.com",
                            "name"  => "Benjamin Van Rentegrhem"
                        ]
                    ]
                ];
                sendMail($message);
            }
            else if(config('app.env') === 'production'){ 
                function sendMail($message){
                    try {
                        $mailchimp = new ApiClient();
                        $mailchimp->setApiKey(config('services.mailchimp.key'));
                
                        $mailchimp->messages->send(["message" => $message]);
                    } catch (Error $e) {
                        echo 'Error: ', $e->getMessage(), "\n";
                    }
                }
                
                $message = [
                    "from_name"     => "KAMEO Bikes",
                    "from_email"    => "info@kameobikes.com",
                    "subject"       => "Welcome to KAMEO Bikes",
                    "html"          => view('emails.welcome', [
                            'password'  => $passwordNotHashed,
                            'firstname' => $request->firstname,
                            'email'     => $request->email,
                            'company'   => Company::find($request->company)->name
                        ])->render(),
                    "to" => [
                        [
                            "type"  => "to",
                            "email" => $request->email,
                            "name"  => $request->firstname . ' ' . $request->lastname
                        ]
                    ]
                ];
                sendMail($message);
            }

        // Return response
        return response()->json(User::select('*')->with('company')->where('id', $id)->first());
    }

    // Update password by user id 
    public function updatePassword(Request $request, $id){

        // Define user
        $user = User::find($id);

        // Validations criteria
        $request->validate([
            'old'       => 'required|string|password' ,
            'new'       => 'required|string|min:8',
            'confirm'   => 'required|string|min:8|same:new',
        ]);

        // Update password
        $user->password = Hash::make($request->new);
        $user->save();

        // Return response
        return response()->json(User::with('company', 'address')->where('id', $id)->first());
    }

    // Get datatable info
    public function getDataForDataTable(Request $request){
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        $sortDirection = json_decode($request->sortDirection, true);
        if($search){
            $q = explode(' ', $search);
            $res = User::join('companies', 'users.companies_id', '=', 'companies.id')
                ->select('users.id', 'users.firstname', 'users.lastname', 'users.phone', 'companies.name as client');
            for($i = 0; $i < count($q); $i++){
                $res->where(function($query) use ($q, $i){
                    $query->where('users.firstname', 'like', '%' . $q[$i] . '%')
                        ->orWhere('users.lastname', 'like', '%' . $q[$i] . '%')
                        ->orWhere('users.phone', 'like', '%' . $q[$i] . '%')
                        ->orWhere('companies.name', 'like', '%' . $q[$i] . '%');
                });
            }
            return $res->orderBy($sortDirection['column'], $sortDirection['direction'])
                ->limit($limit)
                ->offset(($page - 1) * $limit)
                ->get();
        }
        else{
            $companies = User::join('companies', 'users.companies_id', '=', 'companies.id')
            ->select('users.id', 'users.firstname', 'users.lastname', 'users.phone', 'companies.name as client')->orderBy($sortDirection['column'], $sortDirection['direction'])->limit($limit)->offset(($page - 1) * $limit)->get();
        }
        return response()->json($companies);
    }

    public function assistance(Request $request){
        $request->validate([
            'description' => 'required|string',
        ]);

        $user = Auth::user();

        // Send email
        function sendMail($message){
            try {
                $mailchimp = new ApiClient();
                $mailchimp->setApiKey(config('services.mailchimp.key'));
        
                $mailchimp->messages->send(["message" => $message]);
            } catch (Error $e) {
                echo 'Error: ', $e->getMessage(), "\n";
            }
        }
        
        $message = [
            "from_name"     => "KAMEO Bikes",
            "from_email"    => "info@kameobikes.com",
            "subject"       => "Assistance request",
            "html"          => view('emails.assistanceRequest', [
                    'user'          => $user,
                    'description'   => $request->description,
                    'company'       => Company::find($user['companies_id'])->name
                ])->render(),
            "to" => [
                [
                    "type"  => "to",
                    "email" => (config('app.env') === 'local') ? "benjamin@kameobikes.com" : "antoine@kameobikes.com",
                    "name"  => (config('app.env') === 'local') ? "Benjamin Van Rentegrhem" : "Antoine Lust"
                ]
            ]
        ];
        sendMail($message);
    }
}
