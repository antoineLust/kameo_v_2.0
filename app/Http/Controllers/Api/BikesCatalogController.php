<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Bikes_Catalog;
use App\Models\Bike;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class BikesCatalogController extends Controller
{
    // Get all bikes catalog
    public function index()
    {
        $bikes = Bikes_Catalog::all();
        foreach ($bikes as $bike) {
            $bike->leasingPrice     = round(($bike->price_htva * (1 - 0.27) * 1.7 + (4 * 100 + 3 * 84) * 1.3) / 36, 2);
            $bike->fullLeasingPrice = round(($bike->price_htva + (3 * 84 + (700 - 400) / (5000 - 2000) * $bike->price_htva) + 33.3333) * (1 + 0.3), 2);
            $bike->sizes            = explode(',', $bike->sizes);
        }
        return response()->json($bikes);
    }

    public function getModelsAndBrands()
    {
        return response()->json(Bikes_Catalog::all('id', 'brand', 'model'));
    }

    public function getBikeCatalogById($id)
    {
        return response()->json(Bikes_Catalog::select('id', 'brand', 'model', 'price_htva')->find($id));
    }

    public function getSizesModel($id)
    {

        $bike = Bikes_Catalog::find($id);

        if (isset($bike)) {
            return response()->json(explode(',', $bike->sizes));
        } else {
            return response()->json([]);
        }
    }

    public function getModelsFromBrand($brand)
    {
        return response()->json(Bikes_Catalog::select('id', 'model', 'price_htva')->where('brand', $brand)->get());
    }

    // Get one bike catalog
    public function show($id)
    {
        // for($i = 1; $i <= 250; $i++){
        //     File::ensureDirectoryExists(public_path('/storage/bikes_pictures/' . $i));
        //     File::ensureDirectoryExists(public_path('/storage/accessories_pictures/' . $i));
        // }
        $path               = storage_path('app/public/bikes_pictures/' . $id);
        $bike               = Bikes_Catalog::find($id);
        if (is_dir($path)) {
            $files              = scandir($path);
            $files              = array_diff($files, array('.', '..'));
            $bike->pictures     = $files;
        }
        $bike->sizes        = explode(',', $bike->sizes);
        return response()->json($bike);
    }

    // Create bikes catalog
    public function create(Request $request)
    {

        // Decode json
        $request->form = json_decode($request->form, true);
        // Validations rules
        Validator::make($request->form, [
            'model'             => 'required|string',
            'brand'             => 'required|string',
            'frame_type'        => 'required|string',
            'utilisation'       => 'required|string',
            'electric'          => 'required|string',
            'sizes'             => 'required',
            'season'            => 'required',
            'motor'             => 'string|nullable',
            'battery'           => 'string|nullable',
            'transmission'      => 'string|nullable',
            'status'            => 'required|string',
            'minimal_stock'     => 'required',
            'optimal_stock'     => 'required',
            'buying_price'      => 'required',
            'price_htva'        => 'required',
            'display'           => 'required|string',
            'wheel_size'        => 'integer|nullable',
            'fork'              => 'string|nullable',
            'absorber'          => 'string|nullable',
            'gears'             => 'string|nullable',
            'shifter'           => 'string|nullable',
            'cassette'          => 'string|nullable',
            'brakes'            => 'string|nullable',
            'front_brakes_size' => 'integer|nullable',
            'back_brakes_size'  => 'integer|nullable',
            'weight'            => 'integer|nullable',
        ])->validate();

        // Create bikes catalog and return id
        $id = Bikes_Catalog::insertGetId([
            'model'             => $request->form['model'],
            'brand'             => $request->form['brand'],
            'frame_type'        => $request->form['frame_type'],
            'utilisation'       => $request->form['utilisation'],
            'electric'          => $request->form['electric'],
            'sizes'             => implode(',', $request->form['sizes']),
            'season'            => $request->form['season'],
            'motor'             => array_key_exists('motor', $request->form) ? $request->form['motor'] : null,
            'battery'           => array_key_exists('battery', $request->form) ? $request->form['battery'] : null,
            'transmission'      => array_key_exists('transmission', $request->form) ? $request->form['transmission'] : null,
            'status'            => $request->form['status'],
            'minimal_stock'     => ($request->form['minimal_stock'] === NULL) ? 0 : $request->form['minimal_stock'],
            'optimal_stock'     => ($request->form['optimal_stock'] === NULL) ? 0 : $request->form['optimal_stock'],
            'buying_price'      => $request->form['buying_price'],
            'price_htva'        => $request->form['price_htva'],
            'display'           => $request->form['display'],
            'wheel_size'        => isset($request->form['wheel_size']) ? $request->form['wheel_size'] : null,
            'fork'              => isset($request->form['fork']) ? $request->form['fork'] : null,
            'absorber'          => isset($request->form['absorber']) ? $request->form['absorber'] : null,
            'gears'             => isset($request->form['gears']) ? $request->form['gears'] : null,
            'shifter'           => isset($request->form['shifter']) ? $request->form['shifter'] : null,
            'cassette'          => isset($request->form['cassette']) ? $request->form['cassette'] : null,
            'brakes'            => isset($request->form['brakes']) ? $request->form['brakes'] : null,
            'front_brakes_size' => isset($request->form['front_brakes_size']) ? $request->form['front_brakes_size'] : null,
            'back_brakes_size'  => isset($request->form['back_brakes_size']) ? $request->form['back_brakes_size'] : null,
            'weight'            => isset($request->form['weight']) ? $request->form['weight'] : null,
        ]);
        $bike = Bikes_Catalog::find($id);
        $bike->sizes = explode(',', $bike->sizes);

        // Upload file if request has file
        if ($request->file()) {
            $uploadedPictures = $this->upload($request->file(), $id);
            $bike->pictures = $uploadedPictures;
        }
        // Return bikes catalog id
        return response()->json($bike);
    }

    // Update bikes catalog
    public function update(Request $request)
    {
        // Decode json
        $request->form = json_decode($request->form, true);
        // Validations criteria
        Validator::make($request->form, [
            'model'             => 'required|string',
            'brand'             => 'required|string',
            'frame_type'        => 'required|string',
            'utilisation'       => 'required|string',
            'electric'          => 'required|string',
            'sizes'             => 'required',
            'season'            => 'required',
            'motor'             => 'string|nullable',
            'battery'           => 'string|nullable',
            'transmission'      => 'string|nullable',
            'status'            => 'required|string',
            'minimal_stock'     => 'required',
            'optimal_stock'     => 'required',
            'buying_price'      => 'required',
            'price_htva'        => 'required',
            'display'           => 'required|integer',
            'wheel_size'        => 'integer|nullable',
            'fork'              => 'string|nullable',
            'absorber'          => 'string|nullable',
            'gears'             => 'string|nullable',
            'shifter'           => 'string|nullable',
            'cassette'          => 'string|nullable',
            'brakes'            => 'string|nullable',
            'front_brakes_size' => 'integer|nullable',
            'back_brakes_size'  => 'integer|nullable',
            'weight'            => 'integer|nullable',
        ])->validate();

        // Update bikes catalog
        Bikes_Catalog::where('id', $request->form['id'])
            ->update([
                'model'             => $request->form['model'],
                'brand'             => $request->form['brand'],
                'frame_type'        => $request->form['frame_type'],
                'utilisation'       => $request->form['utilisation'],
                'electric'          => $request->form['electric'],
                'sizes'             => implode(',', $request->form['sizes']),
                'season'            => $request->form['season'],
                'motor'             => array_key_exists('motor', $request->form) ? $request->form['motor'] : null,
                'battery'           => array_key_exists('battery', $request->form) ? $request->form['battery'] : null,
                'transmission'      => array_key_exists('transmission', $request->form) ? $request->form['transmission'] : null,
                'status'            => $request->form['status'],
                'minimal_stock'     => ($request->form['minimal_stock'] === NULL) ? 0 : $request->form['minimal_stock'],
                'optimal_stock'     => ($request->form['optimal_stock'] === NULL) ? 0 : $request->form['optimal_stock'],
                'buying_price'      => $request->form['buying_price'],
                'price_htva'        => $request->form['price_htva'],
                'display'           => $request->form['display'],
                'wheel_size'        => $request->form['wheel_size'],
                'fork'              => $request->form['fork'],
                'absorber'          => $request->form['absorber'],
                'gears'             => $request->form['gears'],
                'shifter'           => $request->form['shifter'],
                'cassette'          => $request->form['cassette'],
                'brakes'            => $request->form['brakes'],
                'front_brakes_size' => $request->form['front_brakes_size'],
                'back_brakes_size'  => $request->form['back_brakes_size'],
                'weight'            => $request->form['weight'],
            ]);

        // Return response
        $bike = Bikes_Catalog::find($request->form['id']);
        $bike->sizes = explode(',', $bike->sizes);

        if ($request->file()) {
            if (array_key_exists('pictures', $request->form)) {
                $newArray = $request->form['pictures'];
                foreach ($this->upload($request->file(), $request->form['id']) as $picture) {
                    array_push($newArray, $picture);
                }
            } else {
                $newArray = [];
                foreach ($this->upload($request->file(), $request->form['id']) as $picture) {
                    array_push($newArray, $picture);
                }
            }
            $bike->pictures = $newArray;
        } else {
            $path = storage_path('app/public/bikes_pictures/' . $request->form['id']);
            if (is_dir($path)) {
                $files              = scandir($path);
                $files              = array_diff($files, array('.', '..'));
                $bike->pictures     = $files;
            }
        }
        return response()->json($bike);
    }

    // Destroy bikes catalog
    public function destroy(Request $request)
    {
        Bikes_Catalog::where('id', $request->bikeId)
            ->update([
                'status'    => 'inactif'
            ]);
        return response()->json(Bikes_Catalog::find($request->bikeId));
    }

    // Upload images
    public function upload($files, $id)
    {
        File::ensureDirectoryExists(public_path('/storage/bikes_pictures/' . $id));
        $uploadedPictures = [];
        foreach ($files as $key => $file) {
            $fileName       = $file->getClientOriginalName();
            $formdataKey    = str_replace('.', '_', $fileName);
            $path = $file->storeAs('bikes_pictures/' . $id, $fileName, 'public');
            array_push($uploadedPictures, $fileName);
        }
        return $uploadedPictures;
    }

    // Delete image
    public function deletePicture(Request $request, $id)
    {
        $array  = $request->bike['pictures'];
        $key    = array_search($request->pictureName, $array);
        unset($array[$key]);
        $path = public_path('/storage/bikes_pictures/' . $id . '/' . $request->pictureName);
        if (File::exists($path)) {
            File::delete($path);
        }
        return response()->json($array);
    }

    // Get all bikes catalogs
    public function getAllBikesForShop($utilisation)
    {
        if ($utilisation) {
            $bikes = Bikes_Catalog::where('utilisation', '=', $utilisation)->get();
        } else {
            $bikes = Bikes_Catalog::all();
        }
        foreach ($bikes as $bike) {
            $bike->sizes = explode(',', $bike->sizes);
        }
        return response()->json($bikes);
    }

    // Get bike by id for shop
    public function getBikeForShop($id)
    {
        $bike = Bikes_Catalog::find($id);
        $bike->sizes = explode(',', $bike->sizes);
        return view('shop.pages.bike-details', compact('bike'));
    }

    public function getCategoriesForShop()
    {
        $bikes = Bikes_Catalog::all();
        foreach ($bikes as $bike) {
            $bike->sizes = explode(',', $bike->sizes);
        }
        return view('shop.pages.catalog', compact('bikes'));
    }

    // Get datatable info
    public function getDataForDataTable(Request $request)
    {
        $page = $request->numPage;
        $limit = $request->nbEntries;
        $search = $request->searchValue;
        $sortDirection = json_decode($request->sortDirection, true);
        if ($search) {
            $q = explode(' ', $search);
            for ($i = 0; $i < count($q); $i++) {
                $bikes = DB::select('SELECT @ID := bikes_catalog.id as id, brand, model, frame_type, utilisation, price_htva,
                    (SELECT COUNT(*) FROM bikes where size="S" AND bikes_catalog_id = @ID) as countS,
                    (SELECT COUNT(*) FROM bikes where size="M" AND bikes_catalog_id = @ID) as countM,
                    (SELECT COUNT(*) FROM bikes where size="L" AND bikes_catalog_id = @ID) as countL,
                    (SELECT COUNT(*) FROM bikes where size="XL" AND bikes_catalog_id = @ID) as countXL
                    FROM `bikes_catalog`
                    WHERE brand LIKE "%' . $q[$i] . '%"
                    OR model LIKE "%' . $q[$i] . '%"
                    OR frame_type LIKE "%' . $q[$i] . '%"
                    OR utilisation LIKE "%' . $q[$i] . '%"
                    OR id LIKE "%' . $q[$i] . '%"
                    ORDER BY ' . $sortDirection['column'] . ' ' . $sortDirection['direction'] . ' LIMIT ' . $limit . ' OFFSET ' . ($page - 1) * $limit . '');
            }
        } else {
            $bikes = DB::select('SELECT @ID := bikes_catalog.id as id, brand, model, frame_type, utilisation, price_htva,
                (SELECT COUNT(*) FROM bikes where size="S" AND bikes_catalog_id = @ID) as countS,
                (SELECT COUNT(*) FROM bikes where size="M" AND bikes_catalog_id = @ID) as countM,
                (SELECT COUNT(*) FROM bikes where size="L" AND bikes_catalog_id = @ID) as countL,
                (SELECT COUNT(*) FROM bikes where size="XL" AND bikes_catalog_id = @ID) as countXL
                FROM `bikes_catalog`
                ORDER BY ' . $sortDirection['column'] . ' ' . $sortDirection['direction'] . ' LIMIT ' . $limit . ' OFFSET ' . ($page - 1) * $limit . '');
        }
        return response()->json($bikes);
    }
}
