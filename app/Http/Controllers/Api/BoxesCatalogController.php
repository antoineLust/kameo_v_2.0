<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Boxes_catalog;

class BoxesCatalogController extends Controller
{
    // Get all boxes catalog
    public function index(){
        return response()->json(Boxes_catalog::all()); 
    }

    public function getBoxCatalogById($id){
        return response()->json(Boxes_catalog::find($id)); 
    }
    
}
