<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Models\CalendarDaysManagement;

class CalendarDaysManagementController extends Controller
{
    public function getDays(Request $request)
    {
        $month = ($request->month < 10) ? '0'.$request->month : $request->month;
        $year = $request->year;
        $user = auth()->user();
        $days = CalendarDaysManagement::select('date')->where('users_id', $user->id)->where('date', 'like', '%-'.$month.'-'.$year.'%')->get();
        return response()->json($days);
    }

    public function checkDay(Request $request)
    {
        $date = $request->date;
        $user = auth()->user();
        $dayExist = CalendarDaysManagement::where('users_id', $user->id)->where('date', $date)->exists();
        if($dayExist){
            CalendarDaysManagement::where('users_id', $user->id)->where('date', $date)->delete();
            return response()->json(['status' => 'success', 'message' => 'Day removed', 'state' => 'Removed']);
        }
        else{
            CalendarDaysManagement::insert([
                'users_id' => $user->id,
                'date' => $date
            ]);
            return response()->json(['status' => 'success', 'message' => 'Day added', 'state' => 'Added']);
        }
    }
}
