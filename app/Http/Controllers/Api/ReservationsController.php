<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Boxe;
use App\Models\Reservation;
use App\Models\Reservation_detail;
use App\Models\ReservationCode;
use Error;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MailchimpTransactional\ApiClient;

class ReservationsController extends Controller
{
    // Load all reservations
    public function index(){
        return response()->json(Reservation::all());
    }

    public function indexByAuth($id){
        return response()->json(Reservation::where('users_id', $id)->get());
    }
    
    // Get all code
    public function getAllCodes(){
        return response()->json(ReservationCode::all());
    }

    // Get all details
    public function getAllDetails(){
        return response()->json(Reservation_detail::all());
    }

    public function getReservationById($id){
        return response()->json(Reservation::find($id));
    }

    public function getReservationCodeByReservationId($id){
        return response()->json(ReservationCode::where('reservation_id', $id)->first());
    }

    // Add one reservation
    public function create(Request $request)
    {
        // Validate request
        $this->validate($request, [
            'dateStart'     => 'required',
            'dateEnd'       => 'required',
            'locationStart' => 'required',
            'locationEnd'   => 'required',
            'bikeId'        => 'required',
        ]);

        // Create reservation
        $reservationId = Reservation::insertGetId([
            'start'             => $request->dateStart,
            'end'               => $request->dateEnd,
            'building_start'    => $request->locationStart,
            'building_end'      => $request->locationEnd,
            'status'            => 'OK',
            'bikes_id'          => $request->bikeId,
            'users_id'          => Auth::id(),
        ]);

        // Verify if user companies has a box
        $buildings_id   = $request->locationStart;
        $haveBox        = Boxe::where('buildings_id', $buildings_id)->exists();
        $code           = null;

        // If user company has a box generete a reservation code
        if ($haveBox) {
            $lastCode = ReservationCode::where('building_id', $buildings_id)->orderBy('id', 'desc')->first();
            if ($lastCode) {
                if ($lastCode->validity === 0) {
                    $code = intval($lastCode->code);
                } else {
                    $code = intval($lastCode->code) + 1;
                }
            } else {
                $code = rand(1000, 9999);
            }
        }

        // If code is not null, create a new reservation code
        if ($code !== null) {

            $ReservationCodeId = ReservationCode::insertGetId([
                'code'              =>      $code,
                'building_id'       =>      $buildings_id,
                'reservation_id'    =>      $reservationId,
                'validity'          =>      1,
                'status'            =>      'actif',
            ]);

            function sendEmail($message)
            {
                try {
                    $mailchimp = new ApiClient();
                    $mailchimp->setApiKey(config('services.mailchimp.key'));

                    $mailchimp->messages->send(["message" => $message]);
                } catch (Error $e) {
                    echo 'Error: ', $e->getMessage(), "\n";
                }
            }

            // Send email
            if (config('app.env') == 'local') {
                $message = [
                    "from_name"     => "KAMEO Bikes",
                    "from_email"    => "info@kameobikes.com",
                    "subject"       => "Votre code de reservation",
                    "html"          => view('emails.reservationCode', [
                        'code'      => $code,
                        'company'   => Auth::user()->company->name,
                        'user'      => Auth::user(),
                    ])->render(),
                    "to" => [
                        [
                            "type"  => "to",
                            "email" => "benjamin@kameobikes.com",
                            "name"  => "Benjamin Van Rentegrhem"
                        ]
                    ]
                ];
                sendEmail($message);
            } else if (config('app.env') == 'production') {
                $message = [
                    "from_name"     => "KAMEO Bikes",
                    "from_email"    => "info@kameobikes.com",
                    "subject"       => "Votre code de reservation",
                    "html"          => view('emails.reservationCode', [
                        'code'      => $code,
                        'company'   => Auth::user()->company->name,
                        'user'      => Auth::user(),
                    ])->render(),
                    "to" => [
                        [
                            "type"  => "to",
                            "email" => Auth::user()->email,
                            "name"  => Auth::user()->name . " " . Auth::user()->firstname
                        ]
                    ]
                ];
                sendEmail($message);
            }
        }
        

        // Return data
        return response()->json([
            'reservation'           => Reservation::find($reservationId),
            'reservationCode'       => ($code !== null) ? ReservationCode::find($ReservationCodeId) : null,
        ]);
    }

    // Delete one reservation
    public function delete($id)
    {

        // Delete reservation code
        $reservation_code = ReservationCode::where('reservation_id', intval($id));
        if ($reservation_code->exists()) {
            $reservation_code->first()->delete();
        }

        // Delete reservation
        $reservation = Reservation::find(intval($id))->delete();

        // Return data
        return response()->json($reservation);
    }
}
