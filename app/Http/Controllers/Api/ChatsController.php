<?php
 
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chat;
use App\Models\User;
use App\Models\Chat_sessions;
use MailchimpTransactional\ApiClient;
use Error;
 
class ChatsController extends Controller
{

    public function index()
    {
        return response()->json(Chat::all());
    }

    public function getSessionByClientId($id)
    {
        return response()->json(Chat_sessions::where('client_id', $id)->first());
    }

    public function getSessions()
    {
        return response()->json(Chat_sessions::orderByDesc('updated_at')->get());
    }

    public function show($id)
    {
        $session = Chat_sessions::where('client_id', $id)->first();
        if(isset($session)){
            return response()->json(Chat::with('session')->where('session_id', $session->id)->orderByDesc('created_at')->get());
        }else{
            return response()->json([]); 
        }    
    }

    public function incrementClientNotif(Request $request)
    {
        Chat_sessions::find($request->id)->increment('notif_client_count'); 
    }

    public function resetClientNotif(Request $request)
    {
        Chat_sessions::where('id', $request->id)->update([
            'notif_client_count'   => 0,    
        ]);
        return response()->json(Chat_sessions::find($request->id));  
    }

    public function incrementEmployeeNotif(Request $request)
    {
        Chat_sessions::find($request->id)->increment('notif_employee_count');
    }

    public function resetEmployeeNotif(Request $request)
    {
        Chat_sessions::where('id', $request->id)->update([
            'notif_employee_count'   => 0,    
        ]);
        return response()->json(Chat_sessions::find($request->id));  
    }

    public function assignEmployee(Request $request)
    {
        $request->validate([
            'employee_id'   => 'required|integer',
            'session_id'    => 'required|integer',
        ]);

        Chat_sessions::where('id', $request->session_id)
            ->update([
                'employee_id'   => $request->employee_id,
        ]);

        return response()->json(Chat_sessions::find($request->session_id));
    }

    public function create(Request $request){
        // Validations criteria
        $request->validate([
            'sender_id'     => 'required|integer',
            'client_id'     => 'nullable|integer',
            'session_id'    => 'nullable|integer',
            'content'       => 'required|string'
        ]);
   
        $array = [];

        if(!isset($request->session_id)){

            $session_id = Chat_sessions::insertGetId([
                'client_id'     => $request->sender_id,
                'updated_at'    => now()
            ]);

            $id = Chat::insertGetId([
                'sender_id'     => $request->sender_id,
                'session_id'    => $session_id,
                'content'       => $request->content
            ]);

            $id2 = Chat::insertGetId([
                'sender_id'     => 0,
                'session_id'    => $session_id,
                'content'       => 'Merci pour votre message, un membre de l\'équipe va traiter votre demande au plus vite.'
            ]);

            array_push($array, Chat::with('session')->find($id), Chat::with('session')->find($id2));

            $user = User::find($request->sender_id);

            function sendMail($message){
                try {
                    $mailchimp = new ApiClient();
                    $mailchimp->setApiKey(config('services.mailchimp.key'));
            
                    $mailchimp->messages->send(["message" => $message]);
                } catch (Error $e) {
                    echo 'Error: ', $e->getMessage(), "\n";
                }
            }
            
            $message = [
                "from_name"     => "KAMEO Bikes",
                "from_email"    => "info@kameobikes.com",
                "subject"       => "Nouveau message de chat",
                "html"          => view('emails.newChat', [
                        'firstname' => isset($user) ? $user->firstname : 'Utilisateur',
                        'lastname'  => isset($user) ? $user->lastname : 'anonyme',
                        'content'   => $request->content
                    ])->render(),
                "to" => [
                    [
                        "type"  => "to",
                        "email" => "sherman503@hotmail.fr",
                        "name"  => "Alexis Gommet"
                    ]
                ]
            ];
            // sendMail($message);
        }else{
            $id = Chat::insertGetId([
                'sender_id'     => $request->sender_id,
                'session_id'    => $request->session_id,
                'content'       => $request->content
            ]);

            Chat_sessions::where('id', $request->session_id)->update([
                'updated_at'    => now()
            ]);

            array_push($array, Chat::with('session')->find($id));
        }

        return response()->json($array);
    }
}