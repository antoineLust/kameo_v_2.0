<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Building;

class BuildingsController extends Controller
{
    // Get all buildings
    public function index(){
        return response()->json(Building::all());
    }

    public function getBuildingByCompanyId($id){
        return response()->json(Building::where('companies_id', $id)->get());
    }
}
