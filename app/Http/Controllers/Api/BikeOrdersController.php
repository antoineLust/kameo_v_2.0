<?php

namespace App\Http\Controllers\Api;

use App\Models\Bike_order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bike;
use App\Models\Company;
use App\Models\Grouped_order;

class BikeOrdersController extends Controller
{

    // Get all bikes orders
    public function index(){
        return response()->json(Bike_order::with('catalog')->get());
    }

    // Bike order by id
    public function show($id){
        return response()->json(Bike_order::with('catalog')->find($id));
    }

    // Get all bikes orders
    public function getBikesFromGroupedOrder($id){
        return response()->json(Bike_order::with('catalog')->where('grouped_orders_id', $id)->get());
    }
    
    // Create user
    public function create(Request $request){
        // Validations criteria
        $request->validate([
            'size'                      => 'required|string',
            'amount'                    => 'required|numeric',
            'contract_type'             => 'required|string|max:255',
            'estimated_delivery_date'   => 'required|date|date_format:Y-m-d',
            'status'                    => 'required|string',
            'grouped_orders_id'         => 'required|integer',
            'bikes_catalog_id'          => 'required|integer',
        ]);
        // Create new bike order
        $id = Bike_order::insertGetId([
            'size'                      => $request->size,
            'amount'                    => $request->amount,
            'type'                      => $request->contract_type,
            'estimated_delivery_date'   => $request->estimated_delivery_date,
            'status'                    => $request->status,
            'grouped_orders_id'         => $request->grouped_orders_id,
            'bikes_catalog_id'          => $request->bikes_catalog_id
        ]);
        // Return response
        return response()->json(Bike_order::with('catalog')->find($id));
    }

    // Update box Order
    public function update(Request $request){
        // Validations criteria
        $request->validate([
            'size'                      => 'required|string',
            'amount'                    => 'required|numeric',
            'type'                      => 'required|string|max:255',
            'estimated_delivery_date'   => 'required|date|date_format:Y-m-d',
            'status'                    => 'required|string',
            'grouped_orders_id'         => 'required|integer',
            'bikes_catalog_id'          => 'required|integer',
            'bikes_id'                  => 'nullable|integer',
        ]);
        // Update bike stock 
        if($request->bikes_id !== null){
            if(Bike_order::select('*')->where('id', $request->id)->first()->bikes_id !== null){
                Bike::where('id', Bike_order::select('*')->where('id', $request->id)->first()->bikes_id)->update([
                    'contract_type'                 => 'stock',
                    'companies_id'                  => null,
                    'users_id'                      => null,
                    'selling_price'                 => null,
                    'selling_date'                  => null,
                    'leasing_price'                 => null,
                    'contract_start'                => null,
                    'client_name'                   => null,
                    'delivery_date'                 => null,
                    'estimated_delivery_date'       => null,
                ]);
            }
            Bike::where('id', $request->bikes_id)
                ->update([
                    'contract_type'     => $request->type,
                    'companies_id'      => Grouped_order::find($request->grouped_orders_id)->companies_id,
                    'users_id'          => (Grouped_order::find($request->grouped_orders_id)->users_id !== null) ? Grouped_order::find($request->grouped_orders_id)->users_id : null,
                    'selling_price'     => ($request->type === 'selling') ? $request->amount : null,
                    'leasing_price'     => ($request->type === 'leasing') ? $request->amount : null,
                    'client_name'       => Company::find(Grouped_order::find($request->grouped_orders_id)->companies_id)->name,
                ]);
        }
        else if($request->bikes_id == null){
            Bike::where('id', Bike_order::find($request->id)->bikes_id)
                ->update([
                    'contract_type'                 => 'stock',
                    'companies_id'                  => null,
                    'users_id'                      => null,
                    'selling_price'                 => null,
                    'selling_date'                  => null,
                    'leasing_price'                 => null,
                    'contract_start'                => null,
                    'client_name'                   => null,
                    'delivery_date'                 => null,
                    'estimated_delivery_date'       => null,
                ]);
        }
        if($request->status === 'done'){
            Bike::where('id', Bike_order::find($request->id)->bikes_id)
                ->update([
                    'contract_type'                 => 'pending_delivery',
                ]);
        }
        // Update bike order
        Bike_order::where('id', $request->id)
            ->update([
                'size'                      => $request->size,
                'amount'                    => $request->amount,
                'type'                      => $request->type,
                'estimated_delivery_date'   => $request->estimated_delivery_date,
                'delivery_date'             => $request->delivery_date,
                'status'                    => $request->status,
                'grouped_orders_id'         => $request->grouped_orders_id,
                'bikes_catalog_id'          => $request->bikes_catalog_id,
                'bikes_id'                  => $request->bikes_id
                ]);
        // Return response
        return response()->json(Bike_order::with('catalog')->find($request->id));
    }

    // Delete bike order
    public function destroy(Request $request){
        $bikeOrder = Bike_order::where('id', $request->bikeOrderId);

        $bikeOrderId = $bikeOrder->first()->grouped_orders_id;

        $bikeOrder->delete();

        // Return response
        return response()->json($bikeOrderId);
    }

}
