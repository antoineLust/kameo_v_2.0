<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Calendar;
use App\Models\Entretien;
use App\Models\Bike_order;
use App\Models\Bike;
use App\Models\Bike_test;
use App\Models\Bikes_assembly;
use App\Models\Accessory_order;
use App\Models\Boxe_order;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CalendarsController extends Controller
{
    public function index(){
        return response()->json(Calendar::all());
    }

    public function create(Request $request){
        $request->validate([
            "date"                  => 'required|date|date_format:Y-m-d',
            "start_time"            => 'required|string',
            "end_time"              => 'required|string',
            "comment"               => 'string|nullable ',
            "confidential_comment"  => 'string|nullable ',
            "status"                => 'required|string',
            "employee_id"           => 'required|integer',
            "item_type"             => 'required|string',
            "item_id"               => 'required|integer',
        ]);
        $id = Calendar::insertGetId([
                "date"                  => $request->date,
                "start_time"            => $request->start_time,
                "end_time"              => $request->end_time,
                "comment"               => $request->comment,
                "confidential_comment"  => $request->confidential_comment,
                "status"                => $request->status,
                "employee_id"           => $request->employee_id,
                "item_type"             => $request->item_type,
                "item_id"               => $request->item_id,
            ]);
        return response()->json(Calendar::find($id));
    }

     public function update(Request $request){
        $request->validate([
            "date"          => 'required|date|date_format:Y-m-d',
            "start_time"    => 'required|string',
            "end_time"      => 'required|string',
            "comment"       => 'string|nullable ',
            "status"        => 'required|string',
            "employee_id"   => 'required|integer',
        ]);

        Calendar::where('id', $request->id)
            ->update([
                "date"          => $request->date,
                "start_time"    => $request->start_time,
                "end_time"      => $request->end_time,
                "comment"       => $request->comment,
                "status"        => $request->status,
                "employee_id"   => $request->employee_id,
            ]);

        return response()->json(Calendar::find($request->id));
    }

    public function updateAssembly(Request $request){

        $request->validate([
            "status"        => 'required|string',
            'item_id'       => 'required|integer',
        ]);

        $bikesAssembly = Bikes_assembly::where('id', $request->item_id);
        $bikesId = $bikesAssembly->first()->bikes_id;
        $bike = Bike::where('id', $bikesId);

        if($request->status == "done"){
            $bikesAssembly->update(["status"    => "Terminé"]);
            $bike->update(["assembled"  => 1]);
        }else if($request->status == "todo"){
            $bikesAssembly->update(["status"    => "En cours"]);
            $bike->update(["assembled"  => 0]);
        }

        Calendar::where('id', $request->id)->update(["status"   => $request->status]);
        
        return response()->json([
            'calendar' => Calendar::find($request->id),
            'bikeAssembly' => Bikes_assembly::find($request->item_id)
        ]);
    }

    public function updateTest(Request $request){

        $request->validate([
            "status"        => 'required|string',
            'item_id'       => 'required|integer',
            'comment'       => 'string|nullable',
        ]);

        $bikesTest = Bike_test::where('id', $request->item_id);

        if($request->status == "done"){
            $bikesTest->update([
                "status"    => "done",
                "comment"   => $request->comment
            ]);
        }else if($request->status == "todo"){
            $bikesTest->update([
                "status"    => "confirmed",
                "comment"   => $request->comment
            ]);
        }

        Calendar::where('id', $request->id)->update([
            "status"    => $request->status, 
            "comment"   => $request->comment
        ]);

        return response()->json([
            'calendar' => Calendar::find($request->id),
            'bikeTest' => Bike_test::find($request->item_id)
        ]);
    }

    public function updateEntretien(Request $request){

        $request->validate([
            "status"        => 'required|string',
            'item_id'       => 'required|integer',
        ]);

        Calendar::where('id', $request->id)->update(["status"   => $request->status]);

        return response()->json(Calendar::find($request->id));
    }

    public function updateOrder(Request $request){

        $request->validate([
            "status"        => 'required|string',
            'item_id'       => 'required|integer',
        ]);

        $accOrder = Accessory_order::where('grouped_orders_id', $request->item_id);
        $bikeOrder = Bike_order::where('grouped_orders_id', $request->item_id);
        $boxOrder = Boxe_order::where('grouped_orders_id', $request->item_id);

        if($request->status == "done"){
            $accOrder->update(["status"     => "done"]);
            $bikeOrder->update(["status"    => "done"]);
            $boxOrder->update(["status"     => "done"]);
        }else if($request->status == "todo"){
            $accOrder->update(["status"     => "confirmed"]);
            $bikeOrder->update(["status"    => "confirmed"]);
            $boxOrder->update(["status"     => "confirmed"]);
        }

        Calendar::where('id', $request->id)->update(["status"   => $request->status]);

        return response()->json(Calendar::find($request->id));
    }

    public function updateCommentEntretien(Request $request){

        Calendar::where('id', $request->id)
            ->update([
                'comment'               => $request->comment,
                'updated_at'            => now(),
        ]);

        $bikeId = Entretien::where('id', $request->item_id)->first()->bikes_id;

        Bike::where('id', $bikeId)->update([
            'comment' => $request->confidential_comment,
        ]);

        return response()->json(Calendar::find($request->id));
    }

    public function destroy(Request $request){
        if(str_contains(Auth::user()->permission, 'fleetManager')){
            Calendar::where('id', $request->taskId)
                ->delete();
        }
        else{
            return abort(401);
        }
    }

    public function updateComment(Request $request){

        $request->validate([
            'comment'       => 'string|nullable',
            'item_id'       => 'required|integer',
        ]);

        Calendar::where('id', $request->id)
            ->update([
                "comment"   => $request->comment,
                'updated_at'    => now(),
            ]);
        
        Bike_test::where('id', $request->item_id)->update([
            "comment"   => $request->comment
        ]);

        return response()->json([
            'calendar' => Calendar::find($request->id),
            'bikeTest' => Bike_test::find($request->item_id),
        ]);
    }
}
