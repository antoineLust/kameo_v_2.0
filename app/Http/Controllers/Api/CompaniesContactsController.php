<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company_contact;

class CompaniesContactsController extends Controller
{
    // Get all companies contacts
    public function index(){
        return response()->json(Company_contact::with('company')->get()); 
    }
    
    // Get one company contact
    public function show($id){
        return response()->json(Company_contact::with('company')->find($id)); 
    }

    public function getContactsByCompanyId($id){
        return response()->json(Company_contact::with('company')->where('companies_id', $id)->get()); 
    }

    // Create company contact
    public function create(Request $request){
        // Validations criteria
        $validated = $request->validate([
            'form.firstname'     => 'required|string|max:255',
            'form.lastname'      => 'required|string|max:255',
            'form.email'         => 'required|email',
            'form.phone'         => 'required',
            'form.function'      => 'nullable|string|max:255',
            'form.type'          => 'required',
        ]);
        $id = Company_contact::insertGetId([
                'firstname'                 => $request->form['firstname'],
                'lastname'                  => $request->form['lastname'],
                'email'                     => $request->form['email'],
                'phone'                     => $request->form['phone'],
                'function'                  => array_key_exists('function', $request->form) ? $request->form['function'] : null,
                'type'                      => $request->form['type'],
                'companies_id'              => $request->id
            ]);
        return response()->json(Company_contact::select('*')->with('company')->where('id', $id)->first());
    }

    // Update company contact
    public function update(Request $request){
        // Validations criteria
        $validated = $request->validate([
            'firstname'     => 'required|string|max:255',
            'lastname'      => 'required|string|max:255',
            'email'         => 'required|email',
            'phone'         => 'required',
            'function'      => 'nullable',
            'type'          => 'required',
        ]);
        Company_contact::where('id', $request->id)
            ->update([
                'firstname'     => $request->firstname,
                'lastname'      => $request->lastname,
                'email'         => $request->email,
                'phone'         => $request->phone,
                'function'      => $request->function,
                'type'          => $request->type,
            ]);
        return response()->json(Company_contact::select('*')->with('company')->where('id', $request->id)->first());
    }

    // Destroy contact
    public function destroy(Request $request){
        Company_contact::destroy($request->contactId);
        return response()->json(Company_contact::with('company')->get());
    }
}
