<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LogsQueries
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        // Define log
        $log = [
            'created_at'    => now(),
            'method'        => $request->method(),
            'path'          => $request->path(),
            'ip'            => $request->ip(),
            'utilisateur'   => $request->user() ? $request->user()->firstname . ' ' . $request->user()->lastname : 'Utilisateur non identifié',
            'status'        => $response->getStatusCode(),
        ];
        
        // Log query
        Log::channel('queries')->info(json_encode($log));

        // Return response
        return $response;
    }
}
