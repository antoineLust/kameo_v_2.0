<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, string $permissions)
    {
        foreach (explode('|', $permissions) as $permission) {
            if (str_contains(Auth::user()->permission, $permission)) {
                return $next($request);
            }
        }
        return response()->json(['message' => 'You are not authorized to perform this action.'], 403);
    }
}
