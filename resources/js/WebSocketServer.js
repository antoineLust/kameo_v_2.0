// Commandes
// Start : forever start WebSocketServer.js
// Start avec log : forever start -o socket-output.log -e socket-errors.log WebSocketServer.js
// Stop : forever stopall
// Check : forever list

const express = require("express");
const expressWs = require("express-ws");
const http = require("http");

// App and server
const app = express();
const server = http.createServer(app).listen(3000);

// Apply expressWs
const ews = expressWs(app, server);

const aWss = ews.getWss("/");

function heartbeat() {
    this.isAlive = true;
}

const interval = setInterval(function ping() {
    aWss.clients.forEach(function each(ws) {
        if (ws.isAlive === false) return ws.terminate();

        ws.isAlive = false;
        ws.ping();
    });
}, 30000);

// Get the /ws websocket route
app.ws("/ws", async function (ws, req) {
    ws.on("connection", function connection(ws) {
        ws.isAlive = true;
        ws.on("pong", heartbeat);
    });

    ws.on("message", async function (msg) {
        let event_type = "unknown";

        try {
            event_type = JSON.parse(msg).event_type;

            aWss.clients.forEach((client) => {
                client.send(msg);
            });

            console.log(
                `Broadcasted event ${event_type} to ${aWss.clients.size} clients`
            );
        } catch (error) {
            console.error(
                "Message triggered an error\nMessage : " +
                    msg +
                    "\nError : " +
                    error
            );
        }
    });

    ws.on("close", function close() {
        clearInterval(interval);
    });
});
