import { createRouter, createWebHistory } from "vue-router";
import * as stores from "../store/StoresIndex";
import routes from "./routes";
import Auth from "../Auth";

const router = createRouter({
    history: createWebHistory(),
    routes,
    scrollBehavior: (to, from, savedPosition) => {
        return {
            top: 0,
            behavior: "smooth",
        };
    },
});

async function getUserOrder(authorize) {
    let userOrder = JSON.parse(localStorage.getItem("userOrder"));

    if (!userOrder) {
        await stores.usersStore.loadUserAuth();
        await stores.ordersStore.verifyOrder();
        userOrder = JSON.parse(localStorage.getItem("userOrder"));
    }

    let cafetariaShow = true;
    if (authorize === "order") {
        cafetariaShow = !userOrder.hasOrder || !userOrder.allItemsDone;
    }
    return cafetariaShow;
}

router.beforeEach(async (to, from, next) => {
    const { authorize } = to.meta;
    let cafetariaShow = false;
    if (Auth.check()) {
        cafetariaShow = await getUserOrder(authorize);
    }
    const permissions = JSON.parse(localStorage.getItem("user"))?.permission;

    if (to.name != 404) {
        if (to.matched.some((record) => record.meta.requiresAuth)) {
            if (Auth.check()) {
                if (authorize) {
                    // admin,bill,....
                    if (
                        authorize.length &&
                        permissions.includes(authorize) &&
                        cafetariaShow
                    ) {
                        // role not authorised so redirect to home page
                        // return next({ path: "/" });
                        next();
                        return;
                    } else {
                        return next({ path: "/:catchAll(.*)" });
                    }
                }
                next();
            } else {
                if (to.name === "chat-view") {
                    window.localStorage.setItem("fromChatView", "true");
                }
                router.push({ name: "login" });
            }
        } else {
            next();
        }
    } else {
        if (authorize) {
            // admin,bill,....
            if (authorize.length && !permissions.includes(authorize)) {
                return next({ path: "/:catchAll(.*)" });
            }
        }
        next();
        return;
    }
});

export default router;
