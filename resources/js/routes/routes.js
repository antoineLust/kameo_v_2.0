export default [
    {
        path: "/:catchAll(.*)",
        name: "404",
        component: () => import("../components/404.vue"),
    },
    {
        path: "/",
        redirect: "/" + document.getElementById("app").dataset.lang,
    },
    {
        path: "/" + document.getElementById("app").dataset.lang,
        name: "index",
        component: () => import("../components/Index.vue"),
        redirect: "/" + document.getElementById("app").dataset.lang + "/home",
        children: [
            {
                path: "",
                name: "home",
                component: () => import("../components/Home.vue"),
            },
            {
                path: "login",
                name: "login",
                component: () => import("../components/Auth/Login.vue"),
            },
            {
                path: "backlog",
                name: "backlog",
                component: () => import("../components/Backlog/Index.vue"),
                meta: {
                    requiresAuth: true,
                },
            },
            {
                path: "shop",
                name: "shop",
                component: () => import("../components/Shop/Index.vue"),
                children: [
                    {
                        path: "",
                        name: "shop-home",
                        component: () => import("../components/Shop/Home.vue"),
                    },
                    {
                        path: "velo",
                        name: "shop-velo",
                        component: () => import("../components/Shop/Velo.vue"),
                    },
                    {
                        path: "accessoires",
                        name: "shop-accessoires",
                        component: () =>
                            import("../components/Shop/AccessoiresList.vue"),
                    },
                    {
                        path: "blog",
                        name: "blog",
                        component: () => import("../components/Shop/Blog.vue"),
                    },
                    {
                        path: "blog/nouveau-magasin",
                        name: "nouveau-magasin",
                        component: () =>
                            import(
                                "../components/Shop/Articles/NouveauMagasin.vue"
                            ),
                    },
                    {
                        path: "blog/choisir-son-velo",
                        name: "choisir-son-velo",
                        component: () =>
                            import(
                                "../components/Shop/Articles/ChoisirSonVelo.vue"
                            ),
                    },
                    {
                        path: "categorie-velo",
                        name: "categorie-velo",
                        component: () => import("../components/Shop/Blog.vue"),
                    },
                    {
                        path: "catalogue/utilisation/:utilisation",
                        name: "catalog-utilisation",
                        component: () =>
                            import("../components/Shop/UtilisationList.vue"),
                    },
                    {
                        path: "catalogue/utilisation/detail/:id",
                        name: "details-utilisation",
                        component: () =>
                            import("../components/Shop/UtilisationDetails.vue"),
                    },
                    {
                        path: "catalogue/accessoires/:accessoire",
                        name: "catalog-accessoires",
                        component: () =>
                            import("../components/Shop/AccessoiresList.vue"),
                    },
                    {
                        path: "catalogue/accessoires/detail/:id",
                        name: "details-accessoires",
                        component: () =>
                            import("../components/Shop/AccessoireDetails.vue"),
                    },
                    {
                        path: "location",
                        name: "location",
                        component: () =>
                            import("../components/Shop/Location.vue"),
                    },
                ],
            },
            {
                path: "pro",
                name: "pro",
                component: () => import("../components/Pro/Index.vue"),
                children: [
                    {
                        path: "",
                        name: "pro-home",
                        component: () => import("../components/Pro/Home.vue"),
                    },
                    {
                        path: "employeur",
                        name: "pro-employeur",
                        component: () =>
                            import("../components/Pro/Employeur.vue"),
                    },
                    {
                        path: "employee",
                        name: "pro-employee",
                        component: () =>
                            import("../components/Pro/Employee.vue"),
                    },
                    {
                        path: "independant",
                        name: "pro-independant",
                        component: () =>
                            import("../components/Pro/Independant.vue"),
                    },
                    {
                        path: "contact-pro",
                        name: "contact-pro",
                        component: () =>
                            import("../components/Pro/ContactPro.vue"),
                    },
                    {
                        path: "veloPartage",
                        name: "veloPartage",
                        component: () =>
                            import("../components/Pro/VelosPartages.vue"),
                    },
                    {
                        path: "planVelo",
                        name: "planVelo",
                        component: () =>
                            import("../components/Pro/VelosPartages.vue"),
                    },
                    {
                        path: "budgetMobilite",
                        name: "budgetMobilite",
                        component: () =>
                            import("../components/Pro/VelosPartages.vue"),
                    },
                ],
            },
            {
                path: "repair",
                name: "repair",
                component: () => import("../components/Repair/Index.vue"),
                children: [
                    {
                        path: "",
                        name: "repair-home",
                        component: () =>
                            import("../components/Repair/Home.vue"),
                    },
                ],
            },
            {
                path: "contact",
                name: "contact",
                component: () => import("../components/Contact/Index.vue"),
            },
            {
                // Dashboard
                path: "dashboard",
                name: "index-dashboard",
                component: () => import("../components/Dashboard/Index.vue"),
                meta: {
                    requiresAuth: "bill",
                },
                children: [
                    {
                        path: "",
                        name: "dashboard",
                        component: () =>
                            import("../components/Dashboard/Dashboard.vue"),
                    },
                    // Users
                    {
                        path: "users/list",
                        name: "users-list",
                        component: () =>
                            import(
                                "../components/Dashboard/Users/UsersList.vue"
                            ),
                    },
                    {
                        path: "users/create",
                        name: "users-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Users/UsersCreate.vue"
                            ),
                    },
                    {
                        path: "users/show/:id",
                        name: "users-show",
                        component: () =>
                            import(
                                "../components/Dashboard/Users/UsersShow.vue"
                            ),
                    },
                    {
                        path: "users/update/:id",
                        name: "users-update",
                        component: () =>
                            import(
                                "../components/Dashboard/Users/UsersUpdate.vue"
                            ),
                    },

                    // Companies
                    {
                        path: "companies/list",
                        name: "companies-list",
                        component: () =>
                            import(
                                "../components/Dashboard/Companies/CompaniesList.vue"
                            ),
                    },
                    {
                        path: "companies/create",
                        name: "companies-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Companies/CompaniesCreate.vue"
                            ),
                    },
                    {
                        path: "companies/update/:id",
                        name: "companies-update",
                        component: () =>
                            import(
                                "../components/Dashboard/Companies/CompaniesUpdate.vue"
                            ),
                    },
                    {
                        path: "companies/show/:id",
                        name: "companies-show",
                        component: () =>
                            import(
                                "../components/Dashboard/Companies/CompaniesShow.vue"
                            ),
                    },
                    {
                        path: "select-company",
                        name: "select-company",
                        component: () =>
                            import(
                                "../components/Dashboard/Companies/SelectCompany.vue"
                            ),
                    },

                    // Contacts
                    {
                        path: "contacts/show/:id",
                        name: "contacts-show",
                        component: () =>
                            import(
                                "../components/Dashboard/Companies/Contacts/ContactsShow.vue"
                            ),
                    },
                    {
                        path: "contacts/create/:id",
                        name: "contacts-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Companies/Contacts/ContactsCreate.vue"
                            ),
                    },
                    {
                        path: "contacts/update/:id",
                        name: "contacts-update",
                        component: () =>
                            import(
                                "../components/Dashboard/Companies/Contacts/ContactsUpdate.vue"
                            ),
                    },

                    // Bikes catalog
                    {
                        path: "bikes-catalog/list",
                        name: "bikes-catalog-list",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesCatalog/BikesCatalogList.vue"
                            ),
                    },
                    {
                        path: "bikes-catalog/show/:id",
                        name: "bikes-catalog-show",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesCatalog/BikesCatalogShow.vue"
                            ),
                    },
                    {
                        path: "bikes-catalog/create",
                        name: "bikes-catalog-create",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesCatalog/BikesCatalogCreate.vue"
                            ),
                    },
                    {
                        path: "bikes-catalog/update/:id",
                        name: "bikes-catalog-update",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesCatalog/BikesCatalogUpdate.vue"
                            ),
                    },

                    // Accessoires catalog
                    {
                        path: "accessories-catalog/list",
                        name: "accessories-catalog-list",
                        component: () =>
                            import(
                                "../components/Dashboard/AccessoriesCatalog/AccessoriesCatalogList.vue"
                            ),
                    },
                    {
                        path: "accessories-catalog/show/:id",
                        name: "accessories-catalog-show",
                        component: () =>
                            import(
                                "../components/Dashboard/AccessoriesCatalog/AccessoriesCatalogShow.vue"
                            ),
                    },
                    {
                        path: "accessories-catalog/create",
                        name: "accessories-catalog-create",
                        component: () =>
                            import(
                                "../components/Dashboard/AccessoriesCatalog/AccessoriesCatalogCreate.vue"
                            ),
                    },
                    {
                        path: "accessories-catalog/update/:id",
                        name: "accessories-catalog-update",
                        component: () =>
                            import(
                                "../components/Dashboard/AccessoriesCatalog/AccessoriesCatalogUpdate.vue"
                            ),
                    },

                    // Entretiens
                    {
                        path: "entretiens/list",
                        name: "entretiens-list",
                        component: () =>
                            import(
                                "../components/Dashboard/Entretiens/EntretiensList.vue"
                            ),
                    },
                    {
                        path: "entretiens/create",
                        name: "entretiens-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Entretiens/EntretiensCreate.vue"
                            ),
                    },

                    {
                        path: "entretiens/update/:id",
                        name: "entretiens-update",
                        component: () =>
                            import(
                                "../components/Dashboard/Entretiens/EntretiensUpdate.vue"
                            ),
                    },

                    // Orders
                    {
                        path: "grouped-orders/list",
                        name: "grouped-orders-list",
                        component: () =>
                            import(
                                "../components/Dashboard/GroupedOrders/GroupedOrdersList.vue"
                            ),
                    },
                    {
                        path: "grouped-orders/create",
                        name: "grouped-orders-create",
                        component: () =>
                            import(
                                "../components/Dashboard/GroupedOrders/GroupedOrdersCreate.vue"
                            ),
                    },
                    {
                        path: "grouped-orders/show/:id",
                        name: "grouped-orders-show",
                        component: () =>
                            import(
                                "../components/Dashboard/GroupedOrders/GroupedOrdersShow.vue"
                            ),
                    },
                    {
                        path: "grouped-orders/update/:id",
                        name: "grouped-orders-update",
                        component: () =>
                            import(
                                "../components/Dashboard/GroupedOrders/GroupedOrdersUpdate.vue"
                            ),
                    },

                    // Bikes orders
                    {
                        path: "bike-orders/create/:id",
                        name: "bike-orders-create",
                        component: () =>
                            import(
                                "../components/Dashboard/BikeOrders/BikeOrdersCreate.vue"
                            ),
                    },
                    {
                        path: "bike-orders/update/:id",
                        name: "bike-orders-update",
                        component: () =>
                            import(
                                "../components/Dashboard/BikeOrders/BikeOrdersUpdate.vue"
                            ),
                    },

                    // Boxes orders
                    {
                        path: "box-orders/create/:id",
                        name: "box-orders-create",
                        component: () =>
                            import(
                                "../components/Dashboard/BoxOrders/BoxOrdersCreate.vue"
                            ),
                    },
                    {
                        path: "box-orders/update/:id",
                        name: "box-orders-update",
                        component: () =>
                            import(
                                "../components/Dashboard/BoxOrders/BoxOrdersUpdate.vue"
                            ),
                    },

                    // Accessories orders
                    {
                        path: "accessory-orders/create/:id",
                        name: "accessory-orders-create",
                        component: () =>
                            import(
                                "../components/Dashboard/AccessoryOrders/AccessoryOrdersCreate.vue"
                            ),
                    },
                    {
                        path: "accessory-orders/update/:id",
                        name: "accessory-orders-update",
                        component: () =>
                            import(
                                "../components/Dashboard/AccessoryOrders/AccessoryOrdersUpdate.vue"
                            ),
                    },

                    // Bills
                    {
                        path: "bills/list",
                        name: "bills-list",
                        meta: {
                            permissions: "bill",
                        },
                        component: () =>
                            import(
                                "../components/Dashboard/Bills/BillsList.vue"
                            ),
                    },
                    {
                        path: "bills/create",
                        name: "bills-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Bills/BillsCreate.vue"
                            ),
                    },
                    {
                        path: "bills/update/:id",
                        name: "bills-update",
                        component: () =>
                            import(
                                "../components/Dashboard/Bills/BillsUpdate.vue"
                            ),
                    },

                    // Bikes stock
                    {
                        path: "bikes-stock/list",
                        name: "bikes-stock-list",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesStock/BikesStockList.vue"
                            ),
                    },
                    {
                        path: "bikes-stock/show/:id",
                        name: "bikes-stock-show",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesStock/BikesStockShow.vue"
                            ),
                    },
                    {
                        path: "bikes-stock/create",
                        name: "bikes-stock-create",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesStock/BikesStockCreate.vue"
                            ),
                    },
                    {
                        path: "bikes-stock/update/:id",
                        name: "bikes-stock-update",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesStock/BikesStockUpdate.vue"
                            ),
                    },

                    // Accessories stock
                    {
                        path: "accessories-stock/list",
                        name: "accessories-stock-list",
                        component: () =>
                            import(
                                "../components/Dashboard/AccessoriesStock/AccessoriesStockList.vue"
                            ),
                    },
                    {
                        path: "accessories-stock/show/:id",
                        name: "accessories-stock-show",
                        component: () =>
                            import(
                                "../components/Dashboard/AccessoriesStock/AccessoriesStockShow.vue"
                            ),
                    },
                    {
                        path: "accessories-stock/create",
                        name: "accessories-stock-create",
                        component: () =>
                            import(
                                "../components/Dashboard/AccessoriesStock/AccessoriesStockCreate.vue"
                            ),
                    },
                    {
                        path: "accessories-stock/update/:id",
                        name: "accessories-stock-update",
                        component: () =>
                            import(
                                "../components/Dashboard/AccessoriesStock/AccessoriesStockUpdate.vue"
                            ),
                    },

                    // News
                    {
                        path: "news/list",
                        name: "news-list",
                        component: () =>
                            import("../components/Dashboard/News/NewsList.vue"),
                    },
                    {
                        path: "news/create",
                        name: "news-create",
                        component: () =>
                            import(
                                "../components/Dashboard/News/NewsCreate.vue"
                            ),
                    },
                    {
                        path: "news/show/:id",
                        name: "news-show",
                        component: () =>
                            import("../components/Dashboard/News/NewsShow.vue"),
                    },
                    {
                        path: "news/update/:id",
                        name: "news-update",
                        component: () =>
                            import(
                                "../components/Dashboard/News/NewsUpdate.vue"
                            ),
                    },

                    // Conditions
                    {
                        path: "conditions/list",
                        name: "conditions-list",
                        component: () =>
                            import(
                                "../components/Dashboard/Conditions/ConditionsList.vue"
                            ),
                    },
                    {
                        path: "conditions/update/:id",
                        name: "conditions-update",
                        component: () =>
                            import(
                                "../components/Dashboard/Conditions/ConditionsUpdate.vue"
                            ),
                    },
                    {
                        path: "conditions/show/:id",
                        name: "conditions-show",
                        component: () =>
                            import(
                                "../components/Dashboard/Conditions/ConditionsShow.vue"
                            ),
                    },

                    // Entretien services
                    {
                        path: "entretien/:id/service/create",
                        name: "entretiens-services-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Entretiens/EntretiensDetails/Services/EntretienServicesCreate.vue"
                            ),
                    },

                    // Accessory services
                    {
                        path: "entretien/:id/accessory/create",
                        name: "entretiens-accessories-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Entretiens/EntretiensDetails/Accessories/EntretienAccessoriesCreate.vue"
                            ),
                    },

                    // Others entretiens details
                    {
                        path: "entretien/:id/others/create",
                        name: "entretiens-others-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Entretiens/EntretiensDetails/Others/EntretienOthersCreate.vue"
                            ),
                    },

                    // Bike assembly
                    {
                        path: "bikes-assembly/create",
                        name: "bikes-assembly-create",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesAssembly/BikesAssemblyCreate.vue"
                            ),
                    },
                    {
                        path: "bikes-assembly/update/:id",
                        name: "bikes-assembly-update",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesAssembly/BikesAssemblyUpdate.vue"
                            ),
                    },
                    {
                        path: "bikes-assembly/list",
                        name: "bikes-assembly-list",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesAssembly/BikesAssemblyList.vue"
                            ),
                    },

                    // Atelier
                    {
                        path: "atelier",
                        name: "atelier",
                        component: () =>
                            import(
                                "../components/Dashboard/Atelier/AtelierShow.vue"
                            ),
                    },

                    // Bike test
                    {
                        path: "bike-tests/list",
                        name: "bike-tests-list",
                        component: () =>
                            import(
                                "../components/Dashboard/BikeTests/BikeTestsList.vue"
                            ),
                    },
                    {
                        path: "bike-tests/update/:id",
                        name: "bike-tests-update",
                        component: () =>
                            import(
                                "../components/Dashboard/BikeTests/BikeTestsUpdate.vue"
                            ),
                    },
                    {
                        path: "bike-tests/create",
                        name: "bike-tests-create",
                        component: () =>
                            import(
                                "../components/Dashboard/BikeTests/BikeTestsCreate.vue"
                            ),
                    },

                    // Cafetaria
                    {
                        path: "cafetaria",
                        name: "cafetaria",
                        component: () =>
                            import(
                                "../components/Dashboard/Cafetaria/Index.vue"
                            ),
                        meta: {
                            authorize: "order",
                        },
                    },
                    {
                        path: "comment-choisir",
                        name: "comment-choisir",
                        component: () =>
                            import(
                                "../components/Shop/Articles/ChoisirSonVelo.vue"
                            ),
                    },

                    // Offers
                    {
                        path: "offers/list",
                        name: "offers-list",
                        component: () =>
                            import(
                                "../components/Dashboard/Offers/OffersList.vue"
                            ),
                    },
                    {
                        path: "offers/create",
                        name: "offers-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Offers/OffersCreate.vue"
                            ),
                    },
                    {
                        path: "offers/update/:id",
                        name: "offers-update",
                        component: () =>
                            import(
                                "../components/Dashboard/Offers/OffersUpdate.vue"
                            ),
                    },
                    {
                        path: "offers/show/:id",
                        name: "offers-show",
                        component: () =>
                            import(
                                "../components/Dashboard/Offers/OffersShow.vue"
                            ),
                    },
                    {
                        path: "bike-offers/create/:id",
                        name: "bike-offers-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Offers/OffersDetails/BikeOffersCreate.vue"
                            ),
                    },
                    {
                        path: "accessory-offers/create/:id",
                        name: "accessory-offers-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Offers/OffersDetails/AccessoryOffersCreate.vue"
                            ),
                    },
                    {
                        path: "box-offers/create/:id",
                        name: "box-offers-create",
                        component: () =>
                            import(
                                "../components/Dashboard/Offers/OffersDetails/BoxOffersCreate.vue"
                            ),
                    },

                    // Bike share
                    {
                        path: "bikes-share/list",
                        name: "bikes-share-list",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesShare/BikesShareList.vue"
                            ),
                    },

                    {
                        path: "bikes-share/reservation/confirm/:id",
                        name: "bikes-share-reservation-confirm",
                        component: () =>
                            import(
                                "../components/Dashboard/BikesShare/BikesShareReservationConfirm.vue"
                            ),
                    },

                    // Fleet manager
                    {
                        path: "fleet-manager",
                        name: "fleet-manager",
                        component: () =>
                            import(
                                "../components/Dashboard/FleetManager/Index.vue"
                            ),
                    },
                    {
                        path: "fleet-manager/bike-management",
                        name: "fleet-manager-bike-management-index",
                        component: () =>
                            import(
                                "../components/Dashboard/FleetManager/BikesManagement/Index.vue"
                            ),
                    },
                    {
                        path: "fleet-manager/cafetaria",
                        name: "fleet-manager-cafetaria-index",
                        component: () =>
                            import(
                                "../components/Dashboard/FleetManager/Cafetaria/Index.vue"
                            ),
                    },
                    {
                        path: "fleet-manager/shared-bikes",
                        name: "fleet-manager-shared-bikes-index",
                        component: () =>
                            import(
                                "../components/Dashboard/FleetManager/SharedBikes/Index.vue"
                            ),
                    },
                    {
                        path: "fleet-manager/users-management",
                        name: "fleet-manager-users-management-index",
                        component: () =>
                            import(
                                "../components/Dashboard/FleetManager/UsersManagement/Index.vue"
                            ),
                    },
                    {
                        path: "fleet-manager/bike-management/:id",
                        name: "fleet-manager-bike-management",
                        component: () =>
                            import(
                                "../components/Dashboard/FleetManager/BikesManagement/BikeDetails.vue"
                            ),
                    },
                    {
                        path: "chat-view",
                        name: "chat-view",
                        component: () =>
                            import("../components/Dashboard/Chat/ChatView.vue"),
                    },
                ],
            },
            {
                path: "politique-confidentialite",
                name: "politique-confidentialite",
                component: () =>
                    import("../components/PolitiqueConfidentialite.vue"),
            },
            {
                path: "cgu",
                name: "cgu",
                component: () => import("../components/CGU.vue"),
            },
        ],
    },
];
