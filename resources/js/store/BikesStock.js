import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "./StoresIndex";
import { isEmpty, isEqual } from "../functions/globalFunctions";

export const useBikesStockStore = defineStore("bikesStock", {
    id: "bikesStock",
    state: () => ({
        bikesStock: [],
        bikeStock: {},
        errors: [],
        availablesSharedBikes: [],
    }),
    getters: {
        getBikesStock: (state) => {
            return state.bikesStock;
        },
        getAvailableSharedBikes: (state) => {
            return state.availablesSharedBikes;
        },
        // getBikeStockById: (state) => (id) => {
        //     if (typeof id === "number") {
        //         return state.bikesStock.find((bike) => bike.id === id);
        //     } else {
        //         if(typeof id === "string"){
        //             if (id.split(",").length > 1) {
        //                 let bikeConvert = [];
        //                 id.split(",").forEach((bikeId) => {
        //                     state.bikesStock.forEach((bike) => {
        //                         if (bike.id === parseInt(bikeId)) {
        //                             bikeConvert.push(
        //                                 bike.id + " - " + bike.frame_number
        //                             );
        //                         }
        //                     });
        //                 });
        //                 return bikeConvert.join(", ");
        //             } else if (id.split(",").length === 1) {
        //                 return state.bikesStock.find(
        //                     (bike) => bike.id === Number(id)
        //                 );
        //             }
        //         }
        //     }
        // },
        getBikeStock: (state) => {
            return state.bikeStock;
        },
        getErrors: (state) => {
            return state.errors;
        },
        getBikesByCatalogId: (state) => (catalogId) => {
            return state.bikesStock.filter(
                (bike) => bike.bikes_catalog_id === catalogId
            );
        },
        getNumberOfBikeInStock: (state) => (catalogId) => {
            return state.bikesStock.filter(
                (bike) =>
                    bike.bikes_catalog_id === catalogId &&
                    (bike.companies_id === null || bike.companies_id === 12) &&
                    bike.users_id === null &&
                    bike.contract_type === "stock"
            ).length;
        },
    },
    actions: {
        async getBikesByCompanyAuth() {
            const user = stores.usersStore.getUserAuth;
            if (user) {
                const responsePHP = await axios.get(
                    "/api/bikes-stock/get-shared-bikes-by-company-id/" +
                        user.companies_id
                );
                return responsePHP.data;
            } else {
                return [];
            }
        },

        async getSharedBikesByCompanyId(companyId) {
            const responsePHP = await axios.get(
                "/api/bikes-stock/get-shared-bikes-by-company-id/" + companyId
            );
            return responsePHP.data;
        },

        async getBikesWithSelect() {
            const responsePHP = await axios.get(
                "/api/bikes-stock/get-all-bikes-select"
            );
            return responsePHP.data;
        },

        async getBikesNotAssembled() {
            const responsePHP = await axios.get(
                "/api/bikes-stock/get-bikes-not-assembled"
            );
            return responsePHP.data;
        },

        async getBikesByCatalogIdAndSize(catalogId, size) {
            const responsePHP = await axios.post(
                "/api/bikes-stock/get-bikes-by-catalog-id-and-size",
                { catalogId, size }
            );
            return responsePHP.data;
        },

        async getBikesByCompanyId(id) {
            const responsePHP = await axios.get(
                "/api/bikes-stock/get-bikes-by-company-id/" + id
            );
            return responsePHP.data;
        },

        async getBikeStockById(id) {
            const responsePHP = await axios.get(
                "/api/bikes-stock/get-bike-by-id/" + id
            );
            return responsePHP.data;
        },

        async getBikesByUserAuth() {
            const user = stores.usersStore.getUserAuth;
            if (user) {
                const responsePHP = await axios.get(
                    "/api/bikes-stock/get-bikes-by-user-auth/" + user.id
                );
                return responsePHP.data;
            } else {
                return [];
            }
        },

        updateState(data) {
            if (!isEmpty(this.bikesStock)) {
                if (!isEmpty(this.bikeStock)) {
                    if (
                        !isEqual(this.bikeStock, data.data) &&
                        data.data.id === this.bikeStock.id
                    ) {
                        this.bikeStock = data.data;
                    }
                }
            }
        },

        // Get all bikes
        async loadBikesStock() {
            await axios.get("/api/bikes-stock").then((reponsePHP) => {
                this.bikesStock = reponsePHP.data;
            });
        },
        async show(id) {
            this.errors = [];
            await axios
                .get("/api/bikes-stock/show/" + id)
                .then((reponsePHP) => {
                    this.bikeStock = reponsePHP.data;
                });
        },
        // Create a new bike
        async create(form) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/bikes-stock/create", form)
                .then((responsePHP) => {
                    this.bikesStock.push(responsePHP.data);
                    this.$router.push({ name: "bikes-stock-list" });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Update bike stock
        async update(formData) {
            // Reset errors
            this.errors = [];
            // Append fomData
            formData.append("form", JSON.stringify(this.bikeStock));
            // Send request
            await axios
                .post("/api/bikes-stock/update/" + this.bikeStock.id, formData)
                .then((responsePHP) => {
                    const bike = responsePHP.data;
                    const bikeIndex = this.bikesStock.findIndex(
                        (bike) => bike.id === responsePHP.data.id
                    );
                    this.bikesStock[bikeIndex] = bike;
                    this.bikeStock = bike;
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Destroy bike stock
        async destroy(id) {
            await axios
                .post("/api/bikes-stock/destroy/" + id)
                .then((responsePHP) => {
                    const bike = responsePHP.data;
                    const index = this.bikesStock.findIndex(
                        (bike) => bike.id === responsePHP.data.id
                    );
                    this.bikesStock[index] = bike;
                    this.bikeStock = bike;
                    this.$router.push({ name: "bikes-stock-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Get available bikes for sharing plan
        async searchAvailablesBikesByData(data) {
            await axios
                .post(
                    "/api/bikes-stock/get-availables-bike-for-sharing-plan",
                    data
                )
                .then((res) => {
                    this.availablesSharedBikes = res.data;
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        // Get data for dataTable
        async getDataForDataTable(
            numPage,
            nbEntries,
            searchValue,
            sortDirection,
            contractTypeFilter
        ) {
            const res = await axios.get("/api/datatable/bikes-stock-list", {
                params: {
                    numPage,
                    nbEntries,
                    searchValue,
                    sortDirection,
                    contractTypeFilter,
                },
            });
            return res.data;
        },

        async receptionStock(data) {
            data.forEach(async (item) => {
                item.contract_type = "stock";
                await axios
                    .post("/api/bikes-stock/receptionCommande/" + item.id, item)
                    .then((responsePHP) => {
                        if (
                            responsePHP.data.contract_type === "selling" ||
                            responsePHP.data.contract_type === "leasing"
                        ) {
                            stores.entretiensStore.loadEntretiens();
                        }
                        const bike = responsePHP.data;
                        const bikeIndex = this.bikesStock.findIndex(
                            (bike) => bike.id === responsePHP.data.id
                        );
                        this.bikesStock[bikeIndex] = bike;
                        this.bikeStock = bike;
                    })
                    .catch((error) => {
                        if (error.response && error.response.status === 422) {
                            this.errors = error.response.data.errors || {};
                        } else {
                            console.error(error);
                        }
                    });
            });
        },
    },
});
