import axios from "axios";
import { defineStore } from "pinia";

export const useNewsStore = defineStore("news", {
    id: "news",
    state: () => ({
        allNews: [],
        news: {
            id: null,
            title: null,
            content: null,
            created_at: null,
            updated_at: null,
            author: null,
            target_group: [],
            target_companies: [],
            media: "default.jpg",
        },
        errors: {},
        fileErrors: {
            message: null,
        },
    }),
    getters: {
        getAllNews: (state) => {
            return state.allNews;
        },
        getNews: (state) => {
            return state.news;
        },
        getErrors: (state) => {
            return state.errors;
        },
        getFileErrors: (state) => {
            return state.fileErrors;
        },
        getPertinentNews: (state) => (userCompanyId, userCompanyAudience) => {
            let pertinentNews = state.allNews.filter((news) => {
                if (
                    Date.now() < new Date(news.expiration_date).getTime() &&
                    news.status === "actif"
                ) {
                    if (
                        news.target_group.includes(userCompanyAudience) &&
                        news.target_companies === null
                    ) {
                        return true;
                    } else if (
                        news.target_group.includes(userCompanyAudience) &&
                        news.target_companies
                    ) {
                        if (news.target_companies.includes(userCompanyId)) {
                            return true;
                        }
                    } else if (news.target_group === "all") {
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            let orderedPertinentNews = pertinentNews.sort((a, b) =>
                a.created_at > b.created_at ? 1 : -1
            );
            return orderedPertinentNews;
        },
    },
    actions: {
        // Api request for load all news
        async loadAllNews() {
            this.allNews = await axios
                .get("/api/news")
                .then((reponsePHP) => (this.allNews = reponsePHP.data));
            this.allNews.forEach((element) => {
                element.created_at = element.created_at.split("T")[0];
                element.updated_at = element.updated_at.split("T")[0];
            });
        },
        async loadLastNews() {
            this.lastestNews = await axios
                .get("/api/last-news")
                .then((reponsePHP) => (this.lastestNews = reponsePHP.data));
            console.log(this.lastestNews);
            this.lastestNews.forEach((element) => {
                element.created_at = element.created_at.split("T")[0];
                element.updated_at = element.updated_at.split("T")[0];
            });
        },
        // Load one news by id
        async show(id) {
            this.errors = [];
            await axios
                .get("/api/news/show/" + id)
                .then(async (responsePHP) => {
                    this.news = responsePHP.data;
                    this.news.created_at = this.news.created_at.split("T")[0];
                    this.news.updated_at = this.news.updated_at.split("T")[0];
                });
        },
        // Api request for create news
        async create(formData) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/news/create", formData)
                .then((responsePHP) => {
                    responsePHP.data.created_at =
                        responsePHP.data.created_at.split("T")[0];
                    responsePHP.data.updated_at =
                        responsePHP.data.updated_at.split("T")[0];
                    this.allNews.push(responsePHP.data);
                    this.$router.push({ name: "news-list" });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // Api request for update a news
        async update(formData) {
            formData.append("form", JSON.stringify(this.news));
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/news/update/" + this.news.id, formData)
                .then((responsePHP) => {
                    const news = responsePHP.data;
                    news.created_at = news.created_at.split("T")[0];
                    news.updated_at = news.updated_at.split("T")[0];
                    const foundIndex = this.allNews.findIndex(
                        (news) => news.id === news.id
                    );
                    this.allNews[foundIndex] = news;
                    this.news = news;
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // Api request to delete a picture
        async deletePicture() {
            await axios
                .post("/api/news/delete-picture", { news: this.news })
                .then((responsePHP) => {
                    this.news.media = "default.jpg";
                    this.news.media_type = "image";
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Api request for destroy company
        async destroy(id) {
            // Send request
            await axios
                .post("/api/news/destroy/" + id, { newsId: id })
                .then((responsePHP) => {
                    let news = this.allNews.find(
                        (allNews) => allNews.id === responsePHP.data.id
                    );
                    news.status = responsePHP.data.status;
                    this.$router.push({ name: "news-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        // Get data for dataTable
        async getDataForDataTable(numPage, nbEntries, searchValue) {
            const res = await axios.get("/api/datatable/news-list", {
                params: {
                    numPage,
                    nbEntries,
                    searchValue,
                },
            });
            return res.data;
        },
    },
});
