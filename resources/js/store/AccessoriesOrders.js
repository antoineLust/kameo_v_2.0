import axios from "axios";
import { defineStore } from "pinia";

export const useAccessoriesOrdersStore = defineStore("accessoriesOrders", {
    id: "accessoriesOrders",
    state: () => ({
        accessoriesOrders: [],
        accessoryOrder: {
            id: null,
            size: null,
            type: null,
            amount: null,
            description: null,
            estimated_delivery_date: null,
            delivery_date: null,
            billing_type: null,
            status: null,
            grouped_orders_id: null,
            accessories_catalog_id: null,
            accessories_id: null,
            catalog: {},
        },
        errors: {},
    }),
    getters: {
        getAccessoryOrder: (state) => {
            if (state.accessoryOrder.size === "unique") {
                state.accessoryOrder.size = "UNIQUE";
            }
            return state.accessoryOrder;
        },
        getAccessoriesOrders: (state) => {
            return state.accessoriesOrders;
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        async getAccessoriesFromGroupedOrder(id) {
            const reponsePHP = await axios.get(
                "/api/accessory-orders/get-from-grouped-order/" + id
            );
            return reponsePHP.data;
        },

        // Api request to get all accessories orders
        async loadAccessoriesOrders() {
            await axios.get("/api/accessory-orders").then((reponsePHP) => {
                this.accessoriesOrders = reponsePHP.data;
            });
        },
        // Api request to get one accessory order
        async show(id) {
            this.errors = [];
            await axios
                .get("/api/accessory-orders/show/" + id)
                .then((reponsePHP) => {
                    this.accessoryOrder = reponsePHP.data;
                });
        },
        // Api request for create accessory order
        async create(form) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/accessory-orders/create", form)
                .then((responsePHP) => {
                    this.accessoriesOrders.push(responsePHP.data);
                    this.$router.push({
                        name: "grouped-orders-update",
                        params: { id: responsePHP.data.grouped_orders_id },
                    });
                })
                .catch((error) => {
                    if (
                        typeof error.response != "undefined" &&
                        error.response &&
                        error.response.status === 422
                    ) {
                        this.errors = error.response.data.errors;
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for update accessory order
        async update() {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post(
                    "/api/accessory-orders/update/" + this.accessoryOrder.id,
                    this.accessoryOrder
                )
                .then((responsePHP) => {
                    this.accessoryOrder = responsePHP.data;
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors;
                    } else {
                        if (
                            typeof error.response != "undefined" &&
                            error.response &&
                            error.response.status === 422
                        ) {
                            this.errors = error.response.data.errors;
                        } else {
                            console.error(error);
                        }
                    }
                });
        },
        // Api request for destroy accessory order
        async destroy(id) {
            // Send request
            await axios
                .post("/api/accessory-orders/destroy/" + id, {
                    accessoryOrderId: id,
                })
                .then((responsePHP) => {
                    let removAccessoryOrder = this.accessoriesOrders
                        .map(function (item) {
                            return item.id;
                        })
                        .indexOf(id);
                    this.accessoriesOrders.splice(removAccessoryOrder, 1);
                    this.$router.go(-1);
                })
                .catch((error) => {
                    if (
                        typeof error.response != "undefined" &&
                        error.response &&
                        error.response.status === 422
                    ) {
                        this.errors = error.response.data.errors;
                    } else {
                        console.error(error);
                    }
                });
        },
    },
});
