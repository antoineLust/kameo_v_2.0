import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "./StoresIndex";

export const useCompaniesStore = defineStore("companies", {
    id: "companies",
    state: () => ({
        companies: [],
        company: {
            id: null,
            name: null,
            logo: "companies_logo/default.png",
            vat_number: null,
            billing_group: null,
            type: null,
            audience: null,
            aquisition: null,
            status: null,
            companies_id: null,
            companies_addresses_id: null,
            address: [],
        },
        errors: {},
        fileErrors: {
            message: null,
        },
        newClientForEntretienId: null,
    }),
    getters: {
        getCompanies: (state) => {
            return state.companies;
        },
        getLogo: (state) => {
            return state.logo;
        },
        getCompany: (state) => {
            return state.company;
        },
        getErrors: (state) => {
            return state.errors;
        },
        getFileErrors: (state) => {
            return state.fileErrors;
        },
        // getCompanyById: (state) => (id) => {
        //     if (id) {
        //         return state.companies.find(
        //             (company) => company.id == Number(id)
        //         );
        //     }
        //     // Return empty company for vue router params to work
        //     else {
        //         return {
        //             id: 0,
        //         };
        //     }
        // },
    },
    actions: {
        async getCompaniesByAudience(audience) {
            const responsePHP = await axios.get(
                "/api/companies/get-companies-by-audience/" + audience
            );
            return responsePHP.data;
        },

        async getCompanyById(id) {
            const responsePHP = await axios.get(
                "/api/companies/get-company-by-id/" + id
            );
            return responsePHP.data;
        },

        // Api request for load all entretiens
        async loadCompanies() {
            this.companies = await axios
                .get("/api/companies")
                .then((reponsePHP) => (this.companies = reponsePHP.data));
        },
        // Load one company by id
        async show(id) {
            this.errors = [];
            await axios.get("/api/companies/show/" + id).then((reponsePHP) => {
                this.company = reponsePHP.data.company;
                this.company.conditions = reponsePHP.data.conditions
                    ? reponsePHP.data.conditions.access.split(",")
                    : [];
            });
        },
        // Api request for create company
        async create(formData) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/companies/create", formData)
                .then((responsePHP) => {
                    this.companies.push(responsePHP.data.company);
                    this.$router.push({ name: "companies-list" });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for update company
        update(formData) {
            // Set timeout to wait for the request to be sent
            setTimeout(() => {
                // Append company to formData
                formData.append("form", JSON.stringify(this.company));
                // Reset errors
                this.errors = [];
                // Send request
                axios
                    .post("/api/companies/update/" + this.company.id, formData)
                    .then((responsePHP) => {
                        const company = responsePHP.data.company;
                        const companyIndex = this.companies.findIndex(
                            (companies) =>
                                companies.id === responsePHP.data.company.id
                        );
                        this.companies[companyIndex] = company;
                        this.company = company;
                        if (responsePHP.data.conditions != null) {
                            const conditionIndex =
                                stores.conditionsStore.$state.conditions.findIndex(
                                    (condition) =>
                                        condition.id ==
                                        responsePHP.data.conditions.id
                                );
                            const split =
                                responsePHP.data.conditions.access.split(", ");
                            if (conditionIndex > -1) {
                                stores.conditionsStore.$state.conditions[
                                    conditionIndex
                                ] = responsePHP.data.conditions;

                                this.companies[companyIndex].conditions = split;
                                this.company.conditions = split;
                            } else if (conditionIndex == -1) {
                                stores.conditionsStore.$state.conditions.push(
                                    responsePHP.data.conditions
                                );
                                this.companies[companyIndex].conditions = split;
                                this.company.conditions = split;
                            }
                        } else {
                            stores.conditionsStore.$state.conditions.splice(
                                stores.conditionsStore.$state.conditions.findIndex(
                                    (condition) =>
                                        condition.companies_id ==
                                        responsePHP.data.company.id
                                ),
                                1
                            );
                            this.companies[companyIndex].conditions = [];
                            this.company.conditions = [];
                        }
                    })
                    .catch((error) => {
                        if (error.response) {
                            if (
                                error.response &&
                                error.response.status === 422
                            ) {
                                this.errors = error.response.data.errors || {};
                            }
                        } else {
                            console.error(error);
                        }
                    });
            }, 1000);
        },
        // Api request for destroy company
        async destroy(id) {
            // Send request
            await axios
                .post("/api/companies/destroy/" + id, { companyId: id })
                .then((responsePHP) => {
                    let company = this.companies.find(
                        (companies) => companies.id === responsePHP.data.id
                    );
                    company.status = responsePHP.data.status;
                    this.$router.push({ name: "companies-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Api request to delete a picture
        async deletePicture() {
            await axios
                .post("/api/companies/delete-picture", {
                    company: this.company,
                })
                .then((responsePHP) => {
                    this.company.logo = "companies_logo/default.png";
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // API request for create a new client for a entretien
        async createClientForEntretien(form) {
            // Reset errors
            this.errors = [];
            // Send request
            try {
                const responsePHP = await axios.post(
                    "/api/companies/create-client-for-entretien",
                    form
                );
                this.companies.push(responsePHP.data);
                this.newClientForEntretienId = responsePHP.data.id;
            } catch (error) {
                if (error.response) {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    }
                } else {
                    console.error(error);
                }
            }
        },
        // Get data for dataTable
        async getDataForDataTable(
            numPage,
            nbEntries,
            searchValue,
            sortDirection
        ) {
            const res = await axios.get("/api/datatable/companies-list", {
                params: {
                    numPage,
                    nbEntries,
                    searchValue,
                    sortDirection,
                },
            });
            return res.data;
        },
    },
});
