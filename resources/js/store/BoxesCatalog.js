import axios from "axios";
import { defineStore } from "pinia";

export const useBoxesCatalogStore = defineStore("boxesCatalog", {
    id: "boxesCatalog",
    state: () => ({
        boxesCatalog: [],
        boxCatalog: {
            id: null,
            model: null,
            production_price: null,
            installation_price: null,
            location_price: null,
        },
        errors: [],
    }),
    getters: {
        getBoxesCatalog: (state) => {
            return state.boxesCatalog;
        },
        getBoxCatalog: (state) => {
            return state.boxCatalog;
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        async getBoxCatalogById(id) {
            const responsePHP = await axios.get(
                "/api/boxes-catalog/get-box-catalog-by-id/" + id
            );
            return responsePHP.data;
        },

        // Api request for load all bikes catalog
        async loadBoxesCatalog() {
            this.boxesCatalog = await axios
                .get("/api/boxes-catalog")
                .then((reponsePHP) => (this.boxesCatalog = reponsePHP.data));
        },
    },
});
