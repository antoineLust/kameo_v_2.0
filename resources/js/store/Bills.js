import axios from "axios";
import { defineStore } from "pinia";

export const useBillsStore = defineStore("bills", {
    id: "bills",
    state: () => ({
        bills: [],
        bill: {
            id: null,
            beneficiary_company: null,
            date: null,
            amount_htva: null,
            amount_tvac: null,
            billing_group: null,
            communication: null,
            file_name: null,
            invoice_sent: null,
            invoice_sent_date: null,
            invoice_paid: null,
            invoice_paid_date: null,
            invoice_limit_paid_date: null,
            accounting: null,
            type: null,
            payement: null,
            credit_notes_id: null,
            users_id: null,
            companies_id: null,
            user: {},
            company: {},
        },
        errors: {},
    }),
    getters: {
        getBills: (state) => {
            return state.bills;
        },
        getBill: (state) => {
            return state.bill;
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        // Api request for load all bills
        async loadBills() {
            this.bills = await axios
                .get("/api/bills")
                .then((responsePHP) => (this.bills = responsePHP.data));
        },
        // Load one bill by id
        async show(id) {
            this.errors = [];
            await axios.get("/api/bills/show/" + id).then((responsePHP) => {
                this.bill = responsePHP.data;
            });
        },
        // Api request for create bill
        async create(form) {
            // Send request
            this.errors = [];
            // Send request
            await axios
                .post("/api/bills/create", form)
                .then((responsePHP) => {
                    this.bills.unshift(responsePHP.data);
                    this.$router.push({ name: "bills-list" });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for update bill
        async update(formData) {
            // Reset errors
            this.errors = [];
            // hydrate formData
            formData.append("form", JSON.stringify(this.bill));
            // Send request
            await axios
                .post("/api/bills/update/" + this.bill.id, formData)
                .then((responsePHP) => {
                    const bill = responsePHP.data;
                    const foundIndex = this.bills.findIndex(
                        (bill) => bill.id === bill.id
                    );
                    this.bills[foundIndex] = bill;
                    this.bill = bill;
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors;
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for destroy bill
        async destroy(id) {
            // Send request
            await axios
                .post("/api/bills/destroy/" + id, { billId: id })
                .then((responsePHP) => {
                    this.bills = responsePHP.data;
                    this.$router.push({ name: "bills-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Get data for dataTable
        async getDataForDataTable(
            numPage,
            nbEntries,
            searchValue,
            sortDirection
        ) {
            const res = await axios.get("/api/datatable/bills-list", {
                params: {
                    numPage,
                    nbEntries,
                    searchValue,
                    sortDirection,
                },
            });
            return res.data;
        },
    },
});
