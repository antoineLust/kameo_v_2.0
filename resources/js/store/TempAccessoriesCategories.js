import axios from "axios";
import { defineStore } from "pinia";

export const useTempAccessoriesCategoriesStore = defineStore(
    "tempAccessoriesCategories",
    {
        id: "tempAccessoriesCategories",
        state: () => ({
            tempAccessoriesCategories: [],
            tempAccessoriesCategory: {},
        }),
        getters: {
            getTempAccessoriesCategories: (state) => {
                return state.tempAccessoriesCategories;
            },
        },
        actions: {
            // Api request for load all temp accessories categories
            async loadTempAccessoriesCategories() {
                this.tempAccessoriesCategories = await axios
                    .get("/api/temp-accessories-categories")
                    .then(
                        (reponsePHP) =>
                            (this.tempAccessoriesCategories = reponsePHP.data)
                    );
            },
            async loadOneById(id) {
                this.tempAccessoriesCategory =
                    this.tempAccessoriesCategories.find(
                        (tempAccessoriesCategories) =>
                            tempAccessoriesCategories.id === Number(id)
                    );
            },
        },
    }
);
