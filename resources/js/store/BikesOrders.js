import axios from "axios";
import { defineStore } from "pinia";

export const useBikesOrdersStore = defineStore("bikesOrders", {
    id: "bikesOrders",
    state: () => ({
        bikesOrders: [],
        bikeOrder: {
            id: null,
            size: null,
            color: null,
            remark: null,
            amount: null,
            type: null,
            estimated_delivery_date: null,
            delivery_date: null,
            comment: null,
            status: null,
            bikes_id: null,
            grouped_orders_id: null,
            bikes_catalog_id: null,
            catalog: {},
        },
        errors: {},
    }),
    getters: {
        getOrders: (state) => {
            return state.bikesOrders;
        },
        getOrder: (state) => {
            return state.bikeOrder;
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        async getBikesFromGroupedOrder(id) {
            const reponsePHP = await axios.get(
                "/api/bike-orders/get-from-grouped-orders/" + id
            );
            return reponsePHP.data;
        },

        // Api request to get all bikes orders
        async loadBikesOrders() {
            this.bikesOrders = await axios
                .get("/api/bike-orders")
                .then((reponsePHP) => (this.bikesOrders = reponsePHP.data));
        },
        // Api request to get one bike order
        async show(id) {
            this.errors = [];
            this.bikeOrder = await axios
                .get("/api/bike-orders/show/" + id)
                .then((reponsePHP) => (this.bikeOrder = reponsePHP.data));
        },
        // Api request for create bike order
        async create(form) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/bike-orders/create", form)
                .then((responsePHP) => {
                    this.$router.push({
                        name: "grouped-orders-update",
                        params: { id: responsePHP.data.grouped_orders_id },
                    });
                })
                .catch((error) => {
                    if (
                        typeof error.response != "undefined" &&
                        error.response &&
                        error.response.status === 422
                    ) {
                        this.errors = error.response.data.errors;
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for update bike order
        async update() {
            // Send request
            await axios
                .post(
                    "/api/bike-orders/update/" + this.bikeOrder.id,
                    this.bikeOrder
                )
                .then((responsePHP) => {
                    this.bikeOrder = responsePHP.data;
                    this.errors = [];
                })
                .catch((error) => {
                    this.errors = [];
                    if (
                        typeof error.response != "undefined" &&
                        error.response &&
                        error.response.status === 422
                    ) {
                        this.errors = error.response.data.errors;
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for destroy bike order
        async destroy(id) {
            // Send request
            await axios
                .post("/api/bike-orders/destroy/" + id, { bikeOrderId: id })
                .then((responsePHP) => {
                    const removeBikeOrder = this.bikesOrders.findIndex(
                        function (item) {
                            return item.id === id;
                        }
                    );
                    if (removeBikeOrder !== -1)
                        this.bikesOrders.splice(removeBikeOrder, 1);
                    this.$router.push({
                        name: "grouped-orders-update",
                        params: { id: responsePHP.data },
                    });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
    },
});
