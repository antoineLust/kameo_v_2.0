import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "./StoresIndex";
import { isEmpty, isEqual } from "../functions/globalFunctions";

export const useOrdersStore = defineStore("orders", {
    id: "orders",
    state: () => ({
        orders: [],
        order: {
            id: null,
            companies_id: null,
            users_id: null,
            user: {},
            company: {},
            bikes: {},
            accessories: {},
            boxes: {},
        },
        user_order: {},
        errors: {},
    }),
    getters: {
        getOrders: (state) => {
            return state.orders;
        },
        getOrder: (state) => {
            return state.order;
        },
        getUserOrder: (state) => {
            return state.user_order;
        },
        getOrderByUserAuth: (state) => {
            const groupedOrder = state.orders.find(
                (order) => order.users_id === stores.usersStore.getUserAuth.id
            );
            return groupedOrder;
        },
        getOrderByCompanyAuth: (state) => {
            const groupedOrder = state.orders.filter(
                (order) =>
                    order.companies_id ===
                    stores.usersStore.getUserAuth.companies_id
            );
            return groupedOrder;
        },
        getOrderSellingPriceByUserAuth: (state) => {
            const groupedOrder = state.orders.find(
                (order) => order.users_id === stores.usersStore.getUserAuth.id
            );
            if (groupedOrder) {
                let total = 0;
                groupedOrder.bikes.forEach((bike) => {
                    if (bike.type === "selling") {
                        total += parseInt(bike.amount);
                    }
                });
                groupedOrder.accessories.forEach((accessory) => {
                    if (accessory.type === "selling") {
                        total += parseInt(accessory.amount);
                    }
                });
                return total.toFixed(2);
            }
        },
        getOrderLeasingPriceByUserAuth: (state) => {
            const groupedOrder = state.orders.find(
                (order) => order.users_id === stores.usersStore.getUserAuth.id
            );
            if (groupedOrder) {
                let total = 0;
                groupedOrder.bikes.forEach((bike) => {
                    if (
                        bike.type === "leasing" ||
                        bike.type === "annual_leasing" ||
                        bike.type === "full_leasing"
                    ) {
                        total += parseInt(bike.amount);
                    }
                });
                groupedOrder.accessories.forEach((accessory) => {
                    if (accessory.type === "leasing") {
                        total += parseInt(accessory.amount);
                    }
                });
                return total.toFixed(2);
            }
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        async verifyOrder() {
            const responsePHP = await axios.post(
                "/api/grouped-orders/verify-order"
            );
            this.user_order = responsePHP.data;
            localStorage.setItem("userOrder", JSON.stringify(this.user_order));
        },

        // Api request for load all orders
        async loadOrders() {
            await axios
                .get("/api/grouped-orders")
                .then((responsePHP) => (this.orders = responsePHP.data));
        },
        // Api request for load one order
        async show(id) {
            await axios
                .get("/api/grouped-orders/show/" + id)
                .then((responsePHP) => (this.order = responsePHP.data));
        },
        // Api request for create order
        async create(form) {
            // Send request
            await axios
                .post("/api/grouped-orders/create", form)
                .then((responsePHP) => {
                    this.orders.unshift(responsePHP.data);
                    this.$router.push({
                        name: "grouped-orders-update",
                        params: { id: responsePHP.data.id },
                    });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors;
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for update order
        async update() {
            // Send request
            await axios
                .post("/api/grouped-orders/update/" + this.order.id, this.order)
                .then((responsePHP) => {
                    const order = responsePHP.data;
                    const foundIndex = this.orders.findIndex(
                        (orders) => orders.id === order.id
                    );
                    this.orders[foundIndex] = order;
                    this.order = order;
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors;
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for destroy order
        async destroy(id) {
            // Send request
            await axios
                .post("/api/grouped-orders/destroy/" + id, { orderId: id })
                .then((responsePHP) => {
                    this.orders.splice(id, 1);
                    this.$router.push({ name: "grouped-orders-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // API request for generate purchase order
        async generatePurchaseOrder(data) {
            // Send request
            await axios
                .post("/api/test", data)
                // .post("/api/purchase-order/generate", data)
                .then((responsePHP) => {
                    window.open(responsePHP.data.fileName);
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // API request for generate invoice
        async generateInvoice(data) {
            // Send request
            await axios
                .post(
                    "/api/invoice/generate/" +
                        this.$router.currentRoute.value.params.id,
                    data
                )
                .then((responsePHP) => {
                    window.open(responsePHP.data.fileName);
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // API request for generate credit note
        async generateCreditNote(data) {
            // Send request
            await axios
                .post(
                    "/api/creditNote/generate/" +
                        this.$router.currentRoute.value.params.id,
                    data
                )
                .then((responsePHP) => {
                    window.open(responsePHP.data.fileName);
                    responsePHP.data.accessories.forEach((item) => {
                        const index =
                            stores.accessoriesStockStore.$state.accessoriesStock.findIndex(
                                (accessory) => {
                                    accessory.id === item.id;
                                }
                            );
                        if (index !== -1) {
                            stores.accessoriesStockStore.$state.accessoriesStock[
                                index
                            ] = item;
                        }
                    });
                    responsePHP.data.accessories_order.forEach((item) => {
                        const index =
                            stores.accessoriesOrdersStore.$state.accessoriesOrders.findIndex(
                                (accessory) => {
                                    accessory.id === item.id;
                                }
                            );
                        if (index !== -1) {
                            stores.accessoriesOrdersStore.$state.accessoriesOrders[
                                index
                            ] = item;
                        }
                    });
                    responsePHP.data.bikes_order.forEach((item) => {
                        const index =
                            stores.bikesOrdersStore.$state.bikesOrders.findIndex(
                                (bike) => {
                                    bike.id === item.id;
                                }
                            );
                        if (index !== -1) {
                            stores.bikesOrdersStore.$state.bikesOrders[index] =
                                item;
                        }
                    });
                    responsePHP.data.bikes.forEach((item) => {
                        const index =
                            stores.bikesStockStore.$state.bikesStock.findIndex(
                                (bike) => {
                                    bike.id === item.id;
                                }
                            );
                        if (index !== -1) {
                            stores.bikesStockStore.$state.bikesStock[index] =
                                item;
                        }
                    });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Create order from cafetaria
        async createOrderFromCafetaria(data) {
            // Send request
            await axios
                .post("/api/grouped-orders/create-from-cafetaria", data)
                .then((responsePHP) => {
                    responsePHP.data.accessories_order.forEach((item) => {
                        const index =
                            stores.accessoriesOrdersStore.$state.accessoriesOrders.findIndex(
                                (accessory) => {
                                    accessory.id === item.id;
                                }
                            );
                        if (index !== -1) {
                            stores.accessoriesOrdersStore.$state.accessoriesOrders[
                                index
                            ] = item;
                        }
                    });
                    stores.bikesOrdersStore.$state.bikesOrders.push(
                        responsePHP.data.bike_order
                    );
                    this.loadOrders();
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Verify if all order items are done
        verifyIfAllOrderItemsSatus() {
            const groupedOrder = this.orders.find(
                (order) => order.users_id === stores.usersStore.getUserAuth.id
            );
            let status = [];
            // Verify all orders items status
            if (groupedOrder.bikes.length > 0) {
                groupedOrder.bikes.forEach((bike) => {
                    status.push(bike.status);
                });
            }
            if (groupedOrder.accessories.length > 0) {
                groupedOrder.accessories.forEach((accessory) => {
                    status.push(accessory.status);
                });
            }
            if (groupedOrder.boxes.length > 0) {
                groupedOrder.boxes.forEach((box) => {
                    status.push(box.status);
                });
            }
            // Verify if all status contains 'new', 'confirmed' or 'done'
            if (status.includes("new")) {
                return "new";
            } else if (status.includes("confirmed")) {
                return "confirmed";
            } else if (status.includes("done")) {
                return "done";
            }
        },
        // Verify if all order items are done
        verifyIfAllItemsOrderAreDone() {
            const groupedOrder = this.orders.find(
                (order) => order.users_id === stores.usersStore.getUserAuth.id
            );
            let status = [];
            if (groupedOrder) {
                if (groupedOrder.bikes.length > 0) {
                    groupedOrder.bikes.forEach((bike) => {
                        status.push(bike.status);
                    });
                }
                if (groupedOrder.accessories.length > 0) {
                    groupedOrder.accessories.forEach((accessory) => {
                        status.push(accessory.status);
                    });
                }
                if (groupedOrder.boxes.length > 0) {
                    groupedOrder.boxes.forEach((box) => {
                        status.push(box.status);
                    });
                }
                // Verify if all status contains 'new' or 'confirmed'
                if (status.includes("new") || status.includes("confirmed")) {
                    return false;
                } else if (status.includes("done")) {
                    return true;
                }
            } else {
                return true;
            }
        },
        // Verify if user have an order
        verifyIfUserHaveAnOrder() {
            const groupedOrder = this.orders.find(
                (order) => order.users_id === stores.usersStore.getUserAuth.id
            );
            return !!groupedOrder;
        },
        // Confirm order
        async confirmedOrder(id) {
            // Send request
            await axios
                .post("/api/grouped-orders/confirmed/" + id, { orderId: id })
                .then((responsePHP) => {
                    responsePHP.data.accessories_order.forEach((item) => {
                        const index =
                            stores.accessoriesOrdersStore.$state.accessoriesOrders.findIndex(
                                (accessory) => {
                                    accessory.id === item.id;
                                }
                            );
                        if (index !== -1) {
                            stores.accessoriesOrdersStore.$state.accessoriesOrders[
                                index
                            ] = item;
                        }
                    });
                    responsePHP.data.bike_order.forEach((item) => {
                        const index =
                            stores.bikesOrdersStore.$state.bikesOrders.findIndex(
                                (bike) => {
                                    bike.id === item.id;
                                }
                            );
                        if (index !== -1) {
                            stores.bikesOrdersStore.$state.bikesOrders[index] =
                                item;
                        }
                    });
                    this.loadOrders();
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        updateState(data) {
            if (!isEmpty(this.orders)) {
                if (!isEqual(this.orders, data.data)) {
                    this.orders = data.data;
                }

                if (!isEmpty(this.order)) {
                    const path = this.$router.currentRoute.value.fullPath;
                    if (
                        data.id === this.order.id &&
                        (path.includes(
                            `/dashboard/grouped-orders/update/${data.id}`
                        ) ||
                            path.includes(
                                `/dashboard/grouped-orders/show/${data.id}`
                            ))
                    ) {
                        this.$router.push({ name: "grouped-orders-list" });
                    }
                }
            }
        },

        // Get data for dataTable
        async getDataForDataTable(
            numPage,
            nbEntries,
            searchValue,
            sortDirection
        ) {
            const res = await axios.get("/api/datatable/grouped-orders-list", {
                params: {
                    numPage,
                    nbEntries,
                    searchValue,
                    sortDirection,
                },
            });
            return res.data;
        },
    },
});
