import axios from "axios";
import { defineStore } from "pinia";

export const useExternalBikesStore = defineStore("externalBikes", {
    id: "externalBikes",
    state: () => ({
        externalBikes: [],
        newExternalBikeId: null,
        errors: [],
    }),
    getters: {
        getExternalBikes: (state) => {
            return state.externalBikes;
        },
        getErrors: (state) => {
            return state.errors;
        },
    },

    actions: {
        async getExternalBikeById(id) {
            const responsePHP = await axios.get(
                "/api/external-bikes/get-external-bike-by-id/" + id
            );
            return responsePHP.data;
        },

        async getExternalBikesByCompanyId(companyId) {
            const responsePHP = await axios.get(
                "/api/external-bikes/get-external-bikes-by-company-id/" +
                    companyId
            );
            return responsePHP.data;
        },

        // Get all bikes
        async loadExternalBikes() {
            await axios.get("/api/external-bikes").then((reponsePHP) => {
                this.externalBikes = reponsePHP.data;
            });
        },
        // Add a bike
        async create(form) {
            this.errors = [];
            try {
                const reponsePHP = await axios.post(
                    "/api/external-bikes/create",
                    form
                );
                const externalBike = reponsePHP.data;
                this.newExternalBikeId = externalBike.id;
                this.externalBikes.push(externalBike);
            } catch (error) {
                if (error.response && error.response.status === 422) {
                    this.errors = error.response.data.errors || {};
                } else {
                    console.error(error);
                }
            }
        },
    },
});
