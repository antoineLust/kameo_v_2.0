import axios from "axios";
import { defineStore } from "pinia";
import { isEmpty, isEqual, cloneDeep } from "../functions/globalFunctions";
import * as stores from "./StoresIndex";

export const useChatStore = defineStore("chat", {
    id: "chat",
    state: () => ({
        chats: [],
        chat: [],
        sessions: [],
        session: {},
        typing_state: false,
    }),
    getters: {
        getChat: (state) => {
            return state.chat;
        },

        getTypingState: (state) => {
            return state.typing_state;
        },

        getSession: (state) => {
            return state.session;
        },

        getSessions: (state) => {
            return state.sessions.sort((a, b) => {
                const fa = a.updated_at;
                const fb = b.updated_at;

                if (fa < fb) {
                    return 1;
                }
                if (fa > fb) {
                    return -1;
                }
                return 0;
            });
        },
    },

    actions: {
        async getSessionByClientId(id) {
            const responsePHP = await axios.get(
                "/api/chat/get-session-by-client-id/" + id
            );
            this.session = isEmpty(responsePHP.data) ? {} : responsePHP.data;
        },

        async loadSessions() {
            const responsePHP = await axios.get("/api/chat/sessions");
            this.sessions = responsePHP.data;
        },

        async show(id) {
            const responsePHP = await axios.get("/api/chat/show/" + id);
            this.chat = responsePHP.data;
            this.session = responsePHP.data[0]
                ? responsePHP.data[0].session
                : {};
        },

        async create(data) {
            const responsePHP = await axios.post("/api/chat/create", data);
            this.chat.push(...responsePHP.data);
            const session = responsePHP.data[0].session;
            if (isEmpty(this.session)) {
                this.session = session;
            }
            if (!isEmpty(this.sessions)) {
                const foundIndex = this.sessions.findIndex(
                    (session2) => session2.id === session.id
                );
                if (foundIndex !== -1) {
                    this.sessions[foundIndex] = session;
                }
            }
            return session;
        },

        async assignEmployee(data) {
            const responsePHP = await axios.post("/api/chat/assign", data);
            const foundIndex = this.sessions.findIndex(
                (session) => session.id === responsePHP.data.id
            );
            if (foundIndex !== -1) {
                this.sessions[foundIndex] = responsePHP.data;
            }
            if (!isEqual(this.session, responsePHP.data)) {
                this.session = responsePHP.data;
            }
        },

        newSession(msg) {
            this.sessions.unshift({
                id: msg.session_id,
                client_id: msg.client_id,
                notif_employee_count: 1,
            });
        },

        employeeAssign(msg) {
            if (!isEmpty(this.session) && msg.session_id === this.session.id) {
                this.session.employee_id = msg.employee_id;
            }
        },

        employeeAssignChatView(msg) {
            if (!isEmpty(this.sessions)) {
                const foundIndex = this.sessions.findIndex(
                    (session) => session.id === msg.session_id
                );
                if (foundIndex !== -1) {
                    let obj = cloneDeep(this.sessions[foundIndex]);
                    obj.employee_id = msg.employee_id;
                    this.sessions[foundIndex] = obj;
                }
            }
        },

        typing(msg) {
            if (!isEmpty(this.chat)) {
                if (
                    this.session.id === msg.session_id &&
                    this.typing_state !== msg.typing &&
                    stores.usersStore.getUserAuth.id !== msg.sender_id
                ) {
                    this.typing_state = msg.typing;
                }
            }
        },

        chatSend(msg) {
            if (!isEmpty(this.sessions)) {
                const foundIndex = this.sessions.findIndex(
                    (session) => session.id === msg.session.id
                );
                if (foundIndex !== -1) {
                    this.sessions[foundIndex] = msg.session;
                }
            }
            if (!isEmpty(this.chat)) {
                if (
                    this.session.id === msg.session.id &&
                    stores.usersStore.getUserAuth.id !== msg.sender_id
                ) {
                    this.chat.push(msg);
                    return true;
                }
            }
            return false;
        },

        updateSessionEmployeeCount(id) {
            if (!isEmpty(this.sessions)) {
                const foundIndex = this.sessions.findIndex(
                    (session) => session.id === id
                );
                if (foundIndex !== -1) {
                    this.sessions[foundIndex].notif_employee_count += 1;
                }
            }
        },

        incrementEmployeeNotif() {
            axios.post("/api/chat/incrementEmployeeNotif", this.session);
        },

        async resetEmployeeNotif() {
            const responsePHP = await axios.post(
                "/api/chat/resetEmployeeNotif",
                this.session
            );
            const foundIndex = this.sessions.findIndex(
                (session) => session.id === responsePHP.data.id
            );
            if (foundIndex !== -1) {
                this.sessions[foundIndex] = responsePHP.data;
            }
            if (!isEqual(this.session, responsePHP.data)) {
                this.session = responsePHP.data;
            }
        },

        resetEmployeeNotifSocket(msg) {
            if (!isEmpty(this.sessions)) {
                const foundIndex = this.sessions.findIndex(
                    (session) => session.id === msg.session_id
                );
                if (foundIndex !== -1) {
                    let obj = cloneDeep(this.sessions[foundIndex]);
                    obj.notif_employee_count = 0;
                    this.sessions[foundIndex] = obj;
                }
            }
        },

        incrementClientNotif() {
            axios.post("/api/chat/incrementClientNotif", this.session);
        },

        async resetClientNotif() {
            const responsePHP = await axios.post(
                "/api/chat/resetClientNotif",
                this.session
            );
            const foundIndex = this.sessions.findIndex(
                (session) => session.id === responsePHP.data.id
            );
            if (foundIndex !== -1) {
                this.sessions[foundIndex] = responsePHP.data;
            }
            this.session = responsePHP.data;
        },
    },
});
