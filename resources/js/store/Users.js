import axios from "axios";
import { defineStore } from "pinia";
import { isEmpty } from "../functions/globalFunctions";

export const useUsersStore = defineStore("users", {
    id: "users",
    state: () => ({
        userAuth: {},
        users: [],
        user: {
            id: "",
            firstname: "",
            lastname: "",
            email: "",
            size: "",
            password: "",
            phone: "",
            employees_id: "",
            employee: {},
            permission: "",
            status: "",
            avatar: "",
            company_id: "",
            company: {},
            users_addresses_id: "",
            address: {},
            users_works_addresses: "",
        },
        errors: {},
        message: null,
        password: {
            old: "",
            new: "",
            confirm: "",
        },
    }),
    getters: {
        getUserAuth: (state) => {
            return state.userAuth;
        },
        getUsers: (state) => {
            return state.users;
        },
        getUser: (state) => {
            return state.user;
        },
        getPassword: (state) => {
            return state.password;
        },
        getMessage: (state) => {
            return state.message;
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        async getFirstnameAndLastnameById(id) {
            const responsePHP = await axios.get(
                "/api/users/get-firstname-and-lastname-by-id/" + id
            );
            return responsePHP.data;
        },

        async getEmployee(role) {
            const responsePHP = await axios.get(
                "/api/users/get-employee/" + role
            );
            return responsePHP.data;
        },

        async getUsersByCompanyAuth() {
            const responsePHP = await axios.get(
                "/api/users/get-users-by-company/" + this.userAuth.companies_id
            );
            return responsePHP.data;
        },

        async getEmployeesByWorkAddress(id) {
            const responsePHP = await axios.get(
                "/api/users/get-employees-by-work-address/" + id
            );
            return responsePHP.data;
        },

        async getUsersByCompany(company_id) {
            const responsePHP = await axios.get(
                "/api/users/get-users-by-company/" + company_id
            );
            return responsePHP.data;
        },

        async getUserById(id) {
            const responsePHP = await axios.get(
                "/api/users/get-user-by-id/" + id
            );
            return responsePHP.data;
        },

        // Api request for load all users
        async loadUsers() {
            await axios.get("/api/users").then((reponsePHP) => {
                this.users = reponsePHP.data;
            });
        },
        // Api request for load connected user
        async loadUserAuth() {
            await axios.get("/api/users/auth").then((reponsePHP) => {
                this.userAuth = reponsePHP.data;
            });
        },
        // Load one user by id
        async show(id) {
            this.errors = [];
            await axios.get("/api/users/show/" + id).then((reponsePHP) => {
                this.user = reponsePHP.data;
            });
        },
        // Api request for update user
        update(formData) {
            setTimeout(() => {
                // Reset errors
                this.errors = [];
                // Append user
                let user = null;
                if (typeof this.user.id == "number") {
                    user = this.user;
                    formData.append("form", JSON.stringify(this.user));
                } else {
                    user = this.userAuth;
                    formData.append("form", JSON.stringify(this.userAuth));
                }
                // Send request
                axios
                    .post("/api/users/update/" + user.id, formData)
                    .then((responsePHP) => {
                        const user = responsePHP.data;
                        if (!isEmpty(this.users)) {
                            const foundIndex = this.users.findIndex(
                                (users) => users.id === responsePHP.data.id
                            );
                            this.users[foundIndex] = user;
                        }
                        this.user = user;
                        if (responsePHP.data.id === this.userAuth.id) {
                            this.userAuth = user;
                        }
                    })
                    .catch((error) => {
                        if (error.response && error.response.status === 422) {
                            this.errors = error.response.data.errors || {};
                        } else {
                            user = this.userAuth;
                            formData.append(
                                "form",
                                JSON.stringify(this.userAuth)
                            );
                        }
                    });
            }, 1000);
        },
        // Api request for destroy user
        async destroy(id) {
            // Send request
            await axios
                .post("/api/users/destroy/" + id, { userId: id })
                .then((responsePHP) => {
                    let user = this.users.find(
                        (users) => users.id === responsePHP.data.id
                    );
                    if (user) user.status = responsePHP.data.status;
                    this.$router.push({ name: "users-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Api requets for create user
        async create(form) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/users/create", form)
                .then((responsePHP) => {
                    this.users.push(responsePHP.data);
                    if (this.userAuth.permission.includes("admin")) {
                        this.$router.push({ name: "users-list" });
                    }
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Update password
        updatePassword() {
            // Reset errors
            this.errors = [];
            // Send request
            axios
                .post(
                    "/api/users/updatePassword/" + this.userAuth.id,
                    this.password
                )
                .then((responsePHP) => {
                    this.userAuth = responsePHP.data;
                    this.password = {
                        password: {
                            old: "",
                            new: "",
                            new_confirmation: "",
                        },
                    };
                    this.message = "Votre mot de passe a bien été modifié";
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Get data for dataTable
        async getDataForDataTable(
            numPage,
            nbEntries,
            searchValue,
            sortDirection
        ) {
            const res = await axios.get("/api/datatable/users-list", {
                params: {
                    numPage,
                    nbEntries,
                    searchValue,
                    sortDirection,
                },
            });
            return res.data;
        },
    },
});
