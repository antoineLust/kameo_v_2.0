import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "../store/StoresIndex";
import { isEmpty, isEqual } from "../functions/globalFunctions";

export const useBikeTestsStore = defineStore("bikeTests", {
    id: "bikeTests",
    state: () => ({
        bikeTests: [],
        bikeTest: {},
        errors: [],
    }),
    getters: {
        getBikeTests: (state) => {
            return state.bikeTests;
        },
        getBikeTest: (state) => {
            return state.bikeTest;
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        // Api request for creating a bike tests

        async create(form) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/bike-tests/create", form)
                .then((responsePHP) => {
                    this.bikeTests.push(responsePHP.data);
                    this.$router.push({ name: "bike-tests-list" });
                })
                .catch((error) => {
                    if (
                        typeof error.response != "undefined" &&
                        error.response &&
                        error.response.status === 422
                    ) {
                        this.errors = error.response.data.errors;
                    } else {
                        console.error(error);
                    }
                });
        },

        // Api request for updating a bike test
        async update() {
            // Reset errors
            // this.errors = [];
            // Send request
            await axios
                .post(
                    "/api/bike-tests/update/" + this.bikeTest.id,
                    this.bikeTest
                )
                .then((responsePHP) => {
                    const bikeTest = this.bikeTests.find(
                        (bikeTests) => bikeTests.id === responsePHP.data.id
                    );
                    this.bikeTests[bikeTest] = responsePHP.data;
                    this.errors = [];
                })
                .catch((error) => {
                    this.errors = [];
                    if (
                        typeof error.response != "undefined" &&
                        error.response &&
                        error.response.status === 422
                    ) {
                        this.errors = error.response.data.errors;
                    } else {
                        console.error(error);
                    }
                });
        },

        // Api request for load all bike tests
        async loadBikeTests() {
            await axios.get("/api/bike-tests").then((responsePHP) => {
                this.bikeTests = responsePHP.data;
            });
        },

        async loadOneById(id) {
            this.bikeTest = this.bikeTests.find(
                (bikeTests) => bikeTests.id === Number(id)
            );
        },

        // Load one biketest by id
        async show(id) {
            const responsePHP = await axios.get("/api/bikes-tests/show/" + id);
            this.bikeTest = responsePHP.data;
        },

        updateState(data) {
            if (!isEmpty(this.bikeTests)) {
                if (!isEmpty(this.bikeTest)) {
                    if (
                        data.id === this.bikeTest.id &&
                        !isEqual(this.bikeTest, data)
                    ) {
                        this.bikeTest = data;
                    }
                }
                const bikeTest = this.bikeTests.findIndex(
                    (bikeTests) => bikeTests.id === data.id
                );
                if (bikeTest === -1) {
                    this.bikeTests.unshift(data);
                } else if (!isEqual(this.bikeTests[bikeTest], data)) {
                    this.bikeTests[bikeTest] = data;
                }
            }
        },
    },
});
