import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "./StoresIndex";
import { calculateLeasingPrice, isEmpty } from "../functions/globalFunctions";

export const useAccessoriesCatalogStore = defineStore("accessories", {
    id: "accessories",
    state: () => ({
        accessoriesCatalog: [],
        accessoryCatalog: {
            id: null,
            brand: null,
            model: null,
            sizes: [],
            buy_price: null,
            price_htva: null,
            minimal_stock: null,
            optimal_stock: null,
            description: null,
            provider: null,
            display: null,
            status: null,
            accessory_categories_id: null,
            category: {},
            pictures: [],
        },
        errors: {},
        fileErrors: {},
    }),
    getters: {
        getAccessoriesCatalog: (state) => {
            return state.accessoriesCatalog;
        },
        getBrands: (state) => {
            return state.brands;
        },
        getAccessoryCatalog: (state) => {
            return state.accessoryCatalog;
        },
        getErrors: (state) => {
            return state.errors;
        },
        getFileErrors: (state) => {
            return state.fileErrors;
        },
        getPrice: (state) => (accessory, contract_type) => {
            let amount = 0;
            if (accessory && contract_type) {
                if (contract_type == "leasing") {
                    amount = calculateLeasingPrice(
                        accessory.price_htva,
                        1,
                        false
                    );
                } else if (contract_type == "annual_leasing") {
                    amount = calculateLeasingPrice(
                        accessory.price_htva,
                        12,
                        false
                    );
                } else if (contract_type == "selling") {
                    amount = accessory.price_htva;
                }
            }
            return amount;
        },
        getAccessoriesFromCafetaria: (state) => async (categoryId) => {
            const companyId = stores.usersStore.getUserAuth.company.id;
            const numbOfAccessoriesOrderable =
                stores.conditionsStore.getAccessoriesOrderable.filter(
                    (accessory) => accessory.company_id === companyId.toString()
                ).length;
            if (numbOfAccessoriesOrderable > 0) {
                const responsePHP = await axios.get(
                    "/api/accessories-catalog/get-from-category-cafetaria/" +
                        categoryId
                );
                let accessories = responsePHP.data;

                if (accessories.length > 0) {
                    stores.conditionsStore.getAccessoriesOrderable.forEach(
                        function (item) {
                            if (item.catalog_id === "*") {
                                if (
                                    item.type === "available" &&
                                    item.isFree === "Y"
                                ) {
                                    const index = accessories.findIndex(
                                        (accessory) =>
                                            accessory.id ===
                                            Number(item.catalog_id)
                                    );
                                    if (index !== -1) {
                                        accessories[index].price_htva =
                                            "Gratuit";
                                    }
                                }
                            } else {
                                if (
                                    item.type === "available" &&
                                    item.isFree === "N" &&
                                    item.catalog_id !== "*"
                                ) {
                                    let noFreeAccessory = accessories.find(
                                        (accessory) =>
                                            accessory.id ===
                                            Number(item.catalog_id)
                                    );
                                    if (!isEmpty(noFreeAccessory)) {
                                        accessories.push(noFreeAccessory);
                                    }
                                } else if (
                                    item.type === "available" &&
                                    item.isFree === "Y" &&
                                    item.catalog_id !== "*"
                                ) {
                                    const accessoryExist = accessories.filter(
                                        (accessory) =>
                                            accessory.id ===
                                            Number(item.catalog_id)
                                    ).length;
                                    if (accessoryExist === 0) {
                                        let freeAccessory = accessories.filter(
                                            (accessory) =>
                                                accessory.id ===
                                                Number(item.catalog_id)
                                        );
                                        if (!isEmpty(freeAccessory)) {
                                            freeAccessory[0].price_htva =
                                                "Gratuit";
                                            accessories.push(freeAccessory[0]);
                                        }
                                    } else if (accessoryExist > 0) {
                                        const index = accessories.findIndex(
                                            (accessory) =>
                                                accessory.id ===
                                                Number(item.catalog_id)
                                        );
                                        if (index !== -1) {
                                            accessories[index].price_htva =
                                                "Gratuit";
                                        }
                                    }
                                }
                            }
                        }
                    );
                }
                return accessories;
            } else {
                return false;
            }
        },
        getMandatoryAccessoriesFromCafetaria: async () => {
            const companyId = stores.usersStore.getUserAuth.company.id;
            let accessories = [];
            const numbOfAccessoriesOrderable =
                stores.conditionsStore.getAccessoriesOrderable.filter(
                    (accessory) => accessory.company_id === companyId.toString()
                ).length;
            if (numbOfAccessoriesOrderable > 0) {
                for (const item in stores.conditionsStore
                    .getAccessoriesOrderable) {
                    if (item.type === "mandatory") {
                        if (item.isFree === "Y") {
                            let freeAccessory = await this.getAccessoryById(
                                item.catalog_id
                            );
                            freeAccessory[0].price_htva = "Gratuit";
                            accessories.push(freeAccessory[0]);
                        } else {
                            let mandatoryAccessory =
                                await this.getAccessoryById(item.catalog_id);
                            accessories.push(mandatoryAccessory[0]);
                        }
                    }
                }
            }
            return accessories;
        },
        getAccessoryCatalogById: (state) => (id) => {
            return state.accessoriesCatalog.filter(
                (accessory) => accessory.id === id
            )[0];
        },
    },
    actions: {
        // Api request for load all accessories catalog
        async loadAccessoriesCatalog() {
            this.accessoriesCatalog = await axios
                .get("/api/accessories-catalog")
                .then(
                    (responsePHP) =>
                        (this.accessoriesCatalog = responsePHP.data)
                );
        },
        async loadAccessoriesCatalogByCategorie(categorie) {
            this.accessoriesCatalog = await axios
                .get(
                    "/api/accessories-catalog/get-all-accessories-for-shop/" +
                        categorie
                )
                .then(
                    (responsePHP) =>
                        (this.accessoriesCatalog = responsePHP.data)
                );
        },

        async loadAccessoriesCatalogByCategorieForSelect(category) {
            const responsePHP = await axios.get(
                "/api/accessories-catalog/get-models-from-category-for-select/" +
                    category
            );
            return responsePHP.data;
        },

        async getModelsFromCategory(category) {
            const responsePHP = await axios.get(
                "/api/accessories-catalog/get-models-from-category/" + category
            );
            return responsePHP.data;
        },

        async getAccessoryById(id) {
            const responsePHP = await axios.get(
                "/api/accessories-catalog/get-by-id/" + id
            );
            return responsePHP.data;
        },

        async getAccessoryByIdCafetaria(id) {
            const responsePHP = await axios.get(
                "/api/accessories-catalog/get-by-id-cafetaria/" + id
            );
            return responsePHP.data;
        },

        async getAccessoriesCatalogBrands() {
            const responsePHP = await axios.get(
                "/api/accessories-catalog/get-brands"
            );
            return responsePHP.data;
        },

        async getModelsAndBrands() {
            const responsePHP = await axios.get(
                "/api/accessories-catalog/get-models-and-brands"
            );
            return responsePHP.data;
        },

        async getModelsfromBrand(brand) {
            const responsePHP = await axios.get(
                "/api/accessories-catalog/get-models-from-brand/" + brand
            );
            return responsePHP.data;
        },

        async getAccessorySizes(id) {
            const responsePHP = await axios.get(
                "/api/accessories-catalog/get-accessory-sizes/" + id
            );
            return responsePHP.data;
        },

        // async getAccessoryByStock(id) {
        //     const responsePHP = await axios.get("/api/accessories-catalog/get-accessory-by-stock/" + id);
        //     return responsePHP.data;
        // },

        // Load one accessory catalog by id
        async show(id) {
            this.errors = [];
            await axios
                .get("/api/accessories-catalog/show/" + id)
                .then((responsePHP) => {
                    responsePHP.data.sizes = Array.isArray(
                        responsePHP.data.sizes
                    )
                        ? responsePHP.data.sizes
                        : responsePHP.data.sizes.split(",");
                    this.accessoryCatalog = responsePHP.data;
                });
        },
        // Api request for create a accessory catalog
        async create(formData) {
            this.accessoryCatalog.pictures = [];
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/accessories-catalog/create", formData)
                .then((responsePHP) => {
                    this.accessoriesCatalog.push(responsePHP.data);
                    this.$router.push({ name: "accessories-catalog-list" });
                })
                .catch((error) => {
                    if (
                        error.response &&
                        error.response &&
                        error.response.status === 422
                    ) {
                        this.errors = error.response.data.errors;
                        let fileName = Object.keys(
                            error.response.data.errors
                        )[0].replace("_", ".");
                        formData.delete(fileName);
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for update accessory catalog
        async update(formData) {
            // Append formData
            formData.append("form", JSON.stringify(this.accessoryCatalog));
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post(
                    "/api/accessories-catalog/update/" +
                        this.accessoryCatalog.id,
                    formData
                )
                .then((responsePHP) => {
                    const accessoryCatalog = responsePHP.data;
                    const foundIndex = this.accessoriesCatalog.findIndex(
                        (accessoriesCatalog) =>
                            accessoriesCatalog.id === responsePHP.data.id
                    );
                    this.accessoriesCatalog[foundIndex] = accessoryCatalog;
                    this.accessoryCatalog = accessoryCatalog;
                    for (let picture in responsePHP.data.pictures) {
                        formData.delete(responsePHP.data.pictures[picture]);
                    }
                })
                .catch((error) => {
                    if (
                        error.response &&
                        error.response &&
                        error.response.status === 422
                    ) {
                        this.errors = error.response.data.errors;
                        let fileName = Object.keys(
                            error.response.data.errors
                        )[0].replace("_", ".");
                        formData.delete(fileName);
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for destroy accessory catalog
        async destroy(id) {
            // Send request
            await axios
                .post("/api/accessories-catalog/destroy/" + id, {
                    accessoryId: id,
                })
                .then(() => {
                    this.$router.push({ name: "accessories-catalog-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Api request to delete a picture
        async deletePicture(id, pictureName) {
            await axios
                .post("/api/accessories-catalog/delete-picture/" + id, {
                    pictureName: pictureName,
                    accessory: this.accessoryCatalog,
                })
                .then((responsePHP) => {
                    this.accessoryCatalog.pictures = responsePHP.data;
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Get data for dataTable
        async getDataForDataTable(
            numPage,
            nbEntries,
            searchValue,
            sortDirection
        ) {
            const res = await axios.get(
                "/api/datatable/accessories-catalog-list",
                {
                    params: {
                        numPage,
                        nbEntries,
                        searchValue,
                        sortDirection,
                    },
                }
            );
            return res.data;
        },
    },
});
