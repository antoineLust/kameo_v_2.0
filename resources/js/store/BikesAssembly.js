import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "./StoresIndex";
import { isEmpty, isEqual } from "../functions/globalFunctions";

export const useBikesAssemblyStore = defineStore("bikesAssembly", {
    id: "bikesAssembly",
    state: () => ({
        bikesAssembly: [],
        bikeAssembly: {},
        errors: [],
    }),
    getters: {
        getBikesAssembly: (state) => {
            return state.bikesAssembly;
        },
        getBikeAssembly: (state) => {
            return state.bikeAssembly;
        },
        getBikeAssemblyById: (state) => (id) => {
            return state.bikesAssembly.find((bike) => bike.id === Number(id));
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        // Get all bikes
        async loadBikesAssembly() {
            await axios.get("/api/bikes-assembly").then((reponsePHP) => {
                this.bikesAssembly = reponsePHP.data;
            });
        },
        // Create a new bike
        async create(data) {
            await axios
                .post("/api/bikes-assembly/create", data)
                .then((reponsePHP) => {
                    this.bikesAssembly.push(reponsePHP.data);
                    this.$router.push({ name: "bikes-assembly-list" });
                })
                .catch((error) => {
                    this.errors = error.response.data.errors;
                });
        },
        // Update a assembly bike
        async update() {
            // Send request
            await axios
                .post(
                    "/api/bikes-assembly/update/" + this.bikeAssembly.id,
                    this.bikeAssembly
                )
                .then((responsePHP) => {
                    this.bikeAssembly = responsePHP.data;

                    const index = this.bikesAssembly.findIndex(
                        (bike) => bike.id === this.bikeAssembly.id
                    );

                    if (index !== -1)
                        this.bikesAssembly[index] = responsePHP.data;

                    const index2 = stores.bikesStockStore.bikesStock.findIndex(
                        (bike) => bike.id === this.bikeAssembly.bikes_id
                    );

                    if (index2 !== -1)
                        stores.bikesStockStore.bikesStock[index2].status =
                            responsePHP.data.status;
                    this.errors = [];
                })
                .catch((error) => {
                    this.errors = [];
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        async show(id) {
            await axios
                .get("/api/bikes-assembly/show/" + id)
                .then((reponsePHP) => {
                    this.bikeAssembly = reponsePHP.data;
                });
        },
        // Delete a assembly bike
        async destroy(id) {
            await axios
                .post("/api/bikes-assembly/destroy/" + id)
                .then((responsePHP) => {
                    const index = this.bikesAssembly.findIndex(
                        (bike) => bike.id === id
                    );
                    this.bikesAssembly.splice(index, 1);
                    this.$router.push({ name: "bikes-assembly-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        updateState(data) {
            if (!isEmpty(this.bikesAssembly)) {
                if (!isEmpty(this.bikeAssembly)) {
                    if (
                        data.data.id === this.bikeAssembly.id &&
                        !isEqual(this.bikeAssembly, data.data) &&
                        data.length !== "true"
                    ) {
                        this.bikeAssembly = data.data;
                    }
                }

                if (
                    data.length === "true" &&
                    !isEqual(this.bikesAssembly, data.data)
                ) {
                    this.bikesAssembly = data.data;
                    if (
                        data.id === this.bikeAssembly.id &&
                        this.$router.currentRoute.value.fullPath.includes(
                            `/dashboard/bikes-assembly/update/${data.id}`
                        )
                    ) {
                        this.$router.push({ name: "bikes-assembly-list" });
                    }
                } else {
                    const bikeAssembly = this.bikesAssembly.findIndex(
                        (bikeAssemblies) => bikeAssemblies.id === data.data.id
                    );
                    if (bikeAssembly === -1) {
                        this.bikesAssembly.push(data.data);
                    } else if (
                        !isEqual(this.bikesAssembly[bikeAssembly], data.data)
                    ) {
                        this.bikesAssembly[bikeAssembly] = data.data;
                    }
                }
            }
        },

        // Get data for dataTable
        async getDataForDataTable(numPage, nbEntries, searchValue) {
            const res = await axios.get("/api/datatable/bikes-assembly-list", {
                params: {
                    numPage,
                    nbEntries,
                    searchValue,
                },
            });
            return res.data;
        },
    },
});
