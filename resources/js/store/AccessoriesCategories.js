import axios from "axios";
import { defineStore } from "pinia";

export const useAccessoriesCategoriesStore = defineStore(
    "accessoriesCategories",
    {
        id: "accessoriesCategories",
        state: () => ({
            accessoriesCategories: [],
            accessoriesCategory: {},
        }),
        getters: {
            getAccessoriesCategories: (state) => {
                return state.accessoriesCategories;
            },
        },
        actions: {
            // Api request for load all accessories categories
            async loadAccessoriesCategories() {
                this.accessoriesCategories = await axios
                    .get("/api/accessories-categories")
                    .then(
                        (reponsePHP) =>
                            (this.accessoriesCategories = reponsePHP.data)
                    );
            },
            // Load one accessories category by id
            loadOneById(id) {
                this.accessoriesCategory = this.accessoriesCategories.find(
                    (accessoriesCategories) =>
                        accessoriesCategories.id === Number(id)
                );
            },
        },
    }
);
