import axios from "axios";
import * as stores from "./StoresIndex";
import { defineStore } from "pinia";

export const useAccessoriesStockStore = defineStore("accessoriesStock", {
    id: "accessoriesStock",
    state: () => ({
        accessoriesStock: [],
        accessoryStock: {
            catalog: {},
            company: {},
        },
        errors: [],
    }),
    getters: {
        getAccessoriesStock: (state) => {
            return state.accessoriesStock;
        },
        getAccessoriesByCategories: (state) => (categories) => {
            return state.accessoriesStock.filter((accessory) =>
                categories.includes(accessory.categories)
            );
        },
        getAccessoryStock: (state) => {
            return state.accessoryStock;
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        async getAccessoryStockById(id) {
            const reponsePHP = await axios.get(
                "/api/accessories-stock/get-accessory-by-id/" + id
            );
            return reponsePHP.data;
        },

        async getAccessoriesByCatalogIdAndSize(catalogId, size) {
            const reponsePHP = await axios.get(
                "/api/accessories-stock/get-accessories-by-catalog-and-size",
                { catalogId: catalogId, size: size }
            );
            return reponsePHP.data;
        },

        // Get all accessories
        async loadAccessoriesStock() {
            await axios.get("/api/accessories-stock").then((reponsePHP) => {
                this.accessoriesStock = reponsePHP.data;
            });
        },
        async show(id) {
            await axios
                .get("/api/accessories-stock/show/" + id)
                .then((reponsePHP) => {
                    this.accessoryStock = reponsePHP.data;
                    if (!this.accessoryStock.company) {
                        this.accessoryStock.company = {};
                    }
                });
        },
        // Create a new accessory
        async create(form) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/accessories-stock/create", form)
                .then((responsePHP) => {
                    this.accessoriesStock.push(responsePHP.data);
                    this.$router.push({ name: "accessories-stock-list" });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Update accessory stock
        async update() {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post(
                    "/api/accessories-stock/update/" + this.accessoryStock.id,
                    this.accessoryStock
                )
                .then((responsePHP) => {
                    const accessory = responsePHP.data;
                    const index = this.accessoriesStock.findIndex(
                        (accessory) => accessory.id === responsePHP.data.id
                    );
                    this.accessoriesStock[index] = accessory;
                    this.accessoryStock = accessory;
                    if (!this.accessoryStock.company) {
                        this.accessoryStock.company = {};
                    }
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        async destroy(id) {
            await axios
                .post("/api/accessories-stock/destroy/" + id)
                .then((responsePHP) => {
                    const accessory = responsePHP.data;
                    const index = this.accessoriesStock.findIndex(
                        (accessory) => accessory.id === responsePHP.data.id
                    );
                    this.accessoriesStock[index] = accessory;
                    this.accessoryStock = accessory;
                    if (!this.accessoryStock.company) {
                        this.accessoryStock.company = {};
                    }
                    this.$router.push({ name: "accessories-stock-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Get data for dataTable
        async getDataForDataTable(
            numPage,
            nbEntries,
            searchValue,
            sortDirection,
            contractTypeFilter
        ) {
            const res = await axios.get(
                "/api/datatable/accessories-stock-list",
                {
                    params: {
                        numPage,
                        nbEntries,
                        searchValue,
                        sortDirection,
                        contractTypeFilter,
                    },
                }
            );
            return res.data;
        },
        async receptionStock(data) {
            data.forEach(async (item) => {
                item.contract_type = "stock";
                await axios
                    .post(
                        "/api/accessories-stock/receptionCommande/" + item.id,
                        item
                    )
                    .then((responsePHP) => {
                        if (
                            responsePHP.data.contract_type === "selling" ||
                            responsePHP.data.contract_type === "leasing"
                        ) {
                            stores.entretiensStore.loadEntretiens();
                        }
                        const accessory = responsePHP.data;
                        const accessoryIndex = this.accessoriesStock.findIndex(
                            (accessory) => accessory.id === responsePHP.data.id
                        );
                        this.accessoriesStock[accessoryIndex] = accessory;
                        this.accessoryStock = accessory;
                    })
                    .catch((error) => {
                        if (error.response && error.response.status === 422) {
                            this.errors = error.response.data.errors || {};
                        } else {
                            console.error(error);
                        }
                    });
            });
        },
    },
});
