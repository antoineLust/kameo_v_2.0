import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "./StoresIndex";

export const useReservationsStore = defineStore("reservationsStore", {
    id: "reseravtionsStore",
    state: () => ({
        reservations: [],
        reservationCodes: [],
        reservationDetails: [],
        reservation: {},
        reservationCode: {},
        errors: [],
    }),
    getters: {
        getReservations: (state) => state.reservations,
        getReservationCodes: (state) => state.reservationCodes,
        getReservation: (state) => state.reservation,
        getReservationCode: (state) => state.reservationCode,
        getReservationDetailsByReservationId: (state) => (reservationId) => {
            return state.reservationDetails.find(
                (detail) => detail.reservations_id === reservationId
            );
        },
        getErrors: (state) => state.errors,
        getFutureReservationsByUserAuth: (state) => {
            return state.reservations.filter(
                (reservation) => new Date(reservation.start) >= new Date()
            );
        },
        getPastReservationsByUserAuth: (state) => {
            return state.reservations.filter(
                (reservation) => new Date(reservation.end) < new Date()
            );
        },
        getInProgressReservationsByUserAuth: (state) => {
            return state.reservations.filter(
                (reservation) =>
                    new Date(reservation.start) < new Date() &&
                    new Date(reservation.end) > new Date()
            );
        },
    },
    actions: {
        async getReservationById(id) {
            const responsePHP = await axios.get(
                "/api/reservations/get-reservation-by-id/" + id
            );
            return responsePHP.data;
        },

        async getReservationCodeByReservationId(id) {
            const responsePHP = await axios.get(
                "/api/reservations/get-reservation-code-by-reservation/" + id
            );
            return responsePHP.data;
        },

        // Load all reservations
        async loadAllReservations() {
            await axios
                .get("/api/reservations")
                .then((responsePHP) => {
                    this.reservations = responsePHP.data;
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        async loadAllReservationsByUserAuth() {
            await axios
                .get(
                    "/api/reservations/load-by-user-auth/" +
                        stores.usersStore.getUserAuth.id
                )
                .then((responsePHP) => {
                    this.reservations = responsePHP.data;
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Load all reservation code
        async loadAllReservationCodes() {
            await axios
                .get("/api/reservation-codes")
                .then((responsePHP) => {
                    this.reservationCodes = responsePHP.data;
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Load reservation details
        async loadAllReservationDetails() {
            await axios
                .get("/api/reservation-details")
                .then((responsePHP) => {
                    this.reservationDetails = responsePHP.data;
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Add a new reservation
        async addReservation(data) {
            await axios
                .post("/api/reservations/create", data)
                .then((responsePHP) => {
                    this.reservations.push(responsePHP.data.reservation);
                    if (responsePHP.data.reservationCode !== null) {
                        this.reservationCodes.push(
                            responsePHP.data.reservationCode
                        );
                    }
                    this.$router.push({
                        name: "bikes-share-reservation-confirm",
                        params: { id: responsePHP.data.reservation.id },
                    });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Remove reservation by id
        async removeReservationById(id) {
            await axios
                .post("/api/reservations/delete/" + id)
                .then(() => {
                    this.reservations = this.reservations.filter(
                        (reservation) => reservation.id !== id
                    );
                })
                .catch((error) => {
                    console.error(error);
                });
        },
    },
});
