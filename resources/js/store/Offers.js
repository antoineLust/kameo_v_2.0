import axios from "axios";
import { defineStore } from "pinia";

export const useOffersStore = defineStore("offers", {
    id: "offers",
    state: () => ({
        offers: [],
        offersDetails: [],
        offer: {
            company: [],
            users_id: null,
            user: {},
            bikes: {},
            accessories: {},
            boxes: {},
        },
        offerDetails: {},
        errors: [],
    }),
    getters: {
        getOffers: (state) => {
            return state.offers;
        },
        getOffer: (state) => {
            return state.offer;
        },
        getErrors: (state) => {
            return state.errors;
        },
        getOfferBikes: (state) => {
            const bikes = state.offerDetails.filter(
                (detail) => detail.item_type === "bike"
            );
            return bikes;
        },
        getOfferAccessories: (state) => {
            const accessories = state.offerDetails.filter(
                (detail) => detail.item_type === "accessory"
            );
            return accessories;
        },
        getOfferBoxes: (state) => {
            const boxes = state.offerDetails.filter(
                (detail) => detail.item_type === "boxe"
            );
            return boxes;
        },
    },
    actions: {
        // Api request for load all offers
        async loadOffers() {
            this.offers = await axios
                .get("/api/offers")
                .then((responsePHP) => (this.offers = responsePHP.data));
            this.offersDetails = await axios
                .get("/api/offers/details")
                .then((responsePHP) => (this.offersDetails = responsePHP.data));
        },
        // Api request for load one offer
        async show(id) {
            await axios.get("/api/offers/show/" + id).then((responsePHP) => {
                this.offer = responsePHP.data.offer;
                this.offerDetails = responsePHP.data.offerDetails;
            });
        },
        // Api request for create offer
        async create(offer) {
            // Send request
            await axios
                .post("/api/offers/create", offer.value)
                .then((responsePHP) => {
                    this.offers.push(responsePHP.data);
                    this.$router.push({
                        name: "offers-update",
                        params: { id: responsePHP.data.id },
                    });
                });
        },
        // Create bikeDetails
        async createBikeDetails(form, offerId) {
            // Reset errors
            this.errors = [];
            // Send request
            form.offerId = offerId;
            await axios
                .post("/api/offer-bike-details/create", form)
                .then((responsePHP) => {
                    this.offersDetails.push(responsePHP.data);
                    this.$router.go(-1);
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Create accessoryDetails
        async createAccessoryDetails(form, offerId) {
            // Reset errors
            this.errors = [];
            // Send request
            form.offerId = offerId;
            await axios
                .post("/api/offer-accessory-details/create", form)
                .then((responsePHP) => {
                    this.offersDetails.push(responsePHP.data);
                    this.$router.push({
                        name: "offers-update",
                        params: { id: this.offer.id },
                    });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Create boxDetails
        async createBoxDetails(form, offerId) {
            // Reset errors
            this.errors = [];
            // Send request
            form.offerId = offerId;
            await axios
                .post("/api/offer-box-details/create", form)
                .then((responsePHP) => {
                    this.offersDetails.push(responsePHP.data);
                    this.$router.push({
                        name: "offers-update",
                        params: { id: this.offer.id },
                    });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for update offer
        async update() {
            // Send request
            await axios
                .post("/api/offers/update/" + this.offer.id, this.offer)
                .then((responsePHP) => {
                    const offer = responsePHP.data;
                    const foundIndex = this.offers.findIndex(
                        (offers) => offers.id === offer.id
                    );
                    this.offers[foundIndex] = offer;
                    this.offer = offer;
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors;
                    } else {
                        console.error(error);
                    }
                });
        },
        async deleteOfferDetails(id) {
            await axios
                .post("/api/offers-details/destroy/" + id)
                .then((responsePHP) => {
                    this.offerDetails = this.offerDetails.filter(
                        (offer) => offer.id !== id
                    );
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Api request for destroy offer
        async destroy(id) {
            // Send request
            await axios
                .post("/api/offers/destroy/" + id)
                .then((responsePHP) => {
                    this.offers = this.offers.filter(
                        (offer) => offer.id !== id
                    );
                    this.$router.push({ name: "offers-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // generate offer by id
        async generateOffer() {
            const data = this.offersDetails.filter(
                (offer) => offer.offers_id === this.offer.id
            );
            await axios
                .post("/api/offers/generate/" + this.offer.id, data)
                .then((responsePHP) => {
                    this.offers = this.offers.filter(
                        (offer) => offer.id !== this.offer.id
                    );
                    window.open(responsePHP.data.url, "_blank");
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Get data for dataTable
        async getDataForDataTable(numPage, nbEntries, searchValue) {
            const res = await axios.get("/api/datatable/offers-list", {
                params: {
                    numPage,
                    nbEntries,
                    searchValue,
                },
            });
            return res.data;
        },
    },
});
