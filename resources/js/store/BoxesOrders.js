import axios from "axios";
import { defineStore } from "pinia";

export const useBoxesOrdersStore = defineStore("boxesOrders", {
    id: "boxesOrders",
    state: () => ({
        boxesOrders: [],
        boxeOrder: {
            id: null,
            installation_price: null,
            amount: null,
            estimated_delivery_date: null,
            status: null,
            boxes_catalog_id: null,
            grouped_orders_id: null,
            catalog: [],
        },
        errors: {},
    }),
    getters: {
        getOrders: (state) => {
            return state.boxesOrders;
        },
        getOrder: (state) => {
            return state.boxeOrder;
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        async getBoxesFromGroupedOrder(id = null) {
            const responsePHP = await axios.get(
                "/api/box-orders/get-boxes-from-grouped-order/" + id
            );
            return responsePHP.data;
        },

        // load all boxes orders
        async loadBoxesOrders() {
            this.boxesOrders = await axios
                .get("/api/box-orders")
                .then((reponsePHP) => (this.boxesOrders = reponsePHP.data));
        },
        // Load a box order
        async show(id) {
            this.errors = [];
            this.boxeOrder = await axios
                .get("/api/box-orders/show/" + id)
                .then((reponsePHP) => (this.boxeOrder = reponsePHP.data));
        },
        // Api request for create box order
        async create(form) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/box-orders/create", form)
                .then((responsePHP) => {
                    this.boxesOrders.push(responsePHP.data);
                    this.$router.push({
                        name: "grouped-orders-update",
                        params: { id: responsePHP.data.grouped_orders_id },
                    });
                })
                .catch((error) => {
                    if (
                        typeof error.response != "undefined" &&
                        error.response &&
                        error.response.status === 422
                    ) {
                        this.errors = error.response.data.errors;
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for update box order
        async update() {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post(
                    "/api/box-orders/update/" + this.boxeOrder.id,
                    this.boxeOrder
                )
                .then((responsePHP) => {
                    const box = responsePHP.data;
                    const foundIndex = this.boxesOrders.findIndex(
                        (boxesOrders) => boxesOrders.id === box.id
                    );
                    this.boxesOrders[foundIndex] = box;
                    this.boxeOrder = box;
                })
                .catch((error) => {
                    if (
                        typeof error.response != "undefined" &&
                        error.response &&
                        error.response.status === 422
                    ) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for destroy box order
        async destroy(id) {
            // Send request
            await axios
                .post("/api/box-orders/destroy/" + id, { boxOrderId: id })
                .then((responsePHP) => {
                    let removeBoxOrder = this.boxesOrders
                        .map(function (item) {
                            return item.id;
                        })
                        .indexOf(id);
                    this.boxesOrders.splice(removeBoxOrder, 1);
                    this.$router.go(-1);
                })
                .catch((error) => {
                    console.error(error);
                });
        },
    },
});
