import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "./StoresIndex";
import { isEmpty, isEqual } from "../functions/globalFunctions";

export const useCalendarStore = defineStore("calendar", {
    id: "calendar",
    state: () => ({
        calendar: [],
        task: {},
        errors: [],
    }),
    getters: {
        getCalendar: (state) => {
            return state.calendar;
        },
        getTask: (state) => {
            return state.task;
        },
        getError: (state) => {
            return state.errors;
        },
    },
    actions: {
        // Api request to load calendar
        async loadCalendar() {
            this.calendar = await axios
                .get("/api/calendar")
                .then((reponsePHP) => (this.calendar = reponsePHP.data));
        },

        updateState(data) {
            if (!isEqual(this.calendar, data)) {
                this.calendar = data;
            }
        },

        // create
        async create(formData) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/calendar/create", formData)
                .then((responsePHP) => {
                    this.calendar.push(responsePHP.data);
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // update from calendar
        async updateCommentEntretien(data) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/calendar/updateCommentEntretien", data)
                .then((responsePHP) => {
                    this.updateCalendar(responsePHP.data);
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // update from EntretiensUpdate
        async updateCommentAndStatusFromEntretien(data) {
            axios
                .post("/api/calendar/updateCommentAndStatusFromEntretien", data)
                .then((responsePHP) => {
                    if (!responsePHP.data) {
                        return;
                    }

                    this.updateCalendar(responsePHP.data);
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // update
        async updateComment(data) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/calendar/updateComment", data)
                .then((responsePHP) => {
                    this.updateCalendar(responsePHP.data.calendar);
                    stores.bikeTestsStore.updateState(
                        responsePHP.data.bikeTest
                    );
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // update
        async updateEntretienDone(data) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/calendar/updateEntretien", data)
                .then((responsePHP) => {
                    this.updateCalendar(responsePHP.data);
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // update
        async updateTestDone(formData) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/calendar/updateTest", formData)
                .then((responsePHP) => {
                    this.updateCalendar(responsePHP.data.calendar);
                    stores.bikeTestsStore.updateState(
                        responsePHP.data.bikeTest
                    );
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // update
        async updateOrderDone(formData) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/calendar/updateOrder", formData)
                .then((responsePHP) => {
                    this.updateCalendar(responsePHP.data);
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // update
        async updateAssemblyDone(formData) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/calendar/updateAssembly", formData)
                .then((responsePHP) => {
                    this.updateCalendar(responsePHP.data.calendar);
                    stores.bikesAssemblyStore.updateState(
                        responsePHP.data.bikeAssembly
                    );
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // update
        async update(formData) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/calendar/update", formData)
                .then((responsePHP) => {
                    this.updateCalendar(responsePHP.data);
                })
                .catch((error) => {
                    console.error(error);
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        formatTask(task) {
            try {
                task.split = task.employee_id;
                task.class =
                    task.status !== "done"
                        ? task.item_type.replaceAll(" ", "_")
                        : task.item_type.replaceAll(" ", "_") + "2";
                task.start = `${task.date} ${task.start_time}`;
                task.end = `${task.date} ${task.end_time}`;
                task.content = task.comment;
                task.title = task.item_type;

                return task;
            } catch (error) {
                console.error(error);
            }
        },

        updateCalendar(data) {
            if (!isEmpty(this.calendar)) {
                data = this.formatTask(data);

                const foundIndex = this.calendar.findIndex(
                    (calendar) => calendar.id === data.id
                );
                if (foundIndex !== -1) {
                    this.calendar[foundIndex] = data;
                }
            }
        },

        hotUpdateTest(data) {
            if (!isEmpty(this.calendar)) {
                const foundIndex = this.calendar.findIndex(
                    (calendar) =>
                        calendar.item_id === data.id &&
                        calendar.item_type === "Test"
                );
                if (foundIndex !== -1) {
                    this.calendar[foundIndex].comment = data.comment;
                    this.calendar[foundIndex].status =
                        data.status === "done" ? data.status : "todo";
                    this.calendar[foundIndex] = this.formatTask(
                        this.calendar[foundIndex]
                    );
                }
            }
        },

        hotUpdateAssembly(data) {
            if (!isEmpty(this.calendar)) {
                if (data.length !== "true") {
                    const foundIndex = this.calendar.findIndex(
                        (calendar) =>
                            calendar.item_id === data.data.id &&
                            calendar.item_type === "Bike Assembly"
                    );
                    if (foundIndex !== -1) {
                        this.calendar[foundIndex].status =
                            data.data.status === "done" ? "done" : "todo";
                        this.calendar[foundIndex] = this.formatTask(
                            this.calendar[foundIndex]
                        );
                    }
                } else {
                    const removeTask = this.calendar.findIndex(
                        (item) =>
                            item.item_id === data.id &&
                            item.item_type === "Bike Assembly"
                    );
                    if (removeTask !== -1) {
                        this.calendar.splice(removeTask, 1);
                    }
                }
            }
        },

        hotUpdateOrder(data) {
            if (!isEmpty(this.calendar) && data.id) {
                const removeTask = this.calendar.findIndex(
                    (item) =>
                        item.item_id === data.id &&
                        item.item_type === "Order to prepare"
                );
                if (removeTask !== -1) {
                    this.calendar.splice(removeTask, 1);
                }
            }
        },

        hotUpdateEntretien(data) {
            if (!isEmpty(this.calendar) && data.type === "update") {
                const foundIndex = this.calendar.findIndex(
                    (item) =>
                        item.item_id === data.data.id &&
                        item.item_type === "Maintenance"
                );
                if (foundIndex !== -1) {
                    this.calendar[foundIndex].comment = data.data.description;
                    if (data.data.status === "to_planned") {
                        data.data.status = "todo";
                    }
                    this.calendar[foundIndex].status =
                        data.data.status !== "waiting_pieces" &&
                        data.data.status !== "todo" &&
                        data.data.status !== "done"
                            ? "todo"
                            : data.data.status;
                    this.calendar[foundIndex].confidential_comment =
                        data.data.confidential_description;
                    this.calendar[foundIndex] = this.formatTask(
                        this.calendar[foundIndex]
                    );
                }
            }
        },

        // destroy
        async destroy(id) {
            // Send request
            await axios
                .post("/api/calendar/destroy", { taskId: id })
                .then((responsePHP) => {
                    const removeTask = this.calendar.findIndex(
                        (item) => item.id === id
                    );
                    if (removeTask !== -1) {
                        this.calendar.splice(removeTask, 1);
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        },
    },
});
