import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "./StoresIndex";

export const useConditionsStore = defineStore("conditions", {
    id: "conditions",
    state: () => ({
        conditions: [],
        condition: {},
        bikesOrderable: [],
        bikeOrderable: {},
        accessoriesOrderable: [],
        accessoryOrderable: {},
        sharedBikes: [],
        sharedBikesConditions: [],
        sharedBikesCondition: {},
        errors: [],
    }),
    getters: {
        getConditions: (state) => {
            return state.conditions;
        },
        getCondition: (state) => {
            return state.condition;
        },
        getBikesOrderable: (state) => {
            return state.bikesOrderable;
        },
        getBikeOrderable: (state) => {
            return state.bikeOrderable;
        },
        getAccessoryOrderable: (state) => {
            return state.accessoryOrderable;
        },
        getAccessoriesOrderable: (state) => {
            return state.accessoriesOrderable;
        },
        getSharedBikes: (state) => {
            return state.sharedBikes;
        },
        getSharedBike: (state) => {
            return state.sharedBikesCondition;
        },
        getConditionById: (state) => (id) => {
            if (id) {
                return state.conditions.find(
                    (condition) => condition.companies_id === id.toString()
                );
            }
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        async getConditionsByCompanyId(id) {
            const responsePHP = await axios.get(
                "/api/conditions/get-conditions-by-company-id/" + id
            );
            return responsePHP.data;
        },

        async getSharedBikesConditionsByCompanyId(id) {
            const responsePHP = await axios.get(
                "/api/conditions/get-shared-bikes-conditions-by-company-id/" +
                    id
            );
            return responsePHP.data;
        },

        // API call to get all conditions
        async loadConditions() {
            try {
                const responsePHP = await axios.get("/api/conditions");
                this.conditions = responsePHP.data;
            } catch (error) {
                console.error(error);
            }
        },
        async loadConditionsUserAuth() {
            const res = await axios.get("/api/conditions/user-auth-condition/");
            try {
                this.condition = res.data.condition;
                this.accessoryOrderable = res.data.accessoryOrderable || [];
                this.bikeOrderable = res.data.bikeOrderable || {};
                this.sharedBikes = res.data.sharedBikes || [];
                // Set data
                if (this.condition.access.includes("cafetaria")) {
                    if (typeof this.bikeOrderable.catalog_id !== "undefined") {
                        this.bikeOrderable.catalog_id = Array.from(
                            this.bikeOrderable.catalog_id,
                            (x) => Number(x)
                        );
                    }
                    if (!!this.condition.cafetaria_billing_type) {
                        this.condition.cafetaria_billing_type =
                            this.condition.cafetaria_billing_type.split(",");
                    }
                    if (
                        !!this.bikeOrderable.catalogs_id &&
                        this.bikeOrderable.catalogs_id !== "*"
                    ) {
                        this.bikeOrderable.catalogs_id =
                            this.bikeOrderable.catalogs_id.split(",");
                    }
                }
            } catch (error) {
                console.error(error);
            }
        },
        // API call to get all orderable conditions
        async loadOrderableConditions() {
            try {
                const responsePHP = await axios.get(
                    "/api/conditions/orderable"
                );
                this.bikesOrderable = responsePHP.data.bikes;
                this.accessoriesOrderable = responsePHP.data.accessories;
            } catch (error) {
                console.error(error);
            }
        },
        async loadSharedBikesConditions() {
            try {
                const responsePHP = await axios.get(
                    "/api/conditions/get-shared-bikes-conditions"
                );
                this.sharedBikesConditions = responsePHP.data;
            } catch (error) {
                console.error(error);
            }
        },
        // API call to get a condition by id
        async show(id) {
            this.showNext(await axios.get("/api/conditions/show/" + id));
        },

        async showByCompanyId(company_id) {
            this.showNext(
                await axios.get(
                    "/api/conditions/show-by-company-id/" + company_id
                )
            );
        },

        showNext(res) {
            try {
                this.condition = res.data.condition;
                this.accessoryOrderable = res.data.accessoryOrderable || [];
                this.bikeOrderable = res.data.bikeOrderable || {};
                this.sharedBikes = res.data.sharedBikes || [];
                // Set data
                if (this.condition.access.includes("cafetaria")) {
                    if (typeof this.bikeOrderable.catalog_id !== "undefined") {
                        this.bikeOrderable.catalog_id = Array.from(
                            this.bikeOrderable.catalog_id,
                            (x) => Number(x)
                        );
                    }
                    if (!!this.condition.cafetaria_billing_type) {
                        this.condition.cafetaria_billing_type =
                            this.condition.cafetaria_billing_type.split(",");
                    }
                    if (
                        !!this.bikeOrderable.catalogs_id &&
                        this.bikeOrderable.catalogs_id !== "*"
                    ) {
                        this.bikeOrderable.catalogs_id =
                            this.bikeOrderable.catalogs_id.split(",");
                    }
                }
            } catch (error) {
                console.error(error);
            }
        },

        // API call to update cafetaria plan
        async update(plan) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/conditions/update/" + this.condition.id, {
                    plan: plan,
                    data: this.condition,
                    bikeOrderable: this.bikeOrderable,
                    accessoryOrderable: this.accessoryOrderable,
                })
                .then((res) => {
                    if (res.data.bikeOrderable !== null) {
                        this.bikeOrderable = res.data.bikeOrderable;
                        const bikeOrderableIndex =
                            this.bikesOrderable.findIndex(
                                (bike) => bike.id === res.data.bikeOrderable.id
                            );
                        this.bikesOrderable[bikeOrderableIndex] =
                            res.data.bikeOrderable;
                    }
                    if (!!res.data.accessoryOrderable) {
                        this.accessoryOrderable = res.data.accessoryOrderable;
                        const accessoryOrderableIndex =
                            this.accessoriesOrderable.findIndex(
                                (accessory) =>
                                    accessory.id ===
                                    res.data.accessoryOrderable.id
                            );
                        this.accessoriesOrderable[accessoryOrderableIndex] =
                            res.data;
                    }
                    this.condition = res.data.condition;
                    const conditionIndex = this.conditions.findIndex(
                        (condition) => condition.id === res.data.condition.id
                    );
                    this.conditions[conditionIndex] = res.data.condition;
                })
                .catch((error) => {
                    console.error(error);
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Remove accessory orderable from condition
        async removeAccessoryOrderable(id) {
            await axios
                .post("/api/conditions/remove-accessory-orderable/" + id)
                .then((res) => {
                    const accessoryIndex = this.accessoryOrderable.findIndex(
                        (accessory) => accessory.id === Number(id)
                    );
                    this.accessoryOrderable.splice(accessoryIndex, 1);
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Create a new accessory orderable
        async createNewAccessoryOrderable(data) {
            // Reset errors
            this.errors = [];
            // Define data
            data.company_id = this.condition.companies_id;
            // Send request
            await axios
                .post("/api/conditions/create-accessory-orderable", data)
                .then((res) => {
                    this.accessoryOrderable = res.data;
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Delete all accessory orderables
        async deleteAllAccessoryOrderable() {
            await axios
                .post(
                    "/api/conditions/delete-all-accessory-orderables/" +
                        this.condition.companies_id
                )
                .then((res) => {
                    this.accessoryOrderable = [];
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Add accessory orderable
        async AddAllAccessoryOrderable() {
            await axios
                .post(
                    "/api/conditions/add-all-accessory-orderables/" +
                        this.condition.companies_id
                )
                .then((res) => {
                    if (Array.isArray(res.data)) {
                        this.accessoryOrderable = res.data;
                    }
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Add new shared bike
        async createNewSharedBike(data) {
            // Reset errors
            this.errors = [];
            // Send request
            try {
                const res = await axios.post(
                    "/api/conditions/add-new-shared-bike/" +
                        this.condition.companies_id,
                    data
                );
                if (res.data.delete_shared_bike !== null) {
                    const sharedBikeIndex = this.sharedBikes.findIndex(
                        (sharedBike) =>
                            sharedBike.id === res.data.delete_shared_bike.id
                    );
                    this.sharedBikes.splice(sharedBikeIndex, 1);
                }
                this.sharedBikes.push(res.data.shared_bike);
            } catch (error) {
                if (error.response && error.response.status === 422) {
                    this.errors = error.response.data.errors || {};
                }
                // pour ne pas fermer le dialogue SharedBikesCreate si il y a des erreurs
                throw "Erreur";
            }
        },
        async updateSharedBike(data) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post(
                    "/api/conditions/add-new-shared-bike/" +
                        this.condition.companies_id,
                    data
                )
                .then((res) => {
                    if (res.data.delete_shared_bike !== null) {
                        const sharedBikeIndex = this.sharedBikes.findIndex(
                            (sharedBike) =>
                                sharedBike.id === res.data.delete_shared_bike.id
                        );
                        this.sharedBikes.splice(sharedBikeIndex, 1);
                    }
                    this.sharedBikes.push(res.data.shared_bike);
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.log(error);
                    }
                });
        },
        async deleteSharedBike(id) {
            await axios
                .post("/api/conditions/delete-shared-bike/" + id)
                .then((res) => {
                    const sharedBikeIndex = this.sharedBikes.findIndex(
                        (sharedBike) => sharedBike.id === Number(id)
                    );
                    this.sharedBikes.splice(sharedBikeIndex, 1);
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Get data for dataTable
        async getDataForDataTable(numPage, nbEntries, searchValue) {
            const res = await axios.get("/api/datatable/conditions-list", {
                params: {
                    numPage,
                    nbEntries,
                    searchValue,
                },
            });
            return res.data;
        },
    },
});
