import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "./StoresIndex";

export const useBuildingsStore = defineStore("buildings", {
    id: "buildings",
    state: () => ({
        buildings: [],
        building: {},
        errors: [],
    }),
    getters: {
        getBuildings: (state) => {
            return state.buildings;
        },
        getBuildingById: (state) => (id) => {
            return state.buildings.find(
                (building) => building.id === Number(id)
            );
        },
        getBuildingsByCompanyAuth: (state) => {
            return state.buildings.filter(
                (building) =>
                    building.companies_id ===
                    stores.usersStore.getUserAuth.companies_id
            );
        },
    },
    actions: {
        async getBuildingByCompanyId(companyId) {
            const responsePHP = await axios.get(
                "/api/buildings/get-buildings-by-company-id/" + companyId
            );
            return responsePHP.data;
        },

        // Get all bikes
        async loadBuildings() {
            await axios.get("/api/buildings").then((reponsePHP) => {
                this.buildings = reponsePHP.data;
            });
        },
    },
});
