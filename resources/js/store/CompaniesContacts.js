import axios from "axios";
import { defineStore } from "pinia";

export const useCompaniesContactsStore = defineStore("companiesContacts", {
    id: "companiesContacts",
    state: () => ({
        contacts: [],
        contact: {},
        errors: [],
    }),
    getters: {
        getContacts: (state) => {
            return state.contacts;
        },
        getContact: (state) => {
            return state.contact;
        },
        getErrors: (state) => {
            return state.errors;
        },
    },
    actions: {
        // Api request for load all companies contacts
        async loadCompaniesContacts() {
            this.contacts = await axios
                .get("/api/contacts")
                .then((reponsePHP) => (this.contacts = reponsePHP.data));
        },

        // Load one contact by id
        async show(id) {
            this.errors = [];
            await axios.get("/api/contacts/show/" + id).then((responsePHP) => {
                this.contact = responsePHP.data;
            });
        },
        // Api requets for create company contact
        async create(form, id) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/contacts/create", { form, id })
                .then((responsePHP) => {
                    this.contacts.push(responsePHP.data);
                    this.$router.push({
                        name: "companies-show",
                        params: { id: responsePHP.data.companies_id },
                    });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for update companies contacts
        async update(id, contact) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/contacts/update/" + this.contact.id, this.contact)
                .then((responsePHP) => {
                    let contact = this.contacts.find(
                        (contacts) => contacts.id === responsePHP.data.id
                    );
                    contact = responsePHP.data;
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for destroy company contact
        async destroy(id) {
            // Send request
            await axios
                .post("/api/contacts/destroy/" + id, { contactId: id })
                .then((responsePHP) => {
                    this.contacts = responsePHP.data;
                    this.$router.go(-1);
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        async getContactsByCompanyId(companyId) {
            const responsePHP = await axios.get(
                "/api/contacts/get-contacts-by-company-id/" + companyId
            );
            return responsePHP.data;
        },
    },
});
