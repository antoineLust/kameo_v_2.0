import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "./StoresIndex";
import { calculateLeasingPrice } from "../functions/globalFunctions";

export const useBikesCatalogStore = defineStore("bikesCatalog", {
    id: "bikesCatalog",
    state: () => ({
        bikesCatalog: [],
        bikeCatalog: {
            id: null,
            brand: null,
            model: null,
            frame_type: null,
            utilisation: null,
            electric: null,
            motor: null,
            battery: null,
            transmition: null,
            sizes: [],
            season: null,
            buy_price: null,
            price_htva: null,
            minimal_stock: null,
            optimal_stock: null,
            status: null,
            pictures: [],
        },
        errors: {},
        fileErrors: {},
    }),
    getters: {
        getBikesCatalog: (state) => {
            return state.bikesCatalog;
        },
        getBikeCatalog: (state) => {
            return state.bikeCatalog;
        },
        getBikeCatalogByUtilisation: (state) => (utilisation) => {
            return state.bikesCatalog.find(
                (bike) => bike.utilisation === utilisation
            );
        },
        getErrors: (state) => {
            return state.errors;
        },
        getFileErrors: (state) => {
            return state.fileErrors;
        },
        getBikeCatalogByIdCafetaria: (state) => (id) => {
            return state.bikesCatalog.find((bike) => bike.id === id);
        },
        getSizesModelCafetaria: (state) => (id) => {
            return state.bikesCatalog.find((bike) => bike.id === id).sizes;
        },
        getPrice: (state) => (bike, contract_type) => {
            let amount = 0;
            if (bike && contract_type) {
                if (contract_type == "leasing") {
                    amount = calculateLeasingPrice(bike.price_htva, 1, false);
                } else if (contract_type == "annual_leasing") {
                    amount = calculateLeasingPrice(bike.price_htva, 12, false);
                } else if (contract_type == "full_leasing") {
                    amount = calculateLeasingPrice(bike.price_htva, 12, true);
                } else if (contract_type == "selling") {
                    amount = bike.price_htva;
                }
            }
            return amount;
        },
        getBikesFromCafetaria:
            (state) => (brand, utilisation, frame_type, type, size) => {
                const companyId = stores.usersStore.getUserAuth.company.id;
                let bikes = state.bikesCatalog;
                let bikesOrderable =
                    stores.conditionsStore.getBikesOrderable.filter(
                        (bike) => bike.company_id === companyId.toString()
                    )[0];
                if (bikesOrderable) {
                    bikesOrderable = bikesOrderable.catalog_id;

                    if (bikesOrderable === "*") {
                        if (brand !== "all") {
                            bikes = bikes.filter(
                                (bike) => bike.brand === brand
                            );
                        }
                        if (utilisation !== "all") {
                            bikes = bikes.filter(
                                (bike) => bike.utilisation === utilisation
                            );
                        }
                        if (frame_type !== "all") {
                            bikes = bikes.filter(
                                (bike) => bike.frame_type === frame_type
                            );
                        }
                        if (type !== "all") {
                            bikes = bikes.filter(
                                (bike) => bike.electric === type
                            );
                        }
                        if (size !== "all") {
                            bikes = bikes.filter((bike) =>
                                bike.sizes.includes(size)
                            );
                        }
                    } else {
                        bikes = bikes.filter((bike) =>
                            bikesOrderable.includes(bike.id.toString())
                        );
                        if (brand !== "all") {
                            bikes = bikes.filter(
                                (bike) => bike.brand === brand
                            );
                        }
                        if (utilisation !== "all") {
                            bikes = bikes.filter(
                                (bike) => bike.utilisation === utilisation
                            );
                        }
                        if (frame_type !== "all") {
                            bikes = bikes.filter(
                                (bike) => bike.frame_type === frame_type
                            );
                        }
                        if (type !== "all") {
                            bikes = bikes.filter(
                                (bike) => bike.electric === type
                            );
                        }
                        if (size !== "all") {
                            bikes = bikes.filter((bike) =>
                                bike.sizes.includes(size)
                            );
                        }
                    }
                }
                return bikes;
            },
    },
    actions: {
        async getModelsAndBrands() {
            const responsePHP = await axios.get(
                "/api/bikes-catalog/get-models-and-brands"
            );
            return responsePHP.data;
        },

        async getBikeCatalogById(id) {
            const responsePHP = await axios.get(
                "/api/bikes-catalog/get-by-id/" + id
            );
            return responsePHP.data;
        },

        async getSizesModel(id) {
            const responsePHP = await axios.get(
                "/api/bikes-catalog/get-sizes/" + id
            );
            return responsePHP.data;
        },

        async getModelsFromBrand(brand) {
            const responsePHP = await axios.get(
                "/api/bikes-catalog/get-models-from-brand/" + brand
            );
            return responsePHP.data;
        },

        // Api request for load all bikes catalog
        async loadBikesCatalog() {
            this.bikesCatalog = await axios
                .get("/api/bikes-catalog")
                .then((reponsePHP) => (this.bikesCatalog = reponsePHP.data));
        },
        // Load one bikeCatalog by id
        async show(id) {
            this.errors = [];
            await axios
                .get("/api/bikes-catalog/show/" + id)
                .then(async (responsePHP) => {
                    this.bikeCatalog = responsePHP.data;
                });
        },
        // Api request for create a bike catalog
        async create(formData) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/bikes-catalog/create", formData)
                .then((responsePHP) => {
                    this.bikesCatalog.push(responsePHP.data);
                    this.$router.push({ name: "bikes-catalog-list" });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                        let fileName = Object.keys(
                            error.response.data.errors
                        )[0].replace("_", ".");
                        formData.delete(fileName);
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for update bike catalog
        async update(formData) {
            // Append fomData
            formData.append("form", JSON.stringify(this.bikeCatalog));
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post(
                    "/api/bikes-catalog/update/" + this.bikeCatalog.id,
                    formData
                )
                .then((responsePHP) => {
                    const bikeCatalog = responsePHP.data;
                    const foundIndex = this.bikesCatalog.findIndex(
                        (bikesCatalog) =>
                            bikesCatalog.id === responsePHP.data.id
                    );
                    this.bikesCatalog[foundIndex] = bikeCatalog;
                    this.bikeCatalog = bikeCatalog;
                    // Reset formData
                    for (let picture in responsePHP.data.pictures) {
                        formData.delete(responsePHP.data.pictures[picture]);
                    }
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                        let fileName = Object.keys(
                            error.response.data.errors
                        )[0].replace("_", ".");
                        formData.delete(fileName);
                    } else {
                        console.error(error);
                    }
                });
        },
        // Api request for destroy bike catalog
        async destroy(id) {
            // Send request
            await axios
                .post("/api/bikes-catalog/destroy/" + id, { bikeId: id })
                .then((responsePHP) => {
                    let bike = this.bikesCatalog.find(
                        (bikesCatalog) =>
                            bikesCatalog.id === responsePHP.data.id
                    );
                    bike.status = responsePHP.data.status;
                    this.$router.push({ name: "bikes-catalog-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Load one bikeCatalog by id
        async loadBikesCatalogByUtilisation(utilisation) {
            // const responsePHP = await axios.get(
            //     "/api/bikes-catalog/get-all-bikes-for-shop/" + utilisation
            // );
            // this.bikesCatalog = responsePHP.data;
            this.bikesCatalog = await axios
                .get("/api/bikes-catalog/get-all-bikes-for-shop/" + utilisation)
                .then((reponsePHP) => (this.bikesCatalog = reponsePHP.data));
        },
        // Api request to delete a picture
        async deletePicture(id, pictureName) {
            await axios
                .post("/api/bikes-catalog/delete-picture/" + id, {
                    pictureName: pictureName,
                    bike: this.bikeCatalog,
                })
                .then((responsePHP) => {
                    this.bikeCatalog.pictures = responsePHP.data;
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        // Get data for dataTable
        async getDataForDataTable(
            numPage,
            nbEntries,
            searchValue,
            sortDirection
        ) {
            const res = await axios.get("/api/datatable/bikes-catalog-list", {
                params: {
                    numPage,
                    nbEntries,
                    searchValue,
                    sortDirection,
                },
            });
            return res.data;
        },
    },
});
