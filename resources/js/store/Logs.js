import axios from "axios";
import { defineStore } from "pinia";

export const useLogsStore = defineStore("logs", {
    id: "logs",
    state: () => ({
        queries: [],
    }),
    getters: {
        getQueries: (state) => state.queries,
    },
    actions: {
        // Get all queries
        async retrieveQueries() {
            await axios
                .get("/api/logs/queries")
                .then((res) => {
                    this.queries = res.data;
                    return res.data;
                })
                .catch((err) => {
                    console.log(err);
                });
        },
    },
});
