import axios from "axios";
import { defineStore } from "pinia";
import * as stores from "./StoresIndex";
import { isEmpty, isEqual, cloneDeep } from "../functions/globalFunctions";

export const useEntretiensStore = defineStore("entretiens", {
    id: "entretiens",
    state: () => ({
        entretiens: [],
        entretiensDetails: [],
        entretien: {},
        entretienDetails: {},
        errors: [],
    }),
    getters: {
        getEntretiens: (state) => {
            return state.entretiens;
        },
        getEntretienDetails: (state) => {
            return state.entretiensDetails;
        },
        getEntretien: (state) => {
            return state.entretien;
        },
        getEntretienById: (state) => (id) => {
            return state.entretiens.find(
                (entretien) => entretien.id === Number(id)
            );
        },
        getErrors: (state) => {
            return state.errors;
        },
        getEntretienServices: (state) => (id) => {
            const allDetails = state.entretiensDetails.filter(
                (entretien) => entretien.entretiens_id === Number(id)
            );
            const services = allDetails.filter(
                (detail) => detail.item_type === "service"
            );
            return services;
        },
        getEntretienAccessories: (state) => (id) => {
            const allDetails = state.entretiensDetails.filter(
                (entretien) => entretien.entretiens_id === Number(id)
            );
            const accessories = allDetails.filter(
                (detail) => detail.item_type === "accessory"
            );
            return accessories;
        },
        getEntretienOthersAccessories: (state) => (id) => {
            const allDetails = state.entretiensDetails.filter(
                (entretien) => entretien.entretiens_id === Number(id)
            );
            const others = allDetails.filter(
                (detail) => detail.item_type === "other_accessory"
            );
            return others;
        },
    },
    actions: {
        async getEntretiensByBikeId(id) {
            const responsePHP = await axios.get(
                "/api/entretiens/get-entretiens-by-bike-id/" + id
            );
            return responsePHP.data;
        },

        updateStateDetails(data) {
            if (!isEmpty(this.entretiensDetails)) {
                if (
                    data.type === "create" &&
                    !this.entretiensDetails.some((entretiensDetail) => {
                        return data.data.id === entretiensDetail.id;
                    })
                ) {
                    this.entretiensDetails.push(data.data);
                } else if (
                    data.type === "delete" &&
                    !isEqual(this.entretiensDetails, data.data)
                ) {
                    this.entretiensDetails = data.data;
                }
            }
        },

        updateState(data) {
            if (!isEmpty(this.entretiens)) {
                if (
                    data.type === "create" &&
                    !this.entretiens.some((entretien) => {
                        return data.data.id === entretien.id;
                    })
                ) {
                    this.entretiens.push(data.data);
                } else if (data.type === "update") {
                    const index = this.entretiens.findIndex(
                        (entretien) => entretien.id === data.data.id
                    );
                    if (
                        index !== -1 &&
                        !isEqual(this.entretiens[index], data.data)
                    ) {
                        this.entretiens[index] = data.data;
                        if (!isEmpty(this.entretien)) {
                            if (
                                data.data.id === this.entretien.id &&
                                !isEqual(this.entretien, data.data)
                            ) {
                                this.entretien = data.data;
                            }
                        }
                    }
                }
            }
        },

        // Api request for load all entretiens
        async loadEntretiens() {
            await axios.get("/api/entretiens").then((reponsePHP) => {
                this.entretiens = reponsePHP.data.entretiens;
                this.entretiensDetails = reponsePHP.data.entretiens_details;
            });
        },
        // Create a new entretien
        async createEntretien(form) {
            await axios
                .post("/api/entretiens/create", form)
                .then((reponsePHP) => {
                    this.entretiens.push(reponsePHP.data);
                    this.$router.push({ name: "entretiens-list" });
                });
        },
        // Update an entretien
        updateEntretien() {
            let data = cloneDeep(this.entretien);
            data.confidential_description =
                stores.bikesStockStore.getBikeStock.comment;
            setTimeout(() => {
                axios
                    .post("/api/entretiens/update/" + data.id, this.entretien)
                    .then((reponsePHP) => {
                        const entretien = reponsePHP.data;
                        const index = this.entretiens.findIndex(
                            (entretien) => entretien.id === Number(entretien.id)
                        );
                        this.entretiens[index] = entretien;
                        this.entretien = entretien;
                    })
                    .catch((error) => {
                        if (error.response && error.response.status === 422) {
                            this.errors = error.response.data.errors || {};
                        } else {
                            console.error(error);
                        }
                    });
            }, 750);
        },

        async updateEntretienCommentCalendar(data) {
            await axios
                .post(
                    "/api/entretiens/updateCommentCalendar/" + data.item_id,
                    data
                )
                .then((reponsePHP) => {
                    const entretien = reponsePHP.data;
                    const index = this.entretiens.findIndex(
                        (entretien) => entretien.id === Number(entretien.id)
                    );
                    this.entretiens[index] = entretien;
                    this.entretien = entretien;
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        async updateEntretienStatusCalendar(data) {
            await axios
                .post(
                    "/api/entretiens/updateStatusCalendar/" + data.item_id,
                    data
                )
                .then((reponsePHP) => {
                    const entretien = reponsePHP.data;
                    const index = this.entretiens.findIndex(
                        (entretien) => entretien.id === Number(entretien.id)
                    );
                    this.entretiens[index].status = entretien.status;
                    this.entretien.status = entretien.status;
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // Show entretien by id
        async show(id) {
            const reponsePHP = await axios.get("/api/entretiens/show/" + id);
            this.entretien = reponsePHP.data.entretien;
            this.entretiensDetails = reponsePHP.data.entretien_details;
        },
        // Delete entretienDetails by id
        async deleteEntretienDetails(id) {
            await axios
                .post("/api/entretiens-details/destroy/" + id)
                .then((responsePHP) => {
                    this.entretiensDetails = this.entretiensDetails.filter(
                        (entretien) => entretien.id !== responsePHP.data.id
                    );
                })
                .catch((error) => {
                    console.error(error);
                });
        },
        async destroy(id) {
            await axios
                .post("/api/entretiens-details/destroy/" + id)
                .then((responsePHP) => {
                    this.entretiensDetails = this.entretiensDetails.filter(
                        (entretien) => entretien.id !== responsePHP.data.id
                    );

                    this.$router.push({ name: "entretiens-list" });
                })
                .catch((error) => {
                    console.error(error);
                });
        },

        // Create entretienDetails with calendar popup
        async createEntretienDetailsCalendar(form) {
            // Reset errors
            this.errors = [];
            // Send request
            await axios
                .post("/api/entretiens-details/create", form)
                .then((reponsePHP) => {
                    this.entretiensDetails = [
                        ...this.entretiensDetails,
                        reponsePHP.data,
                    ];
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // Create entretienDetails
        async createEntretienDetails(form) {
            // Send request
            await axios
                .post("/api/entretiens-details/create", form)
                .then((reponsePHP) => {
                    this.entretiensDetails.push(reponsePHP.data);
                    this.$router.push({
                        name: "entretiens-update",
                        params: { id: this.entretien.id },
                    });
                    // pour obtenir une event key = id du nouveau details
                    setTimeout(() => {
                        this.errors = [];
                    }, 1000);
                })
                .catch((error) => {
                    this.errors = [];
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // Update entretienDetails
        async updateEntretienDetails(id, status) {
            await axios
                .post("/api/entretiens-details/update/" + id, {
                    status: status,
                })
                .then((responsePHP) => {
                    responsePHP.data.forEach((item) => {
                        const index =
                            stores.accessoriesStockStore.$state.accessoriesStock.findIndex(
                                (accessory) => {
                                    accessory.id === item.id;
                                }
                            );
                        if (index !== -1) {
                            stores.accessoriesStockStore.$state.accessoriesStock[
                                index
                            ] = item;
                        }
                    });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // Update entretienDetails
        async updateEntretienDetailsReverse(id, status) {
            await axios
                .post("/api/entretiens-details/updateReverse/" + id, {
                    status: status,
                })
                .then((responsePHP) => {
                    responsePHP.data.forEach((item) => {
                        const index =
                            stores.accessoriesStockStore.$state.accessoriesStock.findIndex(
                                (accessory) => {
                                    accessory.id === item.id;
                                }
                            );
                        if (index !== -1) {
                            stores.accessoriesStockStore.$state.accessoriesStock[
                                index
                            ] = item;
                        }
                    });
                })
                .catch((error) => {
                    if (error.response && error.response.status === 422) {
                        this.errors = error.response.data.errors || {};
                    } else {
                        console.error(error);
                    }
                });
        },

        // Get data for dataTable
        async getDataForDataTable(
            numPage,
            nbEntries,
            searchValue,
            sortDirection
        ) {
            const res = await axios.get("/api/datatable/entretiens-list", {
                params: {
                    numPage,
                    nbEntries,
                    searchValue,
                    sortDirection,
                },
            });
            return res.data;
        },
    },
});
