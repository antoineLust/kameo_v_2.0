import axios from "axios";
import { defineStore } from "pinia";

export const useServicesStore = defineStore("services", {
    id: "services",
    state: () => ({
        services: [],
        service: {},
    }),
    getters: {
        getServices: (state) => {
            return state.services;
        },
        getService: (state) => {
            return state.service;
        },
        getServiceByIdAtelier: (state) => (id) => {
            return state.services.find((service) => service.id === Number(id));
        },
        getCategoriesAtelier: (state) => {
            const categories = state.services.map(
                (service) => service.category
            );
            const filterCategory = categories.filter(
                (category, index) => categories.indexOf(category) === index
            );
            return filterCategory;
        },
        getServicesByCategoryAtelier: (state) => (category) => {
            return state.services.filter(
                (service) => service.category === category
            );
        },
    },
    actions: {
        async getServiceById(id) {
            const responsePHP = await axios.get(
                "/api/services/get-service-by-id/" + id
            );
            return responsePHP.data;
        },

        async getServicesByCategory(category) {
            const responsePHP = await axios.get(
                "/api/services/get-services-by-category/" + category
            );
            return responsePHP.data;
        },

        async getCategories() {
            const responsePHP = await axios.get("/api/services/get-categories");
            return responsePHP.data;
        },

        // Api request for load all entretiens
        async loadServices() {
            await axios.get("/api/services").then((reponsePHP) => {
                this.services = reponsePHP.data;
            });
        },
    },
});
