// Import dependencies
import { onMounted, watch } from "vue";
import { useWebSocket } from "@vueuse/core";
import * as stores from "../store/StoresIndex";

export const isEmpty = require("lodash.isempty");
export const isEqual = require("lodash.isequal");
export const cloneDeep = require("lodash.clonedeep");
export const chunk = require("lodash.chunk");

// Verify user permission

export function useVerifyPermission(permission) {
    return (
        stores.usersStore.getUserAuth.permission != undefined &&
        stores.usersStore.getUserAuth.permission.includes(permission)
    );
}

// export function useVerifyPermission(p) {
//     const { userAuth } = storeToRefs(stores.usersStore);

//     const v = ref(false);
//     let user = userAuth.value;
//     if (user.permission != undefined) {
//         if (user.permission.includes(p)) {
//             v.value = true;
//         }
//     }
//     return v.value;
// }

// Generate datatable
export function generateDatatable(
    search = true,
    length = true,
    searchPlus = true
) {
    const loadTable = onMounted(() => {
        const table = $(".datatable").DataTable({
            destroy: true,
            ordering: false,
            deferRender: true,
            language: {
                emptyTable: "Aucune donnée disponible dans le tableau",
                loadingRecords: "Chargement...",
                processing: "Traitement...",
                aria: {
                    sortAscending:
                        ": activer pour trier la colonne par ordre croissant",
                    sortDescending:
                        ": activer pour trier la colonne par ordre décroissant",
                },
                select: {
                    rows: {
                        _: "%d lignes sélectionnées",
                        1: "1 ligne sélectionnée",
                    },
                    cells: {
                        1: "1 cellule sélectionnée",
                        _: "%d cellules sélectionnées",
                    },
                    columns: {
                        1: "1 colonne sélectionnée",
                        _: "%d colonnes sélectionnées",
                    },
                },
                autoFill: {
                    cancel: "Annuler",
                    fill: "Remplir toutes les cellules avec <i>%d</i>",
                    fillHorizontal: "Remplir les cellules horizontalement",
                    fillVertical: "Remplir les cellules verticalement",
                },
                searchBuilder: {
                    conditions: {
                        date: {
                            after: "Après le",
                            before: "Avant le",
                            between: "Entre",
                            empty: "Vide",
                            not: "Différent de",
                            notBetween: "Pas entre",
                            notEmpty: "Non vide",
                            equals: "Égal à",
                        },
                        number: {
                            between: "Entre",
                            empty: "Vide",
                            gt: "Supérieur à",
                            gte: "Supérieur ou égal à",
                            lt: "Inférieur à",
                            lte: "Inférieur ou égal à",
                            not: "Différent de",
                            notBetween: "Pas entre",
                            notEmpty: "Non vide",
                            equals: "Égal à",
                        },
                        string: {
                            contains: "Contient",
                            empty: "Vide",
                            endsWith: "Se termine par",
                            not: "Différent de",
                            notEmpty: "Non vide",
                            startsWith: "Commence par",
                            equals: "Égal à",
                            notContains: "Ne contient pas",
                            notEnds: "Ne termine pas par",
                            notStarts: "Ne commence pas par",
                        },
                        array: {
                            empty: "Vide",
                            contains: "Contient",
                            not: "Différent de",
                            notEmpty: "Non vide",
                            without: "Sans",
                            equals: "Égal à",
                        },
                    },
                    add: "Ajouter une condition",
                    button: {
                        0: "Recherche avancée",
                        _: "Recherche avancée (%d)",
                    },
                    clearAll: "Effacer tout",
                    condition: "Condition",
                    data: "Donnée",
                    deleteTitle: "Supprimer la règle de filtrage",
                    logicAnd: "Et",
                    logicOr: "Ou",
                    title: {
                        0: "Recherche avancée",
                        _: "Recherche avancée (%d)",
                    },
                    value: "Valeur",
                },
                searchPanes: {
                    clearMessage: "Effacer tout",
                    count: "{total}",
                    title: "Filtres actifs - %d",
                    collapse: {
                        0: "Volet de recherche",
                        _: "Volet de recherche (%d)",
                    },
                    countFiltered: "{shown} ({total})",
                    emptyPanes: "Pas de volet de recherche",
                    loadMessage: "Chargement du volet de recherche...",
                    collapseMessage: "Réduire tout",
                    showMessage: "Montrer tout",
                },
                buttons: {
                    collection: "Collection",
                    colvis: "Visibilité colonnes",
                    colvisRestore: "Rétablir visibilité",
                    copy: "Copier",
                    copySuccess: {
                        1: "1 ligne copiée dans le presse-papier",
                        _: "%ds lignes copiées dans le presse-papier",
                    },
                    copyTitle: "Copier dans le presse-papier",
                    csv: "CSV",
                    excel: "Excel",
                    pageLength: {
                        "-1": "Afficher toutes les lignes",
                        _: "Afficher %d lignes",
                    },
                    pdf: "PDF",
                    print: "Imprimer",
                    copyKeys:
                        "Appuyez sur ctrl ou u2318 + C pour copier les données du tableau dans votre presse-papier.",
                    createState: "Créer un état",
                    removeAllStates: "Supprimer tous les états",
                    removeState: "Supprimer",
                    renameState: "Renommer",
                    savedStates: "États sauvegardés",
                    stateRestore: "État %d",
                    updateState: "Mettre à jour",
                },
                decimal: ",",
                search: "Rechercher:",
                datetime: {
                    previous: "Précédent",
                    next: "Suivant",
                    hours: "Heures",
                    minutes: "Minutes",
                    seconds: "Secondes",
                    unknown: "-",
                    amPm: ["am", "pm"],
                    months: {
                        0: "Janvier",
                        2: "Mars",
                        3: "Avril",
                        4: "Mai",
                        5: "Juin",
                        6: "Juillet",
                        8: "Septembre",
                        9: "Octobre",
                        10: "Novembre",
                        1: "Février",
                        11: "Décembre",
                        7: "Août",
                    },
                    weekdays: ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"],
                },
                editor: {
                    close: "Fermer",
                    create: {
                        title: "Créer une nouvelle entrée",
                        button: "Nouveau",
                        submit: "Créer",
                    },
                    edit: {
                        button: "Editer",
                        title: "Editer Entrée",
                        submit: "Mettre à jour",
                    },
                    remove: {
                        button: "Supprimer",
                        title: "Supprimer",
                        submit: "Supprimer",
                        confirm: {
                            _: "Êtes-vous sûr de vouloir supprimer %d lignes ?",
                            1: "Êtes-vous sûr de vouloir supprimer 1 ligne ?",
                        },
                    },
                    multi: {
                        title: "Valeurs multiples",
                        info: "Les éléments sélectionnés contiennent différentes valeurs pour cette entrée. Pour modifier et définir tous les éléments de cette entrée à la même valeur, cliquez ou tapez ici, sinon ils conserveront leurs valeurs individuelles.",
                        restore: "Annuler les modifications",
                        noMulti:
                            "Ce champ peut être modifié individuellement, mais ne fait pas partie d'un groupe. ",
                    },
                    error: {
                        system: 'Une erreur système s\'est produite (<a target="\\" rel="nofollow" href="\\">Plus d\'information</a>).',
                    },
                },
                stateRestore: {
                    removeSubmit: "Supprimer",
                    creationModal: {
                        button: "Créer",
                        order: "Tri",
                        paging: "Pagination",
                        scroller: "Position du défilement",
                        search: "Recherche",
                        select: "Sélection",
                        columns: {
                            search: "Recherche par colonne",
                            visible: "Visibilité des colonnes",
                        },
                        name: "Nom :",
                        searchBuilder: "Recherche avancée",
                        title: "Créer un nouvel état",
                        toggleLabel: "Inclus :",
                    },
                    renameButton: "Renommer",
                    duplicateError: "Il existe déjà un état avec ce nom.",
                    emptyError: "Le nom ne peut pas être vide.",
                    emptyStates: "Aucun état sauvegardé",
                    removeConfirm: "Voulez vous vraiment supprimer %s ?",
                    removeError: "Échec de la suppression de l'état.",
                    removeJoiner: "et",
                    removeTitle: "Supprimer l'état",
                    renameLabel: "Nouveau nom pour %s :",
                    renameTitle: "Renommer l'état",
                },
                info: "Affichage de _START_ à _END_ sur _TOTAL_ entrées",
                infoEmpty: "Affichage de 0 à 0 sur 0 entrées",
                infoFiltered: "(filtrées depuis un total de _MAX_ entrées)",
                lengthMenu: "Afficher _MENU_ entrées",
                paginate: {
                    first: "Première",
                    last: "Dernière",
                    next: "Suivante",
                    previous: "Précédente",
                },
                zeroRecords: "Aucune entrée correspondante trouvée",
                thousands: " ",
            },
            bLengthChange: length,
            lengthMenu: [5, 10, 25, 50, 75, 100],
            pageLength: 10,
            fixedColumns: true,
            searching: search,
        });

        if (searchPlus) {
            $("thead th:not(:first-child, :last-child)").each(function (i) {
                const placeholder = $("thead th").eq($(this).index()).text();
                $(this).html(
                    '<input style="text-align: center; border: none; border-radius: 5px;" class="form-control" type="text" placeholder="' +
                        placeholder +
                        '" data-index="' +
                        i +
                        '" />'
                );
            });

            $(table.table().container()).on(
                "keyup",
                "thead input",
                function () {
                    table
                        .column($(this).data("index") + 1)
                        .search(this.value)
                        .draw();
                }
            );
        }
    });
}
// Add picture to storage or preview
/*
    @param {string} vue, create or update
    @param {array} previews, array containing names of files to preview
    @param {function} storeGetter, function to get the state of the store

    This function will be used in the form to upload pictures, in the case of a creation it will also preview the images
*/
export function uploadFile({
    mode = "multiple",
    e,
    formData,
    action,
    previews,
    fileType = ["image"],
    store,
    stateFiles,
}) {
    if (e.target.files) {
        for (let i = 0; i < e.target.files.length; i++) {
            // Validations rules
            store.getFileErrors.message = null;
            let allowedExtensions = [];
            let maxSize = 0;
            if (fileType.includes("image")) {
                allowedExtensions = allowedExtensions.concat([
                    "image/jpg",
                    "image/jpeg",
                    "image/png",
                    "image/gif",
                    "image/svg",
                    "image/webp",
                ]);
            }
            if (fileType.includes("video")) {
                allowedExtensions = allowedExtensions.concat([
                    "video/mp4",
                    "video/webm",
                    "video/ogg",
                ]);
            }
            if (e.target.files[i].type.split("/")[0] === "image") {
                maxSize = 2048000;
            } else if (e.target.files[i].type.split("/")[0] === "video") {
                maxSize = 25000000;
            }
            // Check if file is valid
            if (allowedExtensions.includes(e.target.files[i].type)) {
                if (e.target.files[i].size <= maxSize) {
                    if (action === "update") {
                        if (mode === "single") {
                            formData.set("file", e.target.files[i]);
                        } else if (mode === "multiple") {
                            // Verify if file name already exists
                            if (stateFiles) {
                                if (
                                    !Object.values(stateFiles).includes(
                                        e.target.files[i].name
                                    )
                                ) {
                                    formData.append(
                                        e.target.files[i].name,
                                        e.target.files[i]
                                    );
                                } else {
                                    store.getFileErrors.message =
                                        "Un fichier porte déjà le même nom";
                                }
                            } else {
                                formData.append(
                                    e.target.files[i].name,
                                    e.target.files[i]
                                );
                            }
                        }
                    } else if (action === "create") {
                        var reader = new FileReader();
                        reader.onload = function (event) {
                            if (e.target.files[i].size <= maxSize) {
                                if (mode === "single") {
                                    formData.set("file", e.target.files[i]);
                                    previews.length = 0;
                                    previews.push({
                                        id: e.target.files[i].name,
                                        src: event.target.result,
                                        type: e.target.files[i].type.split(
                                            "/"
                                        )[0],
                                    });
                                } else if (mode === "multiple") {
                                    // Verify if file already exists
                                    if (
                                        previews.some(
                                            (e) => e.src === event.target.result
                                        )
                                    ) {
                                        store.getFileErrors.message =
                                            "Ce fichier à déjà été ajouté";
                                    }
                                    // Verify if file name already exists
                                    else if (
                                        previews.some(
                                            (elem) =>
                                                elem.id ===
                                                e.target.files[i].name
                                        )
                                    ) {
                                        store.getFileErrors.message =
                                            "Un fichier porte déjà le même nom";
                                    } else {
                                        formData.append(
                                            e.target.files[i].name,
                                            e.target.files[i]
                                        );
                                        previews.push({
                                            id: e.target.files[i].name,
                                            src: event.target.result,
                                            type: e.target.files[i].type.split(
                                                "/"
                                            )[0],
                                        });
                                    }
                                }
                            }
                        };
                        reader.readAsDataURL(e.target.files[i]);
                    }
                } else {
                    store.getFileErrors.message =
                        "Fichier trop volumineux, taille maximum : " +
                        maxSize / 1000 +
                        " Kb";
                }
            } else {
                store.getFileErrors.message = "Extension non acceptée";
            }
        }
    }
}

// Delete previewed or uploaded image
/*
    @param {array} previews, array containing names of files to preview
    @param {string} vue, create or update
    @param {string} name, name of the file to delete
    @param {int} id , id of the folder to access
    @param {store} store, ref to store

    This function will be used in the form to delete pictures, in the case of a creation it will delete the preview

*/
export function removePicture({
    id,
    vue,
    formData,
    name = "file",
    previews,
    store,
}) {
    if (vue === "create") {
        formData.delete(name);
        var index = previews.findIndex((p) => p.id === name);
        previews.splice(index, 1);
    } else if (vue === "update") {
        store.deletePicture(id, name);
    }
}

export function calculateLeasingPrice(price, duration, preFinancing) {
    if (duration === 1 && !preFinancing) {
        return (
            (parseInt(price) * (1 - 0.27) * (1 + 0.7) +
                (3 * 84 + 4 * 100) * (1 + 0.3)) /
            36
        ).toFixed(2);
    } else if (duration === 12 && !preFinancing) {
        return (
            ((parseInt(price) * (1 - 0.27) * (1 + 0.7) +
                (3 * 84 + 4 * 100) * (1 + 0.3)) /
                36) *
            12
        ).toFixed(2);
    } else if (duration === 1 && preFinancing) {
        return (
            parseInt(price) +
            (3 * 84 +
                ((700 - 400) / (5000 - 2000)) * parseInt(price) +
                33.3333) *
                (1 + 0.3)
        ).toFixed(2);
    } else if (duration === 12 && preFinancing) {
        return (
            (parseInt(price) +
                (3 * 84 +
                    ((700 - 400) / (5000 - 2000)) * parseInt(price) +
                    33.3333) *
                    (1 + 0.3)) *
            12
        ).toFixed(2);
    }
}

export function formatDate(date) {
    let d = new Date(date),
        month = "" + (d.getMonth() + 1),
        day = "" + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;

    return [day, month, year].join("-");
}

export function formatTimeDate(timeDate) {
    let d = new Date(timeDate),
        month = "" + (d.getMonth() + 1),
        day = "" + d.getDate(),
        year = d.getFullYear(),
        hour = "" + d.getHours(),
        minute = "" + d.getMinutes(),
        second = "" + d.getSeconds();

    if (month.length < 2) month = "0" + month;
    if (day.length < 2) day = "0" + day;
    if (hour.length < 2) hour = "0" + hour;
    if (minute.length < 2) minute = "0" + minute;
    if (second.length < 2) second = "0" + second;

    const date = [day, month, year].join("-");
    const time = [hour, minute].join(":");

    return date + " " + time;
}

// Websocket

export const { status, data, send, open, close } = useWebSocket(
    "ws://51.68.92.45:3000/ws",
    {
        autoReconnect: true,
    }
);

watch(status, () => {
    console.log("Websocket status : " + status.value);
});
