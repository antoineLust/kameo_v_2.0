require("./bootstrap");

// Import dependencies
import { createApp, markRaw } from "vue";
import { createPinia } from "pinia";
import { getStores } from "./store/StoresIndex";

// Vuetify
import "vuetify/styles";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
const vuetify = createVuetify({
    components,
    directives,
});

// Import components
import App from "./App.vue";

// Import routes
import router from "./routes/index";

import { RecycleScroller } from "vue-virtual-scroller";
import "vue-virtual-scroller/dist/vue-virtual-scroller.css";

// Import fontAwesome
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
    faBars,
    faXmark,
    faServer,
    faUser,
    faAngleDown,
    faUsers,
    faQuestion,
    faGear,
    faSort,
    faCheck,
} from "@fortawesome/free-solid-svg-icons";

// Add fontAwesome icons
library.add(
    faBars,
    faXmark,
    faServer,
    faUser,
    faAngleDown,
    faUsers,
    faQuestion,
    faGear,
    faSort,
    faCheck
);

// Pinia
const pinia = createPinia();
pinia.use(({ store }) => {
    store.$router = markRaw(router);
});

// Vue app creation
const app = createApp(App);
app.component("RecycleScroller", RecycleScroller);
app.component("font-awesome-icon", FontAwesomeIcon);
app.use(router);
app.use(pinia);
app.use(vuetify);

getStores();

// Start app
app.mount("#app");
