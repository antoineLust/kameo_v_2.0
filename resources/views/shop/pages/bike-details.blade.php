@extends('shop.layouts.default')
<style>
    table{
        width: 100%;
        border-collapse: collapse;
        border: 2px solid grey;
    }
    th,td{
        border: 1px solid grey
    }
    .price-tag {
        background: #d60b52;
        border-radius: 5px 0 0 5px;
        color: #fff;
        cursor: pointer;
        width: 130px;
        display: block;
        font-size: 0.875rem;
        height: 30px;
        line-height: 30px;
        padding: 0 0.666rem;
        position: relative;
        margin-top: 0;
        text-align: center;
    }
    .price-tag:after {
        background: inherit;
        border-radius: 5px;
        display: block;
        content: "";
        height: 24px;
        position: absolute;
        right: -23px;
        top: -2px;
        transform-origin: top left;
        transform: rotate(45deg);
        width: 24px;
        z-index: -1;
    }
</style>
@section('content')
    <div style="width: 70%; margin: auto">
        <div class="returnBack" onclick="history.back()">Retour</div>

        <h1 style="color:#d60b52; display: inline-block; margin-left:20px">Catalogue - {{ $bike->brand }} {{ $bike->model }}</h1>

        <div class="grid md:grid-cols-2">
            <div class="text-center">
                <div class="price-tag" style="font-size: 1.5em">{{ round($bike->price_htva*1.21, 2) }} €</div>
                <img src="../../images_bikes/velo.jpg" alt="image vélo {{ $bike->brand }} {{ $bike->model }}" style="width:100%">
            </div>
            <div class="text-center">
                <h2 style="color:#d60b52">Envie d'essayer le vélo ?</h2>
                <a class="btn" href="{{ route('contact_pro') }}" style="font-size:1.5em">Réserver mon essai</a>
                <br><br><br>
                <h2 style="color:#d60b52">Détails techniques</h2>
                <table>
                    <tr>
                        <td style="font-weight:500">Marque</td>
                        <td>{{ $bike->brand }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight:500">Modèle</td>
                        <td>{{ $bike->model }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight:500">Moteur</td>
                        <td>{{ $bike->motor }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight:500">Batterie</td>
                        <td>{{ $bike->battery }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight:500">Console</td>
                        <td>{{ $bike->display_device }}</td>
                    </tr>
                    <tr>
                        <td style="font-weight:500">Transmition</td>
                        <td>{{ $bike->transmission }}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="width:100%">
            <h2 style="color:#d60b52">Description</h2>
            <p>{{ $bike->description }}</p>
        </div>
    </div>
    <style>

        table{
            border-radius: 10px;
            border-collapse: collapse;
            margin: 1em;
        }
        th {
            border-bottom: 1px solid #364043;
            color: black;
            font-size: 0.85em;
            font-weight: 600;
            padding: 0.5em 1em;
            text-align: left;
        }
        td {
            color: black;
            font-weight: 400;
            padding: 0.65em 1em;
        }
        tbody tr {
            transition: background 0.2s ease;
        }
        tbody tr:hover {
            background: #d60b52;
        }
    </style>
@stop
