
@extends('shop.layouts.default')
<style>
    .gallery{
        margin: auto;
        padding : 0 20px;
        display: grid;
        grid-template-columns: repeat(auto-fill, 280px);
    }
    .item{
        min-height: 200px;
        text-align: center;
        margin: 10px;
    }

    .btn{
        background-color: #d60b52;
        color: white;
        padding: 0 24px;
        border-radius: 24px;
    }

    @media screen and (min-width: 1200px) {
        .gallery{
            margin: auto;
            padding : 0 20px;
            display: grid;
            grid-template-columns: repeat(auto-fill, 400px);
        }
        .item{
            min-height: 200px;
            text-align: center;
            margin: 10px;
        }

        .btn{
            background-color: #d60b52;
            color: white;
            padding: 0 24px;
            border-radius: 24px;
        }
    }
</style>

@section('content')
    <div style="width: 70%; margin: auto">
        <div class="returnBack" onclick="history.back()">Retour</div>

        <h1 style="color:#d60b52; display: inline-block; margin-left:20px">Catalogue</h1>

        <div class="gallery">
            <a href="{{ route('catalog-accessories', ['category'=>"cadenas"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_cadenas.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Cadenas</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog-accessories', ['category'=>"casque"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_casques.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Casque</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog-accessories', ['category'=>"textiles"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_textiles.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Textiles</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog-accessories', ['category'=>"sacoches"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_sacoches.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Sacoches</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog-accessories', ['category'=>"phares"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_phares.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Phares</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog-accessories', ['category'=>"siege_enfant"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_siegesenfants.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Siège enfant</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog-accessories', ['category'=>"pompes"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_pompes.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Pompe à vélo</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog-accessories', ['category'=>"produits_nettoyants"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_selles.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Selles</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog-accessories', ['category'=>"selle"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_produits.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Produit d'entretien</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog-accessories', ['category'=>"pedales"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_pedales.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Pédales</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog-accessories', ['category'=>"support_smartphone"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_supportsmartphone.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Support smartphone</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog-accessories', ['category'=>"remorque"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="{{asset('images/Cover_Catalogue/Covers_Catalogue_remorquesvelo.jpg')}}" width="100%">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Remorque</h3></div>
                    <div class="space"></div>
                </div>
            </a>

        </div>
    </div>
@stop
