@extends('shop.layouts.default')
@section('content')
    <style>
        .gallery{
            margin: auto;
            display: grid;
            grid-template-columns: repeat(1, 1fr);
            column-gap:20px;
            row-gap:20px
        }

        .gallery>div{
            border: 1px solid #c7c7c7;
            border-radius:10px;
            padding:10px;
        }
        .gallery>a>div>h2{
            margin-top:0px;
        }
        .gallery>div>div{
            margin-bottom:10px;
        }
        .actionCall{
            background-color: #d60b52;
            padding:10px;
            color: white;
            margin-bottom:10px;
        }
        @media screen and (min-width: 760px) {
            .gallery{
                grid-template-columns: repeat(2, 1fr);
            }
        }
        
        @media screen and (min-width: 1024px) {
            .gallery{
                grid-template-columns: repeat(3, 1fr);
            }
        }

    </style>
    <div class="full-width">
        <div class="returnBack" onclick="history.back()">Retour</div>
        <h1 style="color:#d60b52; display:inline">Blog</h1>
        <div class="gallery" style="padding-top:20px">
            <div>
                <h2>Comment choisir son nouveau vélo ?</h2>
                <img src="{{ asset('images/blog/ChoixVelo_Cover.jpg') }}" width="100%" height="auto">
                <p style="padding:0px 20px 0px 20px">
                    Suivez notre guide pour faire votre meilleur choix.<br><br>
                </p>
                <div style="width:100%; text-align:center">
                    <a href="{{ route('choisir-son-velo') }}" class="actionCall">
                            LIRE L'ARTICLE
                    </a>
                </div>
            </div>
            <div>
                <h2>Nouveau site internet</h2>
                <img src="{{ asset('images/blog/vue3D_cover.jpg') }}" width="100%" height="auto">
                <p style="padding:0px 20px 0px 20px">
                    Nous vous expliquons tous les changements liés à notre nouveau site internet<br><br>
                </p>
                <div style="width:100%; text-align:center">
                    <a href="{{ route('blog-shop') }}" class="actionCall">
                            LIRE L'ARTICLE
                    </a>
                </div>
            </div>
            <div>
                <h2>Nouveau magasin à Liège</h2>
                <img src="{{ asset('images/blog/vue3D_cover.jpg') }}" width="100%" height="auto">
                <p style="padding:0px 20px 0px 20px">
                    Saviez-vous que  comment choisir votre vélo ?<br><br>
                </p>
                <div style="width:100%; text-align:center">
                    <a href="{{ route('nouveau-magasin') }}" class="actionCall">
                            LIRE L'ARTICLE
                    </a>
                </div>
            </div>

        </div>
    </div>
    
@stop
