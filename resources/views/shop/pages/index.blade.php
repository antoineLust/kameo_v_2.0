@extends('shop.layouts.default')

<script src="https://kit.fontawesome.com/02ec9d769d.js" crossorigin="anonymous"></script>

<style>

    .prev, .next {
        cursor: pointer;
        width: auto;
        color: white;
        font-weight: bold;
        font-size: 18px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
    }

    /* On hover, add a black background color with a little bit see-through */
    .prev:hover, .next:hover {
    background-color: rgba(0,0,0,0.8);
    }

    /* Fading animation */
    .mySlides {
        display:none;
        animation-name: fade;
        animation-duration: 1.5s;
    }

    .mySlides img{
        max-width: 150px;
    }

    @keyframes fade {
        from {opacity: .4}
        to {opacity: 1}
    }

    .card {
        /* Add shadows to create the "card" effect */
        margin:auto;
        border: 1px solid #c7c7c7;
        height:100%;
        border-radius: 20px;
        width: 100%;
        text-align: center;
    }

    .title_vignette{
        color: #d60b52;
        font-weight: bold;
        font-size: 1.2em;
    }


    .grid-template-2{
        display: grid;
        grid-template-columns: 1fr;
        padding: 20px 20px 0px 20px;

    }

    .item{
        text-align: center;
        margin: 10px;
        border-radius: 10px;
        width:100px;
        height:43px;
    }
    .item img{
        width: 100%;
    }

    @media screen and (min-width: 1024px){
        .grid-template-2{
            display: grid;
            grid-template-columns: 100px 50%;
            padding: 20px 20px 0px 20px;

        }
        .gallery{
            margin: auto;
            display: grid;
            width: 100%;
            grid-template-columns: repeat(auto-fill, 200px);
        }
        .item{
            width:175px;
            height:75px;
        }
        .item img{
            width: 150px;
        }

        .mySlides img{
            max-width: 200px;
        }


    }

</style>

@section('content')
<div class="sliderTest hidden-sm" style="position: relative; margin-top:20px">
    <div style="position: relative, top:0px">
        <img src="{{ asset('images/shop_banner.jpg') }}" alt="Banner image for shop" width="100%" height="auto">
        <span style="border-radius:20px; background-color:#d60b52" id="portfolioLink">
            <a href="{{ route('catalog-overview') }}">
                Découvrir notre catalogue
            </a>
        </span>
    </div>
    <div style="position: absolute; top:0px; display: none">
        <img src="{{ asset('images/we_are_open_banner.jpg') }}" alt="We are open banner" width="100%">
    </div>
    <span class="icon-arrow_back3" id="prev3" style="float:left"></span>
    <span class="icon-arrow_forward3" style="float:right" id="next3"></span>
</div>
<div class="visible-sm">
    <img src="{{ asset('images/shop_banner_sm.jpg') }}" alt="Banner image for shop" width='100%'>
</div>
<style>

    #portfolioLink{
        display:none;
        padding: 0px 20px 0px 20px;
    }
    @media (min-width: 1440px){

        #portfolioLink{
            display:block;
        }
    }
    .sliderTest{
        overflow: hidden;
    }
    .slide-ana{
        height: 100%;
    }
    .sliderTest div{
        transition: all 1s;
    }
    .sliderTest span{
        height: 50px;
        position: absolute;
        top:50%;
        margin:-25px 0 0 0;
        background-color: rgba(0, 0, 0, 0.4);
        color:#fff;
        line-height: 50px;
        text-align: center;
        cursor: pointer;
    }

    .sliderTest div span{
        position: absolute;
        top:60%;
        left:60%;
        font-size:15px;
        font-weight:800;
    }
    @media (min-width: 640px){
        .sliderTest div span{
            font-size:25px;
        }
    }

    @media (min-width: 1440px){
        .sliderTest div span{
            font-size:30px;
        }
    }


    .slide-ana div{
        width:100%;
        height: 100%;
        top:0px;
        position: absolute;
        transition: all 1s;
        background-size: cover;
        background-position: center center;
        background-repeat: no-repeat;
    }
    .slide-ana{
        font-size: 30px;
        text-align: center;
        text-transform: uppercase;
        color: #fff;
    }
    #prev3{
        left:0;
        border-radius: 0 10px 10px 0;
    }
    #next3{
        right:0;
        border-radius:10px 0 0 10px;
    }
    #prev3 svg,#next svg{
        width:100%;
        fill: #fff;
    }
    #prev3:hover,
    #next3:hover{
        background-color: rgba(0, 0, 0, 0.8);
    }

</style>
<script>
    let slides3 = document.querySelectorAll('.sliderTest div');
    let slideSayisi = slides3.length;

    for (let index = 0; index < slides3.length; index++) {
        const element = slides3[index];
        element.style.transform = "translateX("+100*(index)+"%)";
        element.style.display="block";
    }
    let loop = 0 + 1000*slideSayisi;

    function goNext3(){
        loop++;
        for (let index = 0; index < slides3.length; index++) {
            const element = slides3[index];
            element.style.transform = "translateX("+100*(index-loop%slideSayisi)+"%)";
        }
    }

    setInterval(goNext3, 5000);


</script>
<div style="width: 80%; margin: auto">
    <h1 style="color:#d60b52">{{ __('KAMEO SHOP - SPECIALISTE VELO A LIEGE') }}</h1>
</div>

<div style="width: 80%; margin: auto" class="hidden-sm">
    <div style="width:100%; position: relative;">
        <div style="width:100%" class="overviewCatalogGrid" id="overviewCatalogGrid">
            <a href="{{ route('catalog', ['utilisation'=>"Ville"]) }}">
                <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/ville.jpg')}}')">
                        <h2 class="vignette" style="background-color: black; color: white">Vélos de ville</h2>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"Ville et chemin"]) }}">
                <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/villechemin.jpg')}}')">
                    <h2 class="vignette" style="background-color: black; color: white">Vélos ville et chemin</h2>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"Tout chemin"]) }}">
                <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/toutchemin.jpg')}}')">
                    <h2 class="vignette" style="background-color: black; color: white">Vélos tout chemin</h2>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"VTT"]) }}">
                <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/vtt.jpg')}}')">
                    <h2 class="vignette" style="background-color: black; color: white">Vélos VTT</h2>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"speed"]) }}">
                <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/speed.jpg')}}')">
                    <h2 class="vignette" style="background-color: black; color: white">Vélos Speedelec</h2>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"cargo"]) }}">
                <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/cargo.jpg')}}')">
                    <h2 class="vignette" style="background-color: black; color: white">Vélos Cargo</h2>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"pliant"]) }}">
                <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/pliant.jpg')}}')">
                    <h2 class="vignette" style="background-color: black; color: white">Vélos pliants</h2>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"gravel"]) }}">
                <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/gravel.jpg')}}')">
                    <h2 class="vignette" style="background-color: black; color: white">Vélos gravel</h2>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"enfant"]) }}">
                <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/enfant.jpg')}}')">
                    <h2 class="vignette" style="background-color: black; color: white">Vélos enfant</h2>
                </div>
            </a>
        </div>
        <span class="icon-arrow_back" id="prev" style="float:left; display:none"></span>
        <span class="icon-arrow_forward" style="float:right" id="next"></span>
    </div>
</div>
<div style="width: 100%; margin: auto; padding:10px;" id='bikesGallery'>
    <a href="{{ route('catalog', ['utilisation'=>"Ville"]) }}">
        <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/ville.jpg')}}')">
            <h2 class="vignette" style="background-color: black; color: white">Vélos de ville</h2>
        </div>
    </a>
    <a href="{{ route('catalog', ['utilisation'=>"Ville et chemin"]) }}">
        <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/villechemin.jpg')}}')">
            <h2 class="vignette" style="background-color: black; color: white">Vélos ville et chemin</h2>
        </div>
    </a>
    <a href="{{ route('catalog', ['utilisation'=>"Tout chemin"]) }}">
        <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/toutchemin.jpg')}}')">
            <h2 class="vignette" style="background-color: black; color: white">Vélos tout chemin</h2>
        </div>
    </a>
    <a href="{{ route('catalog', ['utilisation'=>"VTT"]) }}">
        <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/vtt.jpg')}}')">
            <h2 class="vignette" style="background-color: black; color: white">Vélos VTT</h2>
        </div>
    </a>
    <a href="{{ route('catalog', ['utilisation'=>"speed"]) }}">
        <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/speed.jpg')}}')">
            <h2 class="vignette" style="background-color: black; color: white">Vélos Speedelec</h2>
        </div>
    </a>
    <a href="{{ route('catalog', ['utilisation'=>"cargo"]) }}">
        <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/cargo.jpg')}}')">
            <h2 class="vignette" style="background-color: black; color: white">Vélos Cargo</h2>
        </div>
    </a>
    <a href="{{ route('catalog', ['utilisation'=>"pliant"]) }}">
        <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/pliant.jpg')}}')">
            <h2 class="vignette" style="background-color: black; color: white">Vélos pliants</h2>
        </div>
    </a>
    <a href="{{ route('catalog', ['utilisation'=>"gravel"]) }}">
        <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/gravel.jpg')}}')">
            <h2 class="vignette" style="background-color: black; color: white">Vélos gravel</h2>
        </div>
    </a>
    <a href="{{ route('catalog', ['utilisation'=>"enfant"]) }}">
        <div class="categorySelection" style="background-image: url('{{asset('images/covers/mini/enfant.jpg')}}')">
            <h2 class="vignette" style="background-color: black; color: white">Vélos enfant</h2>
        </div>
    </a>
</div>
<style>
    .brandsGallery{
        width: 100%;
        display:grid;
        margin: auto;
        grid-template-columns: repeat(auto-fill, 100px);
        justify-content:center;
    }
    .brandsGallery img{
        width:100px;

    }
    @media (min-width: 640px){
        .brandsGallery{
            width: 100%;
            display:grid;
            margin: auto;
            grid-template-columns: repeat(auto-fill, 200px);
        }
        .brandsGallery img{
            width:200px;
        }

    }

    .overviewCatalogGrid{
        padding: 0px 20px 0px 0px;
        margin:auto;
        display:grid;
        column-gap: 30px;
        row-gap:20px;
        overflow:hidden;
        grid-auto-flow: column;
        margin-bottom:50px;
    }
    .overviewCatalogGrid a{
        transition:0.3s;
    }
    .overviewCatalogGrid>a>div{
        height:400px;
        width:200px;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }

    #prev{
        user-select: none;
        left:0;
        border-radius: 0 10px 10px 0;
        background-color: rgba(0, 0, 0, 0.4);
        margin-left:-50px;
    }
    #next{
        user-select: none;
        right:0;
        border-radius:10px 0 0 10px;
        background-color: rgba(0, 0, 0, 0.4);
        margin-right:-50px;
        z-index: 10000;
    }
    #prev svg,#next svg{
        width:100%;
        fill: #fff;
    }
    #prev:hover,
    #next:hover{
        background-color: rgba(0, 0, 0, 0.8);
    }
    .icon-arrow_back{
        float: left;
        position: absolute;
        left: 0px;
        top: 35%;
        z-index: 1000;
        padding: 5px;
    }

    .icon-arrow_forward{
        float: right;
        position: absolute;
        right: 0px;
        top: 35%;
        z-index: 1000;
        padding: 5px;
    }

    #bikesGallery{
        display:grid;
        grid-template-columns: repeat(3, 1fr);
        width: 100%;
        margin: auto;
        padding:10px;
        column-gap:20px;
        row-gap:20px
    }
    #bikesGallery>a>div{
        height:200px;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    @media screen and (min-width: 640px){
        #bikesGallery{
            grid-template-columns: repeat(5, 1fr);
        }
        #bikesGallery>a>div{
            height:200px
        }
    }
    @media screen and (min-width: 1124px){
        #bikesGallery{
            display:none
        }
    }


</style>
<script>
    let arrowIconLeft = '<?xml version="1.0" ?><!DOCTYPE svg  PUBLIC \'-//W3C//DTD SVG 1.1//EN\'  \'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\'><svg height="50px" id="Layer_1" style="enable-background:new 0 0 50 50;" version="1.1" viewBox="0 0 512 512" width="50px" color="#fff" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><polygon points="352,115.4 331.3,96 160,256 331.3,416 352,396.7 201.5,256 "/></svg>';

    let prev = document.getElementById('prev');
    let next = document.getElementById('next');
    prev.innerHTML = arrowIconLeft;
    next.innerHTML = arrowIconLeft;
    next.querySelector('svg').style.transform = 'rotate(180deg)';
    next.addEventListener('click',goNext);
    prev.addEventListener('click',goPrev);
    document.addEventListener('keydown',function(e){
        if(e.code === 'ArrowRight'){
            goNext();
        }else if(e.code === 'ArrowLeft'){
            goPrev();
        }
    });
    let elements = document.querySelectorAll('.overviewCatalogGrid a');
    let indexSlide=0;
    let arrowRight=document.getElementById('next');
    let arrowLeft=document.getElementById('prev');
    let lastElement = elements[elements.length -1 ];
    let lastElementInitialPosition = lastElement.getBoundingClientRect().left;


    function goNext(){
        indexSlide++;

        for (let index = 0; index < elements.length; index++) {

            const element = elements[index];
            element.style.transform = "translateX("+255*(indexSlide)*(-1)+"px)";
        }
        checkDisplayArrows();
    }

    function goPrev(){
        indexSlide--;

        for (let index = 0; index < elements.length; index++) {
            const element = elements[index];
            element.style.transform = "translateX("+255*(indexSlide)*(-1)+"px)";
        }
        checkDisplayArrows();
    }

    function checkDisplayArrows(){
        if(indexSlide==0){
            arrowLeft.style.display="none";
        }else{
            arrowLeft.style.display="block";
        }
        var transformStyle = elements[elements.length - 1].style.transform;
        var translateX = transformStyle.replace(/[^\d.]/g, '');

        if((lastElementInitialPosition-translateX)<document.getElementById('overviewCatalogGrid').clientWidth){
            arrowRight.style.display="none";
        }else{
            arrowRight.style.display="block";
        }
    }

</script>
<div class="full-width">
    <h2>Nos marques</h2>
    <div class="brandsGallery">
        <div>
            <img src="{{ asset('images_bikes/logo_brands/conway.png') }}" style="max-width:200px">
        </div>
        <div>
            <img src="{{ asset('images_bikes/logo_brands/victoria.png') }}" style="max-width:200px">
        </div>
        <div>
            <img src="{{ asset('images_bikes/logo_brands/benno.png') }}" style="max-width:200px">
        </div>
        <div>
            <img src="{{ asset('images_bikes/logo_brands/kayza.png') }}" style="max-width:200px">
        </div>
        <div>
            <img src="{{ asset('images_bikes/logo_brands/ahooga.png') }}" style="max-width:200px">
        </div>
        <div>
            <img src="{{ asset('images_bikes/logo_brands/Bzen.png') }}" style="max-width:200px">
        </div>
        <div>
            <img src="{{ asset('images_bikes/logo_brands/Hnf.png') }}" style="max-width:200px">
        </div>
        <div>
            <img src="{{ asset('images_bikes/logo_brands/Bemoov.png') }}" style="max-width:200px">
        </div>
        <div>
            <img src="{{ asset('images_bikes/logo_brands/Eovolt.png') }}" style="max-width:200px">
        </div>
    </div>
</div>

<div class="full-width">
    <h2>Catalogue d'accessoires</h2>
    <div id="accessoriesGallery">
        <a href="{{ route('catalog-accessories', ['category'=>"cadenas"]) }}">
            <div style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_cadenas.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Cadenas</h2>
            </div>
        </a>
        <a href="{{ route('catalog-accessories', ['category'=>"casque"]) }}">
            <div class="accessoryCategory" style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_casques.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Casque</h2>
            </div>
        </a>
        <a href="{{ route('catalog-accessories', ['category'=>"textiles"]) }}">
            <div class="accessoryCategory" style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_textiles.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Textiles</h2>
            </div>
        </a>
        <a href="{{ route('catalog-accessories', ['category'=>"sacoches"]) }}">
            <div class="accessoryCategory" style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_sacoches.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Sacoches</h2>
            </div>
        </a>
        <a href="{{ route('catalog-accessories', ['category'=>"phares"]) }}">
            <div class="accessoryCategory" style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_phares.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Phares</h2>
            </div>
        </a>
        <a href="{{ route('catalog-accessories', ['category'=>"siege_enfant"]) }}">
            <div class="accessoryCategory" style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_siegesenfants.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Siège enfant</h2>
            </div>
        </a>
        <a href="{{ route('catalog-accessories', ['category'=>"pompes"]) }}">
            <div class="accessoryCategory" style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_pompes.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Pompe à vélo</h2>
            </div>
        </a>
        <a href="{{ route('catalog-accessories', ['category'=>"produits_nettoyants"]) }}">
            <div class="accessoryCategory" style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_produits.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Produit d'entretien</h2>
            </div>
        </a>
        <a href="{{ route('catalog-accessories', ['category'=>"selle"]) }}">
            <div class="accessoryCategory" style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_selles.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Selle</h2>
            </div>
        </a>
        <a href="{{ route('catalog-accessories', ['category'=>"pedales"]) }}">
            <div class="accessoryCategory" style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_pedales.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Pédales</h2>
            </div>
        </a>
        <a href="{{ route('catalog-accessories', ['category'=>"support_smartphone"]) }}">
            <div class="accessoryCategory" style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_supportsmartphone.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Support smartphone</h2>
            </div>
        </a>
        <a href="{{ route('catalog', ['utilisation'=>"enfant"]) }}">
            <div class="accessoryCategory" style="background-image: url('{{asset('images/Cover_Catalogue/Covers_Catalogue_remorquesvelo.jpg')}}')">
                <h2 class="vignette" style="background-color: black; color: white">Remorque</h2>
            </div>
        </a>
    </div>

</div>
<style>
    #accessoriesGallery{
        display:grid;
        grid-template-columns: repeat(2, 1fr);
        width: 100%;
        margin: auto;
        padding:10px;
        column-gap:20px;
        row-gap:20px
    }
    #accessoriesGallery>a>div{
        height:100px;
        background-position: center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    @media screen and (min-width: 640px){
        #accessoriesGallery{
            grid-template-columns: repeat(4, 1fr);
        }
        #accessoriesGallery>a>div{
            height:150px
        }
    }
    @media screen and (min-width: 1024px){
        #accessoriesGallery{
            grid-template-columns: repeat(6, 1fr);
        }
        #accessoriesGallery>a>div{
            height:200px
        }
    }
    @media screen and (min-width: 1440px){
        #accessoriesGallery{
            grid-template-columns: repeat(6, 1fr);
        }
    }

</style>

<div style="background-color:#d60b52; padding:30px 0px 30px 0px; width:100%">
    <div class="grid sm:grid-flow-col full-width" style="column-gap:20px; row-gap:20px">
        <div class="card" style="position: relative; padding-bottom:20px; background-color: white">
            <div class="grid-template-2" style="padding: 20px 20px 0px 20px; margin-top:0px">
                <div class="grid-1">
                    <img src="{{ asset('images/icons/Icone_Employe_Red.jpg') }}" alt="Icone vélo du moment" width="70px">
                </div>
                <div class="grid-1">
                    <span class="title_vignette">Vélo du mois</span>
                </div>
            </div>
            <div class="text-center" style="padding: 40px; width:100%;margin:auto">
                <img src="{{ asset('images_bikes/velo.jpg') }}" width="100%" alt="image vélo du moment">
                <span style="color:#d60b52; font-weight:600">Benno Boost E10D</span>
            </div>
        </div>
        <div class="card" style="position: relative; padding-bottom:20px; background-color: white">
            <div class="grid-template-2" style="padding: 20px 20px 0px 20px; margin-top:0px">
                <div class="grid-1">
                    <img src="{{ asset('images/icons/Icone_Employe_Red.jpg') }}" alt="logo je suis un employeur" width="70px">
                </div>
                <div class="grid-1">
                    <span class="title_vignette">Location court terme</span>
                </div>
            </div>
            <p style="padding:0px 20px 0px 20px">
                Nous proposons de nombreux vélos en location court terme, découvrez nos prix et réservez votre vélo.
            </p>
            <div style="width:100%; position: absolute; bottom:0px; padding-bottom: 20px">
                <a href="{{ route('bike-short-location') }}">
                    <span style="background-color:#d60b52; color: white; margin-bottom:20px; font-weight:800; display: table; margin: 0 auto; padding: 0px 20px 0px 20px; border-radius:20px">
                        RESERVER UN VELO
                    </span>
                </a>
            </div>
        </div>
        <div class="card" style="position: relative; padding-bottom:20px; background-color: white">
            <div class="grid-template-2" style="padding: 20px 20px 0px 20px; margin-top:0px">
                <div class="grid-1">
                    <img src="{{ asset('images/icons/Icone_Employe_Red.jpg') }}" alt="logo je suis un employeur" width="70px">
                </div>
                <div class="grid-1">
                    <span class="title_vignette">Comment choisir son vélo ?</span>
                </div>
            </div>
            <p style="padding:0px 20px 0px 20px">
                Saviez-vous que nous avons une section blog où nous vous expliquons comment choisir votre vélo ?<br>
            </p>
            <img src="{{ asset('images/blog/ChoixVelo_Cover.jpg') }}" width="100%" height="auto">

            <div style="width:100%; position: absolute; bottom:0px; padding-bottom: 20px">
                <a href="{{ route('blog-shop') }}">
                    <span style="background-color:#d60b52; color: white; margin-bottom:20px; font-weight:800; display: table; margin: 0 auto; padding: 0px 20px 0px 20px; border-radius:20px">
                        COMMENT CHOISIR MON VELO ?
                    </span>
                </a>
            </div>
        </div>
    </div>
</div>

<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1264.7922885746027!2d5.590069958227898!3d50.65340589487594!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c0fa03fbcae78b%3A0xdb098129a125e630!2sKAMEO%20Bikes!5e0!3m2!1sfr!2sbe!4v1584177978200!5m2!1sfr!2sbe" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
@stop
