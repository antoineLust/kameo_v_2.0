
@extends('shop.layouts.default')
<style>
    .gallery{
        margin: auto;
        padding : 0 20px;
        display: grid;
        grid-template-columns: repeat(auto-fill, 50%);
    }
    .item{
        text-align: center;
        margin: 10px;
    }

    .btn{
        background-color: #d60b52;
        color: white;
        padding: 0 24px;
        border-radius: 24px;
    }

    @media screen and (min-width: 1200px) {
        .gallery{
            margin: auto;
            padding : 0 20px;
            display: grid;
            grid-template-columns: repeat(auto-fill, 33%);
        }
        .item{
            min-height: 200px;
            text-align: center;
            margin: 10px;
        }

        .btn{
            background-color: #d60b52;
            color: white;
            padding: 0 24px;
            border-radius: 24px;
        }
    }
</style>

@section('content')
    <div style="width: 70%; margin: auto">
        <div class="returnBack" onclick="history.back()">Retour</div>

        <h1 style="color:#d60b52; display: inline-block; margin-left:20px">Catalogue</h1>

        <div class="gallery">
            <a href="{{ route('catalog', ['utilisation'=>"Ville et chemin"]) }}" class="item">
                test
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="/images_bikes/Cover_Catalogue/Cover_Catalogue_toutchemin.jpg" alt="Snow" style="width:100%;">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Tout chemin</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"Ville"]) }}" class="item" style="position:relative">
                <div style="position : absolute; bottom:0 ; text-align : center ; color : white">
                    <img src="/images_bikes/Cover_Catalogue/Cover_Catalogue_ville.jpg" alt="Snow" style="width:100%;">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Ville</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"Ville et chemin"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="/images_bikes/Cover_Catalogue/Cover_Catalogue_villeetchemin.jpg" alt="Snow" style="width:100%;">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Ville et chemin</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"VTT"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="/images_bikes/Cover_Catalogue/Cover_Catalogue_vtt.jpg" alt="Snow" style="width:100%;">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">VTT</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"Speed Pedelec"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="/images_bikes/Cover_Catalogue/Cover_Catalogue_speedpedelec.jpg" alt="Snow" style="width:100%;">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Speed Pedelec</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"Gravel"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="/images_bikes/Cover_Catalogue/Cover_Catalogue_gravel.jpg" alt="Snow" style="width:100%;">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Gravel</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="{{ route('catalog', ['utilisation'=>"Cargo"]) }}" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="/images_bikes/Cover_Catalogue/Cover_Catalogue_cargo.jpg" alt="Snow" style="width:100%;">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Cargo & Long Tails</h3></div>
                    <div class="space"></div>
                </div>
            </a>
            <a href="catalog/all" class="item">
                <div style="position : relative ; text-align : center ; color : white">
                    <img src="/images_bikes/Cover_Catalogue/Cover_Catalogue_bonsplans.jpg" alt="Snow" style="width:100%;">
                    <div class="centered" style="position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);"><h3 class="text-light">Bons Plans</h3></div>
                    <div class="space"></div>
                </div>
            </a>
        </div>
    </div>
@stop
