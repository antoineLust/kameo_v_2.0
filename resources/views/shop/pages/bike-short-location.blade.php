@extends('shop.layouts.default')
@section('content')
<!-- multistep form -->
<div class="full-width">
    <div class="returnBack" onclick="history.back()">Retour</div>
    <h1 style="color:#d60b52; display: inline-block; margin-left:20px">Location de vélo</h1>
    <form id="msform">

        <!-- progressbar -->
        <ul id="progressbar" class="hidden-sm">
            <li class="active">Nombre de personne</li>
            <li>Type de balade</li>
            <li>Taille et type de cadre</li>
            <li>Accessoires</li>
            <li>Commentaire</li>
            <li>Coordonnées</li>
            <li>Durée de location</li>
            <li>Fin</li>
        </ul>
        <!-- fieldsets -->
        <fieldset class="active-step step-1">
            <h2 class="fs-title">Demande de location</h2>
            <h3 class="fs-subtitle">Combien de vélo souhaitez-vous ?</h3>
            <input class="data-input" type="number" name="nb_bike" min="1" max="100" value="0" />
            <br>
            <input type="button" name="next" class="next action-button" value="Suivant" />
            <br>
            <p style="text-align:left; font-size:0.8em">
                Nous vous proposons des vélos de location de haute qualité, de nos marques Conway, Victoria ou BZEN. 
                Nous ne garantissons pas la disponibilité des vélos en fonction de la saison.
            </p>
        </fieldset>
        <fieldset class="step-2">
            <h2 class="fs-title">Demande de location</h2>
            <h3 class="fs-subtitle">Quel est le type de la balade ?</h3>
            <select class="data-input" name="type_ride">
                <option value="" disabled selected>Choisir une option</option>
                <option value="Ville">Ville</option>
                <option value="Ravel">Ravel</option>
                <option value="VTT">VTT</option>
            </select>
            <br>
            <input type="button" name="previous" class="previous action-button" value="Précédent" />
            <input type="button" name="next" class="next action-button" value="Suivant" />
        </fieldset>
        <fieldset class="step-3">
            <h2 class="fs-title">Demande de location</h2>
            <h3 class="fs-subtitle">Quel taille et type de cadre souhaitez-vous pour les vélos ?</h3>
            <div class="append-zone" id="append-zone">
    
            </div>
            <br>
            <input type="button" name="previous" class="previous action-button" value="Précédent" />
            <input type="button" name="next" class="next action-button" value="Suivant" />
            <p style="text-align:left; font-size:0.8em">
                *Taille des vélos : 
                <ul style="text-align:left; font-size:0.8em">
                    <li>S : en dessous de 1m60</li>
                    <li>M : entre 1m60 et 1m75</li>
                    <li>L : entre 1m75 et 1m85</li>
                    <li>XL ; au dessus de 1m85</li>
                </ul>
            </p>

        </fieldset>
        <fieldset class="step-4">
            <h2 class="fs-title">Demande de location</h2>
            <h3 class="fs-subtitle">Souhaitez-vous des accessoires ?</h3>
            <label>Sacoche</label>
            <input class="data-input" type="number" name="bag" min="0" max="100" value="0" />
            <br>
            <label>Casque</label>
            <input class="data-input" type="number" name="helmet" min="0" max="100" value="0" />
            <br>
            <input type="button" name="previous" class="previous action-button" value="Précédent" />
            <input type="button" name="next" class="next action-button" value="Suivant" />
        </fieldset>
        <fieldset class="step-5">
            <h2 class="fs-title">Demande de location</h2>
            <h3 class="fs-subtitle">Avez-vous des spécificités a nous mentionner ?</h3>
            <textarea class="data-input" name="comment" placeholder="Commentaire"></textarea>
            <br>
            <input type="button" name="previous" class="previous action-button" value="Précédent" />
            <input type="button" name="next" class="next action-button" value="Suivant" />
        </fieldset>
        <fieldset class="step-6">
            <h2 class="fs-title">Demande de location</h2>
            <h3 class="fs-subtitle">Laissez-nous vos coordonnées afin que nous puissions vous recontacter</h3>
            <label>Nom</label>
            <br>
            <input class="data-input" type="text" name="lastname" placeholder="Nom" />
            <br>
            <label>Prénom</label>
            <br>
            <input class="data-input" type="text" name="firstname" placeholder="Prénom" />
            <br>
            <label>Email</label>
            <br>
            <input class="data-input" type="email" name="email" placeholder="Email" />
            <br>
            <label>Téléphone</label>
            <br>
            <input class="data-input" type="text" name="phone" placeholder="Téléphone" />
            <br>
            <input type="button" name="previous" class="previous action-button" value="Précédent" />
            <input type="button" name="next" class="next action-button" value="Suivant" />
        </fieldset>
        <fieldset class="step-7">
            <h2 class="fs-title">Demande de location</h2>
            <h3 class="fs-subtitle">Quelle durée de location souhaitez-vous ?</h3>
            <label>Début</label>
            <br>
            <input style="display: inline; width: 200px;" class="data-input date-picker" type="date" name="start_date" />
            <input style="display: inline; width: 200px;" class="data-input" type="time" min="07:30" max="18:00" step="1500" name="start_time" />
            <br>
            <label>Fin</label>
            <br>
            <input style="display: inline; width: 200px;" class="data-input date-picker" type="date" name="end_date" />
            <input style="display: inline; width: 200px;" class="data-input" type="time" step="1500" name="end_time" />
            <br>
            <input type="button" name="previous" class="previous action-button" value="Précédent" />
            <input type="button" name="next" class="next action-button" value="Envoyer" />
        </fieldset>
        <fieldset class="step-8">
            <h2 class="fs-title">Demande de location envoyée</h2>
            <h3 class="fs-subtitle">Nous vous recontactons dans les plus brefs délais.</h3>
        </fieldset>
      </form>
    <h2>Prix</h2>
    <table style="width:100%;">
        <thead>
            <tr>
                <th>Durée</th>
                <th>Prix vélo mécanique* (TVAC)</th>
                <th>Prix vélo électrique* (TVAC)</th>
    
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="font-weight:500">4 heures</td>
                <td>15 €</td>
                <td>30 €</td>
            </tr>    
            <tr>
                <td style="font-weight:500">1 jour</td>
                <td>25 €</td>
                <td>50 €</td>
            </tr>
            <tr>
                <td style="font-weight:500">2 jours</td>
                <td>40 €</td>
                <td>80 €</td>
            </tr>
            <tr>
                <td style="font-weight:500">3 jours</td>
                <td>50 €</td>
                <td>100 €</td>
            </tr>
            <tr>
                <td style="font-weight:500">4 jours</td>
                <td>57.5 €</td>
                <td>115 €</td>
            </tr>
            <tr>
                <td style="font-weight:500">5 jours</td>
                <td>62.5 €</td>
                <td>125 €</td>
            </tr>    
        </tbody>
    </table>
    <p style="font-size:0.8em">
        *Une réduction de 10% sera appliquée pour chaque vélo supplémentaire dans une limite de 50% de réduction.<br>Exemple : -10 % sur le deuxième vélo, -20% sur le deuxième vélo, etc...
    </p>
    <style>
        table{
            width: 100%;
            border-radius: 10px;
            border-collapse: collapse;
            border: 2px solid grey;
        }
        th{
            border-bottom: 1px solid #364043;
            font-weight: 600;
            padding: 0.5em 1em;
            text-align: left;
        }
        td {
            color: black;
            font-weight: 400;
            padding: 0.65em 1em;
        }
        tbody tr {
            transition: background 0.2s ease;
        }
        tbody tr:hover {
            background: #d60b52;
        }
    </style>    
</div>

@stop

<script>
    document.addEventListener("DOMContentLoaded", function() { 

        let locationData = {
            'nb_bike':      '',
            'type_ride':    '',
            'bag':          '',
            'helmet':       '',
            'comment':      '',
            'lastname':     '',
            'firstname':    '',
            'email':        '',
            'phone':        '',
            'start_date':   '',
            'start_time':   '',
            'end_date':     '',
            'end_time':     '',
            'bike_info':    []
        };
    
        let current_fs, next_fs, previous_fs; 
        const fs        = document.querySelectorAll(".fs");
        const next      = document.querySelectorAll('.next');
        const previous  = document.querySelectorAll('.previous');
        const submit    = document.querySelector('.submit');

        function nextProgress(current_progress, next_progress) {
            current_progress.classList.remove('active');
            next_progress.classList.add('active');
        }

        function nextStep(current_step, next_step) {
            next_step.classList.add('active-step');
            next_step.style.display = 'block';
            current_step.classList.remove('active-step');
            current_step.style.display = 'none';
        }

        const pickers = document.querySelectorAll('.date-picker');
        pickers.forEach(picker => {
            picker.addEventListener('input', function(e){
                const day = new Date(this.value).getUTCDay();
                if([0].includes(day)){
                    e.preventDefault();
                    this.value = '';
                    alert('Weekends not allowed');
                }
            });
        });

        next.forEach(function(element) {
            element.addEventListener('click', function() {

                current_fs      = document.getElementsByClassName('active')[0];
                next_fs         = current_fs.nextElementSibling;
                current_step    = document.querySelectorAll('.active-step')[0];
                next_step       = current_step.nextElementSibling;

                const inputs    = current_step.querySelectorAll('.data-input');
                const classList = current_step.classList.value;

                if(classList.includes('step-1')) {
                    for(let i = 0; i < inputs.length; i++) {
                        if(inputs[i].value !== "" && inputs[i].value !== null && inputs[i].value !== '0' && inputs[i].value !== 0) {
                            locationData[inputs[i].name] = inputs[i].value;
                        }
                        if(i === inputs.length - 1 && locationData[inputs[i].name] !== '') {
                            nextProgress(current_fs, next_fs);
                            nextStep(current_step, next_step);
                        }
                    }
                }
                else if(classList.includes('step-2')) {
                    for(let i = 0; i < inputs.length; i++) {
                        if(inputs[i].value !== "" && inputs[i].value !== null && inputs[i].value !== '0' && inputs[i].value !== 0) {
                            locationData[inputs[i].name] = inputs[i].value;
                        }
                        if(i === inputs.length - 1 && locationData[inputs[i].name] !== '') {
                            nextProgress(current_fs, next_fs);
                            nextStep(current_step, next_step);
                            // Delete all bike info
                            document.querySelectorAll('.bike_info').forEach(function(element) {
                                element.remove();
                            });
                            // Create bike size and type fields for each bike and append them to the next step
                            for(let i = 0; i < pars eInt(locationData['nb_bike']); i++) {
                                const bike_info = document.createElement('div');
                                bike_info.classList.add('bike_info');
                                bike_info.innerHTML = `
                                    <h3 class="fs-subtitle">Vélo ${i+1}</h3>
                                    <label for="bike_size">Taille*</label>
                                    <br>
                                    <select class="data-input" name="bike_size_${i}">
                                        <option value="" disabled selected>Choisir une option</option>
                                        <option value="S">S</option>
                                        <option value="M">M</option>
                                        <option value="L">L</option>
                                        <option value="XL">XL</option>
                                    </select>
                                    <br>
                                    <label for="bike_type">Type de cadre</label>
                                    <br>
                                    <select class="data-input" name="bike_type_${i}">
                                        <option value="" disabled selected>Choisir une option</option>
                                        <option value="Homme">Homme</option>
                                        <option value="Femme">Femme</option>
                                        <option value="Mixte">Mixte</option>
                                    </select>
                                `;
                                // Append bike info before <br> to the next step in vanilla js
                                const append_zone = next_step.querySelector('#append-zone');
                                append_zone.appendChild(bike_info);
                            }
                        }
                    }
                }
                if(classList.includes('step-3')) {
                    locationData['bike_info'] = [];
                    for(let i = 0; i < inputs.length; i++) {
                        if(inputs[i].value !== '') {
                            locationData.bike_info.push(inputs[i].value);
                            // Go to the next step if all inputs in the current step are filled
                            const valid_inputs = Array.from(inputs).filter(element => element.value !== '');
                            if(valid_inputs.length === inputs.length) {
                                nextProgress(current_fs, next_fs);
                                nextStep(current_step, next_step);
                            }
                        }
                    }
                }
                if(classList.includes('step-4')) {
                    for(let i = 0; i < inputs.length; i++) {
                        if(inputs[i].value !== null) {
                            locationData[inputs[i].name] = (inputs[i].value === '') ? '0' : inputs[i].value;
                            if(i === inputs.length - 1) {
                                nextProgress(current_fs, next_fs);
                                nextStep(current_step, next_step);
                            }
                        }
                    }
                }
                if(classList.includes('step-5')) {
                    for(let i = 0; i < inputs.length; i++) {
                        if(inputs[i].value !== null) {
                            locationData[inputs[i].name] = inputs[i].value;
                            if(i === inputs.length - 1) {
                                nextProgress(current_fs, next_fs);
                                nextStep(current_step, next_step);
                            }
                        }
                    }
                }
                if(classList.includes('step-6')) {
                    for(let i = 0; i < inputs.length; i++) {
                        if(inputs[i].value !== null && inputs[i].value !== '') { 
                            locationData[inputs[i].name] = inputs[i].value;
                            // Go to the next step if all inputs in the current step are filled
                            const valid_inputs = Array.from(inputs).filter(element => element.value !== '');
                            if(valid_inputs.length === inputs.length) {
                                nextProgress(current_fs, next_fs);
                                nextStep(current_step, next_step);
                            }
                        }
                    }
                }
                if(classList.includes('step-7')) {
                    for(let i = 0; i < inputs.length; i++) {
                        if(inputs[i].value !== null && inputs[i].value !== '') { 
                            locationData[inputs[i].name] = inputs[i].value;
                            // Go to the next step if all inputs in the current step are filled
                            const valid_inputs = Array.from(inputs).filter(element => element.value !== '');
                            if(valid_inputs.length == inputs.length && i == inputs.length - 1) {
                                nextProgress(current_fs, next_fs);
                                nextStep(current_step, next_step);
                                // Send data to the server
                                const hostname  = window.location.hostname;
                                const xhr       = new XMLHttpRequest();
                                xhr.open('POST', `http://${hostname}/api/location-request/create`, true);
                                xhr.setRequestHeader('Content-Type', 'application/json');
                                xhr.send(JSON.stringify(locationData));
                            }
                        }
                    }
                }

            });
        });

        previous.forEach(function(element) {
            element.addEventListener('click', function() {
                
                // Update progress bar
                current_fs  = document.getElementsByClassName('active')[0];
                previous_fs = current_fs.previousElementSibling;
                if(previous_fs) {
                    previous_fs.classList.add('active');
                    current_fs.classList.remove('active');
                }
                
                // Go to previous step
                current_step = document.querySelectorAll('.active-step')[0];
                previous_step = current_step.previousElementSibling;
                if(previous_step) {
                    previous_step.classList.add('active-step');
                    previous_step.style.display = 'block';
                    current_step.classList.remove('active-step');
                    current_step.style.display = 'none';
                }

            });
        });

    });
    
</script>

<style scoped>
    /*form styles*/
    #msform {
        margin: 50px auto;
        text-align: center;
        position: relative;
    }
    #msform fieldset {
        background: white;
        border: 0 none;
        border-radius: 3px;
        box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
        padding: 20px 30px;
        box-sizing: border-box;
        width: 100%;
        
        /*stacking fieldsets above each other*/
        position: relative;
    }
    /*Hide all except first fieldset*/
    #msform fieldset:not(:first-of-type) {
        display: none;
    }
    /*inputs*/
    #msform input, #msform textarea, #msform select {
        padding: 15px;
        border: 1px solid #ccc;
        border-radius: 3px;
        margin: 1em auto 1em auto;
        width: 50%;
        box-sizing: border-box;
        font-family: montserrat;
        color: #2C3E50;
        font-size: 13px;
    }
    /*buttons*/
    #msform .action-button {
        width: 100px;
        background: #d60b52;
        font-weight: bold;
        color: white;
        border: 0 none;
        border-radius: 1px;
        cursor: pointer;
        padding: 10px 5px;
        margin: 10px 5px;
    }
    #msform .action-button:hover, #msform .action-button:focus {
        box-shadow: 0 0 0 2px #d60b5210, 0 0 0 3px #d60b5210;
    }
    /*headings*/
    .fs-title {
        text-transform: uppercase;
        color: #2C3E50;
        margin-bottom: 10px;
    }
    .fs-subtitle {
        font-weight: normal;
        color: #d60b52;
        margin-bottom: 20px;
    }
    /*progressbar*/
    #progressbar {
        margin-bottom: 30px;
        overflow: hidden;
        /*CSS counters to number the steps*/
        counter-reset: step;
    }

    #progressbar li {
        font-size:0.8em;
        list-style-type: none;
        color: #333;
        text-transform: uppercase;
        width: 12.5%;
        float: left;
        position: relative;
    }

    #progressbar li:before {
        content: counter(step);
        counter-increment: step;
        width: 20px;
        line-height: 20px;
        display: block;
        font-size: 10px;
        color: #333;
        background: white;
        border-radius: 3px;
        border: 1px solid #333;
        margin: 0 auto 5px auto;
    }
    /*progressbar connectors*/
    #progressbar li:after {
        content: '';
        width: 100%;
        height: 2px;
        background: #d60b52;
        position: absolute;
        left: -50%;
        top: 9px;
        z-index: -1; /*put it behind the numbers*/
    }
    #progressbar li:first-child:after {
        /*connector not needed before the first step*/
        content: none; 
    }
    /*marking active/completed steps green*/
    /*The number of the step and the connector before it = green*/
    #progressbar li.active:before,  #progressbar li.active:after{
        background: #d60b52;
        color: white;
    }
    /* Error inout class */
    .error {
        border-color: red;
        background-color: rgba(255, 0, 0, 0.096);
    }
</style>