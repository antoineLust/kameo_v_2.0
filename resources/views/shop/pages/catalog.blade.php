@extends('shop.layouts.default')
<style>
    .gallery{
        margin: auto;
        padding : 0 20px;
        display: grid;
        grid-template-columns: repeat(1, 1fr);
        column-gap:20px;
        row-gap:20px

    }
    .item{
        border: solid 1px grey;
        text-align: center;
    }
    .item-box{
        padding: 20px;
        border-radius: 5px;
        margin: 15px;
    }
    .item-content{
        margin-top: 10px;
        padding-bottom: 10px; 
    }

    .btn{
        background-color: #d60b52;
        color: white;
        padding: 0 24px;
        border-radius: 24px;
    }
    @media screen and (min-width: 760px) {
        .gallery{
            grid-template-columns: repeat(2, 1fr);
        }
    }
    
    @media screen and (min-width: 1024px) {
        .gallery{
            grid-template-columns: repeat(3, 1fr);
        }
    }

    @media screen and (min-width: 1400px) {
        .gallery{
            grid-template-columns: repeat(4, 1fr);
        }
    }
</style>

@section('content')

    <div style="width: 70%; margin: auto">
        <div class="returnBack" onclick="history.back()">Retour</div>

        <h1 style="color:#d60b52; display: inline-block; margin-left:20px">Catalogue</h1>

        <div class="gallery">
            @foreach ($bikes as $bike)
            <div class="item">
                <a href="{{ route('catalog-bike', ['bike'=>$bike->id]) }}">
                    <img src="/images_bikes/velo_mini.jpg" alt="image vélo {{ $bike->brand }} {{ $bike->model }}" style="margin: auto; margin-bottom: 20px; width:100%"><br>
                </a>
                <span style="background-color: #d60b52; padding: 8px; color: white">
                    IN STOCK
                </span>
                <div class="item-box">
                    <a href="{{ route('catalog-bike', ['bike'=>$bike->id]) }}">
                        <div class="item-content" style="margin-top: 0; margin-bottom: 20px">
                            <strong>{{ $bike->brand }} {{ $bike->model }}</strong><br>
                            <strong style="font-size:1.5em">{{ $bike->price_htva*1,21 }} €</strong>    
                        </div>
                    </a>
                    <a href="{{ route('catalog-bike', ['bike'=>$bike->id]) }}">
                        EN SAVOIR PLUS
                    </a>    
                </div>
            </div>
            @endforeach
        </div>

    </div>
@stop
