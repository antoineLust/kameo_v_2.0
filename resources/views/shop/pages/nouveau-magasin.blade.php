@extends('shop.layouts.default')
@section('content')

    <div class="full-width-blog">
        <div class="returnBack" onclick="history.back()">Retour</div>
        <h1 style="color:#d60b52">Nouveau magasin à Liège !</h1>
        <p>Nous avons ouvert notre nouveau magasin le 18 Juin, situé au <strong>22 Quai de Rome, 4000 Liège.</strong><br><br>
        Vous pouvez y trouver divers éléments:
        <ul>
            <li>Un show-room avec nos vélos préférés</li>
            <li>Un atelier pour entretenir les vélos de toutes marques</li>
            <li>Un coin accessoires pour vous équiper de manière optimale</li>
            <li>Un accueil pour les entreprises et présentation de nos solutions B2B</li>
        </ul>
        <h2>Pourquoi avons-nous ouvert ce magasin ?</h2>
        <p>
            <strong>KAMEO Bikes</strong> a été créé en 2017 afin de proposer des solutions de mobilité aux entreprises.<br>
            Nous avons souvent testé des nouvelles offres afin de toujours améliorer la mobilité dans notre beau pays et nous avons commencé à vendre des vélos aux particuliers en août 2020.<br>
            En effet, en pleine période COVID, vous étiez nombreux à nous demander si nous avions des vélos de disponibles. La demande a continué de grandir et nous avons vendu depuis lors plus de 200 vélos.<br>
            Cette offre est devenue de plus en plus importante pour notre société et nous avons décidé de nous y consacrer officiellement.<br>
        </p>
        <h2>Quelles sont les différences entre ce magasin et ceux déjà existants ?</h2>
        <h3>Service à la clientèle</h3>
        <p>
            Nous avons ouvert ce magasin avec le même état d'esprit que nous avons depuis la création de KAMEO Bikes : le service à la clientèle.<br>
            Nous avons prévu plusieurs aménagements à l'intérieur du magasin afin que vous vous sentiez à l'aise et puissiez prendre le temps de discuter avec nous de votre passion pour le vélo.
        </p>
        <h3>Solutions innovantes</h3>
        <p>
            Nous sommes en train de préparer diverses offres afin de pouvoir vous proposer des solutions innovantes:<br>
            <ul>
                <li>Tests des vélos : nous vous proposerons un test de nos vélos d'environ 30 min afin de pouvoir tester notre gamme. Vous serez alors convaincu par votre futur achat !</li>
                <li>Reprise de votre vélo d'occassion</li>
                <li>Possibilité de locaton d'un ou plusieurs vélos électriques pendant le week-end, avec itiniaire GPS intégré pour votre parcours</li>
            </ul>
        </p>
        <h2>Photos</h2>
    </p>
    </div>
    
@stop
