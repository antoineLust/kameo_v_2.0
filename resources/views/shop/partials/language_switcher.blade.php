<div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
    <ul>
        <select onchange="location=this.value">
            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <option rel="alternate" hreflang="{{ $localeCode }}" value="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                    {{ $properties['native'] }}
                </option>
            @endforeach
        </select>

    </ul>
</div>