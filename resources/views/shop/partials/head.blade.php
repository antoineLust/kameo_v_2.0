<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<meta name="robots" content="noindex,nofollow">
<meta name="description" content="Votre spécialiste vélos et vélos électriques en Belgique. Venez découvrir notre très large choix de vélos urbain, VTT, cargo, gravel, tout chemin mais aussi de nombreux accessoires toutes marques dans notre magasin.">
<meta name="keywords" content="KAMEO, Cameo, Cameo Bikes, Kameo Bikes, magasin velo, vélo électrique, velo electrique, Liège">
<meta name="author" content="Antoine Lust">    



<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/keen-slider@6.6.14/keen-slider.min.css"/>

<title>Kameo Bikes - Magasin de Vélos et Vélos électriques à Liège</title>

