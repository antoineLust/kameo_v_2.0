<style>
    .grid-template-header{
        display: grid; 
        grid-template-columns: 1fr;
    }
    .grid-1{
        grid-column: span 1;
    }
    @media screen and (min-width: 768px){
        .grid-template-header{
            display: grid; 
            grid-template-columns: 200px auto 200px;
        }
    }
</style>
<div class="grid-template-header" style="margin-bottom:20px">
    <a href="{{ route('index') }}">
        <div style="text-align:center">
            <img src="{{ asset('images/KAMEO_Logo.svg') }}" height="50px" id="KAMEOLogoHeader">
        </div>    
    </a>
    <div style="display:flex; margin:auto"> 
        <a href="{{ route('shop') }}">
            <div class="py-4 vignette" style='margin-right: 10px; background-color: #d60b52'>
                SHOP
            </div>
        </a>
        <a href="{{ route('pro') }}">
            <div id="proVignette" class="py-4 vignette">
            PRO
            </div>
        </a>
        <a href="{{ route('repair') }}">
            <div id="repairVignette" class="py-4 vignette" style='margin-left: 10px'>
                REPAIR
            </div>
        </a>
    </div>
    <div class="hidden top-0 left-0 px-6 py-4 sm:block">
        @include('shop/partials/language_switcher')
    </div>
</div>
<style>
    .vignette{
        display: block;
        width: 100px;
        margin: auto;
        font-weight: bold;
        font-size:0.8em;
        border-bottom-left-radius: 10px;
        border-bottom-right-radius: 10px;
        text-align: center;
        transition: 0.3s;
        color: white;
    }
    @media (min-width: 640px){
        .vignette{
            font-size:1.2em;
        }
    }

    #proVignette{
        color: #2cb396;
        border: 1px solid #2cb396;
    }
    #proVignette:hover{
        background-color: #2cb396;
        color: white;
    }
    #repairVignette{
        color: #1d71b8;
        border: 1px solid #1d71b8;
    }

    #repairVignette:hover{
        background-color: #1d71b8;
        color: white;
    }
</style>
