<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
    <head>
        <meta http–equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http–equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </head>
    <table style="text-align: center; width: 100%;">
        <tr>
            @include('emails.partials.header')
        </tr>
        <tr>
            <td class="esd-stripe" align="center">
                <table class="es-content-body" style="background-color: transparent;" width="600" cellspacing="0" cellpadding="0" align="center">
                    <tbody>
                        <tr>
                            <td class="esd-structure" align="left">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td class="esd-container-frame" width="600" valign="top" align="center">
                                                <table style="border-radius: 4px; border-collapse: separate; background-color: #ffffff;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                                    <tbody>
                                                        <tr>
                                                            <td class="esd-block-text es-p20t es-p30r es-p30l es-m-txt-l" align="left" bgcolor="#ffffff">
                                                                <h3 style="color: #3cb295; padding-top: 1em;">Votre réservation a bien été validée !!</h3>
                                                                <p>Votre code est : <span style="color: #3cb295; font-weight: bold;">{{ $code }}</span></p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr style="padding-top: 1em;">
            @include('emails.partials.footer')
        </tr>
    </table>
</html>