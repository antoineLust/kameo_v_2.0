<td>
    <table style="background-color: #666666; color: #ffffff; border-collapse: separate;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#efefef">
        <tbody>
            <tr>
                <td align="center" class="esd-block-social" style="font-size:0;">
                    <table cellpadding="0" cellspacing="0" class="es-table-not-adapt es-social">
                        <tbody>
                            <tr>
                                <td align="center" valign="top" class="es-p10r" esd-tmp-icon-type="facebook" style="padding-bottom: 1.5em; padding-top: 1em; padding-left: .5em; padding-right: .5em;">
                                    <a target="_blank" href="https://www.facebook.com/kameobikes/"> <img src="https://stripo.email/static/assets/img/social-icons/logo-colored/facebook-logo-colored.png" alt="Fb" title="Facebook" width="32"> </a>
                                </td>
                                <td align="center" valign="top" class="es-p10r" esd-tmp-icon-type="instagram" style="padding-bottom: 1.5em; padding-top: 1em; padding-left: .5em; padding-right: .5em;">
                                    <a target="_blank" href="https://www.instagram.com/kameobikes/"> <img src="https://stripo.email/static/assets/img/social-icons/logo-colored/instagram-logo-colored.png" alt="Ig" title="Instagram" width="32"> </a>
                                </td>
                                <td align="center" valign="top" class="es-p10r" esd-tmp-icon-type="linkedin" style="padding-bottom: 1.5em; padding-top: 1em; padding-left: .5em; padding-right: .5em;">
                                    <a target="_blank" href="https://www.linkedin.com/company/kameobikes/"> <img src="https://stripo.email/static/assets/img/social-icons/logo-colored/linkedin-logo-colored.png" alt="In" title="Linkedin" width="32"> </a>
                                </td>
                                <td align="center" valign="top" esd-tmp-icon-type="world" style="padding-bottom: 1.5em; padding-top: 1em; padding-left: .5em; padding-right: .5em;">
                                    <a target="_blank" href="https://www.kameobikes.com"> <img src="https://stripo.email/static/assets/img/other-icons/logo-colored/globe-logo-colored.png" alt="World" title="World" width="32"> </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" class="esd-block-spacer es-p20" style="font-size:0">
                    <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td style="border-bottom: 2px solid #3cb295; background: unset; height: 1px; width: 100%; margin: 0px;"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="left" class="esd-block-text"  style="padding-bottom: 2em; padding-top: 2em;">
                    <p style="text-align: center;"><em>Copyright © {{ now()->year }} KAMEO Bikes, Tous droits réservés.</em><br><br><strong>info@kameobikes.com</strong><br><br>Vous ne voulez plus recevoir nos mails?<br>Merci de simplement répondre 'STOP' à ce mail.</p>
                </td>
            </tr>
        </tbody>
    </table>
</td>