@php
    if($company === "Afelio"){
        $logo = "https://mcusercontent.com/c4664c7c8ed5e2d53dc63720c/images/5d4b6710-fda7-a725-2609-81c6bdd67d85.png";
    }
    else if($company === "CET"){
        $logo = "https://mcusercontent.com/c4664c7c8ed5e2d53dc63720c/images/3228fda5-37f1-bc01-6dfd-5b5179efb20c.png";
    }
    else if($company === "Gaming 1"){
        $logo = "https://mcusercontent.com/c4664c7c8ed5e2d53dc63720c/images/60e8dfd3-7c04-a2f3-692b-134d47d94140.png";
    }
    else if($company === "province"){
        $logo = "https://mcusercontent.com/c4664c7c8ed5e2d53dc63720c/images/f900ad6f-86a5-617a-45ed-2ffd48cc1aa1.png";
    }
    else if($company === "Sogepa"){
        $logo = "https://www.kameobikes.com/apis/Kameo/mails/images/sogepa.png";
    }
    else{
        $logo = "https://gallery.mailchimp.com/c4664c7c8ed5e2d53dc63720c/images/8b95e5d1-2ce7-4244-a9b0-c5c046bf7e66.png";
    }
@endphp
<td class="esd-stripe" style="background-color: #3cb295;" esd-custom-block-id="6340" bgcolor="#3cb295" align="center">
    <table class="es-content-body" style="background-color: transparent;" style="width: 100%;" cellspacing="0" cellpadding="0" align="center">
        <tbody>
            <tr>
                <td class="esd-structure" align="left">
                    <table width="100%" cellspacing="0" cellpadding="0">
                        <tbody>
                            <tr>
                                <td class="esd-container-frame" width="600" valign="top" align="center">
                                    <table style="background-color: #3cb295; border-radius: 4px; border-collapse: separate; height: 175px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                        <tbody>
                                            <tr>
                                                <td align="center" class="esd-block-image" style="font-size: 0px;">
                                                    <img align="center" alt="" src="{{ $logo }}" width="600" style="max-width:600px; max-height: 200px !important; padding-bottom: 0; display: inline !important; vertical-align: bottom;">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</td>