<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
    <head>
        <meta http–equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http–equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </head>
    <table style="text-align: center; width: 100%;">
        <tr>
            @include('emails.partials.header')
        </tr>
        <tr>
            <td class="esd-stripe" align="center">
                <table class="es-content-body" style="background-color: transparent;" width="600" cellspacing="0" cellpadding="0" align="center">
                    <tbody>
                        <tr>
                            <td class="esd-structure" align="left">
                                <table width="100%" cellspacing="0" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td class="esd-container-frame" width="600" valign="top" align="center">
                                                <table style="border-radius: 4px; border-collapse: separate; background-color: #ffffff;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                                    <tbody>
                                                        <tr>
                                                            <td class="esd-block-text es-p20t es-p30r es-p30l es-m-txt-l" align="left">
                                                                <h3 style="color: #3cb295; padding-top: 1em;"><strong>Congratulations {{ $firstname }} !&nbsp;</strong></h3>
                                                                <p style="color: #666666;">You receive this mail because you just have been added to MyKameo users.<br>You can connect now to the <a href="https://www.kameobikes.com/dashboard/" target="_blank" style="color: #3cb295;">MyKameo</a> plateform.</p>
                                                                <p style="color: #666666;">
                                                                    <strong><span style="color:#3cb295;">Login:</span></strong> {{ $email }}
                                                                </p>
                                                                <p style="color: #666666;">
                                                                    <strong><span style="color:#3cb295;">Password :</span></strong> {{ $password }}
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="esd-block-spacer es-p20" style="font-size:0">
                                                                <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="border-bottom: 2px solid #3cb295; background: unset; height: 1px; width: 100%; margin: 0px;"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="esd-block-text es-p20t es-p30r es-p30l es-m-txt-l" align="left" bgcolor="#ffffff">
                                                                <h3 style="color: #3cb295; padding-top: 1em;">Félicitations {{ $firstname }} !&nbsp;</h3>
                                                                <p>Vous recevez cet email car vous venez d'être ajouté aux utilisateurs des vélos KAMEO.<br>Vous pouvez dès à présent vous connecter à votre espace en ligne <a href="https://www.kameobikes.com/dashboard/" target="_blank" style="color: #3cb295;">MyKameo</a>.</p>
                                                                <p><br></p>
                                                                <p style="color: #666666;">
                                                                    <strong><span style="color:#3cb295;">Votre identifiant :</span></strong> {{ $email }}
                                                                </p>
                                                                <p style="color: #666666;">
                                                                    <strong><span style="color:#3cb295;">Votre mot de passe :</span></strong> {{ $password }}
                                                                </p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" class="esd-block-spacer es-p20" style="font-size:0">
                                                                <table border="0" width="100%" height="100%" cellpadding="0" cellspacing="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td style="border-bottom: 3px solid #3cb295; background: unset; height: 1px; width: 100%; margin: 0px;"></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="esd-block-text es-p20t es-p30r es-p30l es-m-txt-l" align="left" bgcolor="#ffffff">
                                                                <h3 style="color: #3cb295; padding-top: 1em;">Gefeliciteerd {{ $firstname }} !</h3>
                                                                <p>U ontvangt deze e-mail omdat u vanaf nu toegang hebt aan de KAMEO-fietsen.<br>U kunt zich aanmelden op uw account <a href="https://www.kameobikes.com/dashboard/" target="_blank" style="color: #3cb295;">MyKameo</a>.</p>
                                                                <p style="color: #666666;">
                                                                    <strong><span style="color:#3cb295;">Gebruikersnaam :</span></strong> {{ $email }}
                                                                </p>
                                                                <p style="color: #666666;">
                                                                    <strong><span style="color:#3cb295;">Wachtwoord :</span></strong> {{ $password }}
                                                                </p>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr style="padding-top: 1em;">
            @include('emails.partials.footer')
        </tr>
    </table>
</html>