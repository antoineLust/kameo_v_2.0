<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Offer</title>

    <style type="text/css">
        * {
            font-family: 'Akkurat';
            font-size: 15px;
        }

        p {
            font-family: 'Akkurat-Light';
            line-height: 22pt;
        }

        h1 {
            font-size: 20px;
            color: #2fa37c;
            font-weight: bold;
        }

        h3 {
            font-size: 17px;
            color: #2fa37c;
            font-weight: 400;
        }

        h2 {
            color: #cc304d;
            font-weight: 400;
            font-size: 18px;
        }

        .white {
            color: white;
        }

        .red {
            color: #cc304d;
        }

        .green {
            color: #2fa37c;
        }

        .firstPage {
            font-size: 15px;
        }

        .header {
            color: #808080;
        }

        .header span {
            margin-top: 10mm;
            margin-left: 15mm;
            font-size: 15px;
        }

        .logo {
            width: 400px;
            height: auto;
            margin-bottom: 15mm;
        }

        .title {
            font-weight: 400;
        }

        .mainTitle {
            font-size: 50px;
            margin-bottom: 15mm;
        }

        .secondaryTitle {
            color: white;
            font-size: 35px;
            margin-bottom: 0;
        }

        .separator {
            color: white;
            height: 0.1mm;
        }

        .arcamajora {
            font-family: 'ArcaMajora';
        }

        .inline,
        .inline * {
            display: inline;
        }

        .maxWidth {
            width: 100%;
        }

        .logo-sm {
            width: 180px;
            height: auto;
        }

        .logo-xsm {
            width: 130px;
            height: auto;
        }

        .bold {
            font-weight: bold;
        }

        .light {
            font-family: 'Akkurat-Light';
            font-weight: 400;
        }

        .normalFont {
            font-family: 'Akkurat';
        }

        .list .listItem {
            margin-bottom: 5mm;
        }

        .list .sublist {
            margin-bottom: 2mm;
        }

        .list .subListItem {
            margin-top: -2mm;
            margin-bottom: 5mm;
            padding-left: 10mm;
            font-family: 'Akkurat-Light';
        }

        .img-large {
            width: 160mm;
            height: auto;
            max-height: 250mm;
        }

        .bordered {
            border: 1px, solid, black;
        }

        .bordered-bottom {
            border-bottom: 1px, solid, black;
        }

        .bordered-top {
            border-top: 1px, solid, black;
        }

        .tableBorder {
            border-collapse: collapse;
        }

        .tableBorder th,
        .tableBorder td {
            border: 1px solid black;
        }

        .center {
            text-align: center;
        }

        .tbody-leftMargin td>* {
            margin-left: 3mm;
        }

        .count-border {
            border: 3px solid #2fa37c;
            border-radius: 50px;
            padding: 7mm;
            padding-top: 4mm;
        }

        .tableMargins td,
        .tableMargins th {
            padding-top: 3mm;
            padding-bottom: 3mm;
        }

        .lMargin {
            margin-left: 3mm;
        }
    </style>
</head>

<body>
    <p>Coucou</p>
    {{-- @php
        dd($allItems, $bikes, $accessories, $boxes, $company, $user, $offer, $employee, $totalHTVA, $totalTVAC);
    @endphp
    <table class="white firstPage" backcolor="#2fa37c" backtop="10mm" backleft="10mm" backright="10mm">

        <div style="text-align: center; margin-bottom:5mm;">
            <img src="{{ asset('images/logo.png') }}" alt="logo" class="logo" /><br />
            <h1 class="title mainTitle">OFFRE: <br /></h1>
            <h2 class="title secondaryTitle">SOLUTION DE MOBILITÉ DOUCE</h2>
        </div>
        <div style="padding-left: 80px; padding-right:80px; margin-bottom:0;">
            <hr class="separator" />
        </div>
        <table class="maxWidth" style="margin-bottom:10mm; margin-top: 10mm;">
            <tr>
                <td style="text-align:left; margin:0;padding:0; width:50%;">
                    @php
                        $numberOfSelling = 0;
                        $numberOfLeasing = 0;
                    @endphp
                    @foreach ($allItems as $item)
                        @if ($item['contract_type'] == 'selling')
                            @php
                                $numberOfSelling++;
                            @endphp
                        @else
                            @php
                                $numberOfLeasing++;
                            @endphp
                        @endif
                    @endforeach
                    @if ($numberOfSelling > 0)
                        <div style="font-size: 25px;">Achat de vélos</div>
                    @elseif($numberOfLeasing > 0)
                        <div style="font-size: 25px;">Location tout inclus VAE</div>
                    @elseif($numberOfSelling > 0 && $numberOfLeasing > 0)
                        <div style="font-size: 25px;">Achat de vélos</div>
                        <div style="font-size: 25px;">Location tout inclus VAE</div>
                    @endif

                </td>
            </tr>
        </table>
        <hr class="separator" />
        <div>
            <table class="maxWidth">
                <tr>
                    <td style="text-align:left; padding-left:0; margin-left:0; width:50%;">
                        <span class="arcamajora" style="color:#efefef; font-size:25px;">KAMEO Bikes
                            SRL</span><br /><br />
                        Quai de Rome, 22<br />
                        4000 Liège<br />
                    </td>
                    <td style="text-align:right; padding-right:0; margin-right:0;  width:50%;">
                        <span class="arcamajora"
                            style="color:#efefef; font-size:25px;">{{ $company['name'] }}</span><br /><br />
                        {{ $company['address']['number'] }}<br />{{ $company['address']['zip'] . ' ' . $company['address']['city'] }}<br />
                    </td>
                </tr>
            </table>
            <table class="maxWidth" style="margin-top:10mm;">
                <tr>
                    <td style="text-align:left; padding-left:0; margin-left:0;  width:50%;">
                        <div class="arcamajora" style="color:#efefef; font-size:25px;">
                            {{ $employee['firstname'] . ' ' . $employee['lastname'] }}</div><br />
                        info@kameobikes.com<br />
                        +32 4 290 71 21<br />
                        {{ $offer['created_at'] }}
                    </td>
                    <td style="text-align:right; padding-right:0; margin-right:0;  width:50%;">
                        <span class="arcamajora"
                            style="color:#efefef; font-size:25px;">{{ $user['firstname'] . ' ' . $user['lastname'] }}</span><br /><br />
                        {{ $user['email'] }}<br />
                        {{ isset($user['phone']) ? $user['phone'] : 'Pas de numéro de téléphone' }}
                    </td>
                </tr>
            </table>
        </div>
        <img style="margin-top:15px;" src="{{ asset('images/logo.png') }}" alt="kameo" class="logo-sm">
    </table>

    <div backtop="20mm" backleft="15mm" backright="10mm" backbottom="20mm">
        <div class="header">
            <span></span>
        </div>
        <div style="margin-bottom:10mm;">
            <table class="maxWidth">
                <tr>
                    <td><img alt="kameo" class="logo-xsm"></td>
                    <td style="font-size:13px;">Kameo bikes SRL<br />Rue de la Brasserie, 8<br />B-4000
                        Liège<br />Belgium</td>
                    <td style="width:33%; text-align:right; padding-right:0; margin-right:0;"><span>Page
                            [[page_cu]]</span></td>
                </tr>
            </table>
        </div>
        <div>
            <h1>Introduction</h1>
            <p>
                <span class="bold">KAMEO BIKES a pour mission de rendre plus agréable, économique et durable les
                    déplacements quotidiens des organisations et des individus.</span>
                Dans ce cadre, nous sommes convaincus que le vélo est le mode de transport urbain de demain mais,
                surtout d’aujourd’hui, et nous travaillons tous les jours pour le démontrer à nos clients.
            </p>
            <p>
                <span class="bold">NOS SOLUTIONS</span> s’appuient sur 3 PÔLES interdépendants que sont des CYCLES DE
                QUALITÉ,
                une MAINTENANCE CONTINUE et une GESTION CONNECTÉE des interactions. La maitrise de ces
                3 axes nous permet de garantir une expérience cyclable optimale, quelles que soient les
                circonstances.
            </p>
            <img alt="kameo-scheme" style="margin-left:25mm; width:400px; height:auto;">
            <p>

            </p>
            <p>
                Nous restons à votre disposition pour toute demande d’informations complémentaires.
            </p>
            <img alt="kameo-scheme" style="margin-left:45mm;">
        </div>
    </div>
    <div pageset="old" backtop="30mm" backleft="15mm" backright="10mm" backbottom="20mm">
        <h2>Objet</h2>

        <h2>Documents de référence et contacts</h2>
        <p>
            Cette offre est basée sur les échanges entre, M./Mme.de et M./Mme.
        </p>
        <p>
            La solution proposée est cependant définie entièrement et uniquement par ce document et ses annexes
            ainsi que les conditions générales de vente/location de KAMEO.
        </p>
        <h2>Scope de l’offre</h2>
        <p>
            L’offre inclus l’ensemble des éléments repris ci-dessous.
        </p>
        <div class="list">


        </div>
    </div>
    </div>
    </div>

    <div pageset="old" backtop="30mm" backleft="15mm" backright="10mm" backbottom="20mm">
        <h1>1. Location tout inclus</h1>
        <p>
            La location est un contrat comprenant à la fois la mise à disposition d’un produit et des services liés à ce
            produit. Pendant toute la durée de la période de location, le produit appartient à KAMEO Bikes. A l’échéance
            de la période de location, une possibilité d'achat du vélo peut être réçue par // de la part de Kameo Bikes
            afin d’acquérir définitivement le
            produit. Les services peuvent toujours être contractés en supplément.
        </p>
        <p>Dans le cadre de cette offre, la location englobe les éléments suivants :</p>
        <div class="list">
            <div class="listItem">• PRODUIT</div>
            <div class="subList">

            </div>
            <div class="listItem">• SERVICES</div>
            <div class="subList">
            </div>

        </div>
    </div>
    //vélos

    <table class="maxWidth tableBorder tableMargins" style="margin-top:10mm; margin-bottom:10mm;">
        <thead>
            <tr>
                <th style="width:25%"><span class="lMargin">Marque</span></th>
                <th style="width:25%"><span class="lMargin">Modèle</span></th>
                <th style="width:25%"><span class="lMargin">Utilisation</span></th>
                <th style="width:25%"><span class="lMargin">Électrique</span></th>
            </tr>
        </thead>
        <tbody>
            <tr>
            </tr>
        </tbody>
    </table>
    </div>



    <div pageset="old" backtop="30mm" backleft="15mm" backright="10mm" backbottom="20mm">
        <h2>Maintenance</h2>
        <p>
            La clé d’une expérience de mobilité réussie est d’avoir en permanence des vélos dans un état irréprochable.
            KAMEO Bikes part du principe que pour prendre du plaisir sur votre vélo, celui-ci doit rouler sans souci
            mécanique.<br />
            <span class="bold">
                Les entretiens seront organisés sur le site de l’entreprise entre 8h et 18h lors d’une date annoncée
                avec
                un délai de minimum 2 semaines.
            </span>
        </p>
        <h3 style="margin: 0;">Entretiens</h3>
        <p style="margin-bottom: 10px;">
            Un premier entretien est effectué après 3 mois. Ensuite, une révision annuelle est organisée en fonction de
            la durée de location. L’entretien annuel est conçu pour assurer une remise en état de votre vélo. Il
            correspond
            aux changements de pièces et réglages de l'ensemble des organes de votre vélo.
        </p>
        <table class="maxWidth bordered">
            <tbody>
                <tr>
                    <td class="bordered-bottom"
                        style="width:100%;font-size: 18px; font-weight: bold; padding-bottom: 3mm;">
                        POINTS VÉRIFIÉS LOS D'UN ENTRETIEN
                    </td>
                </tr>
                <tr>
                    <td style="width:100%; margin-top: 3mm;" class="list">
                        <div class="subListItem" style="margin-top: 3mm;">• Nettoyage du vélo</div>
                        <div class="subListItem">• Etat général du vélo</div>
                        <div class="subListItem">• Pression et état des pneus</div>
                        <div class="subListItem">• Fonctionnement des freins</div>
                        <div class="subListItem">• Fonctionnement du changement de vitesse</div>
                        <div class="subListItem">• Etat des roulements</div>
                        <div class="subListItem">• Vérification des jeux et serrages</div>
                        <div class="subListItem">• Vérification de la tension des rayons</div>
                        <div class="subListItem">• Vérification des connexions électriques</div>
                        <div class="subListItem">• Huilage de la chaine et des parties en roulement</div>
                        <div class="subListItem">• Vérification des points de sécurité et des lampes</div>
                    </td>
                </tr>
                <tr>
                    <td
                        class="bordered-top bordered-Bottom"style="width:100%;font-size: 18px; font-weight: bold; padding-bottom: 3mm;">
                        PIÈCES DE RECHANGE COMPRISES LORS LA LOCATION
                    </td>
                </tr>
                <tr>
                    <td class="list"style="width:100%;">
                        <div class="subListItem" style="margin-top: 3mm;">• Pneus</div>
                        <div class="subListItem">• Plaquettes de freins</div>
                        <div class="subListItem">• Transmission (chaine, cassette et plateau)</div>
                        <div class="subListItem">• Poignées</div>
                    </td>
                </tr>
            </tbody>
        </table>
        <p>
            Les éléments suivants ne sont pas couverts par l’entretien :
        </p>
        <div class="list">
            <div class="listItem">• Pièces de rechanges non comprises dans la liste précédente ;</div>
            <div class="listItem">• Réparation des dommages causés par une utilisation impropre, négligence, lors d’une
                compétition, collision, accidents ou les chutes, le vandalisme ou toute autre cause que l'usure normale
                ;</div>
            <div class="listItem">• Entretien et réparation de composants, de fonctions optionnelles et d'accessoires
                qui n'étaient pas fournis et montés à la livraison du vélo.</div>
        </div>
        <h3>Réparation et intervention sur demande</h3>
        <p>
            Cette section reprend toutes les demandes en dehors des 4 entretiens prevus et en particulier : les
            crevaisons, la casse d’une chaine ou une erreur moteur.
        </p>
        <div class="list" style="margin-top: 5mm;">
            <div class="listItem">Vous êtes face à un problème qui vous empêche de rouler avec votre vélo :</div>
            <div class="subList" style="margin-top: 5mm;">
                <div class="subListItem">o Dès la constatation du problème, contactez le service de dépannage au 04 340
                    56 23 avec le numéro de contrat MA33000999</div>
                <div class="subListItem">o lDemandez que le vélo soit dépanné et remorqué jusque chez KAMEO Bikes. Vous
                    avez aussi droit à être déposé où vous le souhaitez</div>
                <div class="subListItem">o KAMEO Bikes réceptionnera votre vélo, s’en occupera dans les plus bref
                    délais et vous contactera pour vous le déposer</div>
            </div>
            <div class="listItem">Vous êtes face à un mauvais fonctionnement mais vous pouvez sans rouler avec le vélo
                en ayant la certitude de ne pas le détériorer :</div>
            <div class="subList" style="margin-top: 5mm;">
                <div class="subListItem">o Terminez votre trajet</div>
                <div class="subListItem">o Contactez KAMEO Bikes à l’adresse sav@kameobikes.com ou via la plateforme
                    MyKAMEO pour une demande d’entretien</div>
                <div class="subListItem">o Vous serez alors recontacteé par un technicien</div>
            </div>
        </div>
        <p>Comment cette intervention vous sera facturée ? <br>
            Si elle arrive dans un délais de 2 mois avant le prochain entretien prévu pour votre vélo, KAMEO Bikes
            effectuera en même temps que l’intervention de réparation l’entretien global de votre vélo.
            Seules les pièces changées et non comprises dans les pièces de rechange vous seront facturées.<br>
            Pour tout cas hors de cette situation KAMEO Bikes facturera les frais suivants :
        </p>
        <div class="list" style="margin-top: 5mm;">
            <div class="listItem">Main d’œuvre :</div>
            <div class="subList" style="margin-top: 5mm;">
                <div class="subListItem">o 45€/h pour le travail effectué par notre technicien de 8h à 18h du lundi au
                    vendredi</div>
                <div class="subListItem">o 85€/h en dehors de cette tranche horaire</div>
            </div>
            <div class="listItem">Les pièces de rechange au prix du marché</div>
            <div class="listItem">Livraison du vélo : 25 € </div>
        </div>
        <p>SI LA DEMANDE RELÈVE D’UNE MALFAÇON DU VÉLO, KAMEO BIKES PREND À SA CHARGE LE COÛT DE L’ENTRETIEN ET LES
            PIÈCES.</p>
    </div>
    //assurance
    <div pageset="old" backtop="30mm" backleft="15mm" backright="10mm" backbottom="20mm">
        <h2>Assurance</h2>

        <p>
            KAMEO Bikes collabore avec Aedes et Dedales afin de vous offrir l’assurance Omnium la plus complète et la
            plus flexible actuellement disponible sur le marché : La P-Vélo
        </p>
        <table class="maxWidth">
            <tbody>
                <tr>
                </tr>
            </tbody>
        </table>
        <div>
            <div class="bold">L’omnium P-Vélo a les caractéristiques suivantes :</div>
            <div class="list" style="margin-top: 5mm;">
                <div class="subListItem">• « En cas de perte totale ou vol complet, une franchise de 150 € sera
                    réclamée au client. Le client pourra ensuite reprendre, chez KAMEO, un nouveau vélo d’un prix
                    catalogue équivalent au vélo volé. KAMEO s’efforcera de proposer un vélo équivalent dans la mesure
                    des stock disponibles.</div>
                <div class="subListItem">• Aedes impose l’achat d’un cadenas d’une valeur d’achat de minimum 60€ pour
                    tout qui souscrit une Omnium Vélo et qui souhaite être couvert contre le vol. </div>
                <div class="subListItem">• La couverture est valable en Belgique comme à l’étranger ;</div>
                <div class="subListItem">• Il est nécessaire de restituer les deux clés du cadenas du vélo afin que
                    l’assurance puisse être activée.</div>
                <div class="subListItem">• L’assurance comprend <span class="bold">2 dépannages</span> d’un même
                    vélo dans la même année. S’il devait y en avoir plus, ceux-ci seront facturés suivant la police
                    Aedes en annexe.</div>
                <div class="subListItem">• Dans le cadre d'une assurance pour un speedpedelec, un RC est souscrite.
                    Elle couvre les dommages matériels et corporels que vous causeriez aux tiers en cas d’accident si
                    votre responsabilité est engagée.</div>
            </div>
        </div>
        <div>
            <div class="bold">Les risques non-couverts sont les suivants :</div>
            <div class="list" style="margin-top: 5mm;">
                <div class="subListItem">• Un sinistre provoqué intentionnellement par l'assuré ;</div>
                <div class="subListItem">• Le vol du vélo s'il n'est pas attaché à un point fixe par un cadenas d'une
                    valeur de 60€ min ;</div>
                <div class="subListItem">• La compétition.</div>
            </div>
        </div>
        <div class="bold red">
            Si vous respectez les 3 points précédents la seule chose à faire pour être couvert est donc ATTACHER
            votre vélo à UN POINT FIXE avec le CADENAS PROPOSÉ PAR KAMEO. Dans un garage privé et fermé,
            c’est un cas particulier, il n’est pas nécessaire d’attacher le vélo.
        </div>
        <p>Informations et conditions complètes sur <a href="https://www.aedessa.be/assurances/velo" class="bold"
                style="color:black; ">https://www.aedessa.be/assurances/velo</a></p>
        <p>
            L’utilisateur s’engage à respecter toutes les conditions de l’assurance afin d’en bénéficier et à utiliser
            le vélo
            en bon père de famille. Dans le cas contraire, l’ensemble des frais causés par sa négligence seront à sa
            charge.
        </p>
        <div>Si vous avez des questions supplémentaires, n’hésitez pas à nous contacter !</div>
        <h3>Procédure en cas de vol</h3>
        <p>
            <span class="bold">Les démarches suivantes sont obligatoires.</span> En cas de non-respect des délais ou
            de non-réalisation
            d’une des démarches, la police d’assurance n’est pas applicable.
        </p>
        <div class="list">
            <div class="subListItem">• Informer KAMEO Bikes (dans le cas d’une location) ou votre assureur (pour une
                assurance personnelle) dans les 24h suivant la découverte du vol ;</div>
            <div class="subListItem">• Porter plainte à la police dans les 24h suivant la découverte du vol ;</div>
            <div class="subListItem">• Envoyer la déclaration de vol à KAMEO Bikes / votre assureur dès qu’elle est en
                votre possession ;</div>
            <div class="subListItem">• KAMEO Bikes s’occupe du reste.</div>
        </div>
        <h3>Procédure en cas de besoin d’assistance</h3>
        <p>Si vous ne savez pas continuer à rouler :</p>
        <div class="list">
            <div class="subListItem">• Téléphoner à Aedes, le numéro d’appel se trouve sur le sticker KAMEO sur le
                cadre de votre vélo ( 04 340 56 23 ) ;</div>
            <div class="subListItem">• Prendre un maximum de photo de votre problème ;</div>
            <div class="subListItem">• Contacter KAMEO Bikes via l’adresse suivante <a
                    href="mailto:sav@kameobikes.com" class="bold" style="color:black">sav@kameobikes.com</a>
                décrire votre problème et joindre vos photos.</div>
        </div>
    </div>
    //accessoires
    <div pageset="old" backtop="30mm" backleft="15mm" backright="10mm" backbottom="20mm">
        <h1>2. Accessoires</h1>
        <table class="maxWidth tableBorder">
            <thead style="font-size: 18px;">
                <tr>
                    <th style="width:33%; height: 10mm;">
                        <div style="margin:3mm;" class="bold">Type</div>
                    </th>
                    <th style="width:33%; height: 10mm;">
                        <div style="margin:3mm;" class="bold">Modèle</div>
                    </th>
                    <th style="width:33%; height: 10mm;">
                        <div style="margin:3mm;" class="bold">Quantité</div>
                    </th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <div pageset="old" backtop="30mm" backleft="15mm" backright="10mm" backbottom="20mm">
        <h2>Résumé du contenu de l'offre</h2>
        <table class="maxWidth tableBorder">
            <thead>
                <tr style="font-size: 18px;">
                    <th style="width:10%; height: 10mm;">
                        <div style="margin:3mm;" class="bold">POSTE</div>
                    </th>
                    <th style="width:20%; height: 10mm;">
                        <div style="margin:3mm;" class="bold">DESCRIPTIF</div>
                    </th>
                    <th style="width:35%; height: 10mm;">
                        <div style="margin:3mm;" class="bold">BUDGET HTVA</div>
                    </th>
                    <th style="width:35%; height: 10mm;">
                        <div style="margin:3mm;" class="bold">BUDGET TVAC</div>
                    </th>
                </tr>
            </thead>
            <tbody class="tbody-leftMargin">
                <tr>
                    <td>
                        <div class="green bold"
                            style="width:10%; padding-top:3mm; padding-bottom:3mm; margin-left:3mm;">Vélo</div>
                    </td>
                    <td style="width:20%">


                    </td>
                    <td style="width:30%; padding-top:3mm; padding-bottom:3mm;">
                    </td>
                    <td style="width:30%; padding-top:3mm; padding-bottom:3mm;">

                    </td>
                </tr>
                <tr>
                    <td style="width:10%;">
                        <div class="green bold" style="padding-top:3mm; padding-bottom:3mm; margin-left:3mm;">
                            Access-<br>oire</div>
                    </td>
                    <td style="width:20%;">

                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;">

                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;">

                    </td>
                </tr>
                <tr>
                    <td style="width:10%">
                        <div class="green bold" style="padding-top:3mm; padding-bottom:3mm; margin-left:3mm;">Boxes:
                            Install-<br>ation + Location mois</div>
                    </td>
                    <td style="width:20%">


                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;">

                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;">

                    </td>
                </tr>
                <tr>
                    <td style="width:10%;">
                        <div class="green bold" style="padding-top:3mm; padding-bottom:3mm; margin-left:3mm;">Autres
                        </div>
                    </td>
                    <td style="width:20%;">

                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;">

                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;">

                    </td>
                </tr>



                <tr>
                    <td style="width:10%;">
                        <div class="green bold" style="padding-top:3mm; padding-bottom:3mm; margin-left:3mm;">

                        </div>
                    </td>
                    <td style="width:20%;">
                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;">

                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;">

                    </td>
                </tr>
                <tr>
                    <td style="width:10%;">
                        <div class="green bold" style="padding-top:3mm; padding-bottom:3mm; margin-left:3mm;">Remise
                        </div>
                    </td>
                    <td style="width:20%;">
                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;" class="red">

                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;" class="red">

                    </td>
                </tr>
                <tr>
                    <td style="width:10%;">
                        <div class="green bold" style="padding-top:3mm; padding-bottom:3mm; margin-left:3mm;">Total
                        </div>
                    </td>
                    <td style="width:20%;">
                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;">

                    </td>
                    <td style="width:35%; padding-top:3mm; padding-bottom:3mm;">

                    </td>
                </tr>
            </tbody>
        </table>


    </div>
    <div pageset="old" backtop="30mm" backleft="15mm" backright="10mm" backbottom="20mm">

        <h1>Conditions de vente</h1>
        <h2>Prix</h2>
        <div class="light">Les prix sont entendus HTVA.</div>
        <h2>Livraison</h2>
        <table class="maxWidth">
            <tbody>
                <tr>
                    <td style="width:50%;" class="light">Livraison directement sur le site de votre entreprise à
                        l'adresse suivante :</td>
                </tr>
            </tbody>
        </table>
        <h2>Délais : </h2>
        <div class="light">

        </div>
        <h2>Validité de l’offre</h2>
        <div class="light">
        </div>
        <h2>Garantie</h2>
        <div class="light">KAMEO Bikes offre une garantie conforme à celle de la marque. Soit 2 ans sur le cadre et
            les composants.</div>
        <h2>Facturation</h2>
        <div class="light">

        </div>
        <h2>Conditions de paiement</h2>
        <div class="light">30 jours à partir de la date de la facture. Prélèvement automatique par domiciliation.
        </div>
    </div>
    <div pageset="old" backtop="30mm" backleft="15mm" backright="10mm" backbottom="20mm">
        <h2>Rupture du contrat</h2>
        <div class="light">
            En cas de rupture unilatérale du contrat de location de la part du client, les indemnités suivantes seront
            dues :<br />
            <table class="maxWidth tableBorder tableMargins">
                <thead>
                    <tr>
                        <th style="width:50%;" class="bold">DURÉE ÉCOULÉE DE LOCATION</th>
                        <th style="width:50%;" class="bold">Indemnité de rupture</th>
                    </tr>
                </thead>
                <tbody>

                    </tr>

                    </tr>
                    <tr>
                        <td style="width:50%;" class="light">1-12 mois</td>
                        <td style="width:50%;" class="light">6 mois</td>
                    </tr>
                    <tr>
                        <td style="width:50%;" class="bold">A l’échéance </td>
                        <td style="width:50%;" class="bold">Possibilité d’achat</td>
                    </tr>
                    <tr>
                        <td style="width:50%;" class="light">12 mois</td>

                        <td style="width:50%;" class="light">16% de la valeur marchande neuve du vélo au moment du
                            début du contrat de location</td>

                        <td style="width:50%;" class="light">Pas d'option d'achat incluse.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <h2>Rachat du vélo en cours de contrat</h2>
        <div class="light">
            <p>Dans le cas où la société cliente et/ou l’employé, souhaite(nt) arrêter le contrat mais devenir
                propriétaire du vélo, cela est possible après 12 mois de contrat.
                Le montant dû est égal à la somme de :</p>
            <div class="list">
                <div class="subListItem"> 60% de la valeur restante du contrat</div>
                <div class="subListItem"> La valeur résiduelle du vélo</div>
            </div>
            <p><strong>Ce montant n’est pas cumulable avec l’indemnité de rupture décrite en début de page, une
                    procédure ou l'autre s'applique, jamais les deux. Après
                    le paiement de la valeur de rachat, le vélo n'est plus la propriété de la KAMEO.</strong></p>
        </div>
    </div>
    <div backcolor="#2fa37c" backtop="30mm" backleft="15mm" backright="10mm" backbottom="20mm">
        <div>
            <div style="text-align:center" class="white">
                <img src="../../TO INCLUDE/pdf/template/img/logo_black_low_opacity.png" alt="logo"
                    class="logo" /><br />
            </div>
            <div class="light white" style="margin-bottom:10mm; text-align:center;">
                <span class="bold">KAMEO Bikes SRL <br /></span>
                Quai de Rome, 22<br />
                4000 Liège <br />
                BE 0681.879.712
            </div>
            <div class="light white" style="margin-bottom:10mm; text-align:center;">
                <br />
            </div>
        </div>
    </div> --}}


</body>

</html>
