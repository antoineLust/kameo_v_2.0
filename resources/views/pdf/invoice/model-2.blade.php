{{-- dd($items, $company, $totalHTVA, $totalTVAC, $reference, $outID, $invoiceId); --}}
<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Invoice</title>

    <style type="text/css">
        * {
            font-family: Verdana, Arial, sans-serif;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .gray {
            background-color: lightgray
        }

        .wave {
            width: 150%;
            height: 400px;
            background-size: cover;
            z-index: -1;
            position: fixed;
            bottom: -50px;
            left: -50px;
        }

        .wave-container {
            width: 100%;
        }

        .payement-container {
            width: 100%;
            margin-top: 75px;
            position: absolute;
            bottom: 425px;
        }

        .last-txt {
            position: absolute;
            bottom: 7.5px;
            left: 0;
            width: 100%;
            text-align: center;
            font-size: 12px;
        }

        .bike-img {
            width: 100%;
            height: auto;
        }

        .bike-details-container {
            width: 100%;
            border: 5px solid #3cb295;
            border-radius: 10px;
            margin-bottom: 2em;
            height: 45%;
        }

        .wave-detail {
            width: 203%;
            z-index: -1;
            position: relative;
            margin-left: -4px;
            bottom: -3.5px;
        }

        .page_break {
            page-break-before: always;
        }
    </style>

</head>

<body>

    <table width="100%">
        <tr>
            <td align="left">
                <h3>KAMEO bikes SPRL</h3>
                Quai de Rome, 22
                <br>
                B-4000 Liège
                <br>
                Belgium
                <br><br>
                BE0681.879.712
            </td>
            <td align="center" valign="top"><img src="{{ public_path('images/logo.png') }}" alt=""
                    width="150" /></td>

            <td align="right">
                <h3>{{ $company['name'] }}</h3>
                {{ $company['address']['street'] }}, {{ $company['address']['number'] }}
                <br>
                {{ $company['address']['zip'] }}, {{ $company['address']['city'] }}
                <br>
                {{ $company['address']['country'] }}
                <br><br>
                {{ $company['vat_number'] ? $company['vat_number'] : '' }}
            </td>
        </tr>

    </table>

    <table width="100%" style="margin-top: 75px;">
        <tr>
            <td style="color: #3cb295; font-weight: bold; text-transform: uppercase; text-align: left;">Facture
                n°{{ $outID }}</td>
        </tr>

    </table>

    <table width="100%" style="margin-top: 25px;">
        <tr>
            <td><strong>De:</strong> KAMEO Bikes SPRL</td>
            <td><strong>À:</strong> {{ $company['name'] }}</td>
        </tr>

    </table>

    <br />

    <table width="100%">
        <thead style="background-color: #3cb295; color: #fff;">
            <tr>
                <th>#</th>
                <th>Description</th>
                <th>Prix</th>
                <th>Quantité</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @php
                $index = 0;
            @endphp
            {{-- Article --}}
            @foreach ($items as $item)
                <tr>
                    <th scope="row">{{ $index++ + 1 }}</th>
                    <td align="center">{{ $item['description'] }}</td>
                    <td align="center">{{ $item['price'] }}</td>
                    <td align="center">1</td>
                    <td align="right">{{ $item['price'] }}</td>
                </tr>
            @endforeach
        </tbody>

        <tfoot>
            {{-- Subtotal, TVA and total --}}
            <tr>
                <td colspan="3"></td>
                <td align="right">Total HTVA €</td>
                <td align="right">{{ $totalHTVA }}</td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td align="right">TVA €</td>
                <td align="right">{{ number_format((($totalHTVA * 1.21) / 100) * 21, 2, '.', '') }}</td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td align="right">Total TVAC €</td>
                <td align="right" class="gray">{{ number_format($totalHTVA * 1.21, 2, '.', '') }}</td>

            </tr>
        </tfoot>
    </table>

    <table class="payement-container">
        <tr>
            <td>
                {{-- Payement infos --}}
                <table>
                    <tr>
                        <td align="left">
                            <strong>Bénéficiaire :</strong>
                        </td>
                        <td align="left">
                            KAMEO Bikes SPRL
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <strong>IBAN :</strong>
                        </td>
                        <td align="left">
                            BE90 5230 8139 8132
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <strong>SWIFT/BIC :</strong>
                        </td>
                        <td align="left">
                            TRIOBEBB
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <strong>Montant :</strong>
                        </td>
                        <td align="left">
                            {{ number_format($totalHTVA * 1.21, 2, '.', '') }} €
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <strong>Communication :</strong>
                        </td>
                        <td align="left">
                            +++{{ $reference }}+++
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                {{-- qrCode --}}
                <table>
                    <tr>
                        <td align="center">
                            <img src="{{ public_path('storage/qrcode/invoiceQrCode.png') }}" alt="KAMEO bikes wave"
                                style="width: 150px;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="wave-container">
        <tr>
            <td valign="top">
                <img src="{{ public_path('images/wave.svg') }}" alt="KAMEO bikes wave" class="wave" />
            </td>
        </tr>
    </table>

    <table class="last-txt">
        <tr>
            <td style="color: #fff;">
                Par ce paiement, vous adhérez à nos conditions générales de vente
            </td>
        </tr>
    </table>

</body>

</html>
