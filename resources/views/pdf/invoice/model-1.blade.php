<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Invoice</title>

    <style type="text/css">
        * {
            font-family: Verdana, Arial, sans-serif;
        }

        table {
            font-size: x-small;
        }

        tfoot tr td {
            font-weight: bold;
            font-size: x-small;
        }

        .gray {
            background-color: lightgray
        }

        .wave {
            width: 150%;
            height: 400px;
            background-size: cover;
            z-index: -1;
            position: fixed;
            bottom: -50px;
            left: -50px;
        }

        .wave-container {
            width: 100%;
        }

        .payement-container {
            width: 100%;
            margin-top: 75px;
            position: absolute;
            bottom: 425px;
        }

        .last-txt {
            position: absolute;
            bottom: 7.5px;
            left: 0;
            width: 100%;
            text-align: center;
            font-size: 12px;
        }

        .bike-img {
            width: 100%;
            height: auto;
        }

        .bike-details-container {
            width: 100%;
            border: 5px solid #3cb295;
            border-radius: 10px;
            margin-bottom: 2em;
            height: 45%;
        }

        .wave-detail {
            width: 203%;
            z-index: -1;
            position: relative;
            margin-left: -4px;
            bottom: -3.5px;
        }

        .page_break {
            page-break-before: always;
        }
    </style>

</head>

<body>

    <table width="100%">
        <tr>
            <td align="left">
                <h3>KAMEO bikes SPRL</h3>
                Quai de Rome, 22
                <br>
                B-4000 Liège
                <br>
                Belgium
                <br><br>
                BE0681.879.712
            </td>
            <td align="center" valign="top"><img src="{{ public_path('images/logo.png') }}" alt=""
                    width="150" /></td>

            <td align="right">
                <h3>{{ $company['name'] }}</h3>
                {{ $company['address']['street'] }}, {{ $company['address']['number'] }}
                <br>
                {{ $company['address']['zip'] }}, {{ $company['address']['city'] }}
                <br>
                {{ $company['address']['country'] }}
                <br><br>
                {{ $company['vat_number'] ? $company['vat_number'] : '' }}
            </td>
        </tr>

    </table>

    <table width="100%" style="margin-top: 75px;">
        <tr>
            <td style="color: #3cb295; font-weight: bold; text-transform: uppercase; text-align: left;">Facture
                n°{{ $outID }}</td>
        </tr>

    </table>

    <table width="100%" style="margin-top: 25px;">
        <tr>
            <td><strong>De:</strong> KAMEO Bikes SPRL</td>
            <td><strong>À:</strong> {{ $company['name'] }}</td>
        </tr>

    </table>

    <br />

    @php
        dd('ok');
    @endphp

    <table width="100%">
        <thead style="background-color: #3cb295; color: #fff;">
            <tr>
                <th>#</th>
                <th>Description</th>
                <th>Quantité</th>
                <th>Prix unitaire</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @php
                $bikes = [];
                for ($i = 0; $i < count($items); $i++) {
                    $items[$i]['quantity'] = array_count_values(array_column($items, 'id'))[$items[$i]['id']];
                    if (array_key_exists('bike_id', $items[$i])) {
                        array_push($bikes, $items[$i]);
                    }
                }
                $unique_items = array_unique(array_column($items, 'id'));
                $items = array_intersect_key($items, $unique_items);
                $totalHTVA = 0;
                $index = 0;
            @endphp
            {{-- Article --}}
            @foreach ($items as $item)
                @php
                    $totalHTVA += $item['selling_price'] * $item['quantity'];
                @endphp
                <tr>
                    <th scope="row">{{ $index++ + 1 }}</th>
                    <td align="center">{{ $item['brand'] }} {{ $item['model'] }}</td>
                    <td align="center">{{ $item['quantity'] }}</td>
                    <td align="right">{{ $item['selling_price'] }}</td>
                    <td align="right">{{ $item['selling_price'] * $item['quantity'] }}</td>
                </tr>
            @endforeach
        </tbody>

        <tfoot>
            {{-- Subtotal, TVA and total --}}
            <tr>
                <td colspan="3"></td>
                <td align="right">Total HTVA €</td>
                <td align="right">{{ $totalHTVA }}</td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td align="right">TVA €</td>
                <td align="right">{{ number_format((($totalHTVA * 1.21) / 100) * 21, 2, '.', '') }}</td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td align="right">Total TVAC €</td>
                <td align="right" class="gray">{{ number_format($totalHTVA * 1.21, 2, '.', '') }}</td>

            </tr>
        </tfoot>
    </table>

    <table class="payement-container">
        <tr>
            <td>
                {{-- Payement infos --}}
                <table>
                    <tr>
                        <td align="left">
                            <strong>Bénéficiaire :</strong>
                        </td>
                        <td align="left">
                            KAMEO Bikes SPRL
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <strong>IBAN :</strong>
                        </td>
                        <td align="left">
                            BE90 5230 8139 8132
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <strong>SWIFT/BIC :</strong>
                        </td>
                        <td align="left">
                            TRIOBEBB
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <strong>Montant :</strong>
                        </td>
                        <td align="left">
                            {{ number_format($totalHTVA * 1.21, 2, '.', '') }} €
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <strong>Communication :</strong>
                        </td>
                        <td align="left">
                            +++{{ $reference }}+++
                        </td>
                    </tr>
                </table>
            </td>
            <td>
                {{-- qrCode --}}
                <table>
                    <tr>
                        <td align="center">
                            <img src="{{ public_path('storage/qrcode/invoiceQrCode.png') }}" alt="KAMEO bikes wave"
                                style="width: 150px;" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

    <table class="wave-container">
        <tr>
            <td valign="top">
                <img src="{{ public_path('images/wave.svg') }}" alt="KAMEO bikes wave" class="wave" />
            </td>
        </tr>
    </table>

    <table class="last-txt">
        <tr>
            <td style="color: #fff;">
                Par ce paiement, vous adhérez à nos conditions générales de vente
            </td>
        </tr>
    </table>

    @if (count($bikes) > 0)
        <div class="page_break">
            {{-- Bike details --}}
            @foreach ($bikes as $bike)
                <table class="bike-details-container">
                    <tr>
                        <td style="width: 50%; padding-top: 65px;">
                            <h3 style="text-align: center; text-transform: uppercase; padding-bottom: 10px;">
                                {{ array_key_exists('firstname', $bike) ? $bike['firstname'] : 'Vélo partagé' }}
                                {{ array_key_exists('lastname', $bike) ? $bike['lastname'] : '' }}</h3>
                            <img src="{{ public_path('storage/bikes_pictures/' . $bike['bike_id'] . '/a.jpg') }}"
                                alt="" class="bike-img">
                        </td>
                        <td style="width: 50%; padding-left: 25px; padding-top: 65px;">
                            <table>
                                <h3 style="text-align: left; text-transform: uppercase; text-decoration: underline;">
                                    Description</h3>
                                <tr>
                                    <td>
                                        <strong style="color: #3cb295">Marque : </strong>
                                    </td>
                                    <td>
                                        {{ $bike['brand'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong style="color: #3cb295">Modèle : </strong>
                                    </td>
                                    <td>
                                        {{ $bike['model'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong style="color: #3cb295">Numéro de cadre : </strong>
                                    </td>
                                    <td>
                                        {{ $bike['frame_number'] }}
                                    </td>
                                </tr>
                            </table>
                            <hr style="margin: 10px 0;">
                            <table>
                                <h3
                                    style="text-align: left; text-transform: uppercase; margin-top: 0px;  text-decoration: underline;">
                                    Accessoire(s)</h3>
                                @php
                                    $accessories = $bike['accessories'];
                                @endphp
                                @if (count($accessories) > 0)
                                    @foreach ($accessories as $accessory)
                                        <tr>
                                            <td>
                                                {{ $accessory['catalog']['brand'] }}
                                                {{ $accessory['catalog']['model'] }}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td>
                                            Aucun accessoire
                                        </td>
                                    </tr>
                                @endif
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <img src="{{ public_path('images/wave.svg') }}" alt="KAMEO bikes wave"
                                class="wave-detail" />
                        </td>
                    </tr>
                </table>
            @endforeach

        </div>
    @endif

</body>

</html>
