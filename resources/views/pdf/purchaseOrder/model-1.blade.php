<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Purchase Order</title>
</head>

<body>
    {{-- Top logo --}}
    <table width="100%">
        <tr>
            <td align="center" valign="top"><img src="{{ public_path('images/logo.png') }}" alt="" width="150" /></td>
        </tr>
    </table>
    {{-- Bike details --}}
    <table style="width: 100%;">
        <tr>
            <td style="text-align:left;width:50%;">
                KAMEO Bikes SPRL
                <br>
                Quai de Rome, 22
                <br>
                4000 Liège
                <br>
                Belgium
            </td>
            <td style="text-align:right;width:50%;">
                sales@kameobikes.com
                <br>
                www.kameobikes.com
            </td>
        </tr>
    </table>
    <div style="width:100%;margin-top:25px;">
        <div style="text-align:center;width:100%;">
            <h1 style="text-decoration:underline;color:#60CA8E;">Accusé de réception</h1>
            <br>
            <p style="font-size:large;line-height:1.75;">Par la présente, je soussigné
                .................................................... accuse la réception des éléments mentionné
                ci-dessous.</p>
        </div>
    </div>
    @php
        $bikes = [];
        $accessories = [];
        foreach ($items as $item) {
            if ($item['name'] == 'bike'){
                array_push($bikes, $item);
            } else {
                array_push($accessories, $item);
            }
        }
    @endphp
    @if (count($bikes) > 0)
        <table style="background: #60CA8E; width: 100%; border: solid 1px black; margin: 50px auto 0 auto;">
            <tr>
                <th style="background: #60CA8E; padding: 5px; width: 25%; text-align: center;">Vélo</th>
                <th style="background: #60CA8E; padding: 5px; width: 25%; text-align: center;">Numéro de cadre</th>
                <th style="background: #60CA8E; padding: 5px; width: 25%; text-align: center;">Clé du vélo</th>
                <th style="background: #60CA8E; padding: 5px; width: 25%; text-align: center;">Clé du cadenas</th>
            </tr>
            @foreach ($bikes as $bike)
                <tr>
                    <td style="padding: 5px; width: 25%; text-align: center;">{{ $bike['brand'] }} - {{ $bike['model'] }} </td>
                    <td style="padding: 5px; width: 25%; text-align: center;">{{ (isset($bike['frame_number'])) ? $bike['frame_number'] : 'Pas de numéro de cadre'}} </td>
                    <td style="padding: 5px; width: 25%; text-align: center;">{{ (isset($bike['key_reference'])) ? $bike['key_reference'] : 'Pas de clé de vélo' }} </td>
                    <td style="padding: 5px; width: 25%; text-align: center;">{{ (isset($bike['locker_reference'])) ? $bike['locker_reference'] : 'Pas de clé de cadenas' }} </td>
                </tr>
            @endforeach
        </table>
    @endif
    <div style="margin-top: 50px;">
        <h2>Accessoires :</h2>
        <ul style="list-style: none;">
            @if (count($accessories) > 0)
                @foreach ($accessories as $accessory)
                    <li>
                        <input type="checkbox"><span style="margin-left: 10px; margin-top: 1px;">Marque :
                            {{ $accessory['brand'] }} - Modèle : {{ $accessory['model'] }} </span>
                    </li>
                @endforeach
            @else
                <li>
                    <p>Pas d'accessoire.</p>
                </li>
            @endif
        </ul>
    </div>
    <div style="position: fixed; bottom: 10px;">
        <div>
            <hr>
            <input type="checkbox"><span style="margin-left: 10px; margin-top: 1px;">Je confirme avoir reçu le manuel d'utilisation du vélo. </span>
            <br>
            <input type="checkbox"><span style="margin-left: 10px; margin-top: 1px;">J'accepte de recevoir par mail des conseils d'utilisations du vélo et des promotions proposées par Kameo.</span>
        </div>
        <div style="width: 100%; margin-top: 10px;">
            <div>
                <p>Date :</p>
            </div>
            <div>
                <p>Signature :</p>
            </div>
        </div>
    </div>
</body>

</html>
