<div class="modal fade modal-dialog modal-fullscreen-sm-down" id="commandesgroupees" tabindex="-1" role="modal" aria-labelledby="modal-label-3" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 id="modal-label-3" class="modal-title">Commandes groupées</h4>
                <button aria-hidden="true" data-bs-dismiss="modal" class="btn-close" type="button">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <p>Hello !</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-bs-dismiss="modal" class="btn btn-b" type="button">Fermer</button>
            </div>
        </div>
    </div>
</div>



<div class="grid-item col-sm-8">
    <div class="widget widget-form p-cb" style="height: 375px;">
        <h4>Informations personnelles</h4>
        <form action="" method="post" name="exemple">
            <div class="form-group mb-3">
                <input type="text" name="Nom" class="form-control" id="Nom" placeholder="Son nom" />
            </div>
            <div class="form-group mb-3">
                <input type="text" name="Prenom" class="form-control" id="Prenom" placeholder="Son prénom" />
            </div>
            <div class="form-group mb-3">
                <input type="email" name="Email" class="form-control" id="Email" placeholder="Son email" />
            </div>
            <div class="form-group mb-3">
                <input type="text" name="Adresse" class="form-control" id="Adresse" placeholder="Son adresse postale" />
            </div>
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-block">Valider</button>
            </div>
        </form>
    </div>
</div>