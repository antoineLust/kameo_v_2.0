{{-- URL --}}
<script type="text/javascript">
    const APP_URL = document.getElementById('app').dataset.url;
</script>
{{-- App --}}
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/datatableSettings.js') }}"></script>
<!--Plugins-->
<script src="{{ asset('js/plugins.js') }}"></script>
<!--Template functions-->
<script src="{{ asset('js/functions.js') }}"></script>
