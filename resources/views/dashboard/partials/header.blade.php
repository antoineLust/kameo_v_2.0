<header id="header" data-transparent="true" class=" submenu-light" style="text-align: center; margin-bottom: -20px; position : unset;">
    <div id="logo" style="@media(max-width: 990px) {display:none};">
        <img src="{{ asset('images/logo.png') }}" alt="KAMEO Bikes Logo">
    </div>
</header>

<style>
    @media(max-width: 990px) {
        #header {
            display: none
        }
    }
</style>
