<!DOCTYPE html>
<html lang="fr">
@include('dashboard.partials.head')

<body>
    <div class="body-inner">
        @include('dashboard.partials.topBar')
        @include('dashboard.partials.header')
        <div id="app" data-url="{{ env('APP_URL') }}">
            
        </div>
        @include('dashboard.partials.footer')
        @include('dashboard.partials.scripts')
    </div>
</body>

</html>
