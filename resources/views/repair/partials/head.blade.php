<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="robots" content="noindex,nofollow">
<meta name="description" content="Atelier de réparation pour tout type de vélo, au 22 Quai de Rome, à Liège. Réparation pour vélo mécanique ou vélo électrique">
<meta name="keywords" content="KAMEO, Cameo, Cameo Bikes, Kameo Bikes, reparation, atelier">
<meta name="author" content="Antoine Lust">    

<title>Kameo Bikes - Atelier de réparation pour tout type de vélo</title>
