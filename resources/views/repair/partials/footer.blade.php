<div style="width:100%; height: 100px; background-color: #1d71b8">
    <div style="width:100%; height: 100%; border-bottom-left-radius: 50px; border-bottom-right-radius: 50px; background-color: white">
    </div>
</div>
<div style="width:100%; padding-top:50px; padding-bottom: 50px; margin-top:0px; background-color: #1d71b8; color: white">
    <div id="footer">
        <div class="visible-sm" style="text-align:center">
            <img src="{{ asset('images/icons/KAMEO_Logo_Activites_Carre_Blanc_Mono.svg') }}" style="margin-left: auto; margin-right:auto; width:200px">
        </div>
        <div class="hidden-sm">
            <strong style="text-align: left">Plan du site</strong>
            <ul>
                <li>Accueil</li>
                <ul style="font-size:0.8em">
                    <li><a href="{{ route('pro') }}">Pro</a></li>
                    <ul>
                        <li><a href="{{ route('employer') }}">Employeur</a></li>
                        <li><a href="{{ route('employee') }}">Employé</a></li>
                        <li><a href="{{ route('independant') }}">Indépendant</a></li>
                    </ul>
                    <li><a href="{{ route('repair') }}">Shop</a></li>
                    <ul>
                        <li><a href="{{ route('catalog-overview') }}">Catalogue Vélo</a></li>
                        <li><a href="{{ route('catalog-accessories-overview') }}">Catalogue Accessoires</a></li>
                        <li><a href="{{ route('bike-short-location') }}">Location vélo</a></li>
                    </ul>
                    <li><a href="{{ route('repair') }}">Repair</a></li>
                    <li><a href="{{ route('blog-shop') }}">Blog</a></li>
                    <li><a href="{{ route('contact_pro') }}">Contact</a></li>
            </ul>
        </div>
        <div>
            <p style="text-align:center">
                Quai de Rome 22, 4000 Liège<br>
                +32 490 71 21<br>
                info@kameobikes.com
            </p>
            <div style="text-align:center">
                <span><strong>Follow us </strong></span>
                <a href="https://www.facebook.com/kameobikes">
                    <img src="{{ asset('images/icons/Facebook.svg') }}" alt='icone facebook' title="icone facebook" width="15px">
                </a>
                <a href='https://www.linkedin.com/company/14053262/'>
                    <img src="{{ asset('images/icons/Linkedin.svg') }}" alt='icone linkedin' title="icone linkedin" width="15px">
                </a>
                <a href='https://www.instagram.com/kameobikes/'>
                    <img src="{{ asset('images/icons/Instagram.svg') }}" alt="icone instagram" title="icone instagram" width="15px">
                </a>
            </div>
            <div style="text-align: center; color: white; font-size:0.8em">
                <p style="width:100%">
                    2022 KAMEO BIKES<br>
                    <a href="{{ route('politique-confidentialite') }}">Politique de confidentialité</a><br>
                    <a href="{{ route('conditions-generales') }}">Conditions générales d'utilisation</a>
                </p>
            </div>
        </div>
        <div>
            <p style="text-align:center">
                <strong>Horaires d'ouverture :</strong><br>
                Lundi : <strong>Fermé</strong><br>
                Mardi : <strong>07h30 - 18h</strong><br>
                Mercredi : <strong>09h - 18h</strong><br>
                Jeudi : <strong>09h - 19h</strong><br>
                Vendredi : <strong>09h - 18h</strong><br>
                Samedi : <strong>09h-18h</strong><br>
                Dimanche : <strong>Fermé</strong>
            </p>
        </div>

    </div>
</div>

<style>
    #footer{
        width: 80%;
        margin: auto;
    }
    #footer div{
        margin:auto;
        width:100%;
    }
    #footer div li{
        text-align:left
    }

    @media (min-width: 1024px){
        #footer{
            display:flex;
            flex-wrap: wrap;
        }
        #footer div{
            margin:auto;
            width:33%;
        }

    }
</style>