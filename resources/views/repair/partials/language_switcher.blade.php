<div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
    @foreach($available_locales as $locale_name => $available_locale)
        @if($available_locale === $current_locale)
            <span style="color:red">{{ $available_locale }}</span>
        @else
            <a class="ml-1 underline ml-2 mr-2" href="{{ $available_locale }}">
                <span>{{ $available_locale }}</span>
            </a>
        @endif
    @endforeach
</div>