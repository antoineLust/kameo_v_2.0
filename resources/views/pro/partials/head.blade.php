<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="robots" content="noindex,nofollow">
<meta name="description" content="La solution la plus complète du marché pour l'utilisation du vélo en entreprise. Découvrez nos solutions innovantes de mobilité douce, plan vélo, vélos partagés, gestion de flotte online">
<meta name="keywords" content="KAMEO, Cameo, Cameo Bikes, Kameo Bikes, magasin velo, vélo électrique, velo electrique, Liège">
<meta name="author" content="Antoine Lust">    

<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.18/vue.min.js"></script>
<script src="{{ asset('js/app.js') }}" defer></script>


<title>Kameo Bikes - Leasing de vélos pour les entreprises</title>
