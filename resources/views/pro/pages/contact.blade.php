
@extends('pro.layouts.default')

<script src="https://kit.fontawesome.com/02ec9d769d.js" crossorigin="anonymous"></script>

<style>
    .grid-template-2{
        display: grid; 
        grid-template-columns: 1fr;
    }
    .grid-1{
        grid-column: span 1;
    }
    @media screen and (min-width: 768px){
        .grid-template-2{
            display: grid; 
            grid-template-columns: 50% 50%;
        }
        .grid-template-3{
            display: grid; 
            grid-template-columns: auto auto;
        }

        .grid-1{
            grid-column: span 1;
        }
        .grid-2{
            grid-column: span 2;
        }
        .grid-3{
            grid-column: span 3;
        }


    }
    label{
        color: #2cb396;
    }
</style>

@section('content')

    <div style="width:80%; margin:auto">
        <div class="returnBack" onclick="history.back()">Retour</div>
        <h1 style="color:#2cb396; display:inline">Formulaire de contact</h1>
    </div>

    <form method="POST" action="#">
        <div style="width:80%; margin:auto" class="grid-template-3">
            @csrf
            <div class="grid-1">
                <label for="firstName">Prénom</label>
                <input id="firstName" type="text" class="@error('firstName') is-invalid @enderror"><br>
            </div>
            <div class="grid-1">        
                <label for="name">Nom</label>
                <input id="name" type="text" class="@error('name') is-invalid @enderror"><br>
            </div>
            <div class="grid-1">
                <label for="email">Adresse E-Mail</label>
                <input id="email" type="email" class="@error('email') is-invalid @enderror"><br>
            </div>

            <div class="grid-1">
                <label for="phone">Téléphone</label>
                <input id="phone" type="phone" class="@error('phone') is-invalid @enderror"><br>
            </div>

            <div class="grid-1">
                <label for="company">Société</label>
                <input id="company" type="text" class="@error('name') is-invalid @enderror"><br>
            </div>
        </div>
        <div style="width:100%; background-color:#2cb396;">
            <div style="width:80%; margin:auto; margin-top:20px; padding-top:20px">
                <label for="message" style="color: white">Message</label>
                <textarea id="message" class="@error('message') is-invalid @enderror" rows="20" style="width:100%"></textarea>
                <div style="width:100%; text-align:center; margin-top:30px; padding-bottom:30px">
                    <button type="submit" style="font-size:1.5em; background-color: white; color: #2cb396; border-radius:20px; padding:10px; cursor: pointer;">Envoyer</button>
                </div>
            </div>
        </div>
    </form>

    <script>
        document.querySelector('form').addEventListener('submit', function(e){
            e.preventDefault();
            console.log(this)
            var form    = new FormData(this);
            var request = new XMLHttpRequest();
            request.open('POST', window.location.origin + '/api/cash4Bike/calculate', true);
            request.send(form);
            request.onload = function(){
                if(request.status == 200){
                    alert('Votre message a bien été envoyé');
                }
            }
        });
    </script>

    <section>
        <div style="width:80%; margin: auto">
            <h2>Notre équipe</h2>
            <p>Un membre de notre équipe vous répondra bientot ! </p>
            <div style="width:80%" id="teamDiv">
                <div class="pictureTeam" style="text-align:center">
                    <img src="{{  asset('images/equipe/antoine.jpg') }}" style="border-radius:50%" width="100px">
                    <div class="nameTeam">Antoine Lust</div>
                    <div class="descriptionTeam">Responsable B2B</div>
                </div>
                <div class="pictureTeam" style="text-align:center">
                    <img src="{{  asset('images/equipe/julien.jpg') }}" style="border-radius:50%" width="100px">
                    <div class="nameTeam">Julien Jamar</div>
                    <div class="descriptionTeam">Responsable B2C</div>
                </div>
                <div class="pictureTeam" style="text-align:center">
                    <img src="{{  asset('images/equipe/Thib.jpg') }}" style="border-radius:50%" width="100px">
                    <div class="nameTeam">Thibaut Mativa</div>
                    <div class="descriptionTeam">Responsable Marketing</div>
                </div>
                <div class="pictureTeam" style="text-align:center">
                    <img src="{{  asset('images/equipe/PY.jpg') }}" style="border-radius:50%" width="100px">
                    <div class="nameTeam">Pierre-Yves Adant</div>
                    <div class="descriptionTeam">Responsable légal</div>
                </div>
                <div class="pictureTeam" style="text-align:center">
                    <img src="{{  asset('images/equipe/Simon.jpg') }}" style="border-radius:50%" width="100px">
                    <div class="nameTeam">Simon Spineux</div>
                    <div class="descriptionTeam">Responsable Atelier</div>
                </div>
                <div class="pictureTeam" style="text-align:center">
                    <img src="{{  asset('images/equipe/benjamin.jpg') }}" style="border-radius:50%" width="100px">
                    <div class="nameTeam">Benjamin Van Reterghem</div>
                    <div class="descriptionTeam">Responsable IT</div>
                </div>
                <div class="pictureTeam" style="text-align:center">
                    <img src="{{  asset('images/equipe/Nicolas.jpeg') }}" style="border-radius:50%" width="100px">
                    <div class="nameTeam">Nicolas Terryn</div>
                    <div class="descriptionTeam">Vendeur / Mécanicien</div>
                </div>
                <div class="pictureTeam" style="text-align:center">
                    <img src="{{  asset('images/equipe/aziz.jpg') }}" style="border-radius:50%" width="100px">
                    <div class="nameTeam">Abdulaziz AKHMYAROV</div>
                    <div class="descriptionTeam">Vendeur / Mécanicien</div>
                </div>

            </div>
        </div>
    </section>
    <style>
        #teamDiv{
            display: grid;   
            grid-template-columns: repeat(2, 1fr);
            column-gap: 20px;
            row-gap: 20px;
            margin:auto
        }

        .nameTeam{
            margin-top:10px;
            font-weight:600;
        }
        @media screen and (min-width: 768px){
            #teamDiv{
                display: grid;   
                grid-template-columns: repeat(4, 1fr);
            }
        }
        @media screen and (min-width: 1024px){
            #teamDiv{
                display: grid;   
                grid-template-columns: repeat(5, 1fr);
            }
            .grid-template-3{
            display: grid; 
            grid-template-columns: auto auto auto;
        }

        }

    </style>
@stop
