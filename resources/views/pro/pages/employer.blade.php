
@extends('pro.layouts.default')

<script src="https://kit.fontawesome.com/02ec9d769d.js" crossorigin="anonymous"></script>
<style>
    .card {
        /* Add shadows to create the "card" effect */
        margin:auto;
        height:100%;
        border-radius: 20px;
        background-color: white;
        padding-bottom: 20px;
    }

    .title_vignette{
        color: #2cb396;
        font-weight: bold;
        font-size: 1.2em;
    }


    .numberCircle {
        border-radius: 50%;
        width: 22px;
        height: 22px;
        background: #2cb396;
        color: white;
        text-align: center;
        font: 20px Arial, sans-serif;
        display: grid
    }

    .grid{
        display:grid;
        grid-template-columns: repeat(1, 1fr);
        grid-auto-rows: 1fr;
        margin: 0 auto;
        column-gap: 9px;
        row-gap:20px;
    };

    .grid a p{
        padding:0px 20px 0px 20px;
    }

    .grid p{
        padding:0px 20px 0px 20px;
    }

    .leftSmallBigRigh{
        grid-template-columns: 1fr;
    }

    .bigLeftSmallRight{
        grid-template-columns: 1fr;
    }

    .grid-template-2{
        display: grid; 
        grid-template-columns: 1fr;
        padding: 20px 20px 0px 20px;
    }
    .grid-1{
        grid-column: span 1;
    }
    @media screen and (min-width: 1024px){
        .grid-template-2{
            display: grid; 
            grid-template-columns: 100px 50%;
            padding: 20px 20px 0px 20px;
        }
        .grid-1{
            grid-column: span 1;
        }
        .grid-2{
            grid-column: span 2;
        }
        .leftSmallBigRigh{
            grid-template-columns:400px auto;
        }
        .bigLeftSmallRight{
            grid-template-columns:auto 400px;
        }


    }

    html{scroll-behavior:smooth}

</style>

@section('content')
    <div style="width:80%; margin: auto">
        <div class="returnBack" onclick="history.back()">Retour</div>
        <h1 style="color:#2cb396; display: inline-block; margin-left:20px">Je suis un employeur</h1>

        <p>
            Vous êtes employeur et vous voulez améliorer la mobilité au sein de votre entreprise afin d'y inclure des vélos ?<br>Voici différentes méthodes qui pourraient vous intéresser :
        </p>
    </div>
    <div style="width:100%; margin: auto; background-color: #2cb396">
        <div class="grid sm:grid-flow-col" style="width:80%; padding-top:20px; padding-bottom: 20px; ">
            <div class="card" style="position: relative">
                <a href="#planVelo">
                    <div class="grid-template-2">
                        <div class="grid-1">
                            <img src="{{ asset('images/icons/Icone_PlanVelo.jpg') }}" alt="logo plan vélo" width="70px">
                        </div>
                        <div class="grid-1">
                            <span class="title_vignette">Plan vélos</span>
                        </div>
                    </div>
                    <p>
                        Cette solution est <strong>budgétairement neutre pour l'entreprise</strong> et permet à vos employés d'utiliser leur salaire brut afin de faire l'acquisition d'un vélo, ce qui permet une réduction de l'ordre de 65% !
                    </p>
                    <span style="display: table; margin: 0 auto; padding: 0px 20px 0px 20px">
                        <img src="{{  asset('images/Arrow_Down.gif') }}" width="50px">
                    </span>    
                </a>
            </div>    
            <div class="card" style="position: relative">
                <a href="#budgetMobilite">
                    <div class="grid-template-2">
                        <div class="grid-1">
                            <img src="{{ asset('images/icons/Icone_BudgetMobilite.jpg') }}" alt="logo budget mobilité" width="70px">
                        </div>
                        <div class="grid-1">
                            <span class="title_vignette">Budget mobilité</span>
                        </div>
                    </div>
                    <p>
                        Le <strong>budget mobilité</strong> est une autre possibilité donnée à vos employés qui bénéficient d'une voiture de société de pouvoir choisir une mobilité correspondant mieux à leurs besoins.
                    </p>
                    <span style="display: table; margin: 0 auto; padding: 0px 20px 0px 20px">
                        <img src="{{  asset('images/Arrow_Down.gif') }}" width="50px">
                    </span>    
                </a>
            </div>    
            <div class="card" style="position: relative">
                <a href="#veloPartage">
                    <div class="grid-template-2">
                        <div class="grid-1">
                            <img src="{{ asset('images/icons/Icone_VeloPartage.jpg') }}" alt="logo velo partage" width="70px">
                        </div>
                        <div class="grid-1">
                            <span class="title_vignette">Vélos partagés</span>
                        </div>
                    </div>
                    <p>
                        Cette option vous permet de mettre à disposition des vélos partagés qui seront utilisés par vos collaborateurs grâce à notre solution autonome.<br><br>
                    </p>
                    <span style="display: table; margin: 0 auto; padding: 0px 20px 0px 20px">
                        <img src="{{  asset('images/Arrow_Down.gif') }}" width="50px">
                    </span>    
                </a>
            </div>    
        </div>
    </div>
    <div style="width:80%; margin: auto">
        <br>
        <hr size="3" color="black">
        <br>
        <div class="grid leftSmallBigRigh" style="width:100%" id="planVelo">
            <div style="margin:auto">
                <img src="{{ asset('images/public_website_images/Photo_PlanVelo.jpg') }}" alt="image plan velo" width="100%" height="auto" style="border-radius: 20px; max-width:450px">
            </div>    
            <div style="padding-left: 20px">
                <h2 style="margin-top: 0px; font-size: 2em">Plan vélo</h2>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">1</div>  
                    </div>
                    <div style="padding-left: 20px">
                        Nous prenons rendez-vous afin de discuter de votre plan (financement, nombre de personnes concernées) et vous expliquer le déroulement de l'implémentation du plan vélo
                    </div>
                </div>
                <br>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">2</div>  
                    </div>
                    <div style="padding-left: 20px">
                        
                        Nous effectuons une séance d'information pour vos collaborateurs et nous organisons dans la foulée un test de vélos sur le parking de votre entreprise
                    </div>
                </div>
                <br>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">3</div>  
                    </div>
                    <div style="padding-left: 20px">
                        Vos collaborateurs recoivent un accès à notre plateforme et peuvent commander leur vélo ! 
                    </div>
                </div>
                <br>
                <div style="width:100%; padding-bottom: 20px">
                    <span style="background-color:#2cb396; color: white; margin-bottom:20px; font-size:20px; font-weight:800; display: table; margin: 0 auto; padding: 0px 20px 0px 20px; border-radius:20px">
                        Réserver un rendez-vous
                    </span>        
                </div>
            </div>    
        </div>
        <br>
        <hr size="3" color="black">
        <br>

        <div class="grid bigLeftSmallRight" style="width:100%" id="budgetMobilite">
            <div style="padding-right: 20px; margin: auto">
                <h2 style="margin-top: 0px; font-size: 2em">Budget mobilité</h2>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">1</div>  
                    </div>
                    <div style="padding-left: 20px">
                        
                        Ce plan doit tout d'abord être préparé avec votre secrétariat social : vous identifiez les personnes qui sont éligibles à un échange de leur voiture de société par des solutions de mobilité respectueuses de l’environnement
                    </div>
                </div>
                <br>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">2</div>  
                    </div>
                    <div style="padding-left: 20px">
                        Nous prenons rendez-vous afin de vous expliquer l'interface de commande de vélo pour vos collaborateurs
                    </div>
                </div>
                <br>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">3</div>  
                    </div>
                    <div style="padding-left: 20px">
                        Nous organisons une présentation à votre personnel et un test sur le parking de votre entreprise
                    </div>
                </div>
                <br>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">4</div>  
                    </div>
                    <div style="padding-left: 20px">
                        Vos collaborateurs recoivent un accès à notre plateforme et peuvent commander leur vélo ! 
                    </div>
                </div>
                <br>
                <div style="width:100%; padding-bottom: 20px">
                    <span style="background-color:#2cb396; color: white; margin-bottom:20px; font-size:20px; font-weight:800; display: table; margin: 0 auto; padding: 0px 20px 0px 20px; border-radius:20px">
                        Réserver un rendez-vous
                    </span>        
                </div>
            </div>    
            <div>
                <img src="{{ asset("images/public_website_images/Photo_Mobilite.jpg") }}" alt="image employer" width="100%" height="auto" style="border-radius: 20px">
            </div>    
        </div>
        <br>
        <hr size="3" color="black">
        <br>
        <div class="grid leftSmallBigRigh" style="width:100%" id="veloPartage">
            <div style="margin:auto">
                <img src="{{ asset('images/public_website_images/Photo_VeloPartage.jpg') }}" alt="image velo partagé" width="100%" height="auto" style="border-radius: 20px; max-width:450px">
            </div>    
            <div style="padding-left: 20px">
                <h2 style="margin-top: 0px; font-size: 2em">Vélos partagés</h2>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">1</div>  
                    </div>
                    <div style="padding-left: 20px">
                        Nous identifions les besoins de votre entreprise : nombre de vélos, types de vélos, taille, etc... <br>Si besoin, nous pouvons également vous créer une enquête à envoyer à vos collaborateurs afin de mesurer la demande.
                    </div>
                </div>
                <br>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">2</div>  
                    </div>
                    <div style="padding-left: 20px">
                        Nous configurons et installons notre solution de partage de vélo sur le site de votre entreprise
                    </div>
                </div>
                <br>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">3</div>  
                    </div>
                    <div style="padding-left: 20px">
                        Vos employés peuvent commencer à utiliser les vélos !
                    </div>
                </div>
                <br>
                <div style="width:100%; padding-bottom: 20px">
                    <a href="{{ route('velos-partages') }}">
                        <span style="background-color:#2cb396; color: white; margin-bottom:20px; font-size:20px; font-weight:800; display: table; margin: 0 auto; padding: 0px 20px 0px 20px; border-radius:20px">
                            Plus d'informations sur notre solution
                        </span>        
                    </a>
                </div>
            </div>    

        </div>
    </div>


@stop
