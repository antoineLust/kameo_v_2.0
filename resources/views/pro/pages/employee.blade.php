
@extends('pro.layouts.default')

<script src="https://kit.fontawesome.com/02ec9d769d.js" crossorigin="anonymous"></script>
<style>
    .card {
        /* Add shadows to create the "card" effect */
        transition: 0.3s;
        border-radius: 10px;
        padding: 20px;
        min-width: 300px;
    }

    .numberCircle {
        border-radius: 50%;
        width: 36px;
        height: 36px;
        background: #2cb396;
        color: white;
        text-align: center;
        font: 32px Arial, sans-serif;
        display: grid
    }
    .title_vignette{
        color: #2cb396;
        font-weight: bold;
        font-size: 1.2em;
    }

    .grid-template-2{
        display: grid; 
        grid-template-columns: 1fr;
    }
    .grid-1{
        grid-column: span 1;
    }
    @media screen and (min-width: 768px){
        .grid-template-2{
            display: grid; 
            grid-template-columns: 100px 50%;
        }
        .grid-1{
            grid-column: span 1;
        }
        .grid-2{
            grid-column: span 2;
        }
    }


</style>

@section('content')
<div class="full-width">
    <div class="returnBack" onclick="history.back()">Retour</div>
    <h1 style="color:#2cb396; display: inline-block; margin-left:20px">Je suis un employé</h1>
    <p>En tant qu'employé, vous avez la possibilité d'utiliser votre salaire brut afin de faire l'acquisition d'un vélo.<br>
    Cela représente évidemment un avantage non néglibeable qui permet de faire diminuer le coût du leasing d'environ 65%.</p>
</div>

<div style="width:100%; background-color:#2cb396; margin:auto">
    
    <div style="margin:auto; padding:30px;" class="calculateCashForBikeDiv">
        <div style="border: 1px solid #c7c7c7; border-radius:30px;background-color : white; padding:0px 10px 0px 10px" class="cash4BikeBlock">
            <form id="calculateCashForBikeForm" onsubmit="event.preventDefault(); calculateCash4Bike(this);">
                @csrf <!-- {{ csrf_field() }} -->
                <div style="margin: 30px 0px 0px 30px">
                    <img src="{{ asset('images/icons/Icone_Calculatrice.jpg') }}" alt="logo plan vélo" width="70px" >
                </div>
                <span class="title_vignette">Calculateur sacrifice salarial</span>
                <h4>Informations financières</h4>
                <table>
                    <tr>
                        <td><label for="gross_salary">Salaire brut</label></td><td class="sm-hidden"><input type="range" min="0" max="6000" value="2500" id="gross_salary" style="width:200px"></td><td><input type="number" min="0" max="6000" value="2500" id="gross_salary_value" name="gross_salary_value" style="border: 1px solid black; font-size: 1em; width:80px"> €</td>
                    </tr>
                    <tr>
                        <td><label for="bike_amount">Montant d'achat du vélo</label></td>
                        <td class="sm-hidden"><input type="range" min="0" max="6000" value="3000" id="bike_amount" style="width:200px"></td><td><input type="number" min="0" max="6000" value="3000" id="bike_amount_value" name="bike_amount_value" style="border: 1px solid black; font-size: 1em; width:80px;"> € TVAC</td>
                    </tr>
                </table>
                <h4>Informations personnelles</h4>
                <table>
                    <tr>
                        <td><label for="distance_home_work">Distance domicile/travail</label></td>
                        <td><input type="number" min="0" max="50" value="5" id="distance_home_work" name="distance_home_work" style="border: 1px solid black; font-size: 1em; width:100px"> kms</td>
                    </tr>
                    <tr>
                        <td style="width: 50%"><label for="home_work_prime">Montant indemnité kilométrique</label></td>
                        <td><input type="number" min="0" max="0.5" value="0.25" step="0.01" id="home_work_prime" name="home_work_prime" style="border: 1px solid black; font-size: 1em; width:100px"> €/km</td>
                    </tr>
                    <tr>
                        <td><label for="frequency">Nombre de jours d'utilisation du vélo par semaine</label></td>
                        <td>
                            <select style="border: 1px solid black; font-size: 1em; border-radius:10px; padding: 8px" name="frequency" id="frequency">
                                <option value="1">1 fois par semaine</option>
                                <option value="2">2 fois par semaine</option>
                                <option value="3">3 fois par semaine</option>
                                <option value="4">4 fois par semaine</option>
                                <option value="5">5 fois par semaine</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <br>
                <button type="submit" class="text-center" id="calculate_cash4bike card" style="background-color: #2cb396; cursor: pointer; color: white; height:50px; vertical-align: center; width:100%">Calculer mon avantage</button>    
            </form>
        </div>
        <div style="border: 1px solid #c7c7c7; border-radius:30px; padding:0px 10px 0px 10px; background-color : white;" class="cash4BikeBlock">
            <div id="no_result_cash4Bike">
                <div style="margin: 30px 0px 0px 30px">
                    <img src="{{ asset('images/icons/Icone_FicheSalaire.jpg') }}" alt="logo plan vélo" width="70px">
                </div>
                <span class="title_vignette">Résultats simulation</span>
                <p id="cash4bike_no_simulation">Vous souhaitez faire une simulation de budget pour votre leasing vélo ?<br>
                <strong>Remplissez les informations dans le bloc précédent et cliquez sur "Calculer mon avantage"</strong> </p>
            </div>
        
            <div id="result_cash4Bike" style="display:none">
                <div style="margin: 30px 0px 0px 30px">
                    <img src="{{ asset('images/icons/Icone_FicheSalaire.jpg') }}" alt="logo plan vélo" width="70px">
                </div>
                <span class="title_vignette">Résultats simulation</span>
                <p>
                    Montant du leasing :<br><span id="leasing_amount"></span><br><br>
                    Impact sur salaire brut :<br><span id="impact_gross_salary"></span><br><br>
                    Impact sur salaire net :<br><span id="impact_net_salary"></span><br><br>
                    Montant prime kilométrique :<br><span id="impact_bike_allowance"></span><br><br>
                    <strong>Impact final :<br><span id="final_impact"></span></strong><br><br>

                </p>
            </div>
        </div>
        <script>

            document.getElementById("gross_salary").addEventListener('change', function(){
                document.getElementById("gross_salary_value").value=this.value;
                document.getElementById('impact_net_salary').style.backgroundColor="#2cb396";
                displayGreen("impact_net_salary");
                displayGreen("final_impact");
                calculateCash4Bike(document.getElementById('calculateCashForBikeForm'));
            });
            document.getElementById("gross_salary_value").addEventListener('change', function(){
                document.getElementById("gross_salary").value=this.value;

            });
            document.getElementById("bike_amount").addEventListener('change', function(){
                displayGreen("leasing_amount");
                displayGreen("impact_gross_salary");
                displayGreen("impact_net_salary");
                displayGreen("final_impact");
                calculateCash4Bike(document.getElementById('calculateCashForBikeForm'));
                document.getElementById("bike_amount_value").value=this.value;
            });
            document.getElementById("bike_amount_value").addEventListener('change', function(){
                document.getElementById("bike_amount").value=this.value;
            });

            document.getElementById("distance_home_work").addEventListener('change', function(){
                displayGreen("impact_bike_allowance");
                displayGreen("final_impact");
                calculateCash4Bike(document.getElementById('calculateCashForBikeForm'));
            });

            document.getElementById("home_work_prime").addEventListener('change', function(){
                displayGreen("impact_bike_allowance");
                displayGreen("final_impact");
                calculateCash4Bike(document.getElementById('calculateCashForBikeForm'));
            });
            document.getElementById("frequency").addEventListener('change', function(){
                displayGreen("impact_bike_allowance");
                displayGreen("final_impact");
                calculateCash4Bike(document.getElementById('calculateCashForBikeForm'));
            });

            function displayGreen(id){
                document.getElementById(id).style.backgroundColor="#2cb396";
                setTimeout(function(){
                    document.getElementById(id).style.backgroundColor="transparent";
                }, 1000);

            }
            function calculateCash4Bike(formData){
                var form    = new FormData(formData);
                var oReq = new XMLHttpRequest();
                oReq.open("post", window.location.origin+'/api/cash4Bike/calculate', true);
                oReq.setRequestHeader('Access-Control-Allow-Origin', '*');
                oReq.send(form);
                oReq.onload = function(){
                    if(oReq.status == 200){
                        response=JSON.parse(oReq.response);
                        document.getElementById('no_result_cash4Bike').style.display="none";
                        document.getElementById('result_cash4Bike').style.display="block";
                        document.getElementById('leasing_amount').innerHTML = response.leasingPrice + " €/mois"
                        document.getElementById('impact_gross_salary').innerHTML = response.impactOnGrossSalary + " €/mois";
                        document.getElementById('impact_net_salary').innerHTML = response.impactOnNetSalary + " €/mois";
                        document.getElementById('impact_bike_allowance').innerHTML = response.impactBikeAllowance + " €/mois";
                        document.getElementById('final_impact').innerHTML = Math.round((response.impactOnNetSalary - response.impactBikeAllowance)*100)/100 + " €/mois";
                    }
                }
            }
        </script>
    </div>
</div>
<style>
    .cash4BikeBlock{
        width:100%;
        margin-bottom:20px;
    }        

    .sm-hidden{
        display:none;
    }
    .calculateCashForBikeDiv{
            width:100%;
    }

    @media screen and (min-width: 1124px){
        .sm-hidden{
            display:block;
        }


        .cash4BikeBlock{
            width:50%;
        }        

        .calculateCashForBikeDiv{
            display:flex; 
            column-gap:20px;
            width:80%
        }
    }
</style>

    <div style="width: 80%; margin:auto">
        <p>
            Vous êtes employé d'une entreprise et vous désirez pouvoir utiliser votre salaire brut afin de faire l'acquisition d'un vélo ?<br>
            Vous êtes sur la bonne page !
        </p>
        <br>
        <br>
        <div style="width:100%" class="calculateCashForBikeDiv">
            <div class="cash4BikeBlock">
                <img src="{{ asset('images/public_website_images/Photo_PlanVelo.jpg') }}" alt="image plan velo" width="100%" height="auto" style="border-radius: 20px">        
            </div>    
            <div style="padding-left: 20px" class="cash4BikeBlock">
                <h2 style="margin-top: 0px; font-size: 2em">Comment avoir un plan vélo dans mon entreprise ?</h2>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">1</div>  
                    </div>
                    <div style="padding-left: 20px">
                        <strong>Convainquez votre employeur</strong><br>
                        Voici quelques arguments : utiliser votre salaire brut afin de faire l'acquisition d'un vélo vous permet d'avoir un impact budgétaire réduit d'environ 60 % et ne coûte rien à votre employeur !
                    </div>
                </div>
                <br>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">2</div>  
                    </div>
                    <div style="padding-left: 20px">
                        <strong>Testez nos vélos</strong><br>
                        Une fois le plan effectif, nous viendrons sur le site de votre entreprise afin de vous faire tester nos vélos.
                    </div>
                </div>
                <br>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">3</div>  
                    </div>
                    <div style="padding-left: 20px">
                        <strong>Recevez vos accès afin de pouvoir commander</strong><br>
                        Une fois que vous aurez reçus tous les conseils nécessaires et testé les vélos, vous recevrez un accès à notre plateforme afin de commander le vélo de vos rêves.
                    </div>
                </div>
                <br>
                <div style="width:100%; padding-bottom: 20px">
                    <span style="background-color:#2cb396; color: white; margin-bottom:20px; font-size:20px; font-weight:800; display: table; margin: 0 auto; padding: 0px 20px 0px 20px; border-radius:20px">
                        Demander plus d'information
                    </span>        
                </div>
            </div>    
        </div>
        <br>
        <hr size="3" color="black">
        <br>
    </div>
    <div style="width:80%; margin:auto">
        <h2>Exemple Sacrifice Salarial</h2>
        <div style="width:100%" class="calculateCashForBikeDiv">
            <div class="cash4BikeBlock">
                <strong style='color:#2cb396'>Conway T 300 (3.200 €)</strong>
                <img style="width:100%" src='{{ asset('images/t300.jpg') }}' alt='image conway T 300'>
            </div>
            <div class="cash4BikeBlock" style='border: 1px solid #c7c7c7; border-radius:20px; padding:  20px'>
                <strong style='color:#2cb396'>Hypothèses</strong>
                <p>
                    Employé avec un revenu de 2.500 €/mois<br>
                    Distance domicile - Travail : 5 kms<br>
                    Fréquence d'utilisation du vélo : 3 fois par semaine
                </p>
                <strong style='color:#2cb396'>Résultats</strong>
                <p>
                    Coût du leasing : 133.86 €/mois TVAC<br>
                    Impact sur salaire net : 62.81 €/mois<br>
                    Prime kilométrique : 30 €/mois<br><br>
                    <strong>Impact final :</strong> 32.81 €/mois
                </p>
                <p>
                    Un vélo d'une valeur de 3.200 € ne coûte à cet utilisateur que 32.81 €/mois pendant 36 mois, soit un total de 1.181,6 €.<br>
                    L'employé bénéficie donc d'un vélo haut de gamme ainsi que tous les services liés pour un montant bien inférieur au prix du vélo !
                </p>

            </div>
        </div>
    </div>
    
@stop
