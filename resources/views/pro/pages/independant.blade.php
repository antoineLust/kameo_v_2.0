
@extends('pro.layouts.default')

<script src="https://kit.fontawesome.com/02ec9d769d.js" crossorigin="anonymous"></script>
<style>
    .title_vignette{
        color: #2cb396;
        font-weight: bold;
        font-size: 1.2em;
    }

    .numberCircle {
        border-radius: 50%;
        width: 18px;
        height: 18px;
        background: #2cb396;
        color: white;
        text-align: center;
        font: 16px Arial, sans-serif;
        display: grid
    }
    .grid-template-2{
        display: grid; 
        grid-template-columns: 1fr;
    }
    .grid-1{
        grid-column: span 1;
    }
    @media screen and (min-width: 768px){
        .grid-template-2{
            display: grid; 
            grid-template-columns: 100px 50%;
        }
        .grid-1{
            grid-column: span 1;
        }
        .grid-2{
            grid-column: span 2;
        }
    }

    .flexBlock{
        width:100%;
        margin-bottom:20px;
        border-radius: 10px;
        padding: 20px;
        margin-right:20px;
        padding:20px;
        border : 1px solid #2cb396;

    }        

    .sm-hidden{
        display:none;
    }
    .flexDiv{
        width:100%;
    }

    @media screen and (min-width: 1124px){
        .sm-hidden{
            display:block;
        }


        .flexBlock{
            width:50%;
        }        

        .flexDiv{
            display:flex; 
            column-gap:20px;
            margin:auto;
        }
    }
</style>

@section('content')
    <div style="width: 80%; margin:auto">
        <div class="returnBack" onclick="history.back()">Retour</div>
        <h1 style="color:#2cb396; display: inline-block; margin-left:20px">Je suis un indépendant</h1>

        <div class="flexDiv">
            <div class="text-center flexBlock" style="margin-left: 0">
                <div class="grid-template-2">
                    <div class="grid-1">
                        <img src="{{ asset('images/icons/Icone_ActivitePhysique.jpg') }}" alt="logo plan vélo" width="70px">
                    </div>
                    <div class="grid-1">
                        <span class="title_vignette">Activité physique</span>
                    </div>
                </div>
                <p>
                    Déplacez-vous de manière écologique et sportive grâce au vélo.
                </p>
            </div>
            <div class="text-center flexBlock">
                <div class="grid-template-2">
                    <div class="grid-1">
                        <img src="{{ asset('images/icons/Icone_financement.jpg') }}" alt="logo plan vélo" width="70px">
                    </div>
                    <div class="grid-1">
                        <span class="title_vignette">Avantage fiscal</span>
                    </div>
                </div>
                <p>
                    Votre leasing peut être jusqu'à 100% déductible !
                </p>
            </div>    
            <div class="text-center flexBlock" style="margin-right: 0px">
                <div class="grid-template-2">
                    <div class="grid-1">
                        <img src="{{ asset('images/icons/Icone_ToutInclus.jpg') }}" alt="logo plan vélo" width="70px">
                    </div>
                    <div class="grid-1">
                        <span class="title_vignette">Formule tout compris</span>
                    </div>
                </div>
                <p>
                    Formule tout service compris : vélo, assurance et entretiens
                </p>
            </div>
        </div>

        <br>
        <br>
        <div class="grid sm:grid-flow-col" style="width:100%; ">
            <div style="margin-left: 0px">
                <img src="{{ asset('images/public_website_images/independant.jpg') }}" alt="image employer" width="100%" height="auto" style="border-radius: 20px">
            </div>    
            <div style="padding-left: 20px">
                <h2 style="margin-top: 0px; font-size: 2em">Comment puis-je prendre un vélo en tant qu'indépendant ?</h2>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">1</div>  
                    </div>
                    <div style="padding-left: 20px">
                        <strong>Venez essayer nos vélos</strong><br>
                        Sur base des modèles qui vous plaisent, nous vous enverrons une offre que vous pourrez alors communiquer à votre comptable
                    </div>
                </div>
                <br>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">2</div>  
                    </div>
                    <div style="padding-left: 20px">
                        <strong>Communiquez l'offre à votre comptable</strong><br>
                        En fonction du type de vélo et de l'utilisation que vous en ferez, votre comptable vous indiquera la déductibilité possible de votre leasing
                    </div>
                </div>
                <br>
                <div style="width:100%; display: flex">
                    <div>
                        <div class="numberCircle" style="margin: 0">3</div>  
                    </div>
                    <div style="padding-left: 20px">
                        <strong>Profitez de votre vélo !</strong><br>
                        Dès que vous nous communiquez votre accord, nous vous livrons votre vélo et vous pouvez commencer à en profiter
                    </div>
                </div>
                <br>
                <div class="text-center" style="background-color: #2cb396; padding:20px">
                    <a href="{{ route('contact_pro') }}">
                        <span style="color: white;">Reservez un test de vélo</span>
                    </a>
                </div>    
            </div>    
        </div>
    </div>
@stop
