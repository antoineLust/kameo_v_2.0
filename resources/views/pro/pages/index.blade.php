
@extends('pro.layouts.default')

<script src="https://kit.fontawesome.com/02ec9d769d.js" crossorigin="anonymous"></script>

<style>

    .prev, .next {
        cursor: pointer;
        width: auto;
        color: white;
        font-weight: bold;
        font-size: 18px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
    }

    .title_vignette{
        color: #2cb396;
        font-weight: bold;
        margin:0px
    }

    /* On hover, add a black background color with a little bit see-through */
    .prev:hover, .next:hover {
    background-color: rgba(0,0,0,0.8);
    }

    /* Fading animation */
    .mySlides, .mySlides2 {
        display:none;
        animation-name: fade;
        animation-duration: 1.5s;
    }

    @keyframes fade {
        from {opacity: .4}
        to {opacity: 1}
    }

    .card {
        /* Add shadows to create the "card" effect */
        margin:auto;
        border: 1px solid #2cb396;
        height:100%;
        border-radius: 20px;
    }

    .testimonial_image{
        width: 70px;
        height: auto;
    }
    .testimonial_name{
        color: #2cb396;
        font-size: 1.5em;
        display: block;
    }
    .testmonial_title{
        background-color: #2cb396
    }
    .gallery{
        margin: auto;
        display: grid;
        width: 100%;
        grid-template-columns: repeat(auto-fill, 150px);
    }
    .item{
        max-width:150px;
        text-align: center;
        margin: 20px;
        border-radius: 10px;
    }
    .item img{
        width: 120px;
    }
    .grid{
        display:grid;
        grid-template-columns: repeat(1, 1fr);
        grid-auto-rows: 1fr;
        margin: 0 auto;
        column-gap: 20px;
        row-gap:20px;
    };

    .grid a p{
        padding:0px 20px 0px 20px;
    }

    .grid p{
        padding:0px 20px 0px 20px;
    }


    .grid-1{
        grid-column: span 1;
    }
    .grid-template-2{
        display: grid; 
        grid-template-columns: 1fr;
        padding: 20px 20px 0px 20px;

    }

    @media screen and (min-width: 768px){
        .grid-template-2{
            display: grid; 
            grid-template-columns: 100px 50%;
            padding: 20px 20px 0px 20px;

        }
        .grid a p{
            padding:0px 20px 0px 20px;
        }

        .grid p{
            padding:0px 20px 0px 20px;
        }

        .grid-1{
            grid-column: span 1;
        }
        .grid-2{
            grid-column: span 2;
        }
        .gallery{
            margin: auto;
            display: grid;
            width: 100%;
            grid-template-columns: repeat(auto-fill, 160px);
        }
        .item{
            max-width:200px;
            text-align: center;
            margin: 20px;
            border-radius: 10px;
        }
        .item img{
            width: 120px;
        }
    }

    .testimonial{
        padding: 20px 20px 0px 20px;
    }
    .testimonialItem{
        display: flex; 
        flex-wrap: wrap;            
        padding: 20px 20px 0px 20px;
    }


</style>



@section('content')
    <img src="{{ asset('images/b2b_banner.jpg') }}" width="100%" height="auto">
    <div style="width:80%; margin: auto">
        <h1 style="color:#2cb396">Leasing de vélos pour les entreprises</h1>
        <br>
        
        <div class="grid sm:grid-flow-col" style="width:100%;">
            <div class="card" style="position: relative; padding-bottom:20px;">
                <a href="{{ route('employer') }}">
                    <div class="grid-template-2" style="padding: 20px 20px 0px 20px; margin-top:0px">
                        <div class="grid-1">
                            <img src="{{ asset('images/icons/Icone_Employeur.jpg') }}" alt="logo je suis un employeur" width="70px">
                        </div>
                        <div class="grid-1">
                            <h2 class="title_vignette">Je suis un employeur</h2>
                        </div>    
                    </div>
                    <p style="padding:0px 20px 0px 20px">
                        Grâce à nos solutions innovantes, vous pourrez faire évoluer rapidement la mobilité au sein de votre entreprise afin d'y inclure le vélo.
                    </p>    
                    <span style="background-color:#2cb396; color: white; margin-bottom:20px; font-weight:800; display: table; margin: 0 auto; padding: 0px 20px 0px 20px">
                        NOS SOLUTIONS
                    </span>    
                </a>
            </div>
            <div class="card" style="position: relative; padding-bottom:20px;">
                <a href="{{ route('employee') }}">
                    <div class="grid-template-2" style="margin-top:0px">
                        <div class="grid-1">
                            <img src="{{ asset('images/icons/Icone_Employe.jpg') }}" alt="logo je suis un employeur" width="70px">
                        </div>
                        <div class="grid-1">
                            <h2 class="title_vignette">Je suis un employé</h2>
                        </div>
                    </div>
                    <p>
                        Vous aimeriez profiter des avantages du leasing vélo à moindre coût ? On vous explique comment faire !
                    </p>
                    <div style="width:100%; position: absolute; bottom:0px; padding-bottom: 20px">
                        <span style="background-color:#2cb396; color: white; margin-bottom:20px; font-weight:800; display: table; margin: 0 auto; padding: 0px 20px 0px 20px">
                            CALCUL IMPACT SALAIRE NET
                        </span>        
                    </div>
                    <br>

                </a>
            </div>
            <div class="card" style="position: relative; ">
                <a href="{{ route('independant') }}" >
                    <div class="grid-template-2" style="margin-top:0px;">
                        <div class="grid-1">
                            <img src="{{ asset('images/icons/Icone_Independant.jpg') }}" alt="logo je suis un employeur" width="70px">
                        </div>
                        <div class="grid-1">
                            <h2 class="title_vignette">Je suis un indépendant</h2>
                        </div>
                    </div>
                    <p>
                        Vous aimeriez profiter des avantages du leasing vélo à moindre coût ? On vous explique comment faire !
                    </p>
                    <div style="width:100%; position: absolute; bottom:0px; padding-bottom: 20px">
                        <span style="background-color:#2cb396; color: white; margin-bottom:20px; font-weight:800; display: table; margin: 0 auto; padding: 0px 20px 0px 20px">
                            EN SAVOIR PLUS
                        </span>        
                    </div>

                </a>
            </div>
        </div>
    </div>
    <div class="full-width">
        <h2>Témoignages</h2>
        <div class="testimonial" style="position: relative; margin-top:20px">
            <div style="position: relative, top:0px" class="testimonialItem">
                <div class="testimonial_owner">
                    <img class="round testimonial_image" src="{{ asset('images/testimonial_w1.png') }}" alt="image" style="border:0px">
                    <span class="testimonial_name">Florienne Humblet</span>
                    <p>Elneo<br><i>HR Manager</i></p>
                    <div class="product-rate" style="color:#FFC300">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                    </div>
                </div>    
                <div class="testimonial_description">
                    <p><strong class="testmonial_title">Plan cafétéria</strong><br><br>
                        J’ai choisi et je recommande KAMEO Bikes car ils ont des valeurs proches de celles de notre entreprise (ELNEO). <br>Leur passion pour le vélo est une valeur ajoutée, des conseils par rapport à une société de leasing classique. <br>Les sessions de testing vélos lors de notre event ainsi que la capacité à couvrir la Wallonie et la Flandre, nous ont convaincu. <br>La livraison et l’entretien des vélos sur leur site est aussi une plus-value.
                    </p>
                </div>
            </div>
            <div style="position: absolute; top:0px;" class="testimonialItem">
                <div class="text-center" style='width:33%; margin:auto; min-width:300px'>
                    <img class="round testimonial_image" src="{{ asset('images/testimonial.png') }}" alt="image" style="border:0px">
                    <span class="testimonial_name">Didier Bassleer</span>
                    <p>CE+T Power<br><i>Global Sourcing Director</i></p>
                    <div class="product-rate" style="color:#FFC300">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                    </div>
                </div>
                <div class="testimonial_description">
                    <p><strong class="testmonial_title">Vélos partagés</strong><br><br>
                        L'avis général des utilisateurs est que les vélos sont très bien et agréables à utiliser. <br><br>En tant que "fleet manager" je considère l’application des vélos partagés comme très bien conçue et adaptée, ayant aussi demandé et obtenu de légères améliorations, et ce rapidement.
                    </p>
                </div>
            </div>  
            <div style="position: absolute; top:0px;" class="testimonialItem">
                <div class="testimonial_owner">
                    <img class="round testimonial_image" src="{{ asset('images/testimonial_w2.png') }}" alt="image" style="border:0px">
                    <span class="testimonial_name">Julie Renson</span>
                    <p>Afelio<br><i>HR Partner</i></p>
                    <div class="product-rate" style="color:#FFC300">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                    </div>
                </div>
            
                <div class="testimonial_description">
                    <p><strong class="testmonial_title">Vélos partagés</strong><br>
                    Nous avons contacté KAMEO Bikes à la suite du souhait d'instaurer une mobilité verte chez Afelio. Nous en sommes très content. D’autant plus que leur application MyKameo est facile d'utilisation. Les vélos sont de qualité. Réelle plus-value pour la structure.</p>
                </div>
            </div>
            <div style="position: absolute; top:0px;" class="testimonialItem">
                <div class="testimonial_owner">
                    <img class="round testimonial_image" src="{{ asset('images/testimonial_m2.png') }}" alt="image" style="border:0px">
                    <span class="testimonial_name">Alain Lazzari</span>
                    <p>CE+T Power<br><i>Utilisateur application</i></p>
                    <div class="product-rate" style="color:#FFC300">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
            
                <div class="testimonial_description">
                    <p><strong class="testmonial_title">Vélos partagés</strong><br>
                        Les vélos partagés que j’utilise sont très bien, en bon état et fonctionnent parfaitement. De plus, leur application est très facile d’utilisation.
                    </p>
                </div>
            </div>
            
            <div style="position: absolute; top:0px;" class="testimonialItem">
                <div class="testimonial_owner">
                    <img class="round testimonial_image" src="{{ asset('images/testimonial_m3.png') }}" alt="image" style="border:0px">
                    <span class="testimonial_name">Bertrand Bastin</span>
                    <p>CE+T Power<br><i>Utilisateur application</i></p>
                    <div class="product-rate" style="color:#FFC300">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>
                </div>
            
                <div class="testimonial_description">
                    <p><strong class="testmonial_title">Vélos partagés</strong><br>
                        Les vélos partagés que j’utilise sont très bien, en bon état et fonctionnent parfaitement. De plus, leur application est très facile d’utilisation.<br><br></p>
                    </p>
                </div>
            </div>
        </div>

        <style>
            .testimonial{
                overflow: hidden;
            }
            .testimonial>div{
                transition: all 1s;
            }


            .testimonial_description, .testimonial_owner{
                width:100%;
                text-align:start;
            }
            .testimonial_owner{
                text-align:center;
            }
            @media screen and (min-width: 1024px){
                .testimonial_owner{
                    width:33%;
                }
                .testimonial_description{
                    width:66%;
                }
            }

        </style>
        <script>

            let slides = document.querySelectorAll('.testimonial .testimonialItem');
            let slideSayisi = slides.length;



            for (let index = 0; index < slides.length; index++) {
                const element = slides[index];
                element.style.transform = "translateX("+100*(index)+"%)";
            }
            let loop = 0 + 1000*slideSayisi;

            function goNext(){
                loop++;
                for (let index = 0; index < slides.length; index++) {
                    const element = slides[index];
                    element.style.transform = "translateX("+100*(index-loop%slideSayisi)+"%)";
                }
            }
            setInterval(goNext, 5000);

        </script>
    </div>

    <div style="width:100%; margin: auto; background-color: #2cb396; padding: 20px 0px 20px 0px; margin-top:20px">
        <div style="width:80%; margin:auto">
            <h2 style="color: white">Pourquoi choisir KAMEO Bikes ?</h2>
        </div>
        <div class="grid sm:grid-flow-col" style="width:80%; margin:auto">
            <div class="card" style="background-color: white; width:100%">
                <div class="grid-template-2">
                    <div class="grid-1">
                        <img src="{{ asset('images/icons/Icone_ToutInclus.jpg') }}" alt="logo one stop shop" width="70px">
                    </div>
                    <div class="grid-1">
                        <h3 class="title_vignette">Un seul point de contact</h3>
                    </div>
                </div>
                <p>
                    <strong>KAMEO Bikes</strong> possède son propre stock de vélos assurés, nous nous occupons de tout : 
                    <ul>
                        <li>Montage et livraison des vélos</li>
                        <li>Assurance et assistance</li>
                        <li>Entretiens</li>
                    </ul>
                </p>
            </div>
            <div class="card" style="background-color: white; width:100%">
                <div class="grid-template-2">
                    <div class="grid-1">
                        <img src="{{ asset('images/icons/Icone_Financement.jpg') }}" alt="logo financement" width="70px">
                    </div>
                    <div class="grid-1">
                        <h3 class="title_vignette">Des financements innovants</h3>
                    </div>
                </div>
                <p>
                    <strong>KAMEO Bikes</strong> est à la fin magasin et leaseur, ce qui nous permet de vous proposer de nombreuses solutions de financement: 
                    <ul>
                        <li>Achat traditionnel</li>
                        <li>Leasing mensuel</li>
                        <li>Leasing annuel</li>
                        <li>Leasing pré-financé</li>
                        <li>Renouvellement de votre flotte avant le terme du contrat</li>
                        <li>... et bien d'autres ! </li>
                    </ul>
                </p>
            </div>
            <div class="card" style="background-color: white; width:100%">
                <div class="grid-template-2">
                    <div class="grid-1">
                        <img src="{{ asset('images/icons/Icone_PlateformeGestion.jpg') }}" alt="logo management platform" width="70px">
                    </div>
                    <div class="grid-1">
                        <h3 class="title_vignette left-0">Une plateforme de gestion</h3>
                    </div>
                </div>
                <p style="font-size:0.9em">
                    <strong>KAMEO Bikes</strong> a développé ces 3 dernières années des solutions digitales afin de répondre à vos besoins:
                    <ul>
                        <li>Solution de partage de vélos complètement automatisée</li>
                        <li>Interface de commande de vélos pour vos collaborateurs</li>
                        <li>Suivi des statistiques, commandes ou factures par les fleet managers</li>
                    </ul>
                </p>
            </div>
        </div>
    </div>

    <div class="full-width">
        <h2>Références</h2>
        <div class="gallery">
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/elneo.png') }}" alt="Nos clients - Elneo"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/Imcyse.png') }}" alt="Nos clients - Imcyse"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/VilleDeCharleroi.png') }}" alt="Nos clients - Ville de Charleroi"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/CityDev.png') }}" alt="Nos clients - city dev - brussels"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/actiris.png') }}" alt="Nos clients - Actiris"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/infrabel.png') }}" alt="Nos clients - Infrabel"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/afelio.png') }}" alt="Nos clients - Afelio"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/atradius.png') }}" alt="Nos clients - Atradius"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/galler.png') }}" alt="Nos clients - Galler Chocolatiers"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/siapartners.png') }}" alt="Nos clients - SiaPartners"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/spi.png') }}" alt="Nos clients - SPI"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/Greisch.png') }}" alt="Nos clients - Bureau Greisch"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/agc.png') }}" alt="Nos clients - AGC"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/rayon9.png') }}" alt="Nos clients - Rayon 9"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/elegis.png') }}" alt="Nos clients - Elegis"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/epsylon.png') }}" alt="Nos clients - Epsylon"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/infine.png') }}" alt="Nos clients - In Fine"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/idea.png') }}" alt="Nos clients - IDEA"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/bxlville.png') }}" alt="Nos clients - Ville de Bruxelles"></div>
            <div class="item" style="cursor: default;"><img src="{{ asset('images/clients/prefer.png') }}" alt="Nos clients - Prefer"></div>
        </div>
        </div>


@stop
