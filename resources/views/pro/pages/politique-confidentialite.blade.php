
@extends('pro.layouts.default')

@section('content')

<div class="full-width">

    <h1>Privacy policy</h1>


    At KAMEO Bikes, we are committed to protecting and respecting your privacy.<br>
    
    This notice describes what personal data we collect, how we will use that data and how we keep your personal data safe.<br>
    
    This policy applies to the personal data that we process about you as a natural person. This policy does not apply if we process anonymous or business data.<br>
    
    If you have any question that is not answered here, or if you would like additional information regarding how we collect, use and store your personal data, you may contact us at info@kameobikes.com<br>
    
    KAMEO Bikes is compliant with the EU General Data Protection Regulation (GDPR) as well as with other national data protection laws of the member states.<br>
    
    KAMEO BIKES SPRL has its office registered at Rue de la brasserie, 8, 4000 Liège, Belgium. Company number 0681.879.712, VAT number BE0681.879.712.<br>
    E-mail info@kameobikes.com<br>
    
    <h2>Plugins</h2>
    
    Social media: Facebook, Twitter, Instagram, Linkedin, …<br>
    Our webpages contain social plug-ins of social networks. It is possible that personal data about the visitors to the web page are also acquired via these plug-ins, transmitted to the respective service and connected with the visitor’s respective service. After clicking on a social plug-in, the respective supplier obtains the information that you have visited the corresponding page of our online offer. If you do not want the association of your profile with the data supplier, you must log out from your user account before clicking on one of the social plug-ins. Note that KAMEO Bikes has no influence if and to what extent the respective suppliers acquire personal data.
    Youtube<br>
    On our website, we use the supplier YouTube for the integration of videos and other material.<br>
    
    <h2>Hyperlinks</h2>
    
    Our website has hyperlinks to websites of other suppliers. Upon clicking on these hyperlinks, you will be transferred directly to the website of the other suppliers. Please learn about the privacy and the confidentiality of your personal data directly on these companies website.<br>
    
    <h2>Data security</h2>
    
    KAMEO Bikes is committed to protecting your privacy. We will treat your personal data in all confidentiality. In order to prevent a manipulation or a loss or misuse of your data stored with us, we are taking constant upgrade of our security measure, including integrating new available technologies.<br>
    
    <h2>Data analysis tools</h2>
    
    We use different analysis tools to assure the continuous optimization of our website. For example, we are able to measure the use of our website by visitors. This allows us to better understand our visitors’ interests and to improve our services.<br>
    
    <h2>Transfer of data</h2>
    
    Our site is fully compliant with the GDPR regulation.<br>
    
    We will never transfer your personal data to a third party without your prior agreement.<br>
    
    <h2>Register on our website</h2>
    
    To purchase bikes or book a test ride on our e-shop, you will have to register with your name, address, e-mail, telephone number, and potentially company detail.<br>
    
    In case of purchasing, necessary data will be transferred to the shipping partner in charge of delivering the goods. The same will occur with the financial institution entrusted to execute your payment. These companies may use your data only for the purpose of delivering the goods or for executing the transaction and for no other purposes.<br>
    
</div>


@stop
