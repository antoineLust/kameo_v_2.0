module.exports = {
    files: ["resources/js/**/*.{js,vue}"],
    trailingComma: "es5",
    tabWidth: 4,
    semi: true,
    singleQuote: false,
}
