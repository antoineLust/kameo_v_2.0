<?php

namespace Database\Seeders;

use App\Models\Boxe;
use Illuminate\Database\Seeder;

class BoxesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/boxes.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Boxe::create([
                    'id'                => $data[0],
                    'reference'         => (($data[1] == '') ? NULL : $data[1]),
                    'model'             => (($data[2] == '') ? NULL : $data[2]),
                    'contract_start'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[3]))),
                    'contract_end'      => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[4]))),
                    'amount'            => (($data[5] == '') ? NULL : $data[5]),
                    'billing_group'     => (($data[6] == '') ? NULL : $data[6]),
                    'door'              => (($data[7] == '') ? NULL : $data[7]),
                    'open_date'         => (($data[8] == '') ? NULL : $data[8]),
                    'status'            => (($data[9] == '') ? NULL : $data[9]),
                    'boxes_catalog_id'  => (($data[10] == '') ? NULL : $data[10]),
                    'buildings_id'      => (($data[11] == '') ? NULL : $data[11]),
                    'created_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[12]))),
                    'updated_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[13]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
