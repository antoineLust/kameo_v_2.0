<?php

namespace Database\Seeders;

use App\Models\Accessory_order;
use Illuminate\Database\Seeder;

class AccessoriesOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/accessory_orders.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Accessory_order::create([
                    'id'                        => $data[0],
                    'size'                      => (($data[1] == '') ? NULL : $data[1]),
                    'type'                      => (($data[2] == '') ? NULL : $data[2]),
                    'amount'                    => (($data[3] == '') ? NULL : $data[3]),
                    'description'               => (($data[4] == '') ? NULL : $data[4]),
                    'estimated_delivery_date'   => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[5]))),
                    'delivery_date'             => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[6]))),
                    'billing_type'              => (($data[7] == '') ? NULL : $data[7]),
                    'status'                    => (($data[8] == '') ? 'new' : $data[8]),
                    'grouped_orders_id'         => (($data[9] == '') ? NULL : $data[9]),
                    'accessories_catalog_id'    => (($data[10] == '') ? NULL : $data[10]),
                    'accessories_id'            => (($data[11] == '') ? NULL : $data[11]),
                    'created_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[12]))),
                    'updated_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[13]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
