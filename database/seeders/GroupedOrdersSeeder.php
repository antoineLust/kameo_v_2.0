<?php

namespace Database\Seeders;

use App\Models\Grouped_order;
use Illuminate\Database\Seeder;

class GroupedOrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/grouped_orders.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Grouped_order::create([
                    'id'            => $data[0],
                    'companies_id'  => (($data[1] == '') ? NULL : $data[1]),
                    'users_id'      => (($data[2] == '') ? NULL : $data[2]),
                    'created_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[3]))),
                    'updated_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[4]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
