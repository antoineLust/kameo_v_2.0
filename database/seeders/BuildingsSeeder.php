<?php

namespace Database\Seeders;

use App\Models\Building;
use Illuminate\Database\Seeder;

class BuildingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/buildings.csv"), "r");

        $firstline = true;
        $dateTime = new \DateTime();
        $dateTime->format('d/m/Y H:i:s');
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                if ($data[4] != '') {
                    Building::create([
                        'id'            => $data[0],
                        'reference'     => (($data[1] == '') ? NULL : $data[1]),
                        'description'   => (($data[2] == '') ? '' : $data[2]),
                        'address'       => (($data[3] == '') ? '' : $data[3]),
                        'companies_id'  => (($data[4] == '') ? NULL : $data[4]),
                        'created_at'    => (($data[5] == '0000-00-00 00:00:00') ? $dateTime : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[5])))),
                        'updated_at'    => (($data[6] == '0000-00-00 00:00:00') ? $dateTime : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[6])))),
                    ]);
                }
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
