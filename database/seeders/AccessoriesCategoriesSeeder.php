<?php

namespace Database\Seeders;

use App\Models\Accessory_category;
use Illuminate\Database\Seeder;

class AccessoriesCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/accessory_categories.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Accessory_category::create([
                    'id'                        => $data[0],
                    'name'                      => (($data[1] == '') ? NULL : $data[1]),
                    'name'                      => (($data[1] == '') ? NULL : $data[1]),
                    'parent_category'           => (($data[2] == '') ? NULL : $data[2]),
                    'created_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[3]))),
                    'updated_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[4]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
