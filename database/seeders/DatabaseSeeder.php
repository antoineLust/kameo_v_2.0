<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CompaniesAddressesSeeder::class);
        $this->call(CompaniesSeeder::class);
        $this->call(CompaniesContactsSeeder::class);
        $this->call(UsersAddressesSeeder::class);
        $this->call(UsersWorksAddressesSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(AccessoriesCategoriesSeeder::class);
        $this->call(TempAccessoriesCategoriesSeeder::class);
        $this->call(AccessoriesCatalogSeeder::class);
        $this->call(AccessoriesSeeder::class);
        $this->call(BoxeCatalogSeeder::class);
        $this->call(BuildingsSeeder::class);
        $this->call(BoxesSeeder::class);
        $this->call(GroupedOrdersSeeder::class);
        $this->call(AccessoriesOrderSeeder::class);
        $this->call(BoxeOrdersSeeder::class);
        $this->call(BikesCatalogSeeder::class);
        $this->call(BikesSeeder::class);
        $this->call(BikesOrdersSeeder::class);
        $this->call(BikesOrderableSeeder::class);
        $this->call(ConditionsSeeder::class);
        $this->call(AccessoriesOrderableSeeder::class);
        $this->call(SharedBikesConditionsSeeder::class);
        $this->call(BillsSeeder::class);
        $this->call(BillsDetailsSeeder::class);
        $this->call(ServiceEntretiensSeeder::class);
        $this->call(EntretiensSeeder::class);
        $this->call(ExternalBikesSeeder::class);
        $this->call(EntretiensDetailsSeeder::class);
        $this->call(EanAndProviderCodesSeeder::class);
        $this->call(FeedbacksSeeder::class);
        $this->call(OffersSeeder::class);
        $this->call(OffersDetailsSeeder::class);
        $this->call(PlanningsSeeder::class);
        $this->call(ReservationsSeeder::class);
        // Rangé en ordre
    }
}
