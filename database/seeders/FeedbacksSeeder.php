<?php

namespace Database\Seeders;

use App\Models\Feedback;
use Illuminate\Database\Seeder;

class FeedbacksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/feedbacks.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Feedback::create([
                    'id'                => $data[0],
                    'not'               => (($data[1] == '') ? NULL : $data[1]),
                    'comment'           => (($data[2] == '') ? NULL : $data[2]),
                    'entretien'         => (($data[3] == '') ? NULL : $data[3]),
                    'status'            => (($data[4] == '') ? NULL : $data[4]),
                    'reservations_id'   => (($data[5] == '') ? NULL : $data[5]),
                    'created_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[6]))),
                    'updated_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[7]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
