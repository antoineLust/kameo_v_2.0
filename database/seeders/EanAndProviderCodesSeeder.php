<?php

namespace Database\Seeders;

use App\Models\Ean_end_provider_code;
use Illuminate\Database\Seeder;

class EanAndProviderCodesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/ean_and_provider_codes.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Ean_end_provider_code::create([
                    'id'            => $data[0],
                    'item_type'     => (($data[1] == '') ? NULL : $data[1]),
                    'item_id'       => (($data[2] == '') ? NULL : $data[2]),
                    'size'          => (($data[3] == '') ? NULL : $data[3]),
                    'ean_code'      => (($data[4] == '') ? NULL : $data[4]),
                    'provider_code' => (($data[5] == '') ? NULL : $data[5]),
                    'created_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[6]))),
                    'updated_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[7]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
