<?php

namespace Database\Seeders;

use App\Models\Accessory;
use Illuminate\Database\Seeder;

class AccessoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/accessories.csv"), "r");

        $firstline = true;
        $dateTime = new \DateTime();
        $dateTime->format('d/m/Y H:i:s');
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Accessory::create([
                    'id'                        => $data[0],
                    'size'                      => (($data[1] == '') ? NULL : $data[1]),
                    'contract_type'             => (($data[2] == '') ? '' : $data[2]),
                    'contract_start'            => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[3]))),
                    'contract_end'              => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[4]))),
                    'leasing_price'             => (($data[5] == '') ? NULL : $data[5]),
                    'selling_date'              => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[6]))),
                    'selling_price'             => (($data[7] == '') ? NULL : $data[7]),
                    'estimated_delivery_date'   => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[8]))),
                    'delivery_date'             => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[9]))),
                    'status'                    => (($data[10] == '') ? NULL : $data[10]),
                    'users_id'                  => (($data[11] == '') ? NULL : $data[11]),
                    'bikes_id'                  => (($data[12] == '') ? NULL : $data[12]),
                    'accessories_catalog_id'    => (($data[13] == '') ? NULL : $data[13]),
                    'companies_id'              => (($data[14] == '') ? NULL : $data[14]),
                    'stolen_date'               => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[15]))),
                    'reimbursement_insurance'   => (($data[16] == '') ? NULL : $data[16]),
                    'created_at'                => $dateTime,
                    'updated_at'                => $dateTime,
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
