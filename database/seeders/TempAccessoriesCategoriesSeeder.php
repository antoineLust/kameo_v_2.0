<?php

namespace Database\Seeders;

use App\Models\Temp_accessory_category;
use Illuminate\Database\Seeder;

class TempAccessoriesCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/temp_accessories_categories.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Temp_accessory_category::create([
                    'id'            => $data[0],
                    'main'          => (($data[1] == '') ? NULL : $data[1]),
                    'main_id'       => (($data[2] == '') ? NULL : $data[2]),
                    'sub_1'         => (($data[3] == '') ? NULL : $data[3]),
                    'sub_1_id'      => (($data[4] == '') ? NULL : $data[4]),
                    'sub_2'         => (($data[5] == '') ? NULL : $data[5]),
                    'sub_2_id'      => (($data[6] == '') ? NULL : $data[6]),
                    'sub_3'         => (($data[7] == '') ? NULL : $data[7]),
                    'sub_3_id'      => (($data[8] == '') ? NULL : $data[8]),
                    'sub_4'         => (($data[9] == '') ? NULL : $data[9]),
                    'sub_4_id'      => (($data[10] == '') ? NULL : $data[10]),
                    'created_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[11]))),
                    'updated_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[12]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
