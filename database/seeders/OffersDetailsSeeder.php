<?php

namespace Database\Seeders;

use App\Models\Offer_detail;
use Illuminate\Database\Seeder;

class OffersDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/offers_details.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Offer_detail::create([
                    'id'                    => $data[0],
                    'item_type'             => (($data[1] == '') ? NULL : $data[1]),
                    'item_id'               => (($data[2] == '') ? NULL : $data[2]),
                    'contract_type'         => (($data[3] == '') ? NULL : $data[3]),
                    'contract_amount'       => (($data[4] == '') ? NULL : $data[4]),
                    'installation_amount'   => (($data[5] == '') ? NULL : $data[5]),
                    'contract_duration'     => (($data[6] == '') ? NULL : $data[6]),
                    'size'                  => (($data[7] == '') ? NULL : $data[7]),
                    'offers_id'             => (($data[8] == '') ? NULL : $data[8]),
                    'created_at'            => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[9]))),
                    'updated_at'            => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[10]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
