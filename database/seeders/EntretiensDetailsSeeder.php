<?php

namespace Database\Seeders;

use App\Models\Entretien_detail;
use Illuminate\Database\Seeder;

class EntretiensDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/entretiens_details.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Entretien_detail::create([
                    'id'                    => $data[0],
                    'item_type'             => (($data[1] == '') ? NULL : $data[1]),
                    'item_id'               => (($data[2] == '') ? NULL : $data[2]),
                    'duration'              => (($data[3] == '') ? NULL : $data[3]),
                    'amount'                => (($data[4] == '') ? NULL : $data[4]),
                    'description'           => (($data[5] == '') ? NULL : $data[5]),
                    'accessory_stock_id'    => (($data[6] == '') ? NULL : $data[6]),
                    'accessory_size'        => (($data[7] == '') ? NULL : $data[7]),
                    'entretiens_id'         => (($data[8] == '') ? NULL : $data[8]),
                    'details'               => NULL,
                    'created_at'            => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[9]))),
                    'updated_at'            => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[10]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
