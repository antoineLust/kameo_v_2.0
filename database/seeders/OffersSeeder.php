<?php

namespace Database\Seeders;

use App\Models\Offer;
use Illuminate\Database\Seeder;

class OffersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/offers.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Offer::create([
                    'id'                => $data[0],
                    'title'             => (($data[1] == '') ? NULL : $data[1]),
                    'description'       => (($data[2] == '') ? NULL : $data[2]),
                    'probability'       => (($data[3] == '') ? NULL : $data[3]),
                    'global_discount'   => (($data[4] == '') ? NULL : $data[4]),
                    'validity_date'     => (($data[5] == '0000-00-00') ? NULL : (($data[5] == '') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[5]))))),
                    'file_name'         => (($data[6] == '') ? NULL : $data[6]),
                    'step'              => (($data[7] == '') ? NULL : $data[7]),
                    'status'            => (($data[8] == '') ? NULL : $data[8]),
                    'employee_id'       => (($data[9] == '') ? NULL : $data[9]),
                    'companies_id'      => (($data[10] == '') ? 12 : $data[10]),
                    'users_id'          => (($data[11] == '') ? NULL : $data[11]),
                    'created_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[12]))),
                    'updated_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[13]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
