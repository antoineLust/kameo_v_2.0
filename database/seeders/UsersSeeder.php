<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/users.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                if ($data[13] != '') {
                    User::create([
                        'id'                        => $data[0],
                        'firstname'                 => (($data[1] == '') ? '' : $data[1]),
                        'lastname'                  => (($data[2] == '') ? '' : $data[2]),
                        'email'                     => (($data[3] == '') ? '' : $data[3]),
                        'email_verified_at'         => (($data[4] == '') ? NULL : $data[4]),
                        'password'                  => (($data[5] == '$2y$10$M/TQiCKG4n6mm/NJtSiLledqFMTbDj2DxOWdRwf3CTLwl36dU/3mO') ? NULL : $data[5]),
                        'phone'                     => (($data[6] == '') ? NULL : $data[6]),
                        'employee'                  => (($data[7] == '') ? 0 : $data[7]),
                        'employee_role'             => (($data[8] == '') ? NULL : $data[8]),
                        'size'                      => (($data[9] == '') ? NULL : $data[9]),
                        'permission'                => (($data[10] == '') ? NULL : $data[10]),
                        'status'                    => (($data[11] == '') ? NULL : $data[11]),
                        'avatar'                    => (($data[12] == '') ? NULL : $data[12]),
                        'companies_id'              => (($data[13] == '') ? NULL : $data[13]),
                        'users_addresses_id'        => (($data[14] == '') ? NULL : $data[14]),
                        'users_works_addresses_id'  => (($data[15] == '') ? NULL : $data[15]),
                        'created_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[17]))),
                        'updated_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[18]))),
                    ]);
                }
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
