<?php

namespace Database\Seeders;

use App\Models\Entretien;
use Illuminate\Database\Seeder;

class EntretiensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/entretiens.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Entretien::create([
                    'id'                => $data[0],
                    'external_bike'     => (($data[1] == '') ? NULL : $data[1]),
                    'date'              => (($data[2] == '0000-00-00 00:00:00') ? NULL : (($data[2] == '') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[2]))))),
                    'address'           => (($data[3] == '') ? NULL : $data[3]),
                    'status'            => (($data[4] == '') ? NULL : $data[4]),
                    'planned_date'      => (($data[5] == '0000-00-00 00:00:00') ? NULL : (($data[5] == '') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[5]))))),
                    'planned_time'      => (($data[6] == '') ? NULL : $data[6]),
                    'start_time'        => (($data[7] == '') ? NULL : $data[7]),
                    'end_time'          => (($data[8] == '') ? NULL : $data[8]),
                    'description'       => (($data[9] == '') ? NULL : $data[9]),
                    'number_entretien'  => (($data[11] == '') ? 1 : $data[11]),
                    'client_warned'     => (($data[12] == '') ? NULL : $data[12]),
                    'avoid_billing'     => (($data[13] == '') ? NULL : $data[13]),
                    'leasing_to_bill'   => (($data[14] == '') ? NULL : $data[14]),
                    'bikes_id'          => (($data[15] == '') ? NULL : $data[15]),
                    'assembled'         => (($data[16] == '') ? NULL : $data[16]),
                    'created_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[17]))),
                    'updated_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[18]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
