<?php

namespace Database\Seeders;

use App\Models\Shared_bikes_condition;
use Illuminate\Database\Seeder;

class SharedBikesConditionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/shared_bikes_conditions.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Shared_bikes_condition::create([
                    'id'                            => $data[0],
                    'company_id'                    => (($data[1] == '') ? NULL : $data[1]),
                    'users_id'                      => (($data[2] == '') ? NULL : $data[2]),
                    'bikes_id'                      => (($data[3] == '') ? NULL : $data[3]),
                    'buildings_id'                  => (($data[4] == '') ? NULL : $data[4]),
                    'days'                          => (($data[5] == '') ? NULL : $data[5]),
                    'max_days_before_reservation'   => (($data[6] == '') ? NULL : $data[6]),
                    'max_resevation_duration'       => (($data[7] == '') ? NULL : $data[7]),
                    'week_end'                      => (($data[8] == '') ? NULL : $data[8]),
                    'start_taking_time'             => (($data[9] == '') ? NULL : $data[9]),
                    'end_taking_time'               => (($data[10] == '') ? NULL : $data[10]),
                    'start_delivery_time'           => (($data[11] == '') ? NULL : $data[11]),
                    'end_delivery_time'             => (($data[12] == '') ? NULL : $data[12]),
                    'created_at'                    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[13]))),
                    'updated_at'                    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[14]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
