<?php

namespace Database\Seeders;

use App\Models\Bikes_orderable;
use Illuminate\Database\Seeder;

class BikesOrderableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/bikes_orderable.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Bikes_orderable::create([
                    'id'            => $data[0],
                    'catalog_id'    => (($data[1] == '') ? NULL : $data[1]),
                    'company_id'    => (($data[2] == '') ? NULL : $data[2]),
                    'created_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[3]))),
                    'updated_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[4]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
