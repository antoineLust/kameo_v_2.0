<?php

namespace Database\Seeders;

use App\Models\Bill;
use Illuminate\Database\Seeder;

class BillsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/bills.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Bill::create([
                    'id'                        => $data[0],
                    'out_id'                    => (($data[1] == '') ? NULL : $data[1]),
                    'beneficiary_company'       => (($data[2] == '') ? NULL : $data[2]),
                    'date'                      => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[4]))),
                    'amount_htva'               => (($data[4] == '') ? NULL : $data[4]),
                    'amount_tvac'               => (($data[5] == '') ? NULL : $data[5]),
                    'billing_group'             => (($data[6] == '') ? NULL : $data[6]),
                    'communication'             => (($data[7] == '') ? NULL : $data[7]),
                    'file_name'                 => (($data[8] == '') ? '' : $data[8]),
                    'invoice_sent'              => (($data[9] == '') ? NULL : $data[9]),
                    'invoice_sent_date'         => (($data[10] == '0000-00-00 00:00:00') ? NULL : (($data[10] == '') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[10]))))),
                    'invoice_paid'              => (($data[11] == '') ? NULL : $data[11]),
                    'invoice_paid_date'         => (($data[12] == '0000-00-00 00:00:00') ? NULL : (($data[12] == '') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[12]))))),
                    'invoice_limit_paid_date'   => (($data[13] == '0000-00-00 00:00:00') ? NULL : (($data[13] == '') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[13]))))),
                    'accounting'                => (($data[14] == '') ? true : $data[14]),
                    'type'                      => (($data[15] == '') ? NULL : $data[15]),
                    'payement'                  => (($data[16] == '') ? NULL : $data[16]),
                    'credit_notes_id'           => (($data[17] == '') ? NULL : $data[17]),
                    'users_id'                  => (($data[18] == '') ? NULL : $data[18]),
                    'companies_id'              => (($data[19] == '') ? NULL : $data[19]),
                    'created_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[20]))),
                    'updated_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[21]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
