<?php

namespace Database\Seeders;

use App\Models\Reservation;
use Illuminate\Database\Seeder;

class ReservationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/reservations.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                if ($data[9] != '') {
                    Reservation::create([
                        'id'                => $data[0],
                        'start'             => (($data[1] == '0000-00-00 00:00:00') ? NULL : (($data[1] == '') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[1]))))),
                        'end'               => (($data[2] == '') ? NULL : $data[2]),
                        'building_start'    => (($data[3] == '') ? NULL : $data[3]),
                        'building_end'      => (($data[4] == '') ? NULL : $data[4]),
                        'extension'         => (($data[5] == '') ? NULL : $data[5]),
                        'initial_end_date'  => (($data[6] == '') ? NULL : $data[6]),
                        'status'            => (($data[7] == '') ? 'No box' : $data[7]),
                        'bikes_id'          => (($data[8] == '') ? NULL : $data[8]),
                        'users_id'          => (($data[9] == '') ? NULL : $data[9]),
                        'created_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[10]))),
                        'updated_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[11]))),
                    ]);
                }
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
