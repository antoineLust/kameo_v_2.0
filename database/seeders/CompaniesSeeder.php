<?php

namespace Database\Seeders;

use App\Models\Company;
use Illuminate\Database\Seeder;

class CompaniesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/companies.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Company::create([
                    'id'                        => $data[0],
                    'name'                      => (($data[1] == '') ? NULL : $data[1]),
                    'logo'                      => (($data[2] == '') ? NULL : $data[2]),
                    'vat_number'                => (($data[3] == '') ? NULL : $data[3]),
                    'billing_group'             => (($data[4] == '') ? 1 : $data[4]),
                    'type'                      => (($data[5] == '') ? NULL : $data[5]),
                    'audience'                  => (($data[6] == '') ? NULL : $data[6]),
                    'aquisition'                => (($data[7] == '') ? NULL : $data[7]),
                    'status'                    => (($data[8] == '') ? NULL : $data[8]),
                    'companies_addresses_id'    => (($data[9] == '') ? NULL : $data[9]),
                    'created_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[10]))),
                    'updated_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[11]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
