<?php

namespace Database\Seeders;

use App\Models\Planning;
use Illuminate\Database\Seeder;

class PlanningsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/plannings.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Planning::create([
                    'id'                    => $data[0],
                    'date'                  => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[1]))),
                    'step'                  => (($data[2] == '') ? NULL : $data[2]),
                    'address'               => (($data[3] == '') ? NULL : $data[3]),
                    'item_type'             => (($data[4] == '') ? NULL : $data[4]),
                    'item_id'               => (($data[5] == '') ? NULL : $data[5]),
                    'moving_time'           => (($data[6] == '') ? NULL : $data[6]),
                    'execution_time'        => (($data[7] == '') ? NULL : $data[7]),
                    'planned_start_hour'    => (($data[8] == '') ? NULL : $data[8]),
                    'planned_end_hour'      => (($data[9] == '') ? NULL : $data[9]),
                    'real_start_hour'       => (($data[10] == '') ? NULL : $data[10]),
                    'real_end_hour'         => (($data[11] == '') ? NULL : $data[11]),
                    'description'           => (($data[12] == '') ? NULL : $data[12]),
                    'status'                => (($data[13] == '') ? NULL : $data[13]),
                    'created_at'            => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[14]))),
                    'updated_at'            => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[15]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
