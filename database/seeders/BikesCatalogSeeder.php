<?php

namespace Database\Seeders;

use App\Models\Bikes_Catalog;
use Illuminate\Database\Seeder;

class BikesCatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/bikes_catalog.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Bikes_Catalog::create([
                    'id'                => $data[0],
                    'brand'             => (($data[1] == '') ? NULL : $data[1]),
                    'model'             => (($data[2] == '') ? NULL : $data[2]),
                    'frame_type'        => (($data[3] == '') ? NULL : $data[3]),
                    'utilisation'       => (($data[4] == '') ? NULL : $data[4]),
                    'electric'          => (($data[5] == '') ? NULL : $data[5]),
                    'motor'             => (($data[6] == '') ? NULL : $data[6]),
                    'battery'           => (($data[7] == '') ? NULL : $data[7]),
                    'display_device'    => (($data[8] == '') ? NULL : $data[8]),
                    'transmission'      => (($data[9] == '') ? NULL : $data[9]),
                    'sizes'             => (($data[10] == '') ? 0 : $data[10]),
                    'season'            => (($data[11] == '') ? NULL : $data[11]),
                    'buying_price'      => (($data[12] == '') ? NULL : $data[12]),
                    'price_htva'        => (($data[13] == '') ? NULL : $data[13]),
                    'minimal_stock'     => (($data[14] == '') ? NULL : $data[14]),
                    'optimal_stock'     => (($data[15] == '') ? NULL : $data[15]),
                    'description'       => (($data[16] == '') ? NULL : $data[16]),
                    'status'            => (($data[17] == '') ? NULL : $data[17]),
                    'display'           => (($data[18] == '') ? NULL : $data[18]),
                    'wheel_size'        => (($data[19] == '') ? NULL : $data[19]),
                    'fork'              => (($data[20] == '') ? NULL : $data[20]),
                    'absorber'          => (($data[21] == '') ? NULL : $data[21]),
                    'gears'             => (($data[22] == '') ? NULL : $data[22]),
                    'shifter'           => (($data[23] == '') ? NULL : $data[23]),
                    'cassette'          => (($data[24] == '') ? NULL : $data[24]),
                    'brakes'            => (($data[25] == '') ? NULL : $data[25]),
                    'front_brakes_size' => (($data[26] == '') ? NULL : $data[26]),
                    'back_brakes_size'  => (($data[27] == '') ? NULL : $data[27]),
                    'weight'            => (($data[28] == '') ? NULL : $data[28]),
                    'created_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[29]))),
                    'updated_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[30]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
