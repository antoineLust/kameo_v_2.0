<?php

namespace Database\Seeders;

use App\Models\Company_contact;
use Illuminate\Database\Seeder;

class CompaniesContactsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/companies_contacts.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Company_contact::create([
                    'id'            => $data[0],
                    'firstname'     => (($data[1] == '') ? '' : $data[1]),
                    'lastname'      => (($data[2] == '') ? '' : $data[2]),
                    'email'         => (($data[3] == '') ? NULL : $data[3]),
                    'phone'         => (($data[4] == '') ? '' : $data[4]),
                    'function'      => (($data[5] == '') ? NULL : $data[5]),
                    'type'          => ((($data[6] == 'En copie pour facture') ? 'ccBilling' : $data[6]) || ($data[6] == '') ? 'contact' : $data[6]),
                    'companies_id'  => (($data[7] == '') ? NULL : $data[7]),
                    'created_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[8]))),
                    'updated_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[9]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
