<?php

namespace Database\Seeders;

use App\Models\Boxes_catalog;
use Illuminate\Database\Seeder;

class BoxeCatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/boxes_catalog.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Boxes_catalog::create([
                    'id'                    => $data[0],
                    'model'                 => (($data[1] == '') ? NULL : $data[1]),
                    'production_price'      => (($data[2] == '') ? 500 : $data[2]),
                    'installation_price'    => (($data[3] == '') ? NULL : $data[3]),
                    'location_price'        => (($data[4] == '') ? NULL : $data[4]),
                    'created_at'            => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[5]))),
                    'updated_at'            => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[6]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
