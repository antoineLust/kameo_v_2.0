<?php

namespace Database\Seeders;

use App\Models\Bill_detail;
use Illuminate\Database\Seeder;

class BillsDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/bill_details.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Bill_detail::create([
                    'id'            => $data[0],
                    'type'          => (($data[1] == '') ? NULL : $data[1]),
                    'item_id'       => (($data[2] == '') ? NULL : $data[2]),
                    'comment'       => (($data[3] == '') ? NULL : $data[3]),
                    'start'         => (($data[4] == '') ? NULL : $data[4]),
                    'end'           => (($data[5] == '') ? NULL : $data[5]),
                    'amount_htva'   => (($data[6] == '') ? NULL : $data[6]),
                    'amount_tvac'   => (($data[7] == '') ? NULL : $data[7]),
                    'bills_id'      => (($data[8] == '') ? NULL : $data[8]),
                    'entretiens_id' => (($data[9] == '') ? NULL : $data[9]),
                    'created_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[10]))),
                    'updated_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[11]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
