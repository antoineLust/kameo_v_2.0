<?php

namespace Database\Seeders;

use App\Models\Bike_order;
use Illuminate\Database\Seeder;

class BikesOrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/bike_orders.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Bike_order::create([
                    'id'                        => $data[0],
                    'size'                      => (($data[1] == '') ? NULL : $data[1]),
                    'color'                     => (($data[2] == '') ? NULL : $data[2]),
                    'remark'                    => (($data[3] == '') ? '' : $data[3]),
                    'amount'                    => (($data[4] == '') ? 0 : $data[4]),
                    'type'                      => (($data[5] == '') ? NULL : $data[5]),
                    'estimated_delivery_date'   => (($data[6] == '') ? NULL : $data[6]),
                    'delivery_date'             => (($data[7] == '') ? NULL : $data[7]),
                    'comment'                   => (($data[8] == '') ? NULL : $data[8]),
                    'status'                    => (($data[9] == '') ? NULL : $data[9]),
                    'bikes_id'                  => (($data[10] == '') ? NULL : $data[10]),
                    'grouped_orders_id'         => (($data[11] == '') ? NULL : $data[11]),
                    'bikes_catalog_id'          => (($data[12] == '') ? NULL : $data[12]),
                    'created_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[13]))),
                    'updated_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[14]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
