<?php

namespace Database\Seeders;

use App\Models\Service_entretien;
use Illuminate\Database\Seeder;

class ServiceEntretiensSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/services_entretiens.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Service_entretien::create([
                    'id'            => $data[0],
                    'category'      => (($data[1] == '') ? NULL : $data[1]),
                    'description'   => (($data[2] == '') ? NULL : $data[2]),
                    'minute'        => (($data[3] == '') ? NULL : $data[3]),
                    'price_vat'     => (($data[4] == '') ? NULL : $data[4]),
                    'created_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[5]))),
                    'updated_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[6]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
