<?php

namespace Database\Seeders;

use App\Models\Condition;
use Illuminate\Database\Seeder;

class ConditionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/conditions.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Condition::create([
                    'id'                            => $data[0],
                    'access'                        => (($data[1] == '') ? NULL : $data[1]),
                    'companies_id'                  => (($data[2] == '') ? NULL : $data[2]),
                    'cafetaria_billing_type'        => (($data[3] == '') ? NULL : $data[3]),
                    'cafetaria_tva'                 => (($data[4] == '') ? NULL : $data[4]),
                    'cafetaria_discount_selling'    => (($data[5] == '') ? NULL : $data[5]),
                    'cafetaria_discount_leasing'    => (($data[6] == '') ? NULL : $data[6]),
                    'created_at'                    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[7]))),
                    'updated_at'                    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[8]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
