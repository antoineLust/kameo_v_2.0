<?php

namespace Database\Seeders;

use App\Models\Company_address;
use Illuminate\Database\Seeder;

class CompaniesAddressesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/companies_addresses.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Company_address::create([
                    'id'            => $data[0],
                    'street'        => (($data[1] == '') ? NULL : $data[1]),
                    'number'        => (($data[2] == '') ? NULL : $data[2]),
                    'zip'           => (($data[3] == '') ? NULL : $data[3]),
                    'city'          => (($data[4] == '') ? NULL : $data[4]),
                    'country'       => (($data[5] == '') ? NULL : $data[5]),
                    'condensed'     => (($data[6] == '') ? NULL : $data[6]),
                    'latitude'      => floatval($data[7]),
                    'longitude'     => floatval($data[8]),
                    'created_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[9]))),
                    'updated_at'    => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[10]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
