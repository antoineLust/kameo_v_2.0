<?php

namespace Database\Seeders;

use App\Models\Boxe_order;
use Illuminate\Database\Seeder;

class BoxeOrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/boxe_orders.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Boxe_order::create([
                    'id'                        => $data[0],
                    'installation_price'        => (($data[1] == '') ? NULL : $data[1]),
                    'amount'                    => (($data[2] == '') ? NULL : $data[2]),
                    'estimated_delivery_date'   => (($data[3] == '') ? NULL : $data[3]),
                    'status'                    => (($data[4] == '') ? NULL : $data[4]),
                    'boxes_id'                  => (($data[5] == '') ? NULL : $data[5]),
                    'boxes_catalog_id'          => (($data[6] == '') ? NULL : $data[6]),
                    'grouped_orders_id'         => (($data[7] == '') ? NULL : $data[7]),
                    'created_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[8]))),
                    'updated_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[9]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
