<?php

namespace Database\Seeders;

use App\Models\Bike;
use Illuminate\Database\Seeder;

class BikesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/bikes.csv"), "r");

        $firstline = true;
        $date = date("d/m/Y");
        $dateTime = new \DateTime();
        $dateTime->format('d/m/Y H:i:s');
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Bike::create([
                    'id'                                        => $data[0],
                    'client_name'                               => (($data[1] == '') ? NULL : $data[1]),
                    'frame_number'                              => (($data[2] == '') ? '' : $data[2]),
                    'size'                                      => (($data[3] == '') ? 'unique' : $data[3]),
                    'color'                                     => (($data[4] == '') ? NULL : $data[4]),
                    'contract_type'                             => (($data[5] == '') ? NULL : $data[5]),
                    'contract_start'                            => (($data[6] == '0000-00-00') ? NULL : (($data[6] == '') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[6]))))),
                    'contract_end'                              => (($data[7] == '0000-00-00') ? NULL : (($data[7] == '') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[7]))))),
                    'estimated_delivery_date'                   => (($data[8] == '0000-00-00') ? NULL : (($data[8] == '') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[8]))))),
                    'delivery_date'                             => (($data[9] == '0000-00-00') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[9])))),
                    'selling_date'                              => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[10]))),
                    'frame_reference'                           => (($data[11] == '') ? NULL : $data[11]),
                    'key_reference'                             => (($data[12] == '') ? NULL : $data[12]),
                    'locker_reference'                          => (($data[13] == '') ? NULL : $data[13]),
                    'plate_number'                              => (($data[14] == '') ? NULL : $data[14]),
                    'billing_type'                              => (($data[15] == '') ? NULL : $data[15]),
                    'leasing_price'                             => (($data[16] == '') ? NULL : $data[16]),
                    'selling_price'                             => (($data[17] == '') ? NULL : $data[17]),
                    'usable'                                    => (($data[18] == '') ? NULL : $data[18]),
                    'insurance'                                 => (($data[19] == '') ? NULL : $data[19]),
                    'insurance_individual'                      => ('N'),
                    'insurance_civil_responsibility'            => ('N'),
                    'insurance_civil_responsibility_contract'   => (($data[22] == '') ? NULL : $data[22]),
                    'bike_price'                                => (($data[23] == '') ? NULL : $data[23]),
                    'bike_buying_date'                          => (($data[24] == '0000-00-00') ? NULL : date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[24])))),
                    'billing_group'                             => (($data[25] == '') ? NULL : $data[25]),
                    'gps_id'                                    => (($data[26] == '') ? NULL : $data[26]),
                    'localisation'                              => (($data[27] == '') ? NULL : $data[27]),
                    'address'                                   => (($data[28] == '') ? NULL : $data[28]),
                    'comment_billing'                           => (($data[29] == '') ? NULL : $data[29]),
                    'stolen_date'                               => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[30]))),
                    'reimbursement_insurance'                   => (($data[31] == '') ? NULL : $data[31]),
                    'comment'                                   => (($data[32] == '') ? NULL : $data[32]),
                    'display_group'                             => (($data[33] == '') ? '1generic' : $data[33]),
                    'status'                                    => (($data[34] == '') ? NULL : $data[34]),
                    'assembled'                                 => (($data[35] == '') ? NULL : $data[35]),
                    'users_id'                                  => (($data[36] == '') ? NULL : $data[36]),
                    'bikes_catalog_id'                          => (($data[37] == '') ? NULL : $data[37]),
                    'companies_id'                              => (($data[38] == '') ? NULL : $data[38]),
                    'created_at'                                => $dateTime,
                    'updated_at'                                => $dateTime,
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
