<?php

namespace Database\Seeders;

use App\Models\Accessories_catalog;     ## Model de la table
use Illuminate\Database\Seeder;

class AccessoriesCatalogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/accessories_catalog.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                Accessories_catalog::create([
                    'id'                        => $data[0],
                    'brand'                     => (($data[1] == '') ? NULL : $data[1]),
                    'model'                     => (($data[2] == '') ? '' : $data[2]),
                    'sizes'                     => (($data[3] == '') ? NULL : $data[3]),
                    'buying_price'              => (($data[4] == '') ? NULL : $data[4]),
                    'price_htva'                => (($data[5] == '') ? NULL : $data[5]),
                    'description'               => (($data[6] == '') ? NULL : $data[6]),
                    'provider'                  => (($data[7] == '') ? '' : $data[7]),
                    'minimal_stock'             => (($data[8] == '') ? NULL : $data[8]),
                    'optimal_stock'             => (($data[9] == '') ? NULL : $data[9]),
                    'display'                   => (($data[10] == '') ? NULL : $data[10]),
                    'status'                    => (($data[11] == '') ? NULL : $data[11]),
                    'accessory_categories_id'   => (($data[12] == '') ? NULL : $data[12]),
                    'created_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[13]))),
                    'updated_at'                => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[14]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
