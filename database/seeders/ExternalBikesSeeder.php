<?php

namespace Database\Seeders;

use App\Models\External_bike;
use Illuminate\Database\Seeder;

class ExternalBikesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = fopen(base_path("database/CSV_ready/external_bikes.csv"), "r");

        $firstline = true;
        while (($data = fgetcsv($csvFile, 10000, ";", $enclosure = '`')) !== FALSE) {
            if (!$firstline) {
                External_bike::create([
                    'id'                => $data[0],
                    'brand'             => (($data[1] == '') ? 'inconnu' : $data[1]),
                    'model'             => (($data[2] == '') ? 'inconnu' : $data[2]),
                    'color'             => (($data[3] == '') ? NULL : $data[3]),
                    'frame_reference'   => (($data[4] == '') ? NULL : $data[4]),
                    'companies_id'      => (($data[5] == '') ? NULL : $data[5]),
                    'created_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[6]))),
                    'updated_at'        => date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $data[7]))),
                ]);
            }
            $firstline = false;
        }
        fclose($csvFile);
    }
}
