<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('bikes', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('client_name', 55)
                ->nullable();
            $table->string('frame_number', 55);
            $table->string('size', 10);
            $table->string('color', 55)
                ->nullable();
            $table->string('contract_type', 55);
            $table->date('contract_start')
                ->nullable();
            $table->date('contract_end')
                ->nullable();
            $table->date('estimated_delivery_date')
                ->nullable();
            $table->date('delivery_date')
                ->nullable();
            $table->date('selling_date')
                ->nullable();
            $table->string('frame_reference', 55)
                ->nullable();
            $table->string('key_reference', 55)
                ->nullable();
            $table->string('locker_reference', 55)
                ->nullable();
            $table->string('plate_number', 55)
                ->nullable();
            $table->string('billing_type', 55)
                ->nullable();
            $table->decimal('leasing_price', 10, 2)
                ->nullable();
            $table->decimal('selling_price', 10, 2)
                ->nullable();
            $table->string('usable', 2)
                ->default('Y');
            $table->string('insurance', 1)
                ->default('N');
            $table->string('insurance_individual', 1)
                ->default('N');
            $table->string('insurance_civil_responsibility', 1)
                ->default('N');
            $table->text('insurance_civil_responsibility_contract')
                ->nullable();
            $table->decimal('bike_price', 10, 2)
                ->nullable();
            $table->date('bike_buying_date')
                ->nullable();
            $table->integer('billing_group')
                ->nullable();
            $table->string('gps_id', 55)
                ->nullable();
            $table->string('localisation', 255)
                ->nullable();
            $table->string('address', 255)
                ->nullable();
            $table->string('comment_billing', 255)
                ->nullable();
            $table->date('stolen_date')
                ->nullable();
            $table->decimal('reimbursement_insurance', 10, 2)
                ->nullable();
            $table->string('comment', 255)
                ->nullable();
            $table->string('display_group', 55)
                ->default('1generic');
            $table->enum('status', ['actif', 'inactif'])
                ->default('actif');
            $table->integer('assembled')
                ->default(0);
            $table->integer('users_id')
                ->default(null)
                ->nullable();
            $table->unsignedBigInteger('bikes_catalog_id')
                ->nullable();
            $table->unsignedBigInteger('companies_id')
                ->nullable();
            $table->timestamps();
            // Keys
            $table->foreign('bikes_catalog_id')
                ->references('id')
                ->on('bikes_catalog')
                ->onDelete('set null')
                ->onUpdate('set null');
            $table->foreign('companies_id')
                ->references('id')
                ->on('companies')
                ->onDelete('set null')
                ->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bikes');
    }
}
