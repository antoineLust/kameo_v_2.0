<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxesCatalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boxes_catalog', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('model', 55);
            $table->decimal('production_price', 10, 2);
            $table->decimal('installation_price', 10, 2);
            $table->decimal('location_price', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boxes_catalog');
    }
}
