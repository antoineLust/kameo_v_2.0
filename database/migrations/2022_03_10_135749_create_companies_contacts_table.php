<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('companies_contacts', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('firstname', 55);
            $table->string('lastname', 55);
            $table->string('email', 55)
                ->nullable();
            $table->string('phone', 55);
            $table->string('function', 55)
                ->nullable();
            $table->enum('type', ['billing', 'ccBilling', 'contact'])
                ->default('contact');
            $table->unsignedBigInteger('companies_id');
            $table->timestamps();
            // Keys
            $table->foreign('companies_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_contacts');
    }
}
