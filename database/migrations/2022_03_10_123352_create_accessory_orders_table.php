<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessoryOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('accessory_orders', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('size', 55);
            $table->enum('type', ['leasing', 'annual_leasing', 'full_leasing', 'selling'])
                ->default('leasing');
            $table->decimal('amount', 10, 2);
            $table->text('description')
                ->nullable();
            $table->date('estimated_delivery_date')
                ->nullable();
            $table->date('delivery_date')
                ->nullable();
            $table->string('billing_type', 55)
                ->nullable();
            $table->enum('status', ['new', 'confirmed', 'done'])
                ->default('new');
            $table->unsignedBigInteger('grouped_orders_id');
            $table->unsignedBigInteger('accessories_catalog_id');
            $table->unsignedBigInteger('accessories_id')
                ->nullable();
            $table->timestamps();
            // Keys
            $table->foreign('accessories_id')
                ->references('id')
                ->on('accessories')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('grouped_orders_id')
                ->references('id')
                ->on('grouped_orders')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accessory_orders');
    }
}
