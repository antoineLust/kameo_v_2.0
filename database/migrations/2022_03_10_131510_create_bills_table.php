<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('bills', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->integer('out_id')
                ->nullable();
            $table->string('beneficiary_company', 255);
            $table->date('date');
            $table->decimal('amount_htva', 10, 2);
            $table->decimal('amount_tvac', 10, 2);
            $table->integer('billing_group')
                ->nullable();
            $table->string('communication', 255)
                ->nullable();
            $table->string('file_name', 255)
                ->default('invoices/example.pdf');
            $table->boolean('invoice_sent', 1)
                ->default(false);
            $table->date('invoice_sent_date')
                ->nullable();
            $table->boolean('invoice_paid', 1)
                ->default(false);
            $table->date('invoice_paid_date')
                ->nullable();
            $table->date('invoice_limit_paid_date')
                ->nullable();
            $table->boolean('accounting', 1)
                ->default(false);
            $table->string('type', 55)
                ->nullable();
            $table->enum('payement', ['cash', 'sumUp', 'transfer', 'visa'])
                ->nullable();
            $table->unsignedBigInteger('credit_notes_id')
                ->nullable();
            $table->unsignedBigInteger('users_id')
                ->nullable();
            $table->unsignedBigInteger('companies_id');
            $table->timestamps();
            // Keys
            $table->foreign('companies_id')
                ->references('id')
                ->on('companies')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
