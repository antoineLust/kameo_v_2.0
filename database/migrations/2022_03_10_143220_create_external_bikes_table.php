<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExternalBikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('external_bikes', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('brand', 55);
            $table->string('model', 55);
            $table->string('color', 55)
                ->nullable();
            $table->string('frame_reference', 55)
                ->nullable();
            $table->unsignedBigInteger('companies_id');
            $table->timestamps();
            // Keys
            $table->foreign('companies_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_bikes');
    }
}
