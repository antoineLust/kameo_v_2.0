<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title', 255);
            $table->string('content', 1000);
            $table->string('media', 255)->default('default.jpg');
            $table->set('media_type', ['image', 'video'])->default('image');
            $table->string('author', 55);
            $table->set('target_group', ['all', 'B2B', 'B2C', 'INDEPENDANT']);
            $table->string('target_companies')->nullable();
            $table->enum('status', ['actif', 'inactif'])->default('actif');
            $table->date('expiration_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
