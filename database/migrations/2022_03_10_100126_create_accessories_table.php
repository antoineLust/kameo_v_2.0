<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('accessories', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('size', 15);
            $table->string('contract_type', 55);
            $table->date('contract_start')->default(null)
                ->nullable();
            $table->date('contract_end')->default(null)
                ->nullable();
            $table->decimal('leasing_price', 10, 2)->default(null)
                ->nullable();
            $table->date('selling_date')->default(null)
                ->nullable();
            $table->decimal('selling_price', 10, 2)->default(null)
                ->nullable();
            $table->date('estimated_delivery_date')->default(null)
                ->nullable();
            $table->date('delivery_date')->default(null)
                ->nullable();
            $table->enum('status', ['actif', 'inactif'])
                ->default('actif');
            $table->unsignedBigInteger('users_id')->default(null)
                ->nullable();
            $table->unsignedBigInteger('bikes_id')->default(null)
                ->nullable();
            $table->unsignedBigInteger('accessories_catalog_id')
                ->nullable();
            $table->unsignedBigInteger('companies_id')->default(null)
                ->nullable();
            $table->date('stolen_date')->default(null)
                ->nullable();
            $table->decimal('reimbursement_insurance', 10, 2)->default(null)
                ->nullable();
            $table->timestamps();
            // Keys
            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('bikes_id')
                ->references('id')
                ->on('bikes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('accessories_catalog_id')
                ->references('id')
                ->on('accessories_catalog')
                ->onDelete('set null')
                ->onUpdate('set null');
            $table->foreign('companies_id')
                ->references('id')
                ->on('companies')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accessories');
    }
}
