<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBikesAssemblyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bikes_assembly', function (Blueprint $table) {
            $table->id();
            $table->date('date');
            $table->enum('status', ['todo', 'done']);
            $table->bigInteger('bikes_id')
                ->unsigned();
            $table->timestamps();
            // Keys
            $table->foreign('bikes_id')
                ->references('id')
                ->on('bikes')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bikes_assembly_create');
    }
}
