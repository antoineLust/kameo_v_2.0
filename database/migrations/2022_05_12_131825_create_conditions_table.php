<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conditions', function (Blueprint $table) {
            $table->id();
            $table->string('access');
            $table->string('companies_id');
            $table->string('cafetaria_billing_type')
                ->nullable();
            $table->string('cafetaria_tva')
                ->nullable();
            $table->string('cafetaria_discount_selling')
                ->nullable();
            $table->string('cafetaria_discount_leasing')
                ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conditions');
    }
}
