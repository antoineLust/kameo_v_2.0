<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('feedback', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->integer('not')
                ->nullable();
            $table->text('comment')
                ->nullable();
            $table->integer('entretien')
                ->nullable();
            $table->enum('status', ['CANCELLED', 'SENT', 'DONE'])
                ->default('SENT');
            $table->unsignedBigInteger('reservations_id');
            $table->timestamps();
            // Keys
            $table->foreign('reservations_id')
                ->references('id')
                ->on('reservations')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback');
    }
}
