<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Schema;

class CreateSharedBikesConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shared_bikes_conditions', function (Blueprint $table) {
            $table->id();
            $table->string('company_id');
            $table->string('users_id')
                ->default('*');
            $table->string('bikes_id')
                ->default('*');
            $table->string('buildings_id')
                ->default('*');
            $table->string('days')
                ->default('[monday, tuesday, wednesday, thursday, friday]');
            $table->integer('max_days_before_reservation')
                ->default(3);
            $table->integer('max_resevation_duration')
                ->default(12);
            $table->string('week_end')
                ->default('N');
            $table->string('start_taking_time')
                ->default('07:00:00');
            $table->string('end_taking_time')
                ->default(Date::now()->format('Y-m-d'));
            $table->string('start_delivery_time')
                ->default('19:00:00');
            $table->string('end_delivery_time')
                ->default(Date::now()->addDays(3)->format('Y-m-d'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shared_bike_conditions');
    }
}
