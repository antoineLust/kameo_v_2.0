<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('bill_details', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('type', 255);
            $table->integer('item_id');
            $table->string('comment', 255)
                ->nullable();
            $table->date('start')
                ->nullable();
            $table->date('end')
                ->nullable();
            $table->decimal('amount_htva', 10, 2);
            $table->decimal('amount_tvac', 10, 2);
            $table->unsignedBigInteger('bills_id');
            $table->unsignedBigInteger('entretiens_id')
                ->nullable();
            $table->timestamps();
            // Keys
            $table->foreign('bills_id')
                ->references('id')
                ->on('bills')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('entretiens_id')
                ->references('id')
                ->on('entretiens')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_details');
    }
}
