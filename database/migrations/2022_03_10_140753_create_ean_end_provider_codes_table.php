<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEanEndProviderCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ean_end_provider_codes', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->enum('item_type', ['bike', 'accessory'])
                ->nullable();
            $table->unsignedBigInteger('item_id');
            $table->string('size', 55)
                ->nullable();
            $table->string('ean_code', 255)
                ->nullable();
            $table->string('provider_code', 255)
                ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ean_end_provider_codes');
    }
}
