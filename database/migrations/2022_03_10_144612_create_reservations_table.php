<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('reservations', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->timestamp('start')
                ->nullable()
                ->default(null);
            $table->timestamp('end')
                ->nullable()
                ->default(null);
            $table->string('building_start', 255);
            $table->string('building_end', 255);
            $table->integer('extension')
                ->default(0);
            $table->timestamp('initial_end_date')
                ->nullable()
                ->default(null);
            $table->enum('status', ['No box', 'Closed', 'Done', 'OK', 'Open'])
                ->default('No box');
            $table->unsignedBigInteger('bikes_id');
            $table->unsignedBigInteger('users_id');
            $table->timestamps();
            // Keys
            $table->foreign('bikes_id')
                ->references('id')
                ->on('bikes')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
