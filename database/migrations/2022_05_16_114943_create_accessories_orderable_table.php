<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessoriesOrderableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accessories_orderable', function (Blueprint $table) {
            $table->id();
            $table->string('catalog_id');
            $table->string('company_id');
            $table->enum('type', ['available', 'mandatory']);
            $table->string('isFree')
                ->default('false');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accessories_orderable');
    }
}
