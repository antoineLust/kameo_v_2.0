<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBikeOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('bike_orders', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('size', 55)
                ->nullable();
            $table->string('color', 55)
                ->nullable();
            $table->text('remark')
                ->nullable();
            $table->decimal('amount', 10, 2)
                ->nullable();
            $table->enum('type', ['leasing', 'annual_leasing', 'full_leasing', 'selling'])
                ->default('leasing');
            $table->date('estimated_delivery_date')
                ->nullable();
            $table->date('delivery_date')
                ->nullable();
            $table->string('comment', 255)
                ->nullable();
            $table->enum('status', ['new', 'confirmed', 'done'])
                ->default('new');
            $table->unsignedBigInteger('bikes_id')
                ->nullable();
            $table->unsignedBigInteger('grouped_orders_id');
            $table->unsignedBigInteger('bikes_catalog_id');
            $table->timestamps();
            // Keys
            $table->foreign('bikes_id')
                ->references('id')
                ->on('bikes')
                ->onDelete('set null')
                ->onUpdate('set null');
            $table->foreign('grouped_orders_id')
                ->references('id')
                ->on('grouped_orders')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('bikes_catalog_id')
                ->references('id')
                ->on('bikes_catalog')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bike_orders');
    }
}
