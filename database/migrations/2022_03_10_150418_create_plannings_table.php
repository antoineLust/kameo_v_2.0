<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlanningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plannings', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->date('date');
            $table->integer('step');
            $table->string('address', 255);
            $table->string('item_type', 55);
            $table->integer('item_id');
            $table->integer('moving_time');
            $table->integer('execution_time');
            $table->time('planned_start_hour');
            $table->time('planned_end_hour');
            $table->time('real_start_hour')
                ->nullable();
            $table->time('real_end_hour')
                ->nullable();
            $table->text('description')
                ->nullable();
            $table->enum('status', ['confirmed', 'done']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plannings');
    }
}
