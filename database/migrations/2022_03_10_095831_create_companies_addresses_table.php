<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies_addresses', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('street', 55)
                ->nullable();
            $table->string('number', 10)
                ->nullable();
            $table->string('zip')
                ->nullable();
            $table->string('city', 55)
                ->nullable();
            $table->string('country', 55)
                ->nullable();
            $table->string('latitude', 17, 6)
                ->nullable();
            $table->string('longitude', 17, 6)
                ->nullable();
            $table->string('condensed', 255)
                ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies_addresses');
    }
}
