<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessoriesCatalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('accessories_catalog', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('brand', 55);
            $table->string('model', 55);
            $table->set('sizes', ['XS', 'S', 'M', 'L', 'XL', 'XXL', 'XXXL', 'S/M', 'M/L', 'L/XL', 'UNIQUE'])
                ->default('UNIQUE');
            $table->decimal('buying_price', 10, 2)
                ->nullable();
            $table->decimal('price_htva', 10, 2);
            $table->string('description', 1500)
                ->nullable();
            $table->string('provider', 55);
            $table->integer('minimal_stock')
                ->default(0);
            $table->integer('optimal_stock')
                ->default(0);
            $table->string('display', 1)
                ->default('N');
            $table->enum('status', ['actif', 'inactif'])
                ->default('actif');
            $table->unsignedBigInteger('accessory_categories_id')
                ->nullable();
            $table->timestamps();
            // Keys
            $table->foreign('accessory_categories_id')
                ->references('id')
                ->on('accessory_categories')
                ->onDelete('set null')
                ->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accessories_catalog');
    }
}
