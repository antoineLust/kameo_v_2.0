<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('boxes', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('reference', 155);
            $table->string('model', 55);
            $table->date('contract_start')
                ->nullable();
            $table->date('contract_end')
                ->nullable();
            $table->decimal('amount', 10, 2)
                ->nullable();
            $table->integer('billing_group')
                ->nullable();
            $table->enum('door', ['open', 'closed'])
                ->default('closed');
            $table->timestamp('open_date')
                ->nullable();
            $table->enum('status', ['actif', 'inactif'])
                ->default('actif');
            $table->unsignedBigInteger('boxes_catalog_id');
            $table->unsignedBigInteger('buildings_id');
            $table->timestamps();
            // Keys
            $table->foreign('boxes_catalog_id')
                ->references('id')
                ->on('boxes_catalog')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('buildings_id')
                ->references('id')
                ->on('buildings')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boxes');
    }
}
