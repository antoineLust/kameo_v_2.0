<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntretiensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('entretiens', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->integer('external_bike')
                ->default(0);
            $table->date('date')
                ->nullable();
            $table->string('address', 255)
                ->nullable();
            $table->string('status', 55)
                ->nullable();
            $table->date('planned_date')
                ->nullable();
            $table->string('planned_time')
                ->nullable();
            $table->timestamp('start_time')
                ->nullable();
            $table->timestamp('end_time')
                ->nullable();
            $table->text('description')
                ->nullable();
            $table->integer('number_entretien');
            $table->integer('client_warned')
                ->default(0);
            $table->integer('avoid_billing')
                ->default(0);
            $table->integer('leasing_to_bill')
                ->default(0);
            $table->unsignedBigInteger('bikes_id');
            $table->boolean('assembled');
            $table->timestamps();
            // Keys
            $table->foreign('bikes_id')
                ->references('id')
                ->on('bikes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entretiens');
    }
}
