<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('reservation_details', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('action', 255);
            $table->string('outcome', 255);
            $table->unsignedBigInteger('reservations_id');
            $table->unsignedBigInteger('boxes_id');
            $table->timestamps();
            // Keys
            $table->foreign('boxes_id')
                ->references('id')
                ->on('boxes')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('reservations_id')
                ->references('id')
                ->on('reservations')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations_details');
    }
}
