<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('users', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('firstname', 55);
            $table->string('lastname', 55);
            $table->string('email');
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone', 20)
                ->nullable();
            $table->boolean('employee')
                ->default(0);
            $table->enum('employee_role', ['mechanic', 'admin', 'developer', 'commercial', 'other'])
                ->nullable();
            $table->string('size')
                ->nullable();
            $table->set('permission', ['order', 'search', 'fleetManager', 'chatsManager', 'admin', 'bill', 'cashflow', 'dashboard', 'personnalBike', 'stock', 'chat', 'bikesStock', 'espaceCollaboratif', 'statistics'])
                ->default('fleetManager')->nullable();
            $table->enum('status', ['actif', 'inactif'])
                ->default('actif');
            $table->string('avatar', 255)
                ->default('users/profil-pictures/default.png');
            $table->unsignedBigInteger('companies_id')
                ->nullable();
            $table->unsignedBigInteger('users_addresses_id')
                ->nullable();
            $table->unsignedBigInteger('users_works_addresses_id')
                ->nullable();
            $table->integer('nb_connection')
                ->default(0);
            $table->timestamp('last_connection')
                ->nullable();
            $table->rememberToken();
            $table->timestamps();
            // Keys
            $table->foreign('companies_id')
                ->references('id')
                ->on('companies')
                ->onDelete('set null')
                ->onUpdate('set null');
            $table->foreign('users_addresses_id')
                ->references('id')
                ->on('users_addresses')
                ->onDelete('set null')
                ->onUpdate('set null');
            $table->foreign('users_works_addresses_id')
                ->references('id')
                ->on('users_works_addresses')
                ->onDelete('set null')
                ->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
