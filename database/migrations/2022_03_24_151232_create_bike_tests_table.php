<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBikeTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bike_tests', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->integer('user_id')
            ->nullable();
            $table->integer('bikes_id');
            $table->date('date');
            $table->text('comment')
                ->nullable();
            $table->enum('status', ['new', 'confirmed', 'done']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bike_tests');
    }
}
