<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->date('date');
            $table->time('start_time');
            $table->time('end_time');
            $table->text('comment')
                ->nullable();
            $table->text('confidential_comment')
                ->nullable();
            $table->enum('status', ['todo', 'done', 'waiting_pieces']);
            $table->unsignedBigInteger('employee_id');
            $table->enum('item_type',[  'Maintenance',
                                        'Bike Assembly',
                                        'Order to prepare',
                                        'Test']);
            $table->integer('item_id');                           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
    }
}
