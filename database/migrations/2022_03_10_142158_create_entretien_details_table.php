<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntretienDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('entretien_details', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->enum('item_type', ['service', 'accessory', 'other_accessory'])
                ->nullable();
            $table->integer('item_id')
                ->nullable();
            $table->integer('duration')
                ->nullable();
            $table->decimal('amount', 10, 2)
                ->nullable();
            $table->string('description', 255)
                ->nullable();
            $table->integer('accessory_stock_id')
                ->nullable();
            $table->string('accessory_size', 10)
                ->nullable();
            $table->unsignedBigInteger('entretiens_id');
            $table->string('details', 255)
                ->nullable();
            $table->timestamps();
            // Keys
            $table->foreign('entretiens_id')
                ->references('id')
                ->on('entretiens')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entretien_details');
    }
}
