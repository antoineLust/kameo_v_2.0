<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOfferDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('offer_details', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('item_type', 55)
                ->nullable();
            $table->integer('item_id')
                ->nullable();
            $table->string('contract_type')
                ->nullable();
            $table->string('contract_amount', 10, 2)
                ->nullable();
            $table->string('installation_amount', 10, 2)
                ->nullable();
            $table->integer('contract_duration')
                ->nullable();
            $table->string('size', 55)
                ->nullable();
            $table->unsignedBigInteger('offers_id');
            $table->timestamps();
            // Keys
            $table->foreign('offers_id')
                ->references('id')
                ->on('offers')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offer_details');
    }
}
