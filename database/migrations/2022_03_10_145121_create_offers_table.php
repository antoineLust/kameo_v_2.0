<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('offers', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('title', 55)
                ->nullable();
            $table->text('description')
                ->nullable();
            $table->integer('probability')
                ->nullable();
            $table->string('global_discount')
                ->nullable();
            $table->date('validity_date')
                ->nullable();
            $table->string('file_name', 255)
                ->nullable();
            $table->enum('step', ['onGoing', 'signed', 'lost'])
                ->default('onGoing');
            $table->enum('status', ['actif', 'inactif'])
                ->default('actif');
            $table->integer('employee_id');
            $table->unsignedBigInteger('companies_id');
            $table->unsignedBigInteger('users_id')
                ->nullable();
            $table->timestamps();
            // Keys
            $table->foreign('companies_id')
                ->references('id')
                ->on('companies')
                ->onDelete('restrict')
                ->onUpdate('restrict');
            $table->foreign('users_id')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
