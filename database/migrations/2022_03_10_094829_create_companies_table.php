<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('companies', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('name', 255);
            $table->string('logo', 255)->nullable()
                ->default(null);
            $table->string('vat_number', 55)
                ->nullable();
            $table->unsignedInteger('billing_group')
                ->default(1);
            $table->enum('type', ['client', 'prospect', 'old_prospect', 'old_client', 'not_interested'])
                ->nullable();
            $table->enum('audience', ['B2B', 'B2C', 'INDEPENDANT'])
                ->nullable();
            $table->string('aquisition', 55)
                ->nullable();
            $table->enum('status', ['actif', 'inactif'])
                ->default('actif');
            $table->unsignedBigInteger('companies_addresses_id')
                ->nullable();
            $table->timestamps();
            // Keys
            $table->foreign('companies_addresses_id')
                ->references('id')
                ->on('companies_addresses')
                ->onDelete('set null')
                ->onUpdate('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
