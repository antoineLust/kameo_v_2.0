<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempAccessoriesCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_accessories_categories', function (Blueprint $table) {
            $table->id();
            $table->string('main', 255);
            $table->unsignedBigInteger('main_id');
            $table->string('sub_1', 255)
                ->nullable();
            $table->unsignedBigInteger('sub_1_id')
                ->nullable();
            $table->string('sub_2', 255)
                ->nullable();
            $table->unsignedBigInteger('sub_2_id')
                ->nullable();
            $table->string('sub_3', 255)
                ->nullable();
            $table->unsignedBigInteger('sub_3_id')
                ->nullable();
            $table->string('sub_4', 255)
                ->nullable();
            $table->unsignedBigInteger('sub_4_id')
                ->nullable();
            $table->timestamps();

            $table->foreign('main_id')
            ->references('id')
            ->on('accessories_categories')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->foreign('sub_1_id')
            ->references('id')
            ->on('accessories_categories')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->foreign('sub_2_id')
            ->references('id')
            ->on('accessories_categories')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->foreign('sub_3_id')
            ->references('id')
            ->on('accessories_categories')
            ->onDelete('restrict')
            ->onUpdate('restrict');

            $table->foreign('sub_4_id')
            ->references('id')
            ->on('accessories_categories')
            ->onDelete('restrict')
            ->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_accessories_categories');
    }
}
