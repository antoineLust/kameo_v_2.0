<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBikesCatalogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bikes_catalog', function (Blueprint $table) {
            // Columns
            $table->id();
            $table->string('brand', 55);
            $table->string('model', 55);
            $table->string('frame_type', 55);
            $table->enum('utilisation', ['Ville et chemin', 'Tout chemin', 'VTT', 'Ville', 'Speed Pedelec', 'Cargo', 'Pliant', 'Gravel', 'Enfant', 'Route', 'Tandem', 'Draisienne', 'Trottinette']);
            $table->string('electric', 1);
            $table->string('motor', 55)
                ->nullable();
            $table->string('battery', 255)
                ->nullable();
            $table->string('display_device', 255)
                ->nullable();
            $table->string('transmission', 255)
                ->nullable();
            $table->set('sizes', ['XS', 'S', 'M', 'L', 'XL', 'UNIQUE']);
            $table->string('season', 55)
                ->nullable();
            $table->decimal('buying_price', 10, 2)
                ->nullable();
            $table->decimal('price_htva', 10, 2)
                ->nullable();
            $table->integer('minimal_stock')
                ->default(0)
                ->nullable();
            $table->integer('optimal_stock')
                ->default(0)
                ->nullable();
            $table->text('description')
                ->nullable();
            $table->enum('status', ['actif', 'inactif'])
                ->default('actif');
            $table->integer('display');
            $table->float('wheel_size', 10, 2)
                ->nullable();
            $table->string('fork', 255)
                ->nullable();
            $table->string('absorber', 255)
                ->nullable();
            $table->enum('gears', ['1', '5', '8', '9', '10', '11', '12', 'Changement continu'])
                ->nullable();
            $table->string('shifter', 255)
                ->nullable();
            $table->string('cassette', 255)
                ->nullable();
            $table->string('brakes', 255)
                ->nullable();
            $table->integer('front_brakes_size')
                ->nullable();
            $table->integer('back_brakes_size')
                ->nullable();
            $table->float('weight', 10, 2)
                ->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bikes_catalog');
    }
}
