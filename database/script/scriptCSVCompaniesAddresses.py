from pprint import pprint
import csv
import googlemaps
import datetime
import threading
# Define
global API_KEY
global MAP_CLIENT
global firstLine
# Implement global variable
API_KEY = 'AIzaSyCJ0xhONmZzmxOklFnKAzToegtA0KvI9iA'     ## Clé de l'API google (à modifier)
MAP_CLIENT = googlemaps.Client(API_KEY)                 ## Permet d'utiliser l'API client avec la clé API
filepath = '../data/companies_addresses.csv'            ## Fichier CSV source
finalCSV = '../CSV_ready/companies_addresses.csv'       ## Fichier CSV final

def call_api(row, final) :
    ## On recherche le lieu voulu avec les valeurs reçu
    response = MAP_CLIENT.find_place(row[8], 'textquery')
    if (0 < len(response['candidates'])):
        ## On retrouve toutes les informations du lieu en utilisant la méthode inverse de l'API
        addr_comp = MAP_CLIENT.reverse_geocode(response['candidates'][0]['place_id'])
        ## Définition des variables
        number_add = ""
        street_add = ""
        zip_add = ""
        town_add = ""
        country_add = ""


        for add in addr_comp[0]['address_components'] :
            if (add['types'][0] == "street_number") :
                number_add = add['long_name']
            if (add['types'][0] == "route") :
                street_add = add['long_name']
            if (add['types'][0] == "postal_code") :
                zip_add = add['long_name']
            if (add['types'][0] == "locality") :
                town_add = add['long_name']
            if (add['types'][0] == "country") :
                country_add = add['long_name']
        formatted_address = "{0}, {1}, {2}, {3}, {4}".format(number_add, street_add, zip_add, town_add, country_add)
        csv_line = '`{0}`;`{1}`;`{2}`;`{3}`;`{4}`;`{5}`;`{6}`;`{7}`;`{8}`;`{9}`;`{10}`'.format(row[0], street_add, number_add, zip_add, town_add, country_add, formatted_address, addr_comp[0]['geometry']['location']['lat'], addr_comp[0]['geometry']['location']['lng'], row[9], row[10])
        with open(final, "a+", encoding="utf-8") as f:
            f.seek(0)
            data = f.read(100)
            if len(data) > 0 :
                f.write("\n")
            f.write(csv_line)
    else :
        currentDT = datetime.datetime.now()
        DT_format = "{0}-{1}-{2} {3}:{4}:{5}".format(currentDT.year, currentDT.month, currentDT.day, currentDT.hour, currentDT.minute, currentDT.second)
        with open(finalCSV, "a+", encoding="utf-8") as f:
            f.seek(0)
            data = f.read(100)
            if len(data) > 0 :
                f.write("\n")
            f.write('`' + row[0] + '`;``;``;``;``;``;``;``;``;`' + DT_format + '`;`' + DT_format + '`')

firstLine = True
with open(filepath,'r', encoding="utf-8") as csvfile:
    ## On lit le fichier CSV voulu en utilisant le delimiter utilisé dans les fichiers CSV de base
    datareader = csv.reader(csvfile, delimiter=';', quotechar='`')
    with open(finalCSV, "a+", encoding="utf-8") as f:
        f.seek(0)
        data = f.read(100)
        if len(data) > 0 :
            f.write("\n")
        ## On écrit la première ligne pour donner les titres
        f.write('`id`;`street`;`number`;`zip`;`city`;`country`;`latitude`;`longitude`;`condensed`;`created_at`;`updated_at`')
    for row in datareader:
        ## On corrige les potentielle erreur de valeurs dans le fichier pour que la lecture du fichier se passe bien
        for rowElm in row :
            rowElm = rowElm.replace("NULL", "")
            rowElm = rowElm.replace('`-`', '``')
            rowElm = rowElm.replace('`/`', '``')
            rowElm = rowElm.replace('`x`', '``')
            rowElm = rowElm.replace('`0`', '``')
            rowElm = rowElm.replace('"\n', '`\n')
            rowElm = '`' + rowElm[1:]

        if  firstLine == False :
            ## Si on a assez de données pour utiliser l'API, on appelle la fonction qui gère l'API
            if len(row[1]) > 5:
                call_api(row, finalCSV)
            ## Si on a pas assez de données on rempli avec les valeurs de null
            else :
                currentDT = datetime.datetime.now()
                DT_format = "{0}-{1}-{2} {3}:{4}:{5}".format(currentDT.year, currentDT.month, currentDT.day, currentDT.hour, currentDT.minute, currentDT.second)
                with open(finalCSV, "a+", encoding="utf-8") as f:
                    f.seek(0)
                    data = f.read(100)
                    if len(data) > 0 :
                        f.write("\n")
                    f.write('`' + row[0] + '`;``;``;``;``;``;``;``;``;`' + DT_format + '`;`' + DT_format + '`')

        firstLine = False


