## Import des packages nécessaires
import os
import re
## chemin du dossier des fichiers a modifier
SEEDERS_PATH = '../data'

for file in os.listdir(SEEDERS_PATH):
    if file != "companies_addresses.csv" :
        if file != "users_works_addresses.csv":
            if file != "users_addresses.csv":

                if os.path.exists("../CSV_ready/" + file):
                    os.remove("../CSV_ready/" + file)
                print(file)
                fin = open(SEEDERS_PATH + "/" + file, "r", encoding="utf8")

                replacement = ""
                lines = fin.readlines()
                for line in lines:
                    line = line.replace(";;", ';``;')
                    line = line.replace("`NULL`", '``')
                    line = line.replace("NULL", '``')
                    line = line.replace('`-`', '``')
                    line = line.replace('`/`', '``')
                    line = line.replace('`x`', '``')
                    line = line.replace('";"', '`;`')
                    line = line.replace('";', '`;')
                    line = line.replace(';"', ';`')
                    line = line.replace(';\n', ';')
                    line = line.replace('br />\n', 'br />')
                    line = line.replace('br />;\n', 'br />')
                    line = line.replace("&gt\n", 'br /&gt')
                    line = line.replace("&gt;\n", 'br /&gt')
                    line = line.replace("\&quot;", '"')
                    line = line.replace("&amp;", '&')
                    line = line.replace("´", "`")
                    line = line.replace("`.`", "`;`")
                    if file == "bikes_catalog.csv":
                        line = line.replace("`..`", "`;``;`")
                        line = line.replace("Speedpedelec", "Speed Pedelec")
                    if file == "conditions.csv":
                        line = line.replace("leasing", "monthly_leasing")
                        line = line.replace("achat", "selling")
                        line = line.replace("fullLeasing", "preFinanced_leasing")
                        line = line.replace("annualleasing", "annuel_leasing")
                    replacement = replacement + line
                fin.close()

                fout = open("../CSV_ready/" + file, "w", encoding="utf8")
                fout.write(replacement)
                fout.close()
