module.exports = {
    "env": {
        "browser": true,
        "es2021": true,
        "jquery": true,
        "node": true,
    },
    "extends": [
        "eslint:recommended",
        "plugin:vue/vue3-essential",
    ],
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "vue",
        "prettier",
    ],
    "rules": {
        "prettier/prettier": "error",
        "no-unused-vars": "off",
        "no-extra-boolean-cast": "off",
        "vue/no-deprecated-router-link-tag-prop": "off",
        "vue/multi-word-component-names": "off",
    }
}
